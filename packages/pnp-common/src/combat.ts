import * as constants from "./constants";

export type CombatStyle =
    "focus" |
    "aim" |
    "awareness" |
    "swiftness";

export type Body =
    "tension" |
    "power";

export type Fitness =
    "stamina" |
    "constitution";

export type Fighting = { [key in CombatStyle]: number } &
    { [key in Body]: number } &
    { [key in Fitness]: number };

export type CombatTraitCategory =
    "combatStyle" |
    "body" |
    "fitness";

export type CombatTrait = CombatStyle | Body | Fitness;

export const allCombatTraitCategories: CombatTraitCategory[] = [
    "combatStyle",
    "body",
    "fitness",
];

export const allCombatTraits: CombatTrait[] = [
    "focus",
    "aim",
    "awareness",
    "swiftness",
    "tension",
    "power",
    "stamina",
    "constitution",
];

export function getCombatTraitCategory(trait: CombatTrait): CombatTraitCategory {
    switch (trait) {
        case "focus":
        case "aim":
        case "awareness":
        case "swiftness":
            return "combatStyle";
        case "tension":
        case "power":
            return "body";
        case "stamina":
        case "constitution":
            return "fitness";
        default:
            return;
    }
}

export function getCombatTraitsForCategory(category: CombatTraitCategory): CombatTrait[] {
    return allCombatTraits.filter(trait => getCombatTraitCategory(trait) === category);
}

export const combatDictionary: { [key in CombatTraitCategory | CombatTrait]: string } = {
    combatStyle: "Combat style",
    focus: "Focus",
    aim: "Aim",
    awareness: "Awareness",
    swiftness: "Swiftness",
    body: "Body",
    tension: "Tension",
    power: "Power",
    fitness: "Fitness",
    stamina: "Stamina",
    constitution: "Constitution",
};

export function combatTraitCategoryAwardedPerLevel(category: CombatTraitCategory): number {
    switch (category) {
        case "combatStyle": return constants.COMBAT_STYLE_AWARDED_PER_LEVEL;
        case "body": return constants.BODY_AWARDED_PER_LEVEL;
        case "fitness": return constants.FITNESS_AWARDED_PER_LEVEL;
        default: return;
    }
}

export function combatTraitCostPerRank(trait: CombatTrait): number {
    switch (trait) {
        case "focus": return constants.COMBAT_TRAIT_COST_FOCUS;
        case "aim": return constants.COMBAT_TRAIT_COST_AIM;
        case "awareness": return constants.COMBAT_TRAIT_COST_AWARENESS;
        case "swiftness": return constants.COMBAT_TRAIT_COST_SWIFTNESS;
        case "tension": return constants.COMBAT_TRAIT_COST_TENSION;
        case "power": return constants.COMBAT_TRAIT_COST_POWER;
        case "stamina": return constants.COMBAT_TRAIT_COST_STAMINA;
        case "constitution": return constants.COMBAT_TRAIT_COST_CONSTITUTION;
        default: return;
    }
}

export function isOffensive(trait: CombatTrait): boolean {
    switch (trait) {
        case "focus": return true;
        case "aim": return true;
        case "awareness": return true;
        case "swiftness": return false;
        case "tension": return false;
        case "power": return true;
        case "stamina": return true;
        case "constitution": return false;
        default: return;
    }
}

export function isDefensive(trait: CombatTrait): boolean {
    switch (trait) {
        case "focus": return false;
        case "aim": return false;
        case "awareness": return false;
        case "swiftness": return true;
        case "tension": return true;
        case "power": return false;
        case "stamina": return true;
        case "constitution": return true;
        default: return;
    }
}

export type CombatTraitClassification = "offensive" | "defensive" | "instant";
