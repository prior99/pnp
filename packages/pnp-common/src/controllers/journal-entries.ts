import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
    query,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getJournalEntry, createJournalEntry, updateJournalEntry } from "../scopes";
import { JournalEntry } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy, isNil } from "ramda";

@controller @component
export class ControllerJournalEntries {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/journal-entries").dump(JournalEntry, getJournalEntry)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<JournalEntry[]> {
        return ok(await this.db.getRepository(JournalEntry).find({
            relations: ["preset", "group", "hero"],
            where: pickBy(val => Boolean(val), {
                group: groupId ? { id: groupId } : undefined,
                preset: presetId ? { id: presetId } : undefined,
            }),
        }));
    }

    @route("POST", "/journal-entries").dump(JournalEntry, getJournalEntry)
    public async create(
        @body(createJournalEntry) journalEntry: JournalEntry,
        @context ctx?: Context,
    ): Promise<JournalEntry> {
        // Access control.
        if (!journalEntry.group && !journalEntry.preset || journalEntry.group && journalEntry.preset) {
            return badRequest<JournalEntry>("Either group or preset need to be supplied.");
        }
        if (journalEntry.group && !(await ctx.hasGroup(journalEntry.group.id))) { return forbidden<JournalEntry>(); }
        if (journalEntry.preset && !(await ctx.hasPreset(journalEntry.preset.id))) { return forbidden<JournalEntry>(); }
        const duplicateSortKey = await this.db.getRepository(JournalEntry).findOne({
            where: pickBy(val => !isNil(val), {
                group: journalEntry.group ? { id: journalEntry.group.id } : undefined,
                preset: journalEntry.preset ? { id: journalEntry.preset.id } : undefined,
                sortKey: journalEntry.sortKey,
            }),
        });
        if (duplicateSortKey) {
            const builder = this.db.getRepository(JournalEntry).createQueryBuilder("journal_entry")
                .update(JournalEntry)
                .set({ sortKey: () => `"sort_key" + 1` })
                .where(`sort_key >= :sortKey`, { sortKey: journalEntry.sortKey });
            if (journalEntry.group) { builder.andWhere(`group_id = :id`, { id: journalEntry.group.id }); }
            if (journalEntry.preset) { builder.andWhere(`preset_id = :id`, { id: journalEntry.preset.id }); }
            await builder.execute();
        }
        // Create new journal entry.
        const newJournalEntry = await this.db.getRepository(JournalEntry).save(journalEntry);
        // Announce live update.
        if (journalEntry.group) {
            this.live.publish(journalEntry.group.id, LiveAction.CREATE, ModelName.JOURNAL_ENTRY, newJournalEntry.id);
        }
        // Return new template item.
        return ok(this.get(journalEntry.id));
    }

    @route("POST", "/journal-entry/:id").dump(JournalEntry, getJournalEntry)
    public async update(
        @param("id") id: string,
        @body(updateJournalEntry) change: JournalEntry,
        @context ctx?: Context,
    ): Promise<JournalEntry> {
        // Get journal entry.
        const journalEntry = await this.db.getRepository(JournalEntry).findOne({
            where: { id },
            relations: ["group", "preset", "hero"],
        });
        // Access control.
        if (!journalEntry) { return notFound<JournalEntry>(); }
        if (journalEntry.group && !(await ctx.hasGroup(journalEntry.group.id))) { return forbidden<JournalEntry>(); }
        if (journalEntry.preset && !(await ctx.hasPreset(journalEntry.preset.id))) { return forbidden<JournalEntry>(); }
        // If the `sortKey` is to be updated, change the order accordingly.
        if (typeof change.sortKey === "number" && change.sortKey !== journalEntry.sortKey) {
            // First, move all  entries the **old** position one key down.
            const decBuilder = this.db.getRepository(JournalEntry).createQueryBuilder("journal_entry")
                .update(JournalEntry)
                .set({ sortKey: () => `"sort_key" - 1` })
                .where(`"sort_key" > :sortKey`, { sortKey: journalEntry.sortKey });
            if (journalEntry.group) { decBuilder.andWhere(`group_id = :id`, { id: journalEntry.group.id }); }
            if (journalEntry.preset) { decBuilder.andWhere(`preset_id = :id`, { id: journalEntry.preset.id }); }
            await decBuilder.execute();
            // Then move all entries the **new** position one key down.
            const incBuilder = this.db.getRepository(JournalEntry).createQueryBuilder("journal_entry")
                .update(JournalEntry)
                .set({ sortKey: () => `"sort_key" + 1` })
                .where(`"sort_key" >= :sortKey`, { sortKey: change.sortKey });
            if (journalEntry.group) { incBuilder.andWhere(`group_id = :id`, { id: journalEntry.group.id }); }
            if (journalEntry.preset) { incBuilder.andWhere(`preset_id = :id`, { id: journalEntry.preset.id }); }
            await incBuilder.execute();
        }
        // Perform update.
        Object.assign(journalEntry, change);
        await this.db.getRepository(JournalEntry).save(journalEntry);
        // Announce live update.
        if (journalEntry.group) {
            this.live.publish(journalEntry.group.id, LiveAction.UPDATE, ModelName.JOURNAL_ENTRY, journalEntry.id);
        }
        // Return updated.
        return ok(journalEntry);
    }

    @route("DELETE", "/journal-entry/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Get journal entry.
        const journalEntry = await this.db.getRepository(JournalEntry).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!journalEntry) { return notFound<void>(); }
        if (journalEntry.group && !(await ctx.hasGroup(journalEntry.group.id))) { return forbidden<void>(); }
        if (journalEntry.preset && !(await ctx.hasPreset(journalEntry.preset.id))) { return forbidden<void>(); }
        // Delete.
        await this.db.getRepository(JournalEntry).createQueryBuilder("journal_entry")
            .where(`journal_entry.id = :id`, { id })
            .delete()
            .execute();
        // Move all entries following the old position one down.
        const builder = this.db.getRepository(JournalEntry).createQueryBuilder("journal_entry")
            .update(JournalEntry)
            .set({
                sortKey: () => `"sort_key" - 1`,
            })
            .where(`"sort_key" > :sortKey`, { sortKey: journalEntry.sortKey });
        if (journalEntry.group) { builder.andWhere(`group_id = :id`, { id: journalEntry.group.id }); }
        if (journalEntry.preset) { builder.andWhere(`preset_id = :id`, { id: journalEntry.preset.id }); }
        await builder.execute();
        // Announce live update.
        if (journalEntry.group) {
            this.live.publish(journalEntry.group.id, LiveAction.DELETE, ModelName.JOURNAL_ENTRY, journalEntry.id);
        }
        // Return.
        return ok();
    }

    @route("GET", "/journal-entry/:id").dump(JournalEntry, getJournalEntry)
    public async get(@param("id") id: string): Promise<JournalEntry> {
        const journalEntry = await this.db.getRepository(JournalEntry).findOne({
            relations: ["group", "preset", "hero"],
            where: { id },
        });
        if (!journalEntry) { return notFound<JournalEntry>(); }
        return ok(populate(JournalEntry, journalEntry));
    }
}
