import {
    controller,
    route,
    body,
    ok,
    notFound,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { createMonsterManeuver, getMonsterManeuver } from "../scopes";
import { MonsterManeuver, TemplateMonster } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerMonsterManeuvers {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/template-monster/:id/monster-maneuvers").dump(MonsterManeuver, getMonsterManeuver)
    public async search(@param("id") id: string): Promise<MonsterManeuver[]> {
        const monsterManeuvers = await this.db.getRepository(MonsterManeuver).find({
            where: { templateMonster: { id } },
            relations: ["templateMonster", "maneuver"],
        });
        return ok(monsterManeuvers);
    }

    @route("POST", "/monster-maneuvers").dump(MonsterManeuver, getMonsterManeuver)
    public async create(
        @body(createMonsterManeuver) monsterManeuver: MonsterManeuver,
        @context ctx?: Context,
    ): Promise<MonsterManeuver> {
        // Fetch template monster.
        const templateMonster = await this.db.getRepository(TemplateMonster).findOne({
            where: { id: monsterManeuver.templateMonster.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!templateMonster) { return notFound<MonsterManeuver>(); }
        if (templateMonster.group && !(await ctx.hasGroup(templateMonster.group.id))) {
            return forbidden<MonsterManeuver>();
        }
        if (templateMonster.preset && !(await ctx.hasPreset(templateMonster.preset.id))) {
            return forbidden<MonsterManeuver>();
        }
        // Update.
        await this.db.getRepository(MonsterManeuver).save(monsterManeuver);
        // Announce live update.
        if (templateMonster.group) {
            this.live.publish(
                templateMonster.group.id,
                LiveAction.CREATE,
                ModelName.MONSTER_MANEUVER,
                monsterManeuver.id,
            );
        }
        // Return updated.
        return ok(monsterManeuver);
    }

    @route("DELETE", "/monster-maneuver/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch monster maneuver.
        const monsterManeuver = await this.db.getRepository(MonsterManeuver).findOne({
            where: { id },
            relations: ["templateMonster", "templateMonster.group", "templateMonster.preset"],
        });
        // Access control.
        if (!monsterManeuver) { return notFound<void>(); }
        const { templateMonster } = monsterManeuver;
        if (templateMonster.group && !(await ctx.isMaster(templateMonster.group.id))) { return forbidden<void>(); }
        if (templateMonster.preset && !(await ctx.hasPreset(templateMonster.preset.id))) { return forbidden<void>(); }
        // Delete.
        await this.db.getRepository(MonsterManeuver).createQueryBuilder("monster_maneuver")
            .where(`monster_maneuver.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (monsterManeuver.templateMonster.group) {
            this.live.publish(
                monsterManeuver.templateMonster.group.id,
                LiveAction.DELETE,
                ModelName.MONSTER_MANEUVER,
                monsterManeuver.id,
            );
        }
        // Return.
        return ok();
    }

    @route("GET", "/monster-maneuver/:id").dump(MonsterManeuver, getMonsterManeuver)
    public async get(@param("id") id: string): Promise<MonsterManeuver> {
        const monsterManeuver = await this.db.getRepository(MonsterManeuver).findOne({
            where: { id },
            relations: ["templateMonster", "maneuver"],
        });
        if (!monsterManeuver) { return notFound<MonsterManeuver>(); }
        return ok(populate(MonsterManeuver, monsterManeuver));
    }
}
