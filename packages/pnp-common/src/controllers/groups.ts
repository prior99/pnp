import {
    body,
    controller,
    route,
    param,
    is,
    uuid,
    ok,
    notFound,
    created,
    noauth,
    context,
    forbidden,
} from "hyrest";
import { component, inject } from "tsdi";
import { Connection } from "typeorm";
import { verbose } from "winston";
import {
    Group,
    Preset,
    TemplateItem,
    Maneuver,
    Hero,
    Item,
    LearnedManeuver,
    BattleMap,
    Area,
    Marker,
    TemplateMonster,
    Monster,
    MonsterManeuver,
    Tag,
    HeroTag,
    BattleMapTag,
    JournalEntry,
    Stash,
    StashItem,
    MarkerItem,
} from "../models";
import { Context } from "../context";
import { signup, getGroup, owner } from "../scopes";
import { omit } from "ramda";
import { copyFile } from "fs-extra";
import * as Path from "path";
import { ConfigInterface } from "../config-interface";
import { LiveAction, LiveServer } from "../live";
import { existsSync } from "fs";

@controller @component
export class ControllerGroups {
    @inject private live: LiveServer;
    @inject private db: Connection;
    @inject("config") private config: ConfigInterface;

    @route("GET", "/groups").dump(Group, getGroup) @noauth
    public async list(): Promise<Group[]> {
        return ok(await this.db.getRepository(Group).find());
    }

    @route("GET", "/group/:id").dump(Group, getGroup)
    public async get(@param("id") @is().validate(uuid) id: string): Promise<Group> {
        const group = await this.db.getRepository(Group).findOne({ id });
        if (!group) { return notFound<Group>(); }
        return ok(group);
    }

    @route("POST", "/group").dump(Group, owner) @noauth
    public async create(@body(signup) group: Group): Promise<Group> {
        const { id, name } = await this.db.getRepository(Group).save(group);
        verbose(`New group ${name} with id ${id} just signed up`);
        return created(await this.get(id));
    }

    @route("POST", "/group/:groupId/apply-preset/:presetId").dump(Group, owner) @noauth
    public async applyPreset(
        @param("groupId") groupId: string,
        @param("presetId") presetId: string,
        @context ctx?: Context,
    ): Promise<Group> {
        const group = await this.db.getRepository(Group).findOne({ id: groupId });
        if (!group) { return notFound<Group>(`No group with id "${groupId}"`); }
        if (!(await ctx.isMaster(group.id))) { return forbidden<Group>(); }

        Object.assign(group, {
            tags: await this.db.getRepository(Tag).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            templateItems: await this.db.getRepository(TemplateItem).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            maneuvers: await this.db.getRepository(Maneuver).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            heroes: await this.db.getRepository(Hero).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            battleMaps: await this.db.getRepository(BattleMap).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            templateMonsters: await this.db.getRepository(TemplateMonster).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            journalEntries: await this.db.getRepository(JournalEntry).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
            stashs: await this.db.getRepository(Stash).find({
                where: { group: { id: group.id } },
                relations: ["original"],
            }),
        });

        // Get preset.
        const preset = await this.db.getRepository(Preset).findOne({ id: presetId });
        if (!preset) { return notFound<Group>(`No preset with id "${presetId}"`); }

        Object.assign(preset, {
            tags: await this.db.getRepository(Tag).find({ preset: { id: preset.id } }),
            templateItems: await this.db.getRepository(TemplateItem).find({ preset: { id: preset.id } }),
            templateMonsters: await this.db.getRepository(TemplateMonster).find({
                relations: ["stash"],
                where: {
                    preset: { id: preset.id },
                },
            }),
            maneuvers: await this.db.getRepository(Maneuver).find({ preset: { id: preset.id } }),
            heroes: await this.db.getRepository(Hero).find({ preset: { id: preset.id } }),
            battleMaps: await this.db.getRepository(BattleMap).find({ preset: { id: preset.id } }),
            journalEntries: await this.db.getRepository(JournalEntry).find({ preset: { id: preset.id } }),
            stashs: await this.db.getRepository(Stash).find({ preset: { id: preset.id } }),
        });

        // Clone journal entries.
        const missingJournalEntries = preset.journalEntries
            .filter(fromPreset => !group.journalEntries.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        await Promise.all(missingJournalEntries.map(journalEntry => this.db.getRepository(JournalEntry).save({
            ...omit(["id", "preset"], journalEntry),
            original: { id: journalEntry.id },
            group: { id: group.id },
        })));

        // Clone tags.
        const missingTags = preset.tags
            .filter(fromPreset => !group.tags.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        await Promise.all(missingTags.map(tag => this.db.getRepository(Tag).save({
            ...omit(["id", "preset"], tag),
            original: { id: tag.id },
            group: { id: group.id },
        })));

        // Clone template items.
        const missingTemplateItems = preset.templateItems
            .filter(fromPreset => !group.templateItems.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        await Promise.all(missingTemplateItems.map(async originalTemplateItem => {
            const newTemplateItem = await this.db.getRepository(TemplateItem).save({
                ...omit(["id", "preset"], originalTemplateItem),
                original: { id: originalTemplateItem.id },
                group: { id: group.id },
            }) as TemplateItem;
            // Copy image.
            const sourcePath = Path.join(this.config.imagesDir, originalTemplateItem.id);
            if (existsSync(sourcePath)) {
                await copyFile(sourcePath, Path.join(this.config.imagesDir, newTemplateItem.id));
            }
            return newTemplateItem;
        }));

        // Clone stashs.
        const missingStashs = preset.stashs
            .filter(fromPreset => !group.stashs.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        const newStashs: Stash[] = await Promise.all(missingStashs.map(stash => this.db.getRepository(Stash).save({
            ...omit(["id", "preset"], stash),
            original: { id: stash.id },
            group: { id: group.id },
        })));

        // Clone stash items.
        await Promise.all(newStashs.map(async newStash => {
            const originalStash = await this.db.getRepository(Stash).findOne({
                relations: [
                    "stashItems",
                    "stashItems.templateItem",
                ],
                where: { id: newStash.original.id },
            });

            // Create new stash items.
            await Promise.all(originalStash.stashItems.map(async stashItem => {
                const newTemplateItem = await this.db.getRepository(TemplateItem).findOne({
                    original: { id: stashItem.templateItem.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(StashItem).save({
                    ...omit(["stash", "id", "templateItem"], stashItem),
                    stash: { id: newStash.id },
                    templateItem: { id: newTemplateItem.id },
                });
            }));
        }));

        // Clone maneuvers.
        const missingManeuvers = preset.maneuvers
            .filter(fromPreset => !group.maneuvers.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        await Promise.all(missingManeuvers.map(item => this.db.getRepository(Maneuver).save({
            ...omit(["id", "preset"], item),
            original: { id: item.id },
            group: { id: group.id },
        })));

        // Clone heroes.
        const missingHeroes = preset.heroes
            .filter(fromPreset => !group.heroes.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        const newHeroes = await Promise.all(missingHeroes.map(hero => this.db.getRepository(Hero).save({
            ...omit(["id", "preset"], hero),
            original: { id: hero.id },
            group: { id: group.id },
        } as Hero)));

        // Clone battle maps.
        const missingBattleMaps = preset.battleMaps
            .filter(fromPreset => !group.battleMaps.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        const newBattleMaps = await Promise.all(
            missingBattleMaps.map(battleMap => this.db.getRepository(BattleMap).save({
                ...omit(["id", "preset"], battleMap),
                original: { id: battleMap.id },
                group: { id: group.id },
            } as BattleMap)),
        );

        // Clone inventory, tags, markers and maneuvers.
        await Promise.all(newHeroes.map(async newHero => {
            const originalHero = await this.db.getRepository(Hero).findOne({
                relations: [
                    "items",
                    "items.templateItem",
                    "learnedManeuvers",
                    "learnedManeuvers.maneuver",
                    "markers",
                    "markers.battleMap",
                    "heroTags",
                    "heroTags.tag",
                ],
                where: { id: newHero.original.id },
            });

            // Copy avatar.
            const sourcePath = Path.join(this.config.imagesDir, originalHero.id);
            if (existsSync(sourcePath)) {
                await copyFile(sourcePath, Path.join(this.config.imagesDir, newHero.id));
            }

            // Create new inventory items.
            await Promise.all(originalHero.items.map(async item => {
                const newTemplateItem = await this.db.getRepository(TemplateItem).findOne({
                    original: { id: item.templateItem.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(Item).save({
                    ...omit(["hero", "id", "templateItem"], item),
                    hero: { id: newHero.id },
                    templateItem: { id: newTemplateItem.id },
                });
            }));

            // Create new maneuvers.
            await Promise.all(originalHero.learnedManeuvers.map(async learnedManeuver => {
                const newLearnedManeuver = await this.db.getRepository(Maneuver).findOne({
                    original: { id: learnedManeuver.maneuver.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(LearnedManeuver).save({
                    ...omit(["hero", "id", "maneuver"], learnedManeuver),
                    hero: { id: newHero.id },
                    maneuver: { id: newLearnedManeuver.id },
                });
            }));

            // Clone markers.
            await Promise.all(originalHero.markers.map(async originalMarker => {
                const newBattleMap = await this.db.getRepository(BattleMap).findOne({
                    original: { id: originalMarker.battleMap.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(Marker).save({
                    ...omit(["hero", "id", "battleMap"], originalMarker),
                    hero: { id: newHero.id },
                    battleMap: { id: newBattleMap.id },
                });
            }));

            // Clone hero tags.
            await Promise.all(originalHero.heroTags.map(async originalHeroTag => {
                const newTag = await this.db.getRepository(Tag).findOne({
                    original: { id: originalHeroTag.tag.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(HeroTag).save({
                    ...omit(["hero", "id", "tag"], originalHeroTag),
                    hero: { id: newHero.id },
                    tag: { id: newTag.id },
                });
            }));
        }));

        // Clone template monsters.
        const missingTemplateMonsters = preset.templateMonsters
            .filter(fromPreset => !group.templateMonsters.some(fromGroup => {
                return fromGroup.original && fromGroup.original.id === fromPreset.id;
            }));
        const newTemplateMonsters = await Promise.all(missingTemplateMonsters.map(async originalTemplateMonster => {
            const newStash = originalTemplateMonster.stash ? await this.db.getRepository(Stash).findOne({
                original: { id: originalTemplateMonster.stash.id },
                group: { id: groupId },
            }) : undefined;
            const newTemplateMonster = await this.db.getRepository(TemplateMonster).save({
                ...omit(["id", "preset"], originalTemplateMonster),
                original: { id: originalTemplateMonster.id },
                group: { id: group.id },
                stash: newStash ? { id: newStash.id } : undefined,
            } as TemplateMonster);
            // Copy image.
            const sourcePath = Path.join(this.config.imagesDir, originalTemplateMonster.id);
            if (existsSync(sourcePath)) {
                await copyFile(sourcePath, Path.join(this.config.imagesDir, newTemplateMonster.id));
            }
            return newTemplateMonster;
        }));

        // Clone monster maneuvers and monsters.
        await Promise.all(newTemplateMonsters.map(async templateMonster => {
            const original = await this.db.getRepository(TemplateMonster).findOne({
                relations: [
                    "monsterManeuvers",
                    "monsterManeuvers.maneuver",
                    "monsters",
                    "monsters.markers",
                    "monsters.markers.battleMap",
                ],
                where: { id: templateMonster.original.id },
            });

            // Create new monster maneuvers.
            await Promise.all(original.monsterManeuvers.map(async monsterManeuver => {
                const newManeuver = await this.db.getRepository(Maneuver).findOne({
                    original: { id: monsterManeuver.maneuver.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(MonsterManeuver).save({
                    ...omit(["templateMonster", "id", "maneuver"], monsterManeuver),
                    templateMonster: { id: templateMonster.id },
                    maneuver: { id: newManeuver.id },
                });
            }));

            // Clone monsters.
            await Promise.all(original.monsters.map(async originalMonster => {
                // Clone monster.
                const newMonster: Monster = await this.db.getRepository(Monster).save({
                    ...omit(["id", "templateMonster", "markers"], originalMonster),
                    templateMonster: { id: templateMonster.id },
                    original: { id: originalMonster.id },
                } as Monster);

                // Clone markers.
                await Promise.all(originalMonster.markers.map(async originalMarker => {
                    const newBattleMap = await this.db.getRepository(BattleMap).findOne({
                        original: { id: originalMarker.battleMap.id },
                        group: { id: groupId },
                    });
                    await this.db.getRepository(Marker).save({
                        ...omit(["monster", "id", "battleMap"], originalMarker),
                        monster: { id: newMonster.id },
                        battleMap: { id: newBattleMap.id },
                    });
                }));
            }));
        }));

        await Promise.all(newBattleMaps.map(async newBattleMap => {
            const originalBattleMap = await this.db.getRepository(BattleMap).findOne({
                relations: [
                    "areas",
                    "markers",
                    "markers.hero",
                    "markers.monster",
                    "markers.markerItems",
                    "markers.markerItems.templateItem",
                    "battleMapTags",
                    "battleMapTags.tag",
                ],
                where: { id: newBattleMap.original.id },
            });

            // Copy file.
            await copyFile(
                Path.join(this.config.battleMapsDir, originalBattleMap.id),
                Path.join(this.config.battleMapsDir, newBattleMap.id),
            );

            // Create new markers.
            await Promise.all(originalBattleMap.markers.map(async originalMarker => {
                let newMonster: Monster = originalMarker.monster && await this.db.getRepository(Monster).findOne({
                    where: {
                        original: { id: originalMarker.monster.id },
                        templateMonster: {
                            group: { id: groupId },
                        },
                    },
                    relations: ["templateMonster", "templateMonster.group"],
                });
                const newHero = originalMarker.hero && await this.db.getRepository(Hero).findOne({
                    original: { id: originalMarker.hero.id },
                    group: { id: groupId },
                });

                // Clone monster if not yet present.
                if (!newMonster && originalMarker.monster) {
                    const oldMonster = await this.db.getRepository(Monster).findOne({
                        relations: ["templateMonster"],
                        where: { id: originalMarker.monster.id },
                    });
                    const newTemplateMonster = await this.db.getRepository(TemplateMonster).findOne({
                        original: { id: oldMonster.templateMonster.id },
                        group: { id: groupId },
                    });
                    newMonster = await this.db.getRepository(Monster).save({
                        ...omit(["templateMonster", "id"], oldMonster),
                        templateMonster: { id: newTemplateMonster.id },
                    } as Monster);
                }

                const newMarker: Marker = await this.db.getRepository(Marker).save({
                    ...omit(["monster", "hero", "id", "battleMap", "markerItems"], originalMarker),
                    battleMap: { id: newBattleMap.id } as BattleMap,
                    hero: newHero ? { id: newHero.id } as Hero : undefined,
                    monster: newMonster ? { id: newMonster.id } : undefined,
                });

                // Create new marker items.
                await Promise.all(originalMarker.markerItems.map(async originalMarkerItem => {
                    const newTemplateItem = await this.db.getRepository(TemplateItem).findOne({
                        original: { id: originalMarkerItem.templateItem.id },
                        group: { id: groupId },
                    });
                    await this.db.getRepository(MarkerItem).save({
                        ...omit(["marker", "id", "templateItem"], originalMarkerItem),
                        marker: { id: newMarker.id },
                        templateItem: { id: newTemplateItem.id },
                    });
                }));
            }));

            // Clone battle map tags.
            await Promise.all(originalBattleMap.battleMapTags.map(async originalBattleMapTag => {
                const newTag = await this.db.getRepository(Tag).findOne({
                    original: { id: originalBattleMapTag.tag.id },
                    group: { id: groupId },
                });
                await this.db.getRepository(BattleMapTag).save({
                    ...omit(["battleMap", "id", "tag"], originalBattleMapTag),
                    battleMap: { id: newBattleMap.id },
                    tag: { id: newTag.id },
                });
            }));

            // Create new areas.
            await Promise.all(originalBattleMap.areas.map(async area => {
                await this.db.getRepository(Area).save({
                    ...omit(["id", "battleMap"], area),
                    battleMap: { id: newBattleMap.id },
                });
            }));
        }));

        this.live.publish(groupId, LiveAction.RELOAD_PAGE);

        return ok(this.get(groupId));
    }
}
