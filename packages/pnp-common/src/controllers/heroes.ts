import {
    controller,
    route,
    body,
    populate,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    query,
} from "hyrest";
import * as Path from "path";
import { warn } from "winston";
import { mkdirp, writeFile, unlink } from "fs-extra";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getHero, updateHero, createHero, upload } from "../scopes";
import { Hero, Item, LearnedManeuver, Note, Marker, ImageUpload, HeroTag } from "../models";
import { generateHeroName } from "../names";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { omit, pickBy } from "ramda";
import { ConfigInterface } from "../config-interface";

@controller @component
export class ControllerHeroes {
    @inject private live: LiveServer;
    @inject private db: Connection;
    @inject("config") private config: ConfigInterface;

    @route("GET", "/heroes").dump(Hero, getHero)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<Hero[]> {
        return ok(await this.db.getRepository(Hero).find({
            relations: ["preset", "group"],
            where: {
                ...pickBy(val => Boolean(val), {
                    group: groupId ? { id: groupId } : undefined,
                    preset: presetId ? { id: presetId } : undefined,
                }),
                deleted: null,
            },
        }));
    }

    @route("POST", "/hero/:id/avatar")
    public async uploadAvatar(
        @param("id") id: string,
        @body(upload) imageUpload: ImageUpload,
        @context ctx?: Context,
    ): Promise<void> {
        // Check upload.
        if ((!imageUpload.content && !imageUpload.url) || (imageUpload.content && imageUpload.url)) {
            return badRequest<void>("Specify either content or url.");
        }
        // Get hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (hero.group && !(await ctx.hasGroup(hero.group.id))) { return forbidden<void>(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden<void>(); }
        // Create directory if it doesn't exist yet.
        try {
            await mkdirp(this.config.imagesDir);
        } catch (e) {
            if (e.code !== "EEXIST") { throw e; }
        }
        // Write file.
        try {
            const data = await imageUpload.loadData();
            const path = Path.join(this.config.imagesDir, hero.id);
            await writeFile(path, data);
            if (hero.group) {
                this.live.publish(hero.group.id, LiveAction.CREATE, ModelName.HERO, hero.id);
            }
            // Done.
            return ok();
        } catch (err) {
            warn(`Error uploading avatar:`, err);
            return badRequest<void>("Could not process upload.");
        }
    }

    @route("POST", "/heroes").dump(Hero, getHero)
    public async create(@body(createHero) hero: Hero, @context ctx?: Context): Promise<Hero> {
        // Access control.
        if (!hero.group && !hero.preset || hero.group && hero.preset) { return badRequest<Hero>(); }
        if (hero.group && !(await ctx.isMaster(hero.group.id))) { return forbidden<Hero>(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden<Hero>(); }
        // Create new hero.
        const { id: newId } = await this.db.getRepository(Hero).save({
            name: generateHeroName(),
            ...hero,
        });
        const newHero = await this.get(newId);
        // Announce live update.
        if (newHero.group) {
            this.live.publish(newHero.group.id, LiveAction.CREATE, ModelName.HERO, newHero.id);
        }
        // Return updated.
        return ok(populate(Hero, newHero));
    }

    @route("POST", "/hero/:id/clone").dump(Hero, getHero)
    public async clone(@param("id") id: string, @context ctx?: Context): Promise<Hero> {
        // Fetch hero.
        const originalHero = await this.db.getRepository(Hero).findOne({
            where: { id },
            relations: [
                "group",
                "preset",
                "learnedManeuvers",
                "learnedManeuvers.maneuver",
                "items",
                "items.templateItem",
                "notes",
            ],
        });
        // Access control.
        if (!originalHero) { return notFound<Hero>(); }
        if (originalHero.group && !(await ctx.isMaster(originalHero.group.id))) { return forbidden<Hero>(); }
        if (originalHero.preset && !(await ctx.hasPreset(originalHero.preset.id))) { return forbidden<Hero>(); }
        // Clone hero.
        const newHero: Hero = await this.db.getRepository(Hero).save({
            ...omit(["id", "learnedManeuvers", "items", "notes"], originalHero),
            name: generateHeroName(),
        });
        // Clone items.
        await Promise.all(originalHero.items.map(originalItem => this.db.getRepository(Item).save({
            ...omit(["id", "hero"], originalItem),
            hero: { id: newHero.id },
        })));
        // Clone learned maneuvers.
        await Promise.all(
            originalHero.learnedManeuvers.map(originalLearnedManeuver => this.db.getRepository(LearnedManeuver).save({
                ...omit(["id", "hero"], originalLearnedManeuver),
                hero: { id: newHero.id },
            })),
        );
        // Clone notes.
        await Promise.all(
            originalHero.notes.map(originalNote => this.db.getRepository(Note).save({
                ...omit(["id", "hero"], originalNote),
                hero: { id: newHero.id },
            })),
        );
        // Return new hero.
        return ok(this.get(newHero.id));
    }

    @route("POST", "/hero/:id").dump(Hero, getHero)
    public async update(
        @param("id") id: string,
        @body(updateHero) change: Hero,
        @context ctx?: Context,
    ): Promise<Hero> {
        // Fetch hero.
        const old = await this.db.getRepository(Hero).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!old) { return notFound<Hero>("No such hero."); }
        // Update hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        Object.assign(hero, change);
        const { errors } = hero.validUpdate(old);
        // Validate update.
        const isMasterOrPreset = hero.group && (await ctx.isMaster(hero.group.id)) ||
            hero.preset && await ctx.hasPreset(hero.preset.id);
        if (hero.level > old.level && !isMasterOrPreset) { return badRequest<Hero>(); }
        if (errors.length > 0 && !isMasterOrPreset) { return badRequest<Hero>(); }
        // Save updated hero.
        const newHero = await this.db.getRepository(Hero).save(hero);
        // Announce live update.
        if (newHero.group) {
            this.live.publish(newHero.group.id, LiveAction.UPDATE, ModelName.HERO, newHero.id);
        }
        // Return updated.
        return ok(populate(Hero, newHero));
    }

    @route("GET", "/hero/:id").dump(Hero, getHero)
    public async get(@param("id") id: string): Promise<Hero> {
        const hero = await this.db.getRepository(Hero).findOne({
            relations: ["group", "preset"],
            where: { id },
        });
        if (!hero) { return notFound<Hero>(); }
        return ok(populate(Hero, hero));
    }

    @route("POST", "/hero/:id/archive")
    public async archive(
        @param("id") id: string,
        @context ctx?: Context,
    ): Promise<undefined> {
        // Fetch hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!hero) { return notFound(); }
        if (hero.group && !(await ctx.isMaster(hero.group.id))) { return forbidden(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden(); }
        await this.db.getRepository(Hero).save({
            ...hero,
            deleted: new Date(),
        });
        // Announce live update.
        if (hero.group) {
            this.live.publish(hero.group.id, LiveAction.DELETE, ModelName.HERO, hero.id);
        }
        return ok();
    }

    @route("DELETE", "/hero/:id")
    public async delete(
        @param("id") id: string,
        @context ctx?: Context,
    ): Promise<undefined> {
        // Fetch hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!hero) { return notFound(); }
        if (hero.group && !(await ctx.isMaster(hero.group.id))) { return forbidden(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden(); }
        // Delete `original` references.
        await this.db.getRepository(Hero).createQueryBuilder("hero")
            .where(`hero.original_id = :id`, { id })
            .update({ original: null })
            .execute();
        // Delete hero tags.
        await this.db.getRepository(HeroTag).createQueryBuilder("hero_tag")
            .where(`hero_tag.hero_id = :id`, { id })
            .delete()
            .execute();
        // Delete items.
        await this.db.getRepository(Item).createQueryBuilder("item")
            .where(`item.hero_id = :id`, { id })
            .delete()
            .execute();
        // Delete learned maneuvers.
        await this.db.getRepository(LearnedManeuver).createQueryBuilder("learned_maneuver")
            .where(`learned_maneuver.hero_id = :id`, { id })
            .delete()
            .execute();
        // Delete notes.
        await this.db.getRepository(Note).createQueryBuilder("note")
            .where(`note.hero_id = :id`, { id })
            .delete()
            .execute();
        // Delete markers.
        await this.db.getRepository(Marker).createQueryBuilder("marker")
            .where(`marker.hero_id = :id`, { id })
            .delete()
            .execute();
        // Delete hero.
        await this.db.getRepository(Hero).createQueryBuilder("hero")
            .where(`hero.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (hero.group) {
            this.live.publish(hero.group.id, LiveAction.DELETE, ModelName.HERO, hero.id);
        }
        // Delete avatar.
        try {
            await unlink(Path.join(this.config.imagesDir, hero.id));
        } catch (err) {
            if (err.code !== "ENOENT") { throw err; }
        }
        return ok();
    }
}
