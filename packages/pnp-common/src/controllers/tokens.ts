import {
    controller,
    route,
    created,
    body,
    unauthorized,
    noauth,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { login, owner } from "../scopes";
import { Group, Token } from "../models";

@controller @component
export class ControllerTokens {
    @inject private db: Connection;

    @route("POST", "/token").dump(Token, owner) @noauth
    public async create(@body(login) credentials: Token): Promise<Token> {
        if (!credentials.group.masterPassword && credentials.master) {
            return unauthorized<Token>("Can't login as master without specifying the master password.");
        }
        const group = await this.db.getRepository(Group).findOne(credentials.group);
        if (!group) { return unauthorized<Token>(); }
        const newToken = await this.db.getRepository(Token).save({ group, master: credentials.master });
        return created(populate(Token, newToken));
    }
}
