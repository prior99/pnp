import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getBattleMapTag, createBattleMapTag } from "../scopes";
import { BattleMap, BattleMapTag } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerBattleMapTags {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/battle-map/:id/tags").dump(BattleMapTag, getBattleMapTag)
    public async search(@param("id") id: string): Promise<BattleMapTag[]> {
        return ok(await this.db.getRepository(BattleMapTag).find({
            where: { battleMap: { id } },
            relations: ["battleMap", "tag"],
        }));
    }

    @route("POST", "/battle-map-tags").dump(BattleMapTag, getBattleMapTag)
    public async create(
        @body(createBattleMapTag) battleMapTag: BattleMapTag,
        @context ctx?: Context,
    ): Promise<BattleMapTag> {
        // Fetch battle map.
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            where: { id: battleMapTag.battleMap.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!battleMap) { return badRequest<BattleMapTag>(); }
        if (battleMapTag.battleMap.group && !(await ctx.isMaster(battleMapTag.battleMap.group.id))) {
            return forbidden<BattleMapTag>();
        }
        if (battleMapTag.battleMap.preset && !(await ctx.hasPreset(battleMapTag.battleMap.preset.id))) {
            return forbidden<BattleMapTag>();
        }
        // Update.
        const newBattleMapTag = await this.db.getRepository(BattleMapTag).save(battleMapTag);
        // Announce live update.
        if (battleMap.group) {
            this.live.publish(battleMap.group.id, LiveAction.CREATE, ModelName.BATTLE_MAP_TAG, newBattleMapTag.id);
        }
        // Return updated.
        return ok(newBattleMapTag);
    }

    @route("DELETE", "/battle-map-tag/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch item.
        const battleMapTag = await this.db.getRepository(BattleMapTag).findOne({
            where: { id },
            relations: ["battleMap", "battleMap.group"],
        });
        // Access control.
        if (!battleMapTag) { return notFound<void>(); }
        if (battleMapTag.battleMap.group && !(await ctx.isMaster(battleMapTag.battleMap.group.id))) {
            return forbidden<void>();
        }
        if (battleMapTag.battleMap.preset && !(await ctx.hasPreset(battleMapTag.battleMap.preset.id))) {
            return forbidden<void>();
        }
        // Delete item.
        await this.db.getRepository(BattleMapTag).createQueryBuilder("battle_map_tag")
            .where(`battle_map_tag.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (battleMapTag.battleMap.group) {
            this.live.publish(
                battleMapTag.battleMap.group.id,
                LiveAction.DELETE,
                ModelName.BATTLE_MAP_TAG,
                battleMapTag.id,
            );
        }
        // Return.
        return ok();
    }

    @route("GET", "/battle-map-tag/:id").dump(BattleMapTag, getBattleMapTag)
    public async get(@param("id") id: string): Promise<BattleMapTag> {
        const battleMapTag = await this.db.getRepository(BattleMapTag).findOne({
            where: { id },
            relations: ["battleMap", "tag"],
        });
        if (!battleMapTag) { return notFound<BattleMapTag>(); }
        return ok(populate(BattleMapTag, battleMapTag));
    }
}
