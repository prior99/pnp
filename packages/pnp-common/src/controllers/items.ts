import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { updateItem, createItem, getItem } from "../scopes";
import { Item, Hero } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerItems {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/hero/:id/items").dump(Item, getItem)
    public async search(@param("id") id: string): Promise<Item[]> {
        return ok(await this.db.getRepository(Item).find({
            where: { hero: { id } },
            relations: ["hero", "templateItem"],
        }));
    }

    @route("POST", "/items").dump(Item, getItem)
    public async create(@body(createItem) item: Item, @context ctx?: Context): Promise<Item> {
        // Fetch hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id: item.hero.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!hero) { return badRequest<Item>(); }
        if (item.hero.group && !(await ctx.isMaster(item.hero.group.id))) { return forbidden<Item>(); }
        if (item.hero.preset && !(await ctx.hasPreset(item.hero.preset.id))) { return forbidden<Item>(); }
        // Update.
        const newItem = await this.db.getRepository(Item).save(item);
        // Announce live update.
        if (hero.group) {
            this.live.publish(hero.group.id, LiveAction.CREATE, ModelName.ITEM, newItem.id);
        }
        // Return updated.
        return ok(newItem);
    }

    @route("DELETE", "/item/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch item.
        const item = await this.db.getRepository(Item).findOne({
            where: { id },
            relations: ["hero", "hero.group"],
        });
        // Access control.
        if (!item) { return notFound<void>(); }
        if (item.hero.group && !(await ctx.hasGroup(item.hero.group.id))) { return forbidden<void>(); }
        if (item.hero.preset && !(await ctx.hasPreset(item.hero.preset.id))) { return forbidden<void>(); }
        // Delete item.
        await this.db.getRepository(Item).createQueryBuilder("item")
            .where(`item.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (item.hero.group) {
            this.live.publish(item.hero.group.id, LiveAction.DELETE, ModelName.ITEM, item.id);
        }
        // Return.
        return ok();
    }

    @route("POST", "/item/:id").dump(Item, getItem)
    public async update(
        @param("id") id: string,
        @body(updateItem) change: Item,
        @context ctx?: Context,
    ): Promise<Item> {
        // Fetch item.
        const item = await this.db.getRepository(Item).findOne({
            where: { id },
            relations: ["hero", "hero.group", "hero.preset", "templateItem"],
        });
        // Access control.
        if (!item) { return notFound<Item>(); }
        if (item.hero.group && !(await ctx.hasGroup(item.hero.group.id))) { return forbidden<Item>(); }
        if (item.hero.preset && !(await ctx.hasPreset(item.hero.preset.id))) { return forbidden<Item>(); }
        // Update.
        Object.assign(item, change);
        await this.db.getRepository(Item).save(item);
        // Announce live update.
        if (item.hero.group) {
            this.live.publish(item.hero.group.id, LiveAction.UPDATE, ModelName.ITEM, item.id);
        }
        // Return updated.
        return ok(item);
    }

    @route("GET", "/item/:id").dump(Item, getItem)
    public async get(@param("id") id: string): Promise<Item> {
        const item = await this.db.getRepository(Item).findOne({
            where: { id },
            relations: ["hero", "templateItem"],
        });
        if (!item) { return notFound<Item>(); }
        return ok(populate(Item, item));
    }
}
