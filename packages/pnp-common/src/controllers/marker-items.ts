import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { updateMarkerItem, createMarkerItem, getMarkerItem } from "../scopes";
import { Marker, MarkerItem, Stash, StashItem, Hero, Item } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { equals, pick } from "ramda";

@controller @component
export class ControllerMarkerItems {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/marker/:id/items").dump(MarkerItem, getMarkerItem)
    public async search(@param("id") id: string): Promise<MarkerItem[]> {
        return ok(await this.db.getRepository(MarkerItem).find({
            where: { marker: { id } },
            relations: ["marker", "templateItem"],
        }));
    }

    @route("POST", "/marker-items-from-hero/:heroId/:markerId").dump(MarkerItem, getMarkerItem)
    public async fromHero(
        @param("heroId") heroId: string,
        @param("markerId") markerId: string,
        @context ctx?: Context,
    ): Promise<void> {
        // Fetch marker.
        const marker = await this.db.getRepository(Marker).findOne({
            where: { id: markerId },
            relations: ["battleMap", "battleMap.group", "battleMap.preset"],
        });
        // Fetch hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id: heroId },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!marker) { return badRequest<void>(); }
        if (!hero) { return badRequest<void>(); }
        if (!equals(pick(["group", "preset"], marker.battleMap), pick(["group", "preset"], hero))) {
            return badRequest<void>();
        }
        if (hero.group && !(await ctx.isMaster(hero.group.id))) { return forbidden<void>(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden<void>(); }
        // Create all marker items.
        const items = await this.db.getRepository(Item).find({
            relations: ["templateItem", "hero"],
            where: { hero: { id: heroId } },
        });
        const markerItems = await Promise.all(items.map(item => this.db.getRepository(MarkerItem).save({
            ...pick(["templateItem", "notes", "quantity"], item),
            marker: { id: markerId },
        } as MarkerItem)));
        // Announce live update for created marker items.
        if (hero.group) {
            markerItems.forEach(markerItem => {
                this.live.publish(hero.group.id, LiveAction.CREATE, ModelName.MARKER_ITEM, markerItem.id);
            });
        }
        // Update marker.
        await this.db.getRepository(Marker).createQueryBuilder("marker")
            .update(Marker)
            .set({ money: hero.money })
            .where(`marker.id = :markerId`, { markerId })
            .execute();
        // Announce live update.
        if (marker.battleMap.group) {
            this.live.publish(marker.battleMap.group.id, LiveAction.UPDATE, ModelName.MARKER, marker.id);
        }
        // Return updated.
        return ok();
    }

    @route("POST", "/marker-items-from-stash/:stashId/:markerId").dump(MarkerItem, getMarkerItem)
    public async fromStash(
        @param("stashId") stashId: string,
        @param("markerId") markerId: string,
        @context ctx?: Context,
    ): Promise<void> {
        // Fetch marker.
        const marker = await this.db.getRepository(Marker).findOne({
            where: { id: markerId },
            relations: ["battleMap", "battleMap.group", "battleMap.preset"],
        });
        // Fetch stash.
        const stash = await this.db.getRepository(Stash).findOne({
            where: { id: stashId },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!marker) { return badRequest<void>(); }
        if (!stash) { return badRequest<void>(); }
        if (!equals(pick(["group", "preset"], marker.battleMap), pick(["group", "preset"], stash))) {
            return badRequest<void>();
        }
        if (stash.group && !(await ctx.isMaster(stash.group.id))) { return forbidden<void>(); }
        if (stash.preset && !(await ctx.hasPreset(stash.preset.id))) { return forbidden<void>(); }
        // Create all marker items.
        const stashItems = await this.db.getRepository(StashItem).find({
            relations: ["templateItem", "stash"],
            where: { stash: { id: stashId } },
        });
        const markerItems = await Promise.all(stashItems.map(stashItem => this.db.getRepository(MarkerItem).save({
            ...pick(["templateItem", "notes", "quantity"], stashItem),
            marker: { id: markerId },
        } as MarkerItem)));
        // Announce live update for created marker items.
        if (stash.group) {
            markerItems.forEach(markerItem => {
                this.live.publish(stash.group.id, LiveAction.CREATE, ModelName.MARKER_ITEM, markerItem.id);
            });
        }
        // Update marker.
        await this.db.getRepository(Marker).createQueryBuilder("marker")
            .update(Marker)
            .set({ money: stash.money })
            .where(`marker.id = :markerId`, { markerId })
            .execute();
        // Announce live update.
        if (marker.battleMap.group) {
            this.live.publish(marker.battleMap.group.id, LiveAction.UPDATE, ModelName.MARKER, marker.id);
        }
        // Return updated.
        return ok();
    }

    @route("POST", "/marker-items").dump(MarkerItem, getMarkerItem)
    public async create(@body(createMarkerItem) markerItem: MarkerItem, @context ctx?: Context): Promise<MarkerItem> {
        // Fetch marker.
        const marker = await this.db.getRepository(Marker).findOne({
            where: { id: markerItem.marker.id },
            relations: ["battleMap", "battleMap.group", "battleMap.preset"],
        });
        // Access control.
        if (!marker) { return badRequest<MarkerItem>(); }
        if (marker.battleMap.group && !(await ctx.isMaster(marker.battleMap.group.id))) {
            return forbidden<MarkerItem>();
        }
        if (marker.battleMap.preset && !(await ctx.hasPreset(marker.battleMap.preset.id))) {
            return forbidden<MarkerItem>();
        }
        // Update.
        const newMarkerItem = await this.db.getRepository(MarkerItem).save(markerItem);
        // Announce live update.
        if (marker.battleMap.group) {
            this.live.publish(marker.battleMap.group.id, LiveAction.CREATE, ModelName.MARKER_ITEM, newMarkerItem.id);
        }
        // Return updated.
        return ok(newMarkerItem);
    }

    @route("DELETE", "/marker-item/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch marker item.
        const markerItem = await this.db.getRepository(MarkerItem).findOne({
            where: { id },
            relations: ["marker", "marker.battleMap", "marker.battleMap.group"],
        });
        // Access control.
        if (!markerItem) { return notFound<void>(); }
        if (markerItem.marker.battleMap.group && !(await ctx.hasGroup(markerItem.marker.battleMap.group.id))) {
            return forbidden<void>();
        }
        if (markerItem.marker.battleMap.preset && !(await ctx.hasPreset(markerItem.marker.battleMap.preset.id))) {
            return forbidden<void>();
        }
        // Delete item.
        await this.db.getRepository(MarkerItem).createQueryBuilder("marker_item")
            .where(`marker_item.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (markerItem.marker.battleMap.group) {
            this.live.publish(
                markerItem.marker.battleMap.group.id,
                LiveAction.DELETE,
                ModelName.MARKER_ITEM,
                markerItem.id,
            );
        }
        // Return.
        return ok();
    }

    @route("POST", "/marker-item/:id").dump(MarkerItem, getMarkerItem)
    public async update(
        @param("id") id: string,
        @body(updateMarkerItem) change: MarkerItem,
        @context ctx?: Context,
    ): Promise<MarkerItem> {
        // Fetch marker item.
        const markerItem = await this.db.getRepository(MarkerItem).findOne({
            where: { id },
            relations: [
                "marker",
                "marker.battleMap",
                "marker.battleMap.group",
                "marker.battleMap.preset",
                "templateItem",
            ],
        });
        // Access control.
        if (!markerItem) { return notFound<MarkerItem>(); }
        if (markerItem.marker.battleMap.group && !(await ctx.hasGroup(markerItem.marker.battleMap.group.id))) {
            return forbidden<MarkerItem>();
        }
        if (markerItem.marker.battleMap.preset && !(await ctx.hasPreset(markerItem.marker.battleMap.preset.id))) {
            return forbidden<MarkerItem>();
        }
        // Update.
        Object.assign(markerItem, change);
        await this.db.getRepository(MarkerItem).save(markerItem);
        // Announce live update.
        if (markerItem.marker.battleMap.group) {
            this.live.publish(
                markerItem.marker.battleMap.group.id,
                LiveAction.UPDATE,
                ModelName.MARKER_ITEM,
                markerItem.id,
            );
        }
        // Return updated.
        return ok(markerItem);
    }

    @route("GET", "/marker-item/:id").dump(MarkerItem, getMarkerItem)
    public async get(@param("id") id: string): Promise<MarkerItem> {
        const markerItem = await this.db.getRepository(MarkerItem).findOne({
            where: { id },
            relations: ["marker", "templateItem"],
        });
        if (!markerItem) { return notFound<Marker>(); }
        return ok(populate(MarkerItem, markerItem));
    }
}
