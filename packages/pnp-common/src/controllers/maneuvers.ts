import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
    query,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getManeuver, updateManeuver, createManeuver } from "../scopes";
import { Maneuver } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy } from "ramda";

@controller @component
export class ControllerManeuvers {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/maneuvers").dump(Maneuver, getManeuver)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<Maneuver[]> {
        return ok(await this.db.getRepository(Maneuver).find({
            relations: ["preset", "group"],
            where: pickBy(val => Boolean(val), {
                group: groupId ? { id: groupId } : undefined,
                preset: presetId ? { id: presetId } : undefined,
            }),
        }));
    }

    @route("POST", "/maneuvers").dump(Maneuver, getManeuver)
    public async create(
        @body(createManeuver) maneuver: Maneuver,
        @context ctx?: Context,
    ): Promise<Maneuver> {
        // Access control.
        if (!maneuver.group && !maneuver.preset || maneuver.group && maneuver.preset) {
            return badRequest<Maneuver>("Either group or preset need to be supplied.");
        }
        if (maneuver.group && !(await ctx.isMaster(maneuver.group.id))) { return forbidden<Maneuver>(); }
        if (maneuver.preset && !(await ctx.hasPreset(maneuver.preset.id))) { return forbidden<Maneuver>(); }
        // Create maneuver.
        const { id: newId } = await this.db.getRepository(Maneuver).save({
            ...maneuver,
            suggestedStaminaCost: maneuver.suggestedStaminaCost || null,
        });
        const newManeuver = await this.get(newId);
        // Announce live update.
        if (newManeuver.group) {
            this.live.publish(maneuver.group.id, LiveAction.CREATE, ModelName.MANEUVER, newManeuver.id);
        }
        // Return update.
        return ok(newManeuver);
    }

    @route("POST", "/maneuver/:id").dump(Maneuver, getManeuver)
    public async update(
        @param("id") id: string,
        @body(updateManeuver) change: Maneuver,
        @context ctx?: Context,
    ): Promise<Maneuver> {
        // Get maneuver.
        const maneuver = await this.db.getRepository(Maneuver).findOne({
            where: { id },
            relations: ["group"],
        });
        // Access control.
        if (!maneuver) { return notFound<Maneuver>(); }
        if (maneuver.group && !(await ctx.isMaster(maneuver.group.id))) { return forbidden<Maneuver>(); }
        if (maneuver.preset && !(await ctx.hasPreset(maneuver.preset.id))) { return forbidden<Maneuver>(); }
        // Perform update.
        Object.assign(maneuver, change);
        await this.db.getRepository(Maneuver).save(maneuver);
        // Announce update.
        if (maneuver.group) {
            this.live.publish(maneuver.group.id, LiveAction.UPDATE, ModelName.MANEUVER, maneuver.id);
        }
        // Return.
        return ok(maneuver);
    }

    @route("DELETE", "/maneuver/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch maneuver.
        const maneuver = await this.db.getRepository(Maneuver).findOne({
            where: { id },
            relations: ["group"],
        });
        // Access control.
        if (!maneuver) { return notFound<void>(); }
        if (maneuver.group && !(await ctx.isMaster(maneuver.group.id))) { return forbidden<void>(); }
        if (maneuver.preset && !(await ctx.hasPreset(maneuver.preset.id))) { return forbidden<void>(); }
        // Delete.
        await this.db.getRepository(Maneuver).createQueryBuilder("maneuver")
            .where(`maneuver.id = :id`, { id })
            .delete()
            .execute();
        // Announce update.
        if (maneuver.group) {
            this.live.publish(maneuver.group.id, LiveAction.DELETE, ModelName.MANEUVER, maneuver.id);
        }
        // Return.
        return ok();
    }

    @route("GET", "/maneuver/:id").dump(Maneuver, getManeuver)
    public async get(@param("id") id: string): Promise<Maneuver> {
        const maneuver = await this.db.getRepository(Maneuver).findOne({
            relations: ["group", "preset"],
            where: { id },
        });
        if (!maneuver) { return notFound<Maneuver>(); }
        return ok(populate(Maneuver, maneuver));
    }
}
