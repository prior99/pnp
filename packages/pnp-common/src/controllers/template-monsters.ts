import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
    query,
} from "hyrest";
import * as Path from "path";
import { warn } from "winston";
import { mkdirp, writeFile, unlink } from "fs-extra";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getTemplateMonster, updateTemplateMonster, createTemplateMonster, upload } from "../scopes";
import { TemplateMonster, Marker, Monster, MonsterManeuver, ImageUpload } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy } from "ramda";
import { ConfigInterface } from "../config-interface";

@controller @component
export class ControllerTemplateMonsters {
    @inject private live: LiveServer;
    @inject private db: Connection;
    @inject("config") private config: ConfigInterface;

    @route("GET", "/template-monsters").dump(TemplateMonster, getTemplateMonster)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<TemplateMonster[]> {
        return ok(await this.db.getRepository(TemplateMonster).find({
            relations: ["preset", "group", "stash"],
            where: pickBy(val => Boolean(val), {
                group: groupId ? { id: groupId } : undefined,
                preset: presetId ? { id: presetId } : undefined,
            }),
        }));
    }

    @route("POST", "/template-monsters").dump(TemplateMonster, getTemplateMonster)
    public async create(
        @body(createTemplateMonster) templateMonster: TemplateMonster,
        @context ctx?: Context,
    ): Promise<TemplateMonster> {
        // Access control.
        if (!templateMonster.group && !templateMonster.preset || templateMonster.group && templateMonster.preset) {
            return badRequest<TemplateMonster>("Either group or preset need to be supplied.");
        }
        if (templateMonster.group && !(await ctx.isMaster(templateMonster.group.id))) {
            return forbidden<TemplateMonster>();
        }
        if (templateMonster.preset && !(await ctx.hasPreset(templateMonster.preset.id))) {
            return forbidden<TemplateMonster>();
        }
        // Create new template monster.
        const newTemplateMonster = await this.db.getRepository(TemplateMonster).save(templateMonster);
        // Announce live update.
        if (templateMonster.group) {
            this.live.publish(
                templateMonster.group.id,
                LiveAction.CREATE,
                ModelName.TEMPLATE_MONSTER,
                newTemplateMonster.id,
            );
        }
        // Return new template monster.
        return ok(newTemplateMonster);
    }

    @route("POST", "/template-monster/:id").dump(TemplateMonster, getTemplateMonster)
    public async update(
        @param("id") id: string,
        @body(updateTemplateMonster) change: TemplateMonster,
        @context ctx?: Context,
    ): Promise<TemplateMonster> {
        // Get template monster.
        const templateMonster = await this.db.getRepository(TemplateMonster).findOne({
            where: { id },
            relations: ["group"],
        });
        // Access control.
        if (!templateMonster) { return notFound<TemplateMonster>(); }
        if (templateMonster.group && !(await ctx.isMaster(templateMonster.group.id))) {
            return forbidden<TemplateMonster>();
        }
        if (templateMonster.preset && !(await ctx.hasPreset(templateMonster.preset.id))) {
            return forbidden<TemplateMonster>();
        }
        // Perform update.
        Object.assign(templateMonster, change);
        await this.db.getRepository(TemplateMonster).save(templateMonster);
        // Announce live update.
        if (templateMonster.group) {
            this.live.publish(
                templateMonster.group.id,
                LiveAction.UPDATE,
                ModelName.TEMPLATE_MONSTER,
                templateMonster.id,
            );
        }
        // Return updated.
        return ok(templateMonster);
    }

    @route("POST", "/template-monster/:id/image")
    public async uploadImage(
        @param("id") id: string,
        @body(upload) imageUpload: ImageUpload,
        @context ctx?: Context,
    ): Promise<void> {
        // Check upload.
        if ((!imageUpload.content && !imageUpload.url) || (imageUpload.content && imageUpload.url)) {
            return badRequest<void>("Specify either content or url.");
        }
        // Get template monster.
        const templateMonster = await this.db.getRepository(TemplateMonster).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (templateMonster.group && !(await ctx.hasGroup(templateMonster.group.id))) { return forbidden<void>(); }
        if (templateMonster.preset && !(await ctx.hasPreset(templateMonster.preset.id))) { return forbidden<void>(); }
        // Create directory if it doesn't exist yet.
        try {
            await mkdirp(this.config.imagesDir);
        } catch (e) {
            if (e.code !== "EEXIST") { throw e; }
        }
        // Write file.
        try {
            const data = await imageUpload.loadData();
            const path = Path.join(this.config.imagesDir, templateMonster.id);
            await writeFile(path, data);
            if (templateMonster.group) {
                this.live.publish(
                    templateMonster.group.id,
                    LiveAction.CREATE,
                    ModelName.TEMPLATE_MONSTER,
                    templateMonster.id,
                );
            }
            // Done.
            return ok();
        } catch (err) {
            warn(`Error uploading image:`, err);
            return badRequest<void>("Could not process upload.");
        }
    }

    @route("DELETE", "/template-monster/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Get template monster.
        const templateMonster = await this.db.getRepository(TemplateMonster).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!templateMonster) { return notFound<void>(); }
        if (templateMonster.group && !(await ctx.isMaster(templateMonster.group.id))) { return forbidden<void>(); }
        if (templateMonster.preset && !(await ctx.hasPreset(templateMonster.preset.id))) { return forbidden<void>(); }
        // Delete original references on monsters.
        await this.db.getRepository(Monster).createQueryBuilder("monster")
            .where(queryBuilder => {
                const subQuery = queryBuilder.subQuery()
                    .select("id")
                    .from(Monster, "monster")
                    .where(`monster.template_monster_id = :id`)
                    .getQuery();
                return `monster.original_id IN ${subQuery}`;
            })
            .setParameter("id", id)
            .update(Monster)
            .set({ original: null })
            .execute();
        // Delete original references on monsters.
        await this.db.getRepository(Monster).createQueryBuilder("monster")
            .where(queryBuilder => {
                const subQuery = queryBuilder.subQuery()
                    .select("id")
                    .from(Monster, "monster")
                    .where(`monster.template_monster_id = :id`)
                    .getQuery();
                return `monster.original_id IN ${subQuery}`;
            })
            .setParameter("id", id)
            .update(Monster)
            .set({ original: null })
            .execute();
        // Delete original references on template monsters.
        await this.db.getRepository(TemplateMonster).createQueryBuilder("template_monster")
            .update(TemplateMonster)
            .set({ original: null })
            .where({ original: { id } })
            .execute();
        // Delete all markers for all monsters.
        await this.db.getRepository(Marker).createQueryBuilder("marker")
            .where(queryBuilder => {
                const subQuery = queryBuilder.subQuery()
                    .select("id")
                    .from(Monster, "monster")
                    .where(`monster.template_monster_id = :id`)
                    .getQuery();
                return `marker.monster_id IN ${subQuery}`;
            })
            .setParameter("id", id)
            .delete()
            .execute();
        // Delete all monster maneuvers.
        await this.db.getRepository(MonsterManeuver).createQueryBuilder("monster_maneuver")
            .where(`monster_maneuver.template_monster_id = :id`, { id })
            .delete()
            .execute();
        // Delete all monsters.
        await this.db.getRepository(Monster).createQueryBuilder("monster")
            .where(`monster.template_monster_id = :id`, { id })
            .delete()
            .execute();
        // Delete.
        await this.db.getRepository(TemplateMonster).createQueryBuilder("template_monster")
            .where(`template_monster.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (templateMonster.group) {
            this.live.publish(
                templateMonster.group.id,
                LiveAction.DELETE,
                ModelName.TEMPLATE_MONSTER,
                templateMonster.id,
            );
        }
        // Delete image.
        try {
            await unlink(Path.join(this.config.imagesDir, templateMonster.id));
        } catch (err) {
            if (err.code !== "ENOENT") { throw err; }
        }
        return ok();
    }

    @route("GET", "/template-monster/:id").dump(TemplateMonster, getTemplateMonster)
    public async get(@param("id") id: string): Promise<TemplateMonster> {
        const templateMonster = await this.db.getRepository(TemplateMonster).findOne({
            relations: ["group", "preset", "stash"],
            where: { id },
        });
        if (!templateMonster) { return notFound<TemplateMonster>(); }
        return ok(populate(TemplateMonster, templateMonster));
    }
}
