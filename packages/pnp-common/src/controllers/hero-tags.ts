import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getHeroTag, createHeroTag } from "../scopes";
import { Hero, HeroTag } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerHeroTags {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/hero/:id/tags").dump(HeroTag, getHeroTag)
    public async search(@param("id") id: string): Promise<HeroTag[]> {
        return ok(await this.db.getRepository(HeroTag).find({
            where: { hero: { id } },
            relations: ["hero", "tag"],
        }));
    }

    @route("POST", "/hero-tags").dump(HeroTag, getHeroTag)
    public async create(@body(createHeroTag) heroTag: HeroTag, @context ctx?: Context): Promise<HeroTag> {
        // Fetch hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id: heroTag.hero.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!hero) { return badRequest<HeroTag>(); }
        if (heroTag.hero.group && !(await ctx.isMaster(heroTag.hero.group.id))) { return forbidden<HeroTag>(); }
        if (heroTag.hero.preset && !(await ctx.hasPreset(heroTag.hero.preset.id))) { return forbidden<HeroTag>(); }
        // Update.
        const newHeroTag = await this.db.getRepository(HeroTag).save(heroTag);
        // Announce live update.
        if (hero.group) {
            this.live.publish(hero.group.id, LiveAction.CREATE, ModelName.HERO_TAG, newHeroTag.id);
        }
        // Return updated.
        return ok(newHeroTag);
    }

    @route("DELETE", "/hero-tag/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch item.
        const heroTag = await this.db.getRepository(HeroTag).findOne({
            where: { id },
            relations: ["hero", "hero.group"],
        });
        // Access control.
        if (!heroTag) { return notFound<void>(); }
        if (heroTag.hero.group && !(await ctx.isMaster(heroTag.hero.group.id))) { return forbidden<void>(); }
        if (heroTag.hero.preset && !(await ctx.hasPreset(heroTag.hero.preset.id))) { return forbidden<void>(); }
        // Delete item.
        await this.db.getRepository(HeroTag).createQueryBuilder("hero_tag")
            .where(`hero_tag.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (heroTag.hero.group) {
            this.live.publish(heroTag.hero.group.id, LiveAction.DELETE, ModelName.HERO_TAG, heroTag.id);
        }
        // Return.
        return ok();
    }

    @route("GET", "/hero-tag/:id").dump(HeroTag, getHeroTag)
    public async get(@param("id") id: string): Promise<HeroTag> {
        const heroTag = await this.db.getRepository(HeroTag).findOne({
            where: { id },
            relations: ["hero", "tag"],
        });
        if (!heroTag) { return notFound<HeroTag>(); }
        return ok(populate(HeroTag, heroTag));
    }
}
