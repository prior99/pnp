import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
    query,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getTag, updateTag, createTag } from "../scopes";
import { Tag, BattleMapTag, HeroTag } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy } from "ramda";

@controller @component
export class ControllerTags {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/tags").dump(Tag, getTag)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<Tag[]> {
        return ok(await this.db.getRepository(Tag).find({
            relations: ["preset", "group"],
            where: pickBy(val => Boolean(val), {
                group: groupId ? { id: groupId } : undefined,
                preset: presetId ? { id: presetId } : undefined,
            }),
        }));
    }

    @route("POST", "/tags").dump(Tag, getTag)
    public async create(@body(createTag) tag: Tag, @context ctx?: Context): Promise<Tag> {
        // Access control.
        if (!tag.group && !tag.preset || tag.group && tag.preset) { return badRequest<Tag>(); }
        if (tag.group && !(await ctx.isMaster(tag.group.id))) { return forbidden<Tag>(); }
        if (tag.preset && !(await ctx.hasPreset(tag.preset.id))) { return forbidden<Tag>(); }
        // Check for duplicates.
        const duplicate = await this.db.getRepository(Tag).findOne({
            where: pickBy(val => Boolean(val), {
                name: tag.name,
                group: tag.group ? { id: tag.group.id } : undefined,
                preset: tag.preset ? { id: tag.preset.id } : undefined,
            }),
        });
        if (duplicate) { return badRequest<Tag>(); }
        // Create new tag.
        const newTag = await this.db.getRepository(Tag).save(tag);
        // Announce live update.
        if (tag.group) {
            this.live.publish(newTag.group.id, LiveAction.CREATE, ModelName.TAG, newTag.id);
        }
        // Return new tag.
        return ok(newTag);
    }

    @route("POST", "/tag/:id").dump(Tag, getTag)
    public async update(@param("id") id: string, @body(updateTag) change: Tag, @context ctx?: Context): Promise<Tag> {
        // Get tag.
        const tag = await this.db.getRepository(Tag).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!tag) { return notFound<Tag>(); }
        if (tag.group && !(await ctx.isMaster(tag.group.id))) { return forbidden<Tag>(); }
        if (tag.preset && !(await ctx.hasPreset(tag.preset.id))) { return forbidden<Tag>(); }
        // Perform update.
        Object.assign(tag, change);
        await this.db.getRepository(Tag).save(tag);
        // Announce live update.
        if (tag.group) {
            this.live.publish(tag.group.id, LiveAction.UPDATE, ModelName.TAG, tag.id);
        }
        // Return updated.
        return ok(tag);
    }

    @route("DELETE", "/tag/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Get template item.
        const tag = await this.db.getRepository(Tag).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!tag) { return notFound<void>(); }
        if (tag.group && !(await ctx.isMaster(tag.group.id))) { return forbidden<void>(); }
        if (tag.preset && !(await ctx.hasPreset(tag.preset.id))) { return forbidden<void>(); }
        // Delete hero relations.
        await this.db.getRepository(HeroTag).createQueryBuilder("hero_tag")
            .where(`hero_tag.tag_id = :id`, { id })
            .delete()
            .execute();
        // Delete battle map relations.
        await this.db.getRepository(BattleMapTag).createQueryBuilder("battle_map_tag")
            .where(`battle_map_tag.tag_id = :id`, { id })
            .delete()
            .execute();
        // Delete tag.
        await this.db.getRepository(Tag).createQueryBuilder("tag")
            .where(`tag.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (tag.group) {
            this.live.publish(tag.group.id, LiveAction.DELETE, ModelName.TAG, tag.id);
        }
        // Return.
        return ok();
    }

    @route("GET", "/tag/:id").dump(Tag, getTag)
    public async get(@param("id") id: string): Promise<Tag> {
        const tag = await this.db.getRepository(Tag).findOne({
            relations: ["group", "preset"],
            where: { id },
        });
        if (!tag) { return notFound<Tag>(); }
        return ok(populate(Tag, tag));
    }
}
