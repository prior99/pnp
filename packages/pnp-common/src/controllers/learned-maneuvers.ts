import {
    controller,
    route,
    body,
    ok,
    notFound,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { createLearnedManeuver, getLearnedManeuver } from "../scopes";
import { LearnedManeuver, Hero } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerLearnedManeuvers {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/hero/:id/learned-maneuvers").dump(LearnedManeuver, getLearnedManeuver)
    public async search(@param("id") id: string): Promise<LearnedManeuver[]> {
        const learnedManeuver = await this.db.getRepository(LearnedManeuver).find({
            where: { hero: { id } },
            relations: ["hero", "maneuver"],
        });
        return ok(learnedManeuver);
    }

    @route("POST", "/learned-maneuvers").dump(LearnedManeuver, getLearnedManeuver)
    public async create(
        @body(createLearnedManeuver) learnedManeuver: LearnedManeuver,
        @context ctx?: Context,
    ): Promise<LearnedManeuver> {
        // Fetch leanered maneuver.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id: learnedManeuver.hero.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!hero) { return notFound<LearnedManeuver>(); }
        if (hero.group && !(await ctx.hasGroup(hero.group.id))) { return forbidden<LearnedManeuver>(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden<LearnedManeuver>(); }
        // Update.
        await this.db.getRepository(LearnedManeuver).save(learnedManeuver);
        // Announce live update.
        if (hero.group) {
            this.live.publish(hero.group.id, LiveAction.CREATE, ModelName.LEARNED_MANEUVER, learnedManeuver.id);
        }
        // Return updated.
        return ok(learnedManeuver);
    }

    @route("DELETE", "/learned-maneuver/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch battle map.
        const learnedManeuver = await this.db.getRepository(LearnedManeuver).findOne({
            where: { id },
            relations: ["hero", "hero.group"],
        });
        // Access control.
        if (!learnedManeuver) { return notFound<void>(); }
        const { hero } = learnedManeuver;
        if (hero.group && !(await ctx.isMaster(hero.group.id))) { return forbidden<void>(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden<void>(); }
        // Delete.
        await this.db.getRepository(LearnedManeuver).createQueryBuilder("learned_maneuver")
            .where(`learned_maneuver.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (learnedManeuver.hero.group) {
            this.live.publish(
                learnedManeuver.hero.group.id,
                LiveAction.DELETE,
                ModelName.LEARNED_MANEUVER,
                learnedManeuver.id,
            );
        }
        // Return.
        return ok();
    }

    @route("GET", "/learned-maneuver/:id").dump(LearnedManeuver, getLearnedManeuver)
    public async get(@param("id") id: string): Promise<LearnedManeuver> {
        const learnedManeuver = await this.db.getRepository(LearnedManeuver).findOne({
            where: { id },
            relations: ["hero", "maneuver"],
        });
        if (!learnedManeuver) { return notFound<LearnedManeuver>(); }
        return ok(populate(LearnedManeuver, learnedManeuver));
    }
}
