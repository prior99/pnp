import {
    controller,
    route,
    body,
    ok,
    badRequest,
    param,
    context,
    forbidden,
    created,
    notFound,
    populate,
    query,
} from "hyrest";
import * as Path from "path";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { mkdirp, writeFile, unlink } from "fs-extra";
import { getBattleMap, createBattleMap, updateBattleMap } from "../scopes";
import { BattleMap, BattleMapUpload, Marker, Area, Monster, MarkerItem, BattleMapTag } from "../models";
import { Context } from "../context";
import { ConfigInterface } from "../config-interface";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy } from "ramda";

@controller @component
export class ControllerBattleMaps {
    @inject private live: LiveServer;
    @inject private db: Connection;
    @inject("config") private config: ConfigInterface;

    @route("GET", "/battle-maps").dump(BattleMap, getBattleMap)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<BattleMap[]> {
        return ok(await this.db.getRepository(BattleMap).find({
            relations: ["group", "preset"],
            where: {
                ...pickBy(val => Boolean(val), {
                    group: groupId ? { id: groupId } : undefined,
                    preset: presetId ? { id: presetId } : undefined,
                }),
                deleted: null,
            },
        }));
    }

    @route("POST", "/battle-maps").dump(BattleMap, getBattleMap)
    public async create(
        @body(createBattleMap) upload: BattleMapUpload,
        @context ctx?: Context,
    ): Promise<BattleMap> {
        // Access control.
        if (!upload.group && !upload.preset || upload.group && upload.preset) {
            return badRequest<BattleMap>("Either group or preset need to be supplied.");
        }
        if (upload.group && !(await ctx.isMaster(upload.group.id))) { return forbidden<BattleMap>(); }
        if (upload.preset && !(await ctx.hasPreset(upload.preset.id))) { return forbidden<BattleMap>(); }
        // Create directory if it doesn't exist yet.
        try {
            await mkdirp(this.config.battleMapsDir);
        } catch (e) {
            if (e.code !== "EEXIST") { throw e; }
        }
        // Create new battle map in database.
        const { id: newId } = await this.db.getRepository(BattleMap).save({
            group: upload.group,
            name: upload.name,
            preset: upload.preset,
        } as BattleMap);
        const battleMap = await this.get(newId);
        const data = Buffer.from(upload.content, "base64");
        const path = Path.join(this.config.battleMapsDir, battleMap.id);
        await writeFile(path, data);
        if (battleMap.group) {
            this.live.publish(battleMap.group.id, LiveAction.CREATE, ModelName.BATTLE_MAP, battleMap.id);
        }
        // Return new battle map.
        return created(battleMap);
    }

    @route("DELETE", "/battle-map/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Get battle map.
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!battleMap) { return notFound<void>(); }
        if (battleMap.group && !(await ctx.isMaster(battleMap.group.id))) { return forbidden<void>(); }
        if (battleMap.preset && !(await ctx.hasPreset(battleMap.preset.id))) { return forbidden<void>(); }
        // Announce live update.
        if (battleMap.group) {
            this.live.publish(
                battleMap.group.id,
                LiveAction.DELETE,
                ModelName.BATTLE_MAP,
                battleMap.id,
            );
            const markers = await this.db.getRepository(Marker).find({
                relations: ["battleMap"],
                where: {
                    battleMap: { id },
                },
            });
            markers.forEach(marker => {
                this.live.publish(battleMap.group.id, LiveAction.DELETE, ModelName.MARKER, marker.id);
            });
            const battleMapTags = await this.db.getRepository(BattleMapTag).find({
                relations: ["battleMap"],
                where: {
                    battleMap: { id },
                },
            });
            battleMapTags.forEach(battleMapTag => {
                this.live.publish(battleMap.group.id, LiveAction.DELETE, ModelName.BATTLE_MAP_TAG, battleMapTag.id);
            });
            const areas = await this.db.getRepository(Area).find({
                relations: ["battleMap"],
                where: {
                    battleMap: { id },
                },
            });
            areas.forEach(area => {
                this.live.publish(battleMap.group.id, LiveAction.DELETE, ModelName.AREA, area.id);
            });
            const markerItems = await this.db.getRepository(MarkerItem).createQueryBuilder("marker_item")
                .where(queryBuilder => {
                    const subQuery = queryBuilder.subQuery()
                        .select("id")
                        .from(Marker, "marker")
                        .where(`marker.battle_map_id = :id`)
                        .getQuery();
                    return `marker_item.marker_id IN ${subQuery}`;
                })
                .setParameter("id", id)
                .select()
                .execute();
            markerItems.forEach(markerItem => {
                this.live.publish(battleMap.group.id, LiveAction.DELETE, ModelName.MARKER_ITEM, markerItem.id);
            });
            const monsters = await this.db.getRepository(Monster).createQueryBuilder("monster")
                .where(queryBuilder => {
                    const subQuery = queryBuilder.subQuery()
                        .select("monster_id")
                        .from(Marker, "marker")
                        .where(`marker.battle_map_id = :id`)
                        .getQuery();
                    return `monster.id IN ${subQuery}`;
                })
                .setParameter("id", id)
                .select()
                .execute();
            monsters.forEach(monster => {
                this.live.publish(battleMap.group.id, LiveAction.DELETE, ModelName.MONSTER, monster.id);
            });
        }
        // Delete original references on battle maps.
        await this.db.getRepository(BattleMap).createQueryBuilder("battle_map")
            .update(BattleMap)
            .set({ original: null })
            .where({ original: { id } })
            .execute();
        // Delete all battle map tags.
        await this.db.getRepository(BattleMapTag).createQueryBuilder("battle_map_tag")
            .where(`battle_map_tag.battle_map_id = :id`, { id })
            .delete()
            .execute();
        // Delete all marker items.
        await this.db.getRepository(MarkerItem).createQueryBuilder("marker_item")
            .where(queryBuilder => {
                const subQuery = queryBuilder.subQuery()
                    .select("id")
                    .from(Marker, "marker")
                    .where(`marker.battle_map_id = :id`)
                    .getQuery();
                return `marker_item.marker_id IN ${subQuery}`;
            })
            .setParameter("id", id)
            .delete()
            .execute();
        // Delete all markers.
        await this.db.getRepository(Marker).createQueryBuilder("marker")
            .where(`marker.battle_map_id = :id`, { id })
            .delete()
            .execute();
        // Delete all monsters.
        await this.db.getRepository(Monster).createQueryBuilder("monster")
            .where(queryBuilder => {
                const subQuery = queryBuilder.subQuery()
                    .select("monster_id")
                    .from(Marker, "marker")
                    .where(`marker.battle_map_id = :id`)
                    .getQuery();
                return `monster.id IN ${subQuery}`;
            })
            .setParameter("id", id)
            .delete()
            .execute();
        // Delete all areas.
        await this.db.getRepository(Area).createQueryBuilder("area")
            .where(`area.battle_map_id = :id`, { id })
            .delete()
            .execute();
        // Delete.
        await this.db.getRepository(BattleMap).createQueryBuilder("battle_map")
            .where(`battle_map.id = :id`, { id })
            .delete()
            .execute();
        // Delete file.
        const path = Path.join(this.config.battleMapsDir, battleMap.id);
        await unlink(path);
        return ok();
    }

    @route("POST", "/battle-map/:id/archive")
    public async archive(
        @param("id") id: string,
        @context ctx?: Context,
    ): Promise<undefined> {
        // Fetch hero.
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!battleMap) { return notFound(); }
        if (battleMap.group && !(await ctx.isMaster(battleMap.group.id))) { return forbidden(); }
        if (battleMap.preset && !(await ctx.hasPreset(battleMap.preset.id))) { return forbidden(); }
        await this.db.getRepository(BattleMap).save({
            ...battleMap,
            deleted: new Date(),
        });
        // Announce live update.
        if (battleMap.group) {
            this.live.publish(battleMap.group.id, LiveAction.DELETE, ModelName.BATTLE_MAP, battleMap.id);
        }
        return ok();
    }

    @route("POST", "/battle-map/:id").dump(BattleMap, getBattleMap)
    public async update(
        @param("id") id: string,
        @body(updateBattleMap) change: BattleMap,
        @context ctx?: Context,
    ): Promise<BattleMap> {
        // Fetch battle map.
        const old = await this.db.getRepository(BattleMap).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!old) { return notFound<BattleMap>(); }
        if (old.group && !(await ctx.isMaster(old.group.id))) { return forbidden<BattleMap>(); }
        if (old.preset && !(await ctx.hasPreset(old.preset.id))) { return forbidden<BattleMap>(); }
        // Update battle map.
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        Object.assign(battleMap, change);
        // Save updated battle map.
        const newBattleMap = await this.db.getRepository(BattleMap).save(battleMap);
        // Announce live update.
        if (newBattleMap.group) {
            this.live.publish(newBattleMap.group.id, LiveAction.UPDATE, ModelName.BATTLE_MAP, newBattleMap.id);
        }
        // Return updated.
        return ok(populate(BattleMap, newBattleMap));
    }

    @route("GET", "/battle-map/:id").dump(BattleMap, getBattleMap)
    public async get(@param("id") id: string): Promise<BattleMap> {
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            relations: ["group", "preset"],
            where: { id },
        });
        if (!battleMap) { return notFound<BattleMap>(); }
        return ok(populate(BattleMap, battleMap));
    }
}
