import {
    controller,
    route,
    body,
    ok,
    param,
    context,
    forbidden,
    created,
    notFound,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getArea, createArea } from "../scopes";
import { Area, BattleMap } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerAreas {
    @inject private db: Connection;
    @inject private live: LiveServer;

    @route("GET", "/battle-map/:id/areas").dump(Area, getArea)
    public async search(@param("id") id: string): Promise<Area[]> {
        return ok(await this.db.getRepository(Area).find({
            where: {
                battleMap: { id },
            },
            relations: ["battleMap"],
        }));
    }

    @route("POST", "/areas").dump(Area, getArea)
    public async create(
        @body(createArea) area: Area,
        @context ctx?: Context,
    ): Promise<Area> {
        // Fetch battle map.
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            where: { id: area.battleMap.id },
            relations: ["group"],
        });
        // Access control.
        if (!battleMap) { return notFound<Area>(); }
        if (battleMap.group && !(await ctx.isMaster(battleMap.group.id))) { return forbidden<Area>(); }
        if (battleMap.preset && !(await ctx.hasPreset(battleMap.preset.id))) { return forbidden<Area>(); }
        // Update.
        await this.db.getRepository(Area).save(area);
        // Announce live update.
        if (battleMap.group) {
            this.live.publish(battleMap.group.id, LiveAction.CREATE, ModelName.AREA, area.id);
        }
        // Return updated.
        return created(area);
    }

    @route("GET", "/area/:id").dump(Area, getArea)
    public async get(@param("id") id: string): Promise<Area> {
        const area = await this.db.getRepository(Area).findOne({
            where: { id },
            relations: ["battleMap"],
        });
        if (!area) { return notFound<Area>(); }
        return ok(populate(Area, area));
    }
}
