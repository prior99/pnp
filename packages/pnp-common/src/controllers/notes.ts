import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { updateNote, createNote, getNote } from "../scopes";
import { Note, Hero } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerNotes {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/hero/:id/notes").dump(Note, getNote)
    public async search(@param("id") id: string): Promise<Note[]> {
        const notes = await this.db.getRepository(Note).find({
            where: { hero: { id } },
            relations: ["hero", "hero.group"],
        });
        return ok(notes);
    }

    @route("POST", "/notes").dump(Note, getNote)
    public async create(@body(createNote) note: Note, @context ctx?: Context): Promise<Note> {
        // Get hero.
        const hero = await this.db.getRepository(Hero).findOne({
            where: { id: note.hero.id },
            relations: ["group"],
        });
        // Access control.
        if (!hero) { return badRequest<Note>("No such hero."); }
        if (hero.group && !(await ctx.hasGroup(hero.group.id))) { return forbidden<Note>(); }
        if (hero.preset && !(await ctx.hasPreset(hero.preset.id))) { return forbidden<Note>(); }
        // Create new note.
        const newNote = await this.db.getRepository(Note).save(note);
        // Announce live update.
        if (hero.group) {
            this.live.publish(hero.group.id, LiveAction.CREATE, ModelName.NOTE, newNote.id);
        }
        // Return new note.
        return ok(newNote);
    }

    @route("DELETE", "/note/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch battle map.
        const note = await this.db.getRepository(Note).findOne({
            where: { id },
            relations: ["hero", "hero.group"],
        });
        // Access control.
        if (!await this.db.getRepository(Note).findOne({ id })) { return notFound<void>(); }
        if (note.hero.group && !(await ctx.hasGroup(note.hero.group.id))) { return forbidden<void>(); }
        if (note.hero.preset && !(await ctx.hasPreset(note.hero.preset.id))) { return forbidden<void>(); }
        // Delete note.
        await this.db.getRepository(Note).createQueryBuilder("note")
            .where(`note.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (note.hero.group) {
            this.live.publish(note.hero.group.id, LiveAction.DELETE, ModelName.NOTE, note.id);
        }
        // Return updated.
        return ok();
    }

    @route("POST", "/note/:id").dump(Note, getNote)
    public async update(
        @param("id") id: string,
        @body(updateNote) change: Note,
        @context ctx?: Context,
    ): Promise<Note> {
        // Fetch note.
        const note = await this.db.getRepository(Note).findOne({
            where: { id },
            relations: ["hero", "hero.group"],
        });
        // Access control.
        if (!note) { return notFound<Note>(); }
        if (note.hero.group && !(await ctx.hasGroup(note.hero.group.id))) { return forbidden<Note>(); }
        if (note.hero.preset && !(await ctx.hasPreset(note.hero.preset.id))) { return forbidden<Note>(); }
        // Perform change.
        Object.assign(note, change);
        await this.db.getRepository(Note).save(note);
        // Announce live update.
        if (note.hero.group) {
            this.live.publish(note.hero.group.id, LiveAction.UPDATE, ModelName.NOTE, note.id);
        }
        // Return updated note.
        return ok(note);
    }

    @route("GET", "/note/:id").dump(Note, getNote)
    public async get(@param("id") id: string): Promise<Note> {
        const note = await this.db.getRepository(Note).findOne({
            where: { id },
            relations: ["hero", "hero.group"],
        });
        if (!note) { return notFound<Note>(); }
        return ok(populate(Note, note));
    }
}
