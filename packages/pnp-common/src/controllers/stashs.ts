import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
    query,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { createStash, getStash, updateStash } from "../scopes";
import { Stash, TemplateMonster, StashItem } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy } from "ramda";

@controller @component
export class ControllerStashs {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/stashs").dump(Stash, getStash)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<Stash[]> {
        return ok(await this.db.getRepository(Stash).find({
            where: pickBy(val => Boolean(val), {
                group: groupId ? { id: groupId } : undefined,
                preset: presetId ? { id: presetId } : undefined,
            }),
            relations: ["preset", "group"],
        }));
    }

    @route("POST", "/stashs").dump(Stash, getStash)
    public async create(@body(createStash) stash: Stash, @context ctx?: Context): Promise<Stash> {
        // Access control.
        if (!stash.group && !stash.preset || stash.group && stash.preset) {
            return badRequest<Stash>("Either group or preset need to be supplied.");
        }
        if (stash.group && !(await ctx.isMaster(stash.group.id))) { return forbidden<Stash>(); }
        if (stash.preset && !(await ctx.hasPreset(stash.preset.id))) { return forbidden<Stash>(); }
        // Create new journal entry.
        await this.db.getRepository(Stash).save(stash);
        // Announce live update.
        if (stash.group) {
            this.live.publish(stash.group.id, LiveAction.CREATE, ModelName.STASH, stash.id);
        }
        // Return new stash.
        return ok(this.get(stash.id));
    }

    @route("DELETE", "/stash/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch stash.
        const stash = await this.db.getRepository(Stash).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!stash) { return notFound<void>(); }
        if (stash.group && !(await ctx.hasGroup(stash.group.id))) { return forbidden<void>(); }
        if (stash.preset && !(await ctx.hasPreset(stash.preset.id))) { return forbidden<void>(); }
        // Remove references on template monsters.
        await this.db.getRepository(TemplateMonster).createQueryBuilder("template_monster")
            .where(`template_monster.stash_id = :id`, { id })
            .update({ stash: null })
            .execute();
        // Delete stash items.
        await this.db.getRepository(StashItem).createQueryBuilder("stash_item")
            .where(`stash_item.stash_id = :id`, { id })
            .delete()
            .execute();
        // Delete stash.
        await this.db.getRepository(Stash).createQueryBuilder("stash")
            .where(`stash.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (stash.group) {
            this.live.publish(stash.group.id, LiveAction.DELETE, ModelName.STASH, stash.id);
        }
        // Return.
        return ok();
    }

    @route("POST", "/stash/:id").dump(Stash, getStash)
    public async update(
        @param("id") id: string,
        @body(updateStash) change: Stash,
        @context ctx?: Context,
    ): Promise<Stash> {
        // Fetch stash.
        const stash = await this.db.getRepository(Stash).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!stash) { return notFound<Stash>(); }
        if (stash.group && !(await ctx.hasGroup(stash.group.id))) { return forbidden<Stash>(); }
        if (stash.preset && !(await ctx.hasPreset(stash.preset.id))) { return forbidden<Stash>(); }
        // Update.
        Object.assign(stash, change);
        await this.db.getRepository(Stash).save(stash);
        // Announce live update.
        if (stash.group) {
            this.live.publish(stash.group.id, LiveAction.UPDATE, ModelName.STASH, stash.id);
        }
        // Return updated.
        return ok(stash);
    }

    @route("GET", "/stash/:id").dump(Stash, getStash)
    public async get(@param("id") id: string): Promise<Stash> {
        const stash = await this.db.getRepository(Stash).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        if (!stash) { return notFound<Stash>(); }
        return ok(populate(Stash, stash));
    }
}
