import {
    controller,
    route,
    created,
    body,
    unauthorized,
    noauth,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { owner, loginPreset } from "../scopes";
import { PresetToken, Preset } from "../models";

@controller @component
export class ControllerPresetTokens {
    @inject private db: Connection;

    @route("POST", "/preset-token").dump(PresetToken, owner) @noauth
    public async create(@body(loginPreset) credentials: PresetToken): Promise<PresetToken> {
        const preset = await this.db.getRepository(Preset).findOne(credentials.preset);
        if (!preset) { return unauthorized<PresetToken>(); }
        const newPresetToken = await this.db.getRepository(PresetToken).save({ preset });
        return created(populate(PresetToken, newPresetToken));
    }
}
