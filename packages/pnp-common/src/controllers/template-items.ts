import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
    query,
} from "hyrest";
import * as Path from "path";
import { warn } from "winston";
import { mkdirp, writeFile, unlink } from "fs-extra";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getTemplateItem, updateTemplateItem, createTemplateItem, upload } from "../scopes";
import { TemplateItem, Item, MarkerItem, StashItem, ImageUpload } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";
import { pickBy } from "ramda";
import { ConfigInterface } from "../config-interface";

@controller @component
export class ControllerTemplateItems {
    @inject private live: LiveServer;
    @inject private db: Connection;
    @inject("config") private config: ConfigInterface;

    @route("GET", "/template-items").dump(TemplateItem, getTemplateItem)
    public async search(
        @query("groupId") groupId: string | undefined,
        @query("presetId") presetId?: string,
    ): Promise<TemplateItem[]> {
        return ok(await this.db.getRepository(TemplateItem).find({
            relations: ["preset", "group"],
            where: pickBy(val => Boolean(val), {
                group: groupId ? { id: groupId } : undefined,
                preset: presetId ? { id: presetId } : undefined,
            }),
        }));
    }

    @route("POST", "/template-items").dump(TemplateItem, getTemplateItem)
    public async create(
        @body(createTemplateItem) templateItem: TemplateItem,
        @context ctx?: Context,
    ): Promise<TemplateItem> {
        // Access control.
        if (!templateItem.group && !templateItem.preset || templateItem.group && templateItem.preset) {
            return badRequest<TemplateItem>("Either group or preset need to be supplied.");
        }
        if (templateItem.group && !(await ctx.isMaster(templateItem.group.id))) { return forbidden<TemplateItem>(); }
        if (templateItem.preset && !(await ctx.hasPreset(templateItem.preset.id))) { return forbidden<TemplateItem>(); }
        // Create new template item.
        const { id: newId } = await this.db.getRepository(TemplateItem).save({
            ...templateItem,
            suggestedStaminaCost: templateItem.suggestedStaminaCost || null,
        });
        const newTemplateItem = await this.get(newId);
        // Announce live update.
        if (templateItem.group) {
            this.live.publish(templateItem.group.id, LiveAction.CREATE, ModelName.TEMPLATE_ITEM, newTemplateItem.id);
        }
        // Return new template item.
        return ok(newTemplateItem);
    }

    @route("POST", "/template-item/:id/image")
    public async uploadImage(
        @param("id") id: string,
        @body(upload) imageUpload: ImageUpload,
        @context ctx?: Context,
    ): Promise<void> {
        // Check upload.
        if ((!imageUpload.content && !imageUpload.url) || (imageUpload.content && imageUpload.url)) {
            return badRequest<void>("Specify either content or url.");
        }
        // Get template item.
        const templateItem = await this.db.getRepository(TemplateItem).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (templateItem.group && !(await ctx.hasGroup(templateItem.group.id))) { return forbidden<void>(); }
        if (templateItem.preset && !(await ctx.hasPreset(templateItem.preset.id))) { return forbidden<void>(); }
        // Create directory if it doesn't exist yet.
        try {
            await mkdirp(this.config.imagesDir);
        } catch (e) {
            if (e.code !== "EEXIST") { throw e; }
        }
        // Write file.
        try {
            const data = await imageUpload.loadData();
            const path = Path.join(this.config.imagesDir, templateItem.id);
            await writeFile(path, data);
            if (templateItem.group) {
                this.live.publish(templateItem.group.id, LiveAction.CREATE, ModelName.TEMPLATE_ITEM, templateItem.id);
            }
            // Done.
            return ok();
        } catch (err) {
            warn(`Error uploading image:`, err);
            return badRequest<void>("Could not process upload.");
        }
    }

    @route("POST", "/template-item/:id").dump(TemplateItem, getTemplateItem)
    public async update(
        @param("id") id: string,
        @body(updateTemplateItem) change: TemplateItem,
        @context ctx?: Context,
    ): Promise<TemplateItem> {
        // Get template item.
        const templateItem = await this.db.getRepository(TemplateItem).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!templateItem) { return notFound<TemplateItem>(); }
        if (templateItem.group && !(await ctx.isMaster(templateItem.group.id))) { return forbidden<TemplateItem>(); }
        if (templateItem.preset && !(await ctx.hasPreset(templateItem.preset.id))) { return forbidden<TemplateItem>(); }
        // Perform update.
        Object.assign(templateItem, change);
        await this.db.getRepository(TemplateItem).save(templateItem);
        // Announce live update.
        if (templateItem.group) {
            this.live.publish(templateItem.group.id, LiveAction.UPDATE, ModelName.TEMPLATE_ITEM, templateItem.id);
        }
        // Return updated.
        return ok(templateItem);
    }

    @route("DELETE", "/template-item/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Get template item.
        const templateItem = await this.db.getRepository(TemplateItem).findOne({
            where: { id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!templateItem) { return notFound<void>(); }
        if (templateItem.group && !(await ctx.isMaster(templateItem.group.id))) { return forbidden<void>(); }
        if (templateItem.preset && !(await ctx.hasPreset(templateItem.preset.id))) { return forbidden<void>(); }
        // Announce live update.
        if (templateItem.group) {
            this.live.publish(templateItem.group.id, LiveAction.DELETE, ModelName.TEMPLATE_ITEM, templateItem.id);
            const items = await this.db.getRepository(Item).find({
                relations: ["templateItem"],
                where: {
                    templateItem: { id },
                },
            });
            items.forEach(item => {
                this.live.publish(templateItem.group.id, LiveAction.DELETE, ModelName.ITEM, item.id);
            });
            const stashItems = await this.db.getRepository(StashItem).find({
                relations: ["templateItem"],
                where: {
                    templateItem: { id },
                },
            });
            stashItems.forEach(stashItem => {
                this.live.publish(templateItem.group.id, LiveAction.DELETE, ModelName.ITEM, stashItem.id);
            });
            const markerItems = await this.db.getRepository(MarkerItem).find({
                relations: ["templateItem"],
                where: {
                    templateItem: { id },
                },
            });
            markerItems.forEach(markerItem => {
                this.live.publish(templateItem.group.id, LiveAction.DELETE, ModelName.ITEM, markerItem.id);
            });
        }
        // Delete original references on template item.
        await this.db.getRepository(TemplateItem).createQueryBuilder("template_item")
            .update(TemplateItem)
            .set({ original: null })
            .where({ original: { id }})
            .execute();
        // Delete items.
        await this.db.getRepository(Item).createQueryBuilder("item")
            .where(`item.template_item_id = :id`, { id })
            .delete()
            .execute();
        // Delete marker items.
        await this.db.getRepository(MarkerItem).createQueryBuilder("marker_item")
            .where(`marker_item.template_item_id = :id`, { id })
            .delete()
            .execute();
        // Delete stash items.
        await this.db.getRepository(StashItem).createQueryBuilder("stash_item")
            .where(`stash_item.template_item_id = :id`, { id })
            .delete()
            .execute();
        // Delete.
        await this.db.getRepository(TemplateItem).createQueryBuilder("template_item")
            .where(`template_item.id = :id`, { id })
            .delete()
            .execute();
        // Delete image.
        try {
            await unlink(Path.join(this.config.imagesDir, templateItem.id));
        } catch (err) {
            if (err.code !== "ENOENT") { throw err; }
        }
        // Return.
        return ok();
    }

    @route("GET", "/template-item/:id").dump(TemplateItem, getTemplateItem)
    public async get(@param("id") id: string): Promise<TemplateItem> {
        const templateItem = await this.db.getRepository(TemplateItem).findOne({
            relations: ["group", "preset"],
            where: { id },
        });
        if (!templateItem) { return notFound<TemplateItem>(); }
        return ok(populate(TemplateItem, templateItem));
    }
}
