import {
    body,
    controller,
    route,
    param,
    is,
    uuid,
    ok,
    notFound,
    created,
    noauth,
} from "hyrest";
import { component, inject } from "tsdi";
import { Connection } from "typeorm";
import { verbose } from "winston";
import { Preset } from "../models";
import { owner, getPreset, createPreset } from "../scopes";

@controller @component
export class ControllerPresets {
    @inject private db: Connection;

    @route("GET", "/presets").dump(Preset, getPreset) @noauth
    public async list(): Promise<Preset[]> {
        return ok(await this.db.getRepository(Preset).find());
    }

    @route("GET", "/preset/:id").dump(Preset, getPreset)
    public async get(@param("id") @is().validate(uuid) id: string): Promise<Preset> {
        const preset = await this.db.getRepository(Preset).findOne({ id });
        if (!preset) { return notFound<Preset>(`No preset with id "${id}"`); }
        return ok(preset);
    }

    @route("POST", "/preset").dump(Preset, owner) @noauth
    public async create(@body(createPreset) preset: Preset): Promise<Preset> {
        const { id, name } = await this.db.getRepository(Preset).save(preset);
        verbose(`New preset ${name} with id ${id} created`);
        return created(await this.get(id));
    }
}
