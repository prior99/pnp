export * from "./groups";
export * from "./validation";
export * from "./tokens";
export * from "./heroes";
export * from "./items";
export * from "./template-items";
export * from "./notes";
export * from "./battle-maps";
export * from "./markers";
export * from "./areas";
export * from "./maneuvers";
export * from "./learned-maneuvers";
export * from "./preset-tokens";
export * from "./presets";
export * from "./template-monsters";
export * from "./monster-maneuvers";
export * from "./monsters";
export * from "./tags";
export * from "./hero-tags";
export * from "./battle-map-tags";
export * from "./journal-entries";
export * from "./stashs";
export * from "./stash-items";
export * from "./marker-items";

import { ControllerGroups } from "./groups";
import { ControllerValidation } from "./validation";
import { ControllerTokens } from "./tokens";
import { ControllerHeroes } from "./heroes";
import { ControllerItems } from "./items";
import { ControllerTemplateItems } from "./template-items";
import { ControllerNotes } from "./notes";
import { ControllerBattleMaps } from "./battle-maps";
import { ControllerMarkers } from "./markers";
import { ControllerAreas } from "./areas";
import { ControllerManeuvers } from "./maneuvers";
import { ControllerLearnedManeuvers } from "./learned-maneuvers";
import { ControllerPresetTokens } from "./preset-tokens";
import { ControllerPresets } from "./presets";
import { ControllerTemplateMonsters } from "./template-monsters";
import { ControllerMonsterManeuvers } from "./monster-maneuvers";
import { ControllerMonsters } from "./monsters";
import { ControllerTags } from "./tags";
import { ControllerHeroTags } from "./hero-tags";
import { ControllerBattleMapTags } from "./battle-map-tags";
import { ControllerJournalEntries } from "./journal-entries";
import { ControllerMarkerItems } from "./marker-items";
import { ControllerStashs } from "./stashs";
import { ControllerStashItems } from "./stash-items";

export const controllers: any[] = [
    ControllerGroups,
    ControllerValidation,
    ControllerTokens,
    ControllerHeroes,
    ControllerItems,
    ControllerTemplateItems,
    ControllerNotes,
    ControllerBattleMaps,
    ControllerMarkers,
    ControllerAreas,
    ControllerManeuvers,
    ControllerLearnedManeuvers,
    ControllerPresets,
    ControllerPresetTokens,
    ControllerTemplateMonsters,
    ControllerMonsterManeuvers,
    ControllerMonsters,
    ControllerTags,
    ControllerHeroTags,
    ControllerBattleMapTags,
    ControllerJournalEntries,
    ControllerStashs,
    ControllerStashItems,
    ControllerMarkerItems,
];
