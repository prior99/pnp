import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { updateMonster, createMonster, getMonster } from "../scopes";
import { Monster, TemplateMonster } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerMonsters {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/template-monster/:id/monsters").dump(Monster, getMonster)
    public async search(@param("id") id: string): Promise<Monster[]> {
        return ok(await this.db.getRepository(Monster).find({
            where: { templateMonster: { id } },
            relations: ["templateMonster"],
        }));
    }

    @route("GET", "/monster/:id").dump(Monster, getMonster)
    public async get(@param("id") id: string): Promise<Monster> {
        // Fetch monster.
        const monster = await this.db.getRepository(Monster).findOne({
            where: { id },
            relations: ["templateMonster"],
        });
        // Access control.
        if (!monster) { return notFound<Monster>(); }
        // Return monster.
        return ok(populate(Monster, monster));
    }

    @route("POST", "/monsters").dump(Monster, getMonster)
    public async create(@body(createMonster) monster: Monster, @context ctx?: Context): Promise<Monster> {
        // Fetch template monster.
        const templateMonster = await this.db.getRepository(TemplateMonster).findOne({
            where: { id: monster.templateMonster.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!templateMonster) { return badRequest<Monster>(); }
        if (monster.templateMonster.group && !(await ctx.isMaster(monster.templateMonster.group.id))) {
            return forbidden<Monster>();
        }
        if (monster.templateMonster.preset && !(await ctx.hasPreset(monster.templateMonster.preset.id))) {
            return forbidden<Monster>();
        }
        // Update.
        const newMonster = await this.db.getRepository(Monster).save(monster);
        // Announce live update.
        if (templateMonster.group) {
            this.live.publish(templateMonster.group.id, LiveAction.CREATE, ModelName.MONSTER, newMonster.id);
        }
        // Return updated.
        return ok(newMonster);
    }

    @route("DELETE", "/monster/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch monster.
        const monster = await this.db.getRepository(Monster).findOne({
            where: { id },
            relations: ["templateMonster", "templateMonster.group"],
        });
        // Access control.
        if (!monster) { return notFound<void>(); }
        if (monster.templateMonster.group && !(await ctx.isMaster(monster.templateMonster.group.id))) {
            return forbidden<void>();
        }
        if (monster.templateMonster.preset && !(await ctx.hasPreset(monster.templateMonster.preset.id))) {
            return forbidden<void>();
        }
        // Delete original references.
        await this.db.getRepository(Monster).createQueryBuilder("monster")
            .update(Monster)
            .set({
                original: null,
            })
            .where({ original: { id }})
            .execute();
        // Delete monster.
        await this.db.getRepository(Monster).createQueryBuilder("monster")
            .where(`monster.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (monster.templateMonster.group) {
            this.live.publish(monster.templateMonster.group.id, LiveAction.DELETE, ModelName.MONSTER, monster.id);
        }
        // Return.
        return ok();
    }

    @route("POST", "/monster/:id").dump(Monster, getMonster)
    public async update(
        @param("id") id: string,
        @body(updateMonster) change: Monster,
        @context ctx?: Context,
    ): Promise<Monster> {
        // Fetch monster.
        const monster = await this.db.getRepository(Monster).findOne({
            where: { id },
            relations: ["templateMonster", "templateMonster.group", "templateMonster.preset"],
        });
        // Access control.
        if (!monster) { return notFound<Monster>(); }
        if (monster.templateMonster.group && !(await ctx.hasGroup(monster.templateMonster.group.id))) {
            return forbidden<Monster>();
        }
        if (monster.templateMonster.preset && !(await ctx.hasPreset(monster.templateMonster.preset.id))) {
            return forbidden<Monster>();
        }
        // Update.
        Object.assign(monster, change);
        await this.db.getRepository(Monster).save(monster);
        // Announce live update.
        if (monster.templateMonster.group) {
            this.live.publish(monster.templateMonster.group.id, LiveAction.UPDATE, ModelName.MONSTER, monster.id);
        }
        // Return updated.
        return ok(monster);
    }
}
