import { controller, route, ok, body, noauth } from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { Group, Token, PresetToken, Preset } from "../models";
import { AuthToken, isAuthToken } from "../context";

interface ValidationResult {
    error?: string;
}

@controller @component
export class ControllerValidation {
    @inject private db: Connection;

    @route("POST", "/validate/group/name") @noauth
    public async nameAvailable(@body() name: string): Promise<ValidationResult> {
        const group = await this.db.getRepository(Group).findOne({ name });
        if (group) {
            return ok({ error: "Name already taken." });
        }
        return ok({});
    }

    @route("POST", "/validate/preset/name") @noauth
    public async presetNameAvailable(@body() name: string): Promise<ValidationResult> {
        const preset = await this.db.getRepository(Preset).findOne({ name });
        if (preset) {
            return ok({ error: "Name already taken." });
        }
        return ok({});
    }

    @route("POST", "/validate/token") @noauth
    public async tokenValid(@body() authToken: AuthToken): Promise<ValidationResult> {
        // Check that the token isn't empty.
        if (!isAuthToken(authToken)) { return ok({ error: "Malformed auth token." }); }
        const noGroupTokens = Boolean(!authToken.groupTokens || authToken.groupTokens.length === 0);
        const noPresetTokens = Boolean(!authToken.presetTokens || authToken.presetTokens.length === 0);
        if (noGroupTokens && noPresetTokens) { return ok({ error: "Empty auth token. "}); }
        // Check that all groups are fine.
        if (!noGroupTokens) {
            const tokenPromises = authToken.groupTokens.map(id => this.db.getRepository(Token).findOne({
                where: { id },
                relations: ["group"],
            }));
            const groupTokens = await Promise.all(tokenPromises);
            const valid = groupTokens.every(groupToken => Boolean(groupToken) && !groupToken.deleted);
            if (!valid) { return ok({ error: "Invalid group credentials." }); }
        }
        // Check that all preset tokens are fine.
        if (!noPresetTokens) {
            const tokenPromises = authToken.presetTokens.map(id => this.db.getRepository(PresetToken).findOne({
                where: { id },
                relations: ["preset"],
            }));
            const presetTokens = await Promise.all(tokenPromises);
            const valid = presetTokens.every(presetToken => Boolean(presetToken) && !presetToken.deleted);
            if (!valid) { return ok({ error: "Invalid preset credentials." }); }
        }
        // All was valid.
        return ok({});
    }
}
