import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    created,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { getMarker, updateMarker, createMarker } from "../scopes";
import { Marker, BattleMap, MarkerItem } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerMarkers {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/battle-map/:id/markers").dump(Marker, getMarker)
    public async search(@param("id") id: string): Promise<Marker[]> {
        return ok(await this.db.getRepository(Marker).find({
            where: {
                battleMap: { id },
            },
            relations: ["battleMap", "hero", "monster"],
        }));
    }

    @route("POST", "/markers").dump(Marker, getMarker)
    public async create(
        @body(createMarker) marker: Marker,
        @context ctx?: Context,
    ): Promise<Marker> {
        // Fetch battle map.
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            where: { id: marker.battleMap.id },
            relations: ["group"],
        });

        // Access control.
        const isCustomMarker = !marker.hero && !marker.monster;

        if (!battleMap) { return badRequest<Marker>(); }
        if (!isCustomMarker && battleMap.group && !(await ctx.isMaster(battleMap.group.id))) {
            return forbidden<Marker>();
        }
        if (battleMap.preset && !(await ctx.hasPreset(battleMap.preset.id))) { return forbidden<Marker>(); }

        // Perform update.
        await this.db.getRepository(Marker).save(marker);

        // Announce live update.
        if (battleMap.group) {
            this.live.publish(battleMap.group.id, LiveAction.CREATE, ModelName.MARKER, marker.id);
        }

        // Return updated.
        return created(marker);
    }

    @route("DELETE", "/marker/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Get marker.
        const marker = await this.db.getRepository(Marker).findOne({
            where: { id },
            relations: ["battleMap", "battleMap.group"],
        });
        // Access control.
        if (!await this.db.getRepository(Marker).findOne({ id })) { return notFound<void>(); }
        if (marker.battleMap.group && !(await ctx.isMaster(marker.battleMap.group.id))) { return forbidden<void>(); }
        if (marker.battleMap.preset && !(await ctx.hasPreset(marker.battleMap.preset.id))) { return forbidden<void>(); }
        // Delete marker items.
        await this.db.getRepository(MarkerItem).createQueryBuilder("marker_item")
            .where(`marker_item.marker_id = :id`, { id })
            .delete()
            .execute();
        // Delete marker.
        await this.db.getRepository(Marker).createQueryBuilder("marker")
            .where(`marker.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (marker.battleMap.group) {
            this.live.publish(marker.battleMap.group.id, LiveAction.DELETE, ModelName.MARKER, marker.id);
        }
        // Return.
        return ok();
    }

    @route("POST", "/marker/:id").dump(Marker, getMarker)
    public async update(
        @param("id") id: string,
        @body(updateMarker) change: Marker,
        @context ctx?: Context,
    ): Promise<Marker> {
        // Get old marker.
        const oldMarker = await this.db.getRepository(Marker).findOne({
            where: { id },
            relations: ["battleMap", "hero", "monster", "battleMap.group"],
        });
        // Access control.
        if (!oldMarker) { return notFound<Marker>(); }
        // Perform update.
        const updated = await this.db.getRepository(Marker).save(Object.assign(oldMarker, change));
        // Announce live update.
        if (oldMarker.battleMap.group) {
            this.live.publish(oldMarker.battleMap.group.id, LiveAction.UPDATE, ModelName.MARKER, oldMarker.id);
        }
        // Return updated.
        return created(populate(Marker, updated));
    }

    @route("GET", "/marker/:id").dump(Marker, getMarker)
    public async get(@param("id") id: string): Promise<Marker> {
        const marker = await this.db.getRepository(Marker).findOne({
            where: { id },
            relations: ["battleMap", "hero", "monster"],
        });
        if (!marker) { return notFound<Marker>(); }
        return ok(populate(Marker, marker));
    }
}
