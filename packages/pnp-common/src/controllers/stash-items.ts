import {
    controller,
    route,
    body,
    ok,
    notFound,
    badRequest,
    param,
    context,
    forbidden,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { updateStashItem, createStashItem, getStashItem } from "../scopes";
import { Stash, StashItem } from "../models";
import { Context } from "../context";
import { LiveServer, LiveAction, ModelName } from "../live";

@controller @component
export class ControllerStashItems {
    @inject private live: LiveServer;
    @inject private db: Connection;

    @route("GET", "/stash/:id/items").dump(StashItem, getStashItem)
    public async search(@param("id") id: string): Promise<StashItem[]> {
        return ok(await this.db.getRepository(StashItem).find({
            where: { stash: { id } },
            relations: ["stash", "templateItem"],
        }));
    }

    @route("POST", "/stash-items").dump(StashItem, getStashItem)
    public async create(@body(createStashItem) stashItem: StashItem, @context ctx?: Context): Promise<StashItem> {
        // Fetch stash.
        const stash = await this.db.getRepository(Stash).findOne({
            where: { id: stashItem.stash.id },
            relations: ["group", "preset"],
        });
        // Access control.
        if (!stash) { return badRequest<StashItem>(); }
        if (stashItem.stash.group && !(await ctx.isMaster(stashItem.stash.group.id))) { return forbidden<StashItem>(); }
        if (stashItem.stash.preset && !(await ctx.hasPreset(stashItem.stash.preset.id))) {
            return forbidden<StashItem>();
        }
        // Update.
        const newStashItem = await this.db.getRepository(StashItem).save(stashItem);
        // Announce live update.
        if (stash.group) {
            this.live.publish(stash.group.id, LiveAction.CREATE, ModelName.STASH_ITEM, newStashItem.id);
        }
        // Return updated.
        return ok(newStashItem);
    }

    @route("DELETE", "/stash-item/:id")
    public async delete(@param("id") id: string, @context ctx?: Context): Promise<void> {
        // Fetch stash item.
        const stashItem = await this.db.getRepository(StashItem).findOne({
            where: { id },
            relations: ["stash", "stash.group"],
        });
        // Access control.
        if (!stashItem) { return notFound<void>(); }
        if (stashItem.stash.group && !(await ctx.hasGroup(stashItem.stash.group.id))) { return forbidden<void>(); }
        if (stashItem.stash.preset && !(await ctx.hasPreset(stashItem.stash.preset.id))) { return forbidden<void>(); }
        // Delete item.
        await this.db.getRepository(StashItem).createQueryBuilder("stash_item")
            .where(`stash_item.id = :id`, { id })
            .delete()
            .execute();
        // Announce live update.
        if (stashItem.stash.group) {
            this.live.publish(stashItem.stash.group.id, LiveAction.DELETE, ModelName.STASH_ITEM, stashItem.id);
        }
        // Return.
        return ok();
    }

    @route("POST", "/stash-item/:id").dump(StashItem, getStashItem)
    public async update(
        @param("id") id: string,
        @body(updateStashItem) change: StashItem,
        @context ctx?: Context,
    ): Promise<StashItem> {
        // Fetch stash item.
        const stashItem = await this.db.getRepository(StashItem).findOne({
            where: { id },
            relations: ["stash", "stash.group", "stash.preset", "templateItem"],
        });
        // Access control.
        if (!stashItem) { return notFound<StashItem>(); }
        if (stashItem.stash.group && !(await ctx.hasGroup(stashItem.stash.group.id))) {
            return forbidden<StashItem>();
        }
        if (stashItem.stash.preset && !(await ctx.hasPreset(stashItem.stash.preset.id))) {
            return forbidden<StashItem>();
        }
       // Update.
        Object.assign(stashItem, change);
        await this.db.getRepository(StashItem).save(stashItem);
        // Announce live update.
        if (stashItem.stash.group) {
            this.live.publish(stashItem.stash.group.id, LiveAction.UPDATE, ModelName.STASH_ITEM, stashItem.id);
        }
        // Return updated.
        return ok(stashItem);
    }

    @route("GET", "/stash-item/:id").dump(StashItem, getStashItem)
    public async get(@param("id") id: string): Promise<StashItem> {
        const stashItem = await this.db.getRepository(StashItem).findOne({
            where: { id },
            relations: ["stash", "templateItem"],
        });
        if (!stashItem) { return notFound<Stash>(); }
        return ok(populate(StashItem, stashItem));
    }
}
