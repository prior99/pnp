export interface ConfigInterface {
    port: number;
    configFile: string;
    dbName: string;
    dbUsername: string;
    dbPassword: string;
    dbPort: number;
    dbHost: string;
    dbLogging: boolean;
    language: string;
    battleMapsDir: string;
    imagesDir: string;
}
