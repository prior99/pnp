export const SKILL_COST_PER_RANK = 10;
export const SKILL_AWARDED_PER_LEVEL = 5;
export const COMBAT_STYLE_AWARDED_PER_LEVEL = 3;
export const BODY_AWARDED_PER_LEVEL = 2;
export const FITNESS_AWARDED_PER_LEVEL = 3;
export const COMBAT_TRAIT_COST_FOCUS = 5;
export const COMBAT_TRAIT_COST_AIM = 3;
export const COMBAT_TRAIT_COST_AWARENESS = 3;
export const COMBAT_TRAIT_COST_SWIFTNESS = 5;
export const COMBAT_TRAIT_COST_TENSION = 2;
export const COMBAT_TRAIT_COST_POWER = 2;
export const COMBAT_TRAIT_COST_STAMINA = 4;
export const COMBAT_TRAIT_COST_CONSTITUTION = 2;

// These need to be kept in sync with the default values of the database
export const INITIAL_HERO_CONSTITUTION_POINTS = 40;
export const INITIAL_HERO_STAMINA_POINTS = 20;
export const INITIAL_MONSTER_CONSTITUTION_POINTS = 40;
export const INITIAL_MONSTER_STAMINA_POINTS = 20;
