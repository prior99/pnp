import {
    OneToMany,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid } from "hyrest";
import { world, getBattleMap, updateBattleMap, createBattleMap, ids, getMarker } from "../scopes";
import { Group } from "./group";
import { Marker } from "./marker";
import { Area } from "./area";
import { Preset } from "./preset";
import { BattleMapTag } from "./battle-map-tag";

declare const baseUrl: string;

@Entity()
export class BattleMap {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getBattleMap, ids, getMarker)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.battleMaps)
    @scope(world, getBattleMap, createBattleMap) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.battleMaps)
    @scope(world, getBattleMap, createBattleMap) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getBattleMap) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getBattleMap) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying")
    @scope(world, getBattleMap, updateBattleMap) @is()
    public name?: string;

    public get previewUrl() {
        if (!baseUrl) { return; }
        return `${baseUrl}/battle-map/${this.id}/preview`;
    }

    @OneToMany(() => Marker, marker => marker.battleMap)
    @scope(world, getBattleMap) @specify(() => Marker)
    public markers?: Marker[];

    @OneToMany(() => Area, area => area.battleMap)
    @scope(world, getBattleMap) @specify(() => Area)
    public areas?: Area[];

    @ManyToOne(() => BattleMap, battleMap => battleMap.copies)
    public original?: BattleMap;

    @OneToMany(() => BattleMap, battleMap => battleMap.original)
    public copies?: BattleMap[];

    @Column("text")
    @is() @scope(world, getBattleMap, updateBattleMap, createBattleMap)
    public description?: string;

    @Column("text")
    @is() @scope(world, getBattleMap, updateBattleMap, createBattleMap)
    public masterNotes?: string;

    @Column("boolean", { nullable: false, default: true })
    @scope(world, getBattleMap, updateBattleMap) @is()
    public masterOnly?: boolean;

    @OneToMany(() => BattleMapTag, battleMapTag => battleMapTag.battleMap)
    @specify(() => BattleMapTag)
    public battleMapTags?: BattleMapTag[];
}
