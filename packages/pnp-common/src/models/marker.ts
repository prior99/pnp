import {
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
} from "typeorm";
import { is, scope, specify, uuid, required, only, DataType } from "hyrest";
import { world, getMarker, updateMarker, createMarker, ids } from "../scopes";
import { BattleMap } from "./battle-map";
import { Hero } from "./hero";
import { Monster } from "./monster";
import { MarkerItem } from "./marker-item";

@Entity()
export class Marker {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getMarker, ids)
    @is().validate(uuid)
    public id?: string;

    @CreateDateColumn()
    @scope(world, getMarker) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getMarker) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying", { nullable: false, default: "" })
    @scope(world, createMarker, getMarker, updateMarker) @is()
    public name?: string;

    @Column("character varying", { nullable: true })
    @scope(world, createMarker, getMarker, updateMarker) @is()
    public icon?: string;

    @Column("character varying", { nullable: false, default: "" })
    @scope(world, createMarker, getMarker, updateMarker) @is()
    public masterDescription?: string;

    @Column("character varying", { nullable: true })
    @scope(world, createMarker, getMarker, updateMarker) @is()
    public description?: string;

    @Column("boolean", { nullable: false, default: true })
    @scope(world, createMarker, getMarker, updateMarker) @is()
    public masterOnly?: boolean;

    @Column("double precision")
    @scope(world, createMarker, getMarker, updateMarker) @is().validate(only(createMarker, required))
    public latitude?: number;

    @Column("double precision")
    @scope(world, createMarker, getMarker, updateMarker) @is().validate(only(createMarker, required))
    public longitude?: number;

    @ManyToOne(() => BattleMap, battleMap => battleMap.markers)
    @scope(world, getMarker, createMarker) @is().validate(only(createMarker, required)) @specify(() => BattleMap)
    public battleMap?: BattleMap;

    @ManyToOne(() => Hero, hero => hero.markers)
    @scope(world, getMarker, createMarker) @is() @specify(() => Hero)
    public hero?: Hero;

    @ManyToOne(() => Monster, monster => monster.markers)
    @scope(world, getMarker, createMarker) @is() @specify(() => Monster)
    public monster?: Monster;

    @OneToMany(() => MarkerItem, markerItem => markerItem.marker)
    public markerItems?: MarkerItem[];

    @Column("int", { nullable: true })
    @scope(world, getMarker, updateMarker, createMarker) @is(DataType.int, { nullable: true })
    public money?: number;
}
