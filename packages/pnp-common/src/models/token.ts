import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { login, owner } from "../scopes";
import { Group } from "./group";

@Entity()
export class Token {
    @PrimaryGeneratedColumn("uuid")
    @scope(owner)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.tokens)
    @scope(login, owner) @is().validate(required) @specify(() => Group)
    public group?: Group;

    @CreateDateColumn()
    public created?: Date;

    @UpdateDateColumn()
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("bool", { nullable: false })
    @scope(login, owner)
    @is()
    public master?: boolean;
}
