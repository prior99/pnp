import {
    OneToMany,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid, DataType } from "hyrest";
import { world, getTemplateItem, updateTemplateItem, createTemplateItem, ids } from "../scopes";
import { Group } from "./group";
import { Item } from "./item";
import { Preset } from "./preset";

declare const baseUrl: string;

@Entity()
export class TemplateItem {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getTemplateItem, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.templateItems)
    @scope(world, getTemplateItem, createTemplateItem) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.templateItems)
    @scope(world, getTemplateItem, createTemplateItem) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getTemplateItem) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getTemplateItem) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying")
    @scope(world, getTemplateItem, updateTemplateItem) @is()
    public name?: string;

    @Column("text")
    @scope(world, getTemplateItem, updateTemplateItem) @is()
    public description?: string;

    @Column("text")
    @scope(world, getTemplateItem, updateTemplateItem) @is()
    public effect?: string;

    @Column("bool", { default: false })
    @scope(world, getTemplateItem, updateTemplateItem) @is()
    public quantifiable?: boolean;

    @Column("bool", { default: false })
    @scope(world, getTemplateItem, updateTemplateItem) @is()
    public canBeActive?: boolean;

    @Column("character varying", { default: "piece" })
    @scope(world, getTemplateItem, updateTemplateItem) @is()
    public unit?: string;

    @OneToMany(() => Item, item => item.templateItem) @specify(() => Item)
    public items?: Item[];

    @ManyToOne(() => TemplateItem, templateItem => templateItem.copies)
    public original?: TemplateItem;

    @OneToMany(() => TemplateItem, templateItem => templateItem.original)
    public copies?: TemplateItem[];

    @Column("character varying")
    @scope(world, getTemplateItem, updateTemplateItem) @is(DataType.str, { nullable: true })
    public suggestedCombatTrait?: string;

    @Column("int")
    @scope(world, getTemplateItem, updateTemplateItem) @is(DataType.int, { nullable: true })
    public suggestedStaminaCost?: number;

    public get imageUrl() {
        if (!baseUrl) { return; }
        return `${baseUrl}/template-item/${this.id}/image`;
    }
}
