import {
    OneToMany,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid } from "hyrest";
import { world, ids, createTag, getTag, updateTag } from "../scopes";
import { Group } from "./group";
import { Preset } from "./preset";
import { HeroTag } from "./hero-tag";
import { BattleMapTag } from "./battle-map-tag";

@Entity()
export class Tag {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getTag, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.tokens)
    @scope(world, getTag, createTag) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.heroes)
    @scope(world, getTag, createTag) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getTag) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getTag) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying")
    @scope(world, getTag, updateTag) @is()
    public name?: string;

    @OneToMany(() => HeroTag, heroTag => heroTag.tag) @specify(() => HeroTag)
    public heroTags?: HeroTag[];

    @OneToMany(() => BattleMapTag, battleMapTag => battleMapTag.tag) @specify(() => BattleMapTag)
    public battleMapTags?: BattleMapTag[];

    @ManyToOne(() => Tag, tag => tag.copies)
    public original?: Tag;

    @OneToMany(() => Tag, tag => tag.original)
    public copies?: Tag[];

    @Column("boolean")
    @scope(world, getTag, updateTag) @is()
    public pinned?: boolean;

    @Column("boolean")
    @scope(world, getTag, updateTag) @is()
    public masterOnly?: boolean;
}
