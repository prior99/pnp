import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, updateStashItem, createStashItem, ids, getStashItem } from "../scopes";
import { TemplateItem } from "./template-item";
import { Marker } from "./marker";
import { Stash } from "./stash";

@Entity()
export class StashItem {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getStashItem, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Stash, stash => stash.stashItems)
    @scope(world, getStashItem, createStashItem) @is().validate(required) @specify(() => Marker)
    public stash?: Stash;

    @ManyToOne(() => TemplateItem, templateItem => templateItem.items)
    @scope(world, getStashItem, createStashItem) @is().validate(required) @specify(() => TemplateItem)
    public templateItem?: TemplateItem;

    @CreateDateColumn()
    @scope(world, getStashItem) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getStashItem) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("text", { default: "" })
    @scope(world, getStashItem, updateStashItem) @is()
    public notes?: string;

    @Column("int", { default: 1 })
    @scope(world, getStashItem, updateStashItem, createStashItem) @is()
    public quantity?: number;
}
