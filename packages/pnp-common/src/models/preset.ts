import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from "typeorm";
import { is, scope, specify, length, uuid, transform, only, required } from "hyrest";
import { world, getPreset, owner, ids, createPreset, loginPreset } from "../scopes";
import { hash } from "../hash";
import { BattleMap } from "./battle-map";
import { Maneuver } from "./maneuver";
import { TemplateItem } from "./template-item";
import { TemplateMonster } from "./template-monster";
import { Hero } from "./hero";
import { PresetToken } from "./preset-token";
import { Tag } from "./tag";
import { JournalEntry } from "./journal-entry";
import { Stash } from "./stash";

@Entity()
export class Preset {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid)
    @scope(world, getPreset, ids, loginPreset)
    public id?: string;

    @Column("varchar", { length: 100 })
    @is()
        .validate(length(3, 100), only(loginPreset, required))
        .validate(length(3, 100), only(createPreset, required))
        .validateCtx(ctx => only(createPreset, value => ctx.validation.presetNameAvailable(value)))
    @scope(world, getPreset, createPreset)
    public name?: string;

    @Column("text")
    @is().validate(length(3, 100), only(createPreset, required))
    @scope(world, getPreset, createPreset)
    public description?: string;

    @Column("varchar", { length: 200 })
    @transform(hash)
    @is().validate(
        length(8, 255),
        only(loginPreset, required),
        only(createPreset, required),
    )
    @scope(loginPreset, createPreset)
    public password?: string;

    @OneToMany(() => PresetToken, presetToken => presetToken.preset)
    @is() @specify(() => PresetToken) @scope(owner)
    public presetTokens?: PresetToken[];

    @OneToMany(() => BattleMap, battleMap => battleMap.preset)
    @is() @specify(() => BattleMap) @scope(owner)
    public battleMaps?: BattleMap[];

    @OneToMany(() => Hero, hero => hero.preset)
    @is() @specify(() => Hero) @scope(owner)
    public heroes?: Hero[];

    @OneToMany(() => TemplateItem, templateItem => templateItem.preset)
    @is() @specify(() => TemplateItem) @scope(owner)
    public templateItems?: TemplateItem[];

    @OneToMany(() => Maneuver, maneuver => maneuver.preset)
    @is() @specify(() => Maneuver) @scope(owner)
    public maneuvers?: Maneuver[];

    @OneToMany(() => TemplateMonster, templateMonster => templateMonster.preset)
    @is() @specify(() => TemplateMonster) @scope(owner)
    public templateMonsters?: TemplateMonster[];

    @OneToMany(() => Tag, tag => tag.group)
    @is() @specify(() => Tag) @scope(owner)
    public tags?: Tag[];

    @OneToMany(() => JournalEntry, journalEntry => journalEntry.group)
    @is() @specify(() => JournalEntry) @scope(owner)
    public journalEntries?: JournalEntry[];

    @OneToMany(() => Stash, stash => stash.preset)
    @is() @specify(() => Stash) @scope(owner)
    public stashs?: Stash[];
}
