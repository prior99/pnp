import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, updateItem, createItem, ids, getItem } from "../scopes";
import { Hero } from "./hero";
import { TemplateItem } from "./template-item";

@Entity()
export class Item {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getItem, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Hero, hero => hero.items)
    @scope(world, getItem, createItem) @is().validate(required) @specify(() => Hero)
    public hero?: Hero;

    @ManyToOne(() => TemplateItem, templateItem => templateItem.items)
    @scope(world, getItem, createItem) @is().validate(required) @specify(() => TemplateItem)
    public templateItem?: TemplateItem;

    @CreateDateColumn()
    @scope(world, getItem) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getItem) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("text", { default: "" })
    @scope(world, getItem, updateItem) @is()
    public notes?: string;

    @Column("int", { default: 1 })
    @scope(world, getItem, updateItem, createItem) @is()
    public quantity?: number;
}
