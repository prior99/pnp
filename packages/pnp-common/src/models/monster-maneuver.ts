import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, createMonsterManeuver, ids, getMonsterManeuver } from "../scopes";
import { TemplateMonster } from "./template-monster";
import { Maneuver } from "./maneuver";

@Entity()
export class MonsterManeuver {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getMonsterManeuver, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => TemplateMonster, templateMonster => templateMonster.monsterManeuvers)
    @scope(world, getMonsterManeuver, createMonsterManeuver) @is().validate(required) @specify(() => TemplateMonster)
    public templateMonster?: TemplateMonster;

    @ManyToOne(() => Maneuver, maneuver => maneuver.monsterManeuvers)
    @scope(world, getMonsterManeuver, createMonsterManeuver) @is().validate(required) @specify(() => Maneuver)
    public maneuver?: Maneuver;

    @CreateDateColumn()
    @scope(world, getMonsterManeuver) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getMonsterManeuver) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;
}
