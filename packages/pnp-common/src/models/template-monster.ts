import {
    OneToMany,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid, only, required, DataType } from "hyrest";
import { world, getTemplateMonster, updateTemplateMonster, createTemplateMonster, ids } from "../scopes";
import { Group } from "./group";
import { MonsterManeuver } from "./monster-maneuver";
import { Monster } from "./monster";
import { Preset } from "./preset";
import {
    CombatTraitCategory,
    allCombatTraitCategories,
    getCombatTraitsForCategory,
    combatTraitCategoryAwardedPerLevel,
} from "../combat";
import { INITIAL_MONSTER_STAMINA_POINTS, INITIAL_MONSTER_CONSTITUTION_POINTS } from "../constants";
import { Stash } from "./stash";

declare const baseUrl: string;

@Entity()
export class TemplateMonster {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getTemplateMonster, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.tokens)
    @scope(world, getTemplateMonster, createTemplateMonster) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.heroes)
    @scope(world, getTemplateMonster, createTemplateMonster) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getTemplateMonster) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getTemplateMonster) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying")
    @scope(world, getTemplateMonster, updateTemplateMonster) @is().validate(only(createTemplateMonster, required))
    public name?: string;

    @Column("text")
    @scope(world, getTemplateMonster, updateTemplateMonster) @is()
    public description?: string;

    @Column("int", { default: 0 })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public focus?: number;

    @Column("int", { default: 0 })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public aim?: number;

    @Column("int", { default: 0 })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public awareness?: number;

    @Column("int", { default: 0 })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public swiftness?: number;

    @Column("int", { default: 0 })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public tension?: number;

    @Column("int", { default: 0 })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public power?: number;

    @Column("int", { default: INITIAL_MONSTER_STAMINA_POINTS })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public stamina?: number;

    @Column("int", { default: INITIAL_MONSTER_CONSTITUTION_POINTS })
    @scope(world, getTemplateMonster, updateTemplateMonster) @is(DataType.int)
    public constitution?: number;

    @OneToMany(() => MonsterManeuver, monsterManeuver => monsterManeuver.templateMonster)
    @is() @specify(() => MonsterManeuver)
    public monsterManeuvers?: MonsterManeuver[];

    @OneToMany(() => Monster, item => item.templateMonster) @specify(() => Monster)
    public monsters?: Monster[];

    @ManyToOne(() => TemplateMonster, templateMonster => templateMonster.copies)
    public original?: TemplateMonster;

    @OneToMany(() => TemplateMonster, templateMonster => templateMonster.original)
    public copies?: TemplateMonster[];

    @ManyToOne(() => Stash, stash => stash.templateMonsters)
    @scope(world, getTemplateMonster, createTemplateMonster, updateTemplateMonster) @is() @specify(() => Stash)
    public stash?: Stash | null;

    public level(category: CombatTraitCategory): number {
        let points = getCombatTraitsForCategory(category).reduce(
            (currentPoints, trait) =>
                currentPoints + (this[trait] || 0),
            0,
        );

        if (category === "fitness") {
            points -= INITIAL_MONSTER_CONSTITUTION_POINTS + INITIAL_MONSTER_STAMINA_POINTS;
        }

        return points / combatTraitCategoryAwardedPerLevel(category);
    }

    public levelRange(): {min: number, max: number} {
        const allRanks = allCombatTraitCategories.map(category => this.level(category));

        return {
            min: Math.min(...allRanks),
            max: Math.max(...allRanks),
        };
    }

    public get imageUrl() {
        if (!baseUrl) { return; }
        return `${baseUrl}/template-monster/${this.id}/image`;
    }
}
