import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from "typeorm";
import { is, scope, specify, length, uuid, transform, only, required } from "hyrest";
import { world, getGroup, login, owner, signup, ids } from "../scopes";
import { hash } from "../hash";
import { Token } from "./token";
import { BattleMap } from "./battle-map";
import { Hero } from "./hero";
import { TemplateItem } from "./template-item";
import { TemplateMonster } from "./template-monster";
import { Maneuver } from "./maneuver";
import { Tag } from "./tag";
import { JournalEntry } from "./journal-entry";
import { Stash } from "./stash";

@Entity()
export class Group {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid)
    @scope(world, getGroup, ids)
    public id?: string;

    @Column("varchar", { length: 100 })
    @is()
        .validate(length(3, 100), only(signup, required))
        .validateCtx(ctx => only(signup, value => ctx.validation.nameAvailable(value)))
    @scope(world, getGroup, login)
    public name?: string;

    @Column("varchar", { length: 200 })
    @transform(hash)
    @is().validate(
        length(8, 255),
        only(signup, required),
    )
    @scope(login)
    public masterPassword?: string;

    @Column("varchar", { length: 200 })
    @transform(hash)
    @is().validate(
        length(8, 255),
        only(login, required),
        only(signup, required),
    )
    @scope(login)
    public password?: string;

    @OneToMany(() => Token, token => token.group)
    @is() @specify(() => Token) @scope(owner)
    public tokens?: Token[];

    @OneToMany(() => BattleMap, battleMap => battleMap.group)
    @is() @specify(() => BattleMap) @scope(owner)
    public battleMaps?: BattleMap[];

    @OneToMany(() => Hero, hero => hero.group)
    @is() @specify(() => Hero) @scope(owner)
    public heroes?: Hero[];

    @OneToMany(() => TemplateItem, templateItem => templateItem.group)
    @is() @specify(() => TemplateItem) @scope(owner)
    public templateItems?: TemplateItem[];

    @OneToMany(() => Maneuver, maneuver => maneuver.group)
    @is() @specify(() => Maneuver) @scope(owner)
    public maneuvers?: Maneuver[];

    @OneToMany(() => TemplateMonster, templateMonster => templateMonster.group)
    @is() @specify(() => TemplateMonster) @scope(owner)
    public templateMonsters?: TemplateMonster[];

    @OneToMany(() => Tag, tag => tag.group)
    @is() @specify(() => Tag) @scope(owner)
    public tags?: Tag[];

    @OneToMany(() => JournalEntry, journalEntry => journalEntry.group)
    @is() @specify(() => JournalEntry) @scope(owner)
    public journalEntries?: JournalEntry[];

    @OneToMany(() => Stash, stash => stash.group)
    @is() @specify(() => Stash) @scope(owner)
    public stashs?: Stash[];
}
