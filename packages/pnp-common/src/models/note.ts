import {
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid, required, transform } from "hyrest";
import { world, updateNote, createNote, ids, getNote } from "../scopes";
import { Hero } from "./hero";
import * as sanitizeHtml from "sanitize-html";

function sanitize(value: string) {
    return sanitizeHtml(value, {
        allowedTags: [
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "blockquote",
            "p",
            "a",
            "ul",
            "ol",
            "span",
            "nl",
            "li",
            "b",
            "i",
            "strong",
            "em",
            "strike",
            "code",
            "hr",
            "br",
            "div",
            "table",
            "thead",
            "caption",
            "tbody",
            "tr",
            "th",
            "td",
            "pre",
            "s",
        ],
        allowedAttributes: {
            a: ["href", "name", "target"],
            span: ["style"],
        },
    });
}

@Entity()
export class Note {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getNote, ids)
    @(is().validate(uuid))
    public id?: string;

    @ManyToOne(
        () => Hero,
        hero => hero.notes,
    )
    @scope(world, getNote, createNote)
    @(is().validate(required))
    @specify(() => Hero)
    public hero?: Hero;

    @CreateDateColumn()
    @scope(world, getNote)
    @is()
    @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getNote)
    @is()
    @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("text", { default: "" })
    @scope(world, getNote, updateNote)
    @is()
    @transform(sanitize)
    public note?: string;

    @Column("boolean")
    @scope(world, getNote, updateNote)
    @is()
    public secret?: boolean;

    @Column("boolean")
    @scope(world, getNote, updateNote)
    @is()
    public masterOnly?: boolean;
}
