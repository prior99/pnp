import { PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, createHeroTag, ids, getHeroTag } from "../scopes";
import { Hero } from "./hero";
import { Tag } from "./tag";

@Entity()
export class HeroTag {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getHeroTag, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Hero, hero => hero.heroTags)
    @scope(world, getHeroTag, createHeroTag) @is().validate(required) @specify(() => Hero)
    public hero?: Hero;

    @ManyToOne(() => Tag, tag => tag.heroTags)
    @scope(world, getHeroTag, createHeroTag) @is().validate(required) @specify(() => Tag)
    public tag?: Tag;

    @ManyToOne(() => HeroTag, heroTag => heroTag.copies)
    public original?: HeroTag;

    @OneToMany(() => HeroTag, heroTag => heroTag.original)
    public copies?: HeroTag[];
}
