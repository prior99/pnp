import { OneToMany,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid, DataType } from "hyrest";
import { world, getManeuver, updateManeuver, createManeuver, ids } from "../scopes";
import { Group } from "./group";
import { LearnedManeuver } from "./learned-maneuver";
import { MonsterManeuver } from "./monster-maneuver";
import { Preset } from "./preset";
import { CombatTrait, CombatTraitClassification } from "../combat";

@Entity()
export class Maneuver {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getManeuver, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.tokens)
    @scope(world, getManeuver, createManeuver) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.heroes)
    @scope(world, getManeuver, createManeuver) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getManeuver) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getManeuver) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying")
    @scope(world, getManeuver, updateManeuver) @is()
    public name?: string;

    @Column("text")
    @scope(world, getManeuver, updateManeuver) @is()
    public description?: string;

    @Column("text")
    @scope(world, getManeuver, updateManeuver) @is()
    public effect?: string;

    @OneToMany(() => LearnedManeuver, learnedManeuver => learnedManeuver.maneuver) @specify(() => LearnedManeuver)
    public learnedManeuvers?: LearnedManeuver[];

    @ManyToOne(() => Maneuver, maneuver => maneuver.copies)
    public original?: Maneuver;

    @OneToMany(() => Maneuver, maneuver => maneuver.original)
    public copies?: Maneuver[];

    @OneToMany(() => MonsterManeuver, monsterManeuver => monsterManeuver.maneuver) @specify(() => MonsterManeuver)
    public monsterManeuvers?: MonsterManeuver[];

    @Column("character varying", { nullable: true })
    @scope(world, getManeuver, updateManeuver) @is(DataType.str, { nullable: true })
    public suggestedCombatTrait?: CombatTrait;

    @Column("int", { nullable: true })
    @scope(world, getManeuver, updateManeuver) @is(DataType.int, { nullable: true })
    public suggestedStaminaCost?: number;

    @Column("character varying", { default: "offensive" })
    @scope(world, getManeuver, updateManeuver) @is(DataType.str, { nullable: true })
    public classification?: CombatTraitClassification;
}
