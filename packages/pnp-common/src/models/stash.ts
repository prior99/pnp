import {
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
} from "typeorm";
import { is, scope, specify, uuid, DataType, required, only } from "hyrest";
import { world, updateStash, createStash, ids, getStash } from "../scopes";
import { Group } from "./group";
import { Preset } from "./preset";
import { StashItem } from "./stash-item";
import { TemplateMonster } from "./template-monster";

@Entity()
export class Stash {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getStash, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.heroes)
    @scope(world, getStash, createStash) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.heroes)
    @scope(world, getStash, createStash) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getStash) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getStash) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("int")
    @scope(world, getStash, updateStash, createStash) @is(DataType.int).validate(only(createStash, required))
    public money?: number;

    @Column("character varying")
    @scope(world, getStash, updateStash, createStash) @is().validate(only(createStash, required))
    public name?: string;

    @OneToMany(() => StashItem, stashItem => stashItem.stash)
    public stashItems?: StashItem[];

    @ManyToOne(() => Stash, stash => stash.copies)
    public original?: Stash;

    @OneToMany(() => Stash, stash => stash.original)
    public copies?: Stash[];

    @OneToMany(() => TemplateMonster, templateMonster => templateMonster.stash)
    public templateMonsters?: TemplateMonster[];
}
