import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, createLearnedManeuver, ids, getLearnedManeuver } from "../scopes";
import { Hero } from "./hero";
import { Maneuver } from "./maneuver";

@Entity()
export class LearnedManeuver {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getLearnedManeuver, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Hero, hero => hero.learnedManeuvers)
    @scope(world, getLearnedManeuver, createLearnedManeuver) @is().validate(required) @specify(() => Hero)
    public hero?: Hero;

    @ManyToOne(() => Maneuver, maneuver => maneuver.learnedManeuvers)
    @scope(world, getLearnedManeuver, createLearnedManeuver) @is().validate(required) @specify(() => Maneuver)
    public maneuver?: Maneuver;

    @CreateDateColumn()
    @scope(world, getLearnedManeuver) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getLearnedManeuver) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;
}
