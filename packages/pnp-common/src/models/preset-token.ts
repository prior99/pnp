import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { owner, loginPreset } from "../scopes";
import { Preset } from "./preset";

@Entity()
export class PresetToken {
    @PrimaryGeneratedColumn("uuid")
    @scope(owner)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Preset, preset => preset.presetTokens)
    @scope(loginPreset, owner) @is().validate(required) @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    public created?: Date;

    @UpdateDateColumn()
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;
}
