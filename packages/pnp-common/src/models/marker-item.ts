import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, updateItem, createMarkerItem, ids, getMarkerItem } from "../scopes";
import { TemplateItem } from "./template-item";
import { Marker } from "./marker";

@Entity()
export class MarkerItem {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getMarkerItem, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Marker, marker => marker.markerItems)
    @scope(world, getMarkerItem, createMarkerItem) @is().validate(required) @specify(() => Marker)
    public marker?: Marker;

    @ManyToOne(() => TemplateItem, templateItem => templateItem.items)
    @scope(world, getMarkerItem, createMarkerItem) @is().validate(required) @specify(() => TemplateItem)
    public templateItem?: TemplateItem;

    @CreateDateColumn()
    @scope(world, getMarkerItem) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getMarkerItem) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("text", { default: "" })
    @scope(world, getMarkerItem, updateItem) @is()
    public notes?: string;

    @Column("int", { default: 1 })
    @scope(world, getMarkerItem, updateItem, createMarkerItem) @is()
    public quantity?: number;
}
