import {
    OneToMany,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
} from "typeorm";
import { computed } from "mobx";
import { is, scope, specify, uuid, DataType } from "hyrest";
import { world, getHero, updateHero, createHero, ids, getMarker } from "../scopes";
import { Group } from "./group";
import {
    SKILL_COST_PER_RANK,
    SKILL_AWARDED_PER_LEVEL,
    INITIAL_HERO_STAMINA_POINTS,
    INITIAL_HERO_CONSTITUTION_POINTS,
} from "../constants";
import { getBaseSkill, getSubSkills, allSubSkills, Skill, isBaseSkill } from "../skills";
import {
    getCombatTraitsForCategory,
    allCombatTraitCategories,
    CombatTraitCategory,
    combatTraitCategoryAwardedPerLevel,
    combatTraitCostPerRank,
    CombatTrait,
} from "../combat";
import { Item } from "./item";
import { Marker } from "./marker";
import { LearnedManeuver } from "./learned-maneuver";
import { Preset } from "./preset";
import { Note } from "./note";
import { HeroTag } from "./hero-tag";
import { JournalEntry } from "./journal-entry";

declare const baseUrl: string;

@Entity()
export class Hero {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getHero, ids, getMarker)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.heroes)
    @scope(world, getHero, createHero) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.heroes)
    @scope(world, getHero, createHero) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getHero) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getHero) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("character varying")
    @scope(world, getHero, updateHero) @is()
    public name?: string;

    @Column("text")
    @scope(world, getHero, updateHero) @is()
    public description?: string;

    @Column("boolean", { default: false })
    @scope(world, createHero, getHero, updateHero) @is()
    public isNpc?: boolean;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public level?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public research?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public nature?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public knowledge?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public navigation?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public burglary?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public stealing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public hiding?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public sneaking?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public steadyHand?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public examining?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public captivating?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public mechanics?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public pushing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public throwing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public bashing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public lifting?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public balance?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public running?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public climbing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public swimming?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public bartering?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public art?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public deception?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public questioning?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public improvisedMedicine?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public poisons?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public craftedMedicine?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public nursing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public vision?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public hearing?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public instinct?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public tracking?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public focus?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public aim?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public awareness?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public swiftness?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public tension?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public power?: number;

    @Column("int", { default: INITIAL_HERO_STAMINA_POINTS })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public stamina?: number;

    @Column("int", { default: INITIAL_HERO_CONSTITUTION_POINTS })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public constitution?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public damage?: number;

    @Column("int", { default: 0 })
    @scope(world, getHero, updateHero) @is(DataType.int)
    public spentStamina?: number;

    @OneToMany(() => Item, item => item.hero)
    @is() @specify(() => Item)
    public items?: Item[];

    @OneToMany(() => LearnedManeuver, learnedManeuver => learnedManeuver.hero)
    @is() @specify(() => LearnedManeuver)
    public learnedManeuvers?: LearnedManeuver[];

    @OneToMany(() => Note, note => note.hero)
    @scope(world, getHero, updateHero) @specify(() => Note)
    public notes?: Note[];

    @OneToMany(() => Marker, marker => marker.hero)
    @specify(() => Marker)
    public markers?: Marker[];

    @ManyToOne(() => Hero, hero => hero.copies)
    public original?: Hero;

    @OneToMany(() => Hero, hero => hero.original)
    public copies?: Hero[];

    @OneToMany(() => HeroTag, heroTag => heroTag.hero)
    @specify(() => HeroTag)
    public heroTags?: HeroTag[];

    @OneToMany(() => JournalEntry, journalEntry => journalEntry.hero)
    @specify(() => JournalEntry)
    public journalEntries?: JournalEntry[];

    @Column("boolean")
    @scope(world, getHero, updateHero) @is()
    public masterOnly?: boolean;

    @Column("int", { nullable: true })
    @scope(world, getHero, updateHero) @is(DataType.int, { nullable: true })
    public initiative?: number;

    @Column("int", { nullable: true })
    @scope(world, getHero, updateHero) @is(DataType.int, { nullable: true })
    public currentCombatTurns?: number;

    @Column("int")
    @scope(world, getHero, updateHero) @is()
    public money?: number;

    public rankProgress(skill: Skill): number {
        const fraction = this.skill(skill) / SKILL_COST_PER_RANK;
        return fraction - Math.floor(fraction);
    }

    public pointsToNextSkillRank(skill: Skill): number {
        return Math.round((1 - this.rankProgress(skill)) * SKILL_COST_PER_RANK) / (isBaseSkill(skill) ? 1 : 2);
    }

    public skill(skill: Skill): number {
        if (isBaseSkill(skill)) {
            return getSubSkills(skill).map(subSkill => this[subSkill]).reduce((sum, current) => current + sum, 0);
        }
        const baseSkill = getBaseSkill(skill);
        return this[skill] + this.skill(baseSkill);
    }

    public rank(skill: Skill): number {
        return Math.floor(this.skill(skill) / SKILL_COST_PER_RANK);
    }

    @computed public get totalSkill() {
        return this.level * SKILL_AWARDED_PER_LEVEL;
    }

    @computed public get spentSkill() {
        return allSubSkills.map(skill => this[skill]).reduce((sum, current) => sum + current);
    }

    @computed public get unspentSkill() {
        return this.totalSkill - this.spentSkill;
    }

    @computed public get carryCapacity() {
        return this.level * 2 + 10;
    }

    public combatTraitProgress(trait: CombatTrait): number {
        const fraction = this.combatTrait(trait) / combatTraitCostPerRank(trait);
        return fraction - Math.floor(fraction);
    }

    public pointsToNextCombatTraitRank(trait: CombatTrait): number {
        return Math.round((1 - this.combatTraitProgress(trait)) * combatTraitCostPerRank(trait));
    }

    public combatTrait(trait: CombatTrait): number {
        return this[trait];
    }

    public combatRank(trait: CombatTrait): number {
        return Math.floor(this.combatTrait(trait) / combatTraitCostPerRank(trait));
    }

    public totalCombatTrait(category: CombatTraitCategory) {
        return this.level * combatTraitCategoryAwardedPerLevel(category);
    }

    public spentCombatTrait(category: CombatTraitCategory): number {
        const spent = getCombatTraitsForCategory(category)
            .map(trait => this[trait])
            .reduce((sum, current) => sum + current);
        if (category === "fitness") {
            // Stamina and constitution don't start at 0. These values are hardcoded in the database (keep in sync!)
            return spent - INITIAL_HERO_STAMINA_POINTS - INITIAL_HERO_CONSTITUTION_POINTS;
        }
        return spent;
    }

    public unspentCombatTrait(category: CombatTraitCategory): number {
        return this.totalCombatTrait(category) - this.spentCombatTrait(category);
    }

    public validUpdate(old: Hero): { errors: string[] } {
        const errors: string[] = [];
        const skillsIncreased = allSubSkills.map(skill => this[skill] - old[skill]).every(change => change >= 0);
        if (!skillsIncreased) {
            errors.push("Can't decrease skill.");
        }
        const skillSpent = this.spentSkill <= this.totalSkill;
        if (!skillSpent) {
            errors.push("Can't spent more skill than available.");
        }
        const combatSpent = allCombatTraitCategories
            .every(category => this.spentCombatTrait(category) <= this.totalCombatTrait(category));
        if (!combatSpent) {
            errors.push("Can't spent more combat trait than available.");
        }
        return { errors };
    }

    public get avatarUrl() {
        if (!baseUrl) { return; }
        return `${baseUrl}/hero/${this.id}/avatar`;
    }
}
