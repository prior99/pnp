import {
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
} from "typeorm";
import { is, scope, specify, uuid } from "hyrest";
import { world, ids, getJournalEntry, createJournalEntry, updateJournalEntry } from "../scopes";
import { Group } from "./group";
import { Preset } from "./preset";
import { Hero } from "./hero";

@Entity()
export class JournalEntry {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getJournalEntry, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => Group, group => group.journalEntries)
    @scope(world, getJournalEntry, createJournalEntry) @is() @specify(() => Group)
    public group?: Group;

    @ManyToOne(() => Preset, preset => preset.journalEntries)
    @scope(world, getJournalEntry, createJournalEntry) @is() @specify(() => Preset)
    public preset?: Preset;

    @CreateDateColumn()
    @scope(world, getJournalEntry) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getJournalEntry) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @ManyToOne(() => Hero, hero => hero.journalEntries)
    @scope(world, getJournalEntry, createJournalEntry) @is() @specify(() => Hero)
    public hero?: Hero;

    @Column("character varying")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public description?: string;

    @Column("character varying")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public summary?: string;

    @Column("character varying")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public icon?: string;

    @Column("character varying")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public meta?: string;

    @Column("boolean")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public masterOnly?: boolean;

    @Column("boolean")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public secret?: boolean;

    @Column("int")
    @scope(world, getJournalEntry, updateJournalEntry) @is()
    public sortKey?: number;

    @ManyToOne(() => JournalEntry, journalEntry => journalEntry.copies)
    public original?: JournalEntry;

    @OneToMany(() => JournalEntry, journalEntry => journalEntry.original)
    public copies?: JournalEntry[];
}
