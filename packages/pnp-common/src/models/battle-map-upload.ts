import { is, scope, specify, required } from "hyrest";
import { createBattleMap } from "../scopes";
import { Group } from "./group";
import { Preset } from "./preset";

export class BattleMapUpload {
    @scope(createBattleMap) @is() @specify(() => Group)
    public group?: Group;

    @scope(createBattleMap) @is() @specify(() => Preset)
    public preset?: Preset;

    @scope(createBattleMap) @is().validate(required)
    public name?: string;

    @scope(createBattleMap) @is().validate(required)
    public content?: string;
}
