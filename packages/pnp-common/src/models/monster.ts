import {
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
} from "typeorm";
import { is, scope, specify, uuid, required, DataType } from "hyrest";
import { world, updateMonster, createMonster, ids, getMonster } from "../scopes";
import { TemplateMonster } from "./template-monster";
import { Marker } from "./marker";

@Entity()
export class Monster {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getMonster, ids)
    @is().validate(uuid)
    public id?: string;

    @OneToMany(() => Marker, marker => marker.monster)
    @scope(world, getMonster, createMonster) @is() @specify(() => Marker)
    public markers?: Marker[];

    @ManyToOne(() => TemplateMonster, templateMonster => templateMonster.monsters)
    @scope(world, getMonster, createMonster) @is().validate(required) @specify(() => TemplateMonster)
    public templateMonster?: TemplateMonster;

    @CreateDateColumn()
    @scope(world, getMonster) @is() @specify(() => Date)
    public created?: Date;

    @UpdateDateColumn()
    @scope(world, getMonster) @is() @specify(() => Date)
    public updated?: Date;

    @Column("timestamp with time zone", { nullable: true })
    public deleted?: Date;

    @Column("int", { default: 0 })
    @scope(world, getMonster, updateMonster) @is(DataType.int)
    public damage?: number;

    @Column("int", { default: 0 })
    @scope(world, getMonster, updateMonster) @is(DataType.int)
    public spentStamina?: number;

    @ManyToOne(() => Monster, monster => monster.copies)
    public original?: Monster;

    @OneToMany(() => Monster, monster => monster.original)
    public copies?: Monster[];

    @Column("int", { nullable: true })
    @scope(world, getMonster, updateMonster) @is(DataType.int, { nullable: true })
    public initiative?: number;

    @Column("int", { nullable: true })
    @scope(world, getMonster, updateMonster) @is(DataType.int, { nullable: true })
    public currentCombatTurns?: number;
}
