import { PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { is, scope, specify, uuid, required } from "hyrest";
import { world, createBattleMapTag, ids, getBattleMapTag } from "../scopes";
import { BattleMap } from "./battle-map";
import { Tag } from "./tag";

@Entity()
export class BattleMapTag {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getBattleMapTag, ids)
    @is().validate(uuid)
    public id?: string;

    @ManyToOne(() => BattleMap, battleMap => battleMap.battleMapTags)
    @scope(world, getBattleMapTag, createBattleMapTag) @is().validate(required) @specify(() => BattleMap)
    public battleMap?: BattleMap;

    @ManyToOne(() => Tag, tag => tag.battleMapTags)
    @scope(world, getBattleMapTag, createBattleMapTag) @is().validate(required) @specify(() => Tag)
    public tag?: Tag;

    @ManyToOne(() => BattleMapTag, battleMap => battleMap.copies)
    public original?: BattleMapTag;

    @OneToMany(() => BattleMapTag, battleMap => battleMap.original)
    public copies?: BattleMapTag[];
}
