import {
    Column,
    PrimaryGeneratedColumn,
    Entity,
    ManyToOne,
    CreateDateColumn,
} from "typeorm";
import { is, scope, specify, uuid, required, only } from "hyrest";
import { world, getArea, createArea, ids } from "../scopes";
import { BattleMap } from "./battle-map";

@Entity()
export class Area {
    @PrimaryGeneratedColumn("uuid")
    @scope(world, getArea, ids)
    @is().validate(uuid)
    public id?: string;

    @CreateDateColumn()
    @scope(world, getArea) @is() @specify(() => Date)
    public created?: Date;

    @Column("double precision")
    @scope(world, createArea, getArea) @is().validate(only(createArea, required))
    public top?: number;

    @Column("double precision")
    @scope(world, createArea, getArea) @is().validate(only(createArea, required))
    public left?: number;

    @Column("double precision")
    @scope(world, createArea, getArea) @is().validate(only(createArea, required))
    public bottom?: number;

    @Column("double precision")
    @scope(world, createArea, getArea) @is().validate(only(createArea, required))
    public right?: number;

    @ManyToOne(() => BattleMap, battleMap => battleMap.areas)
    @scope(world, getArea, createArea) @is().validate(only(createArea, required)) @specify(() => BattleMap)
    public battleMap?: BattleMap;

    public contains(latitude: number, longitude: number): boolean {
        return this.left <= longitude && this.top <= latitude && this.right >= longitude && this.bottom >= latitude;
    }
}
