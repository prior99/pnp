import { is, scope } from "hyrest";
import { upload } from "../scopes";

export class ImageUpload {
    @scope(upload) @is()
    public content?: string;

    @scope(upload) @is()
    public url?: string;

    public async loadData() {
        if (this.content) {
            return Buffer.from(this.content, "base64");
        }
        const request: any = await fetch(this.url);
        return await request.buffer();
    }
}
