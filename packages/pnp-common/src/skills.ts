export type MindSubSkill =
    "research" |
    "nature" |
    "knowledge" |
    "navigation";

export type StealthSubSkill =
    "burglary" |
    "stealing" |
    "hiding" |
    "sneaking";

export type DexteritySubSkill =
    "steadyHand" |
    "examining" |
    "captivating" |
    "mechanics";

export type StrengthSubSkill =
    "pushing" |
    "throwing" |
    "bashing" |
    "lifting";

export type AgilitySubSkill =
    "balance" |
    "running" |
    "climbing" |
    "swimming";

export type CharismaSubSkill =
    "bartering" |
    "art" |
    "deception" |
    "questioning";

export type MedicineSubSkill =
    "improvisedMedicine" |
    "poisons" |
    "craftedMedicine" |
    "nursing";

export type SensesSubSkill =
    "vision" |
    "hearing" |
    "instinct" |
    "tracking";

export type BaseSkill = "mind" |
    "stealth" |
    "dexterity" |
    "strength" |
    "agility" |
    "charisma" |
    "medicine" |
    "senses";

export type SubSkill =
    MindSubSkill |
    StealthSubSkill |
    DexteritySubSkill |
    StrengthSubSkill |
    AgilitySubSkill |
    CharismaSubSkill |
    MedicineSubSkill |
    SensesSubSkill;

export type Skill = BaseSkill | SubSkill;

export const allSkills: Skill[] = [
    "research",
    "nature",
    "knowledge",
    "navigation",
    "mind",
    "burglary",
    "stealing",
    "hiding",
    "sneaking",
    "stealth",
    "steadyHand",
    "examining",
    "captivating",
    "mechanics",
    "dexterity",
    "pushing",
    "throwing",
    "bashing",
    "lifting",
    "strength",
    "balance",
    "running",
    "climbing",
    "swimming",
    "agility",
    "bartering",
    "art",
    "deception",
    "questioning",
    "charisma",
    "improvisedMedicine",
    "poisons",
    "craftedMedicine",
    "nursing",
    "medicine",
    "vision",
    "hearing",
    "instinct",
    "tracking",
    "senses",
];

export const allSubSkills: SubSkill[] = allSkills.filter(skill => !isBaseSkill(skill)) as SubSkill[];
export const allBaseSkills: BaseSkill[] = allSkills.filter(skill => isBaseSkill(skill)) as BaseSkill[];

export function getBaseSkill(skill: Skill): BaseSkill {
    switch (skill) {
        case "research":
        case "nature":
        case "knowledge":
        case "navigation":
            return "mind";
        case "burglary":
        case "stealing":
        case "hiding":
        case "sneaking":
            return "stealth";
        case "steadyHand":
        case "examining":
        case "captivating":
        case "mechanics":
            return "dexterity";
        case "pushing":
        case "throwing":
        case "bashing":
        case "lifting":
            return "strength";
        case "balance":
        case "running":
        case "climbing":
        case "swimming":
            return "agility";
        case "bartering":
        case "art":
        case "deception":
        case "questioning":
            return "charisma";
        case "improvisedMedicine":
        case "poisons":
        case "craftedMedicine":
        case "nursing":
            return "medicine";
        case "vision":
        case "hearing":
        case "instinct":
        case "tracking":
            return "senses";
        default:
            return undefined;
    }
}

export function getSubSkills(skill: BaseSkill): SubSkill[] {
    return allSubSkills.filter(subSkill => getBaseSkill(subSkill) === skill);
}

export type Skilled = {
    [key in SubSkill]: number;
};

export function isBaseSkill(skill: Skill): skill is BaseSkill {
    return !getBaseSkill(skill);
}

export const skillDictionary: { [key in Skill]: string } = {
    research: "Research",
    nature: "Nature",
    knowledge: "Knowledge",
    navigation: "Navigation",
    mind: "Mind",
    burglary: "Burglary",
    stealing: "Stealing",
    hiding: "Hiding",
    sneaking: "Sneaking",
    stealth: "Stealth",
    steadyHand: "Steady hand",
    examining: "Examining",
    captivating: "Captivating",
    mechanics: "Mechanics",
    dexterity: "Dexterity",
    pushing: "Pushing",
    throwing: "Throwing",
    bashing: "Bashing",
    lifting: "Lifting",
    strength: "Strength",
    balance: "Balance",
    running: "Running",
    climbing: "Climbing",
    swimming: "Swimming",
    agility: "Agility",
    bartering: "Bartering",
    art: "Art",
    deception: "Deception",
    questioning: "Questioning",
    charisma: "Charisma",
    improvisedMedicine: "Improvised",
    poisons: "Poisons",
    craftedMedicine: "Crafted",
    nursing: "Nursing",
    medicine: "Medicine",
    vision: "Vision",
    hearing: "Hearing",
    instinct: "Instinct",
    tracking: "Tracking",
    senses: "Senses",
};
