import { LiveNavigationTarget } from "./live-messages";

const allColors = [
    "#DB2828",
    "#F2711C",
    "#FBBD08",
    "#B5CC18",
    "#21BA45",
    "#00B5AD",
    "#2185D0",
    "#6435C9",
    "#A333C8",
    "#E03997",
    "#A5673F",
];

function findFreeColor(usedColors: string[]) {
    return allColors.find(color => !usedColors.includes(color));
}

export class LiveGroup {
    public colors = new Map<string, string>();
    public claimedHeroes = new Map<string, string>();
    public navigationTargets = new Map<string, LiveNavigationTarget>();

    constructor(public id: string) {}

    public getColor(clientId: string) {
        return this.colors.get(clientId);
    }

    public assignColor(clientId: string) {
        const color = this.colors.size >= allColors.length
            ? allColors[0]
            : findFreeColor(Array.from(this.colors.values()));
        this.colors.set(clientId, color);
    }

    public navigateTo(clientId: string, target: LiveNavigationTarget) {
        this.navigationTargets.set(clientId, target);
    }

    public claimHero(clientId: string, heroId: string) {
        this.claimedHeroes.set(clientId, heroId);
    }

    public unclaimHero(clientId: string) {
        this.claimedHeroes.delete(clientId);
    }

    public freeClient(clientId: string) {
        this.colors.delete(clientId);
        this.claimedHeroes.delete(clientId);
        this.navigationTargets.delete(clientId);
    }

}
