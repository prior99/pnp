import * as WebSocket from "ws";
import { LiveMessage } from "./live-messages";

export class LiveClient {
    constructor(public webSocket: WebSocket, public groupIds: string[], public id: string) {}

    public send(message: LiveMessage) {
        this.webSocket.send(JSON.stringify(message));
    }
}
