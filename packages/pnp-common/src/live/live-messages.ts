export enum LiveAction {
    CREATE = "create",
    DELETE = "delete",
    UPDATE = "update",
    RELOAD_PAGE = "reload-page",
}

export enum ModelName {
    AREA = "area",
    BATTLE_MAP_UPLOAD = "battle_map_upload",
    BATTLE_MAP = "battle_map",
    GROUP = "group",
    HERO = "hero",
    ITEM = "item",
    MARKER = "marker",
    NOTE = "note",
    TEMPLATE_ITEM = "template_item",
    TOKEN = "token",
    MANEUVER = "maneuver",
    LEARNED_MANEUVER = "learned_maneuver",
    TEMPLATE_MONSTER = "template_monster",
    MONSTER_MANEUVER = "monster_maneuver",
    MONSTER = "monster",
    TAG = "tag",
    HERO_TAG = "hero_tag",
    BATTLE_MAP_TAG = "battle_map_tag",
    JOURNAL_ENTRY = "journal_entry",
    STASH = "stash",
    STASH_ITEM = "stash_item",
    MARKER_ITEM = "marker_item",
}

export enum PageName {
    HERO = "hero",
    BATTLE_MAP = "battle map",
    OTHER = "other",
}

export enum LiveMessageType {
    POINTER = "pointer",
    SUBSCRIBE = "subscribe",
    PUBLISH = "publish",
    CLAIM = "claim",
    UNCLAIM = "unclaim",
    STATE = "state",
    CONNECTED = "connected",
    DISCONNECTED = "disconnected",
    NAVIGATE = "navigate",
}

export interface LiveNavigationTarget {
    page: PageName;
    entityId?: string;
}

export interface LiveMessageGroup {
    claimedHeroes: [string, string][];
    colors: [string, string][];
    groupId: string;
    navigationTargets: [string, LiveNavigationTarget][];
}

export interface LiveConnectedMessage {
    message: LiveMessageType.CONNECTED;
    clientId: string;
    colors: { groupId: string, color: string }[];
}

export interface LiveDisconnectedMessage {
    message: LiveMessageType.DISCONNECTED;
    clientId: string;
}

export interface LiveStateMessage {
    message: LiveMessageType.STATE;
    groups: LiveMessageGroup[];
    ownId: string;
}

export interface LiveSubscribeMessage {
    message: LiveMessageType.SUBSCRIBE;
    groupIds: string[];
}

export interface LivePointerMessage {
    message: LiveMessageType.POINTER;
    groupId: string;
    color?: string;
    battleMapId: string;
    pos: [number, number];
}

export interface LivePublishMessage {
    message: LiveMessageType.PUBLISH;
    groupId: string;
    action: LiveAction;
    model: ModelName;
    id: string;
}

export interface LiveClaimMessage {
    message: LiveMessageType.CLAIM;
    groupId: string;
    heroId: string;
    clientId?: string;
}

export interface LiveUnclaimMessage {
    message: LiveMessageType.UNCLAIM;
    groupId: string;
    heroId: string;
    clientId?: string;
}

export interface LiveNavigationMessage {
    message: LiveMessageType.NAVIGATE;
    target: LiveNavigationTarget;
    groupId: string;
    clientId?: string;
}

export type LiveMessage =
    | LiveSubscribeMessage
    | LivePointerMessage
    | LivePublishMessage
    | LiveClaimMessage
    | LiveUnclaimMessage
    | LiveStateMessage
    | LiveConnectedMessage
    | LiveDisconnectedMessage
    | LiveNavigationMessage;

export function isLiveStateMessage(obj: any): obj is LiveStateMessage {
    if (typeof obj !== "object") { return false; }
    return "groups" in obj;
}

export function isLiveUnclaimMessage(obj: any): obj is LiveUnclaimMessage {
    if (typeof obj !== "object") { return false; }
    return "groupId" in obj;
}

export function isLiveClaimMessage(obj: any): obj is LiveClaimMessage {
    if (typeof obj !== "object") { return false; }
    return ("heroId" in obj) && ("groupId" in obj);
}

export function isLiveSubscribeMessage(obj: any): obj is LiveSubscribeMessage {
    if (typeof obj !== "object") { return false; }
    return "groupIds" in obj;
}

export function isLivePointerMessage(obj: any): obj is LivePointerMessage {
    if (typeof obj !== "object") { return false; }
    return ("groupId" in obj) && ("battleMapId" in obj) && ("pos" in obj) && Array.isArray(obj.pos);
}

export function isLiveNavigationMessage(obj: any): obj is LiveNavigationMessage {
    if (typeof obj !== "object") { return false; }
    return ("target" in obj) && ("groupId" in obj);
}

export function isLiveNavigationTarget(obj: any): obj is LiveNavigationTarget {
    if (typeof obj !== "object") { return false; }
    return ("page" in obj) && ("entityId" in obj);
}
