import * as WebSocket from "ws";
import { component } from "tsdi";
import { warn, info } from "winston";
import { bind } from "lodash-decorators";
import * as uuid from "uuid";
import { LiveGroup } from "./live-group";
import { LiveClient } from "./live-client";
import {
    LiveAction,
    ModelName,
    LiveMessageType,
    LiveMessage,
    isLiveSubscribeMessage,
    isLivePointerMessage,
    isLiveClaimMessage,
    isLiveUnclaimMessage,
    isLiveNavigationMessage,
} from "./live-messages";
import { intersection } from "ramda";

@component
export class LiveServer {
    private clients = new Map<string, LiveClient>();
    private liveGroups = new Map<string, LiveGroup>();

    public publish(groupId: string, action: LiveAction.RELOAD_PAGE);
    public publish(groupId: string, action: LiveAction, model: ModelName, id: string);
    @bind public publish(groupId: string, action: LiveAction, model?: ModelName, id?: string) {
        this.broadcast(groupId, { message: LiveMessageType.PUBLISH, groupId, action, model, id });
    }

    private broadcast(groupId: string, message: LiveMessage) {
        this.clients.forEach(client => {
            if (!client.groupIds.includes(groupId)) { return; }
            client.send(message);
        });
    }

    @bind private getGroup(id: string) {
        if (!this.liveGroups.has(id)) {
            this.liveGroups.set(id, new LiveGroup(id));
        }
        return this.liveGroups.get(id)!;
    }

    @bind private handleDisconnect(client: LiveClient) {
        this.clients.delete(client.id);
        this.liveGroups.forEach(group => {
            const heroId = group.claimedHeroes.get(client.id);
            if (heroId) {
                this.broadcast(group.id, {
                    message: LiveMessageType.UNCLAIM,
                    clientId: client.id,
                    groupId: group.id,
                    heroId,
                });
            }
            group.freeClient(client.id);
        });
        this.clients.forEach(otherClient => {
            if (intersection(otherClient.groupIds, client.groupIds).length > 0) {
                otherClient.send({ message: LiveMessageType.DISCONNECTED, clientId: client.id });
            }
        });
        info(`Client "${client.id} disconnected.`);
    }

    @bind private handleSubscribeMessage(message: LiveMessage, webSocket: WebSocket) {
        if (!isLiveSubscribeMessage(message)) { throw new Error("Invalid message."); }
        const { groupIds } = message;
        const client = new LiveClient(webSocket, groupIds, uuid.v4());
        this.clients.set(client.id, client);
        groupIds.map(this.getGroup).forEach(group => group.assignColor(client.id));
        const groups = groupIds.map(this.getGroup).map(group => ({
            claimedHeroes: Array.from(group.claimedHeroes.entries()),
            colors: Array.from(group.colors.entries()),
            groupId: group.id,
            navigationTargets: Array.from(group.navigationTargets.entries()),
        }));
        const colors = groupIds.map(this.getGroup).map(group => ({
            groupId: group.id,
            color: group.getColor(client.id),
        }));
        this.clients.forEach(otherClient => {
            if (intersection(otherClient.groupIds, client.groupIds).length > 0) {
                otherClient.send({ message: LiveMessageType.CONNECTED, clientId: client.id, colors });
            }
        });
        client.send({ message: LiveMessageType.STATE, groups, ownId: client.id });
        return client;
    }

    @bind private handlePointerMessage(client: LiveClient, message: LiveMessage) {
        if (!client || !isLivePointerMessage(message)) { throw new Error("Invalid message."); }
        const { groupId, battleMapId, pos } = message;
        const color = this.getGroup(groupId).getColor(client.id);
        this.broadcast(groupId, {
            message: LiveMessageType.POINTER,
            groupId,
            battleMapId,
            pos,
            color,
        });
    }

    @bind private handleClaimMessage(client: LiveClient, message: LiveMessage) {
        if (!isLiveClaimMessage(message)) { throw new Error("Invalid message."); }
        this.getGroup(message.groupId).claimHero(client.id, message.heroId);
        this.broadcast(message.groupId, { ...message, clientId: client.id });
    }

    @bind private handleUnclaimMessage(client: LiveClient, message: LiveMessage) {
        if (!isLiveUnclaimMessage(message)) { throw new Error("Invalid message."); }
        this.getGroup(message.groupId).unclaimHero(client.id);
        this.broadcast(message.groupId, { ...message, clientId: client.id });
    }

    @bind private handleNavigateMessage(client: LiveClient, message: LiveMessage) {
        if (!isLiveNavigationMessage(message)) { throw new Error("Invalid message."); }
        this.getGroup(message.groupId).navigateTo(client.id, message.target);
        this.broadcast(message.groupId, { ...message, clientId: client.id });
    }

    @bind public addClient(webSocket: WebSocket) {
        let client: LiveClient;
        webSocket.addEventListener("message", ({ data }) => {
            try {
                const obj: LiveMessage = JSON.parse(data);
                info(`Received message of type "${obj.message}" from client "${client ? client.id : "unknown"}`);
                switch (obj.message) {
                    case LiveMessageType.SUBSCRIBE: {
                        client = this.handleSubscribeMessage(obj, webSocket);
                        webSocket.addEventListener("close", () => this.handleDisconnect(client));
                        webSocket.addEventListener("error", () => this.handleDisconnect(client));
                        info(`Client "${client.id} subscribed with groups: ${obj.groupIds.join(", ")}`);
                        return;
                    }
                    case LiveMessageType.POINTER: return this.handlePointerMessage(client, obj);
                    case LiveMessageType.CLAIM: return this.handleClaimMessage(client, obj);
                    case LiveMessageType.UNCLAIM: return this.handleUnclaimMessage(client, obj);
                    case LiveMessageType.NAVIGATE: return this.handleNavigateMessage(client, obj);
                    default: {
                        warn(
                            `Unknown message of type "${obj.message}" ` +
                            `received from client "${client ? client.id : "unknown"}".`,
                        );
                        this.handleDisconnect(client);
                        webSocket.close();
                        return;
                    }
                }
            } catch (err) {
                warn(`Error parsing message from live websocket client: ${err.message}`);
                console.error(err);
            }
        });
    }
}
