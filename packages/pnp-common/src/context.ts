import { Group, Preset } from "./models";
import { ControllerValidation } from "./controllers";

export function isStringArray(arg: any): arg is string[] {
    if (!Array.isArray(arg)) { return false; }
    return arg.every(item => typeof item === "string");
}

export interface AuthToken {
    groupTokens?: string[];
    presetTokens?: string[];
}

export function isAuthToken(arg: any): arg is AuthToken {
    if (typeof arg !== "object") { return false; }
    if (Object.keys(arg).some(key => !["groupTokens", "presetTokens"].includes(key))) { return false; }
    if (arg.presetTokens && !isStringArray(arg.presetTokens)) { return false; }
    if (arg.groupTokens && !isStringArray(arg.groupTokens)) { return false; }
    return true;
}

export interface Context {
    validation: ControllerValidation;
    currentGroups(): Promise<Group[]>;
    currentPresets(): Promise<Preset[]>;
    hasPreset(id: string): Promise<boolean>;
    hasGroup(id: string): Promise<boolean>;
    isMaster(id: string): Promise<boolean>;
}
