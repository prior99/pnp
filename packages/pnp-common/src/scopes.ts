import { createScope } from "hyrest";

// General visibility.
export const ids = createScope();
export const world = createScope().include(ids);
export const owner = createScope().include(world);
export const upload = createScope();
// Group.
export const login = createScope();
export const signup = createScope().include(login);
export const getGroup = createScope().include(ids);
// Hero.
export const updateHero = createScope();
export const createHero = createScope().include(ids);
export const getHero = createScope().include(ids);
// TemplateItem.
export const updateTemplateItem = createScope();
export const createTemplateItem = createScope().include(ids, updateTemplateItem);
export const getTemplateItem = createScope().include(ids);
// Item.
export const updateItem = createScope();
export const createItem = createScope().include(ids, updateItem);
export const getItem = createScope().include(ids);
// Note.
export const updateNote = createScope();
export const createNote = createScope().include(ids);
export const getNote = createScope().include(ids);
// BattleMap.
export const updateBattleMap = createScope();
export const createBattleMap = createScope().include(ids);
export const getBattleMap = createScope().include(ids);
// Marker.
export const updateMarker = createScope();
export const createMarker = createScope().include(ids);
export const getMarker = createScope().include(ids);
// Area.
export const createArea = createScope().include(ids);
export const getArea = createScope().include(ids);
// Maneuver.
export const updateManeuver = createScope().include(ids);
export const createManeuver = createScope().include(ids, updateManeuver);
export const getManeuver = createScope().include(ids);
// LearnedManeuver.
export const createLearnedManeuver = createScope().include(ids);
export const getLearnedManeuver = createScope().include(ids);
// Preset.
export const createPreset = createScope();
export const loginPreset = createScope();
export const getPreset = createScope().include(ids);
// TemplateMonster.
export const updateTemplateMonster = createScope().include(ids);
export const createTemplateMonster = createScope().include(ids, updateTemplateMonster);
export const getTemplateMonster = createScope().include(ids);
// MonsterManeuver.
export const createMonsterManeuver = createScope().include(ids);
export const getMonsterManeuver = createScope().include(ids);
// Monster.
export const updateMonster = createScope();
export const createMonster = createScope().include(ids, updateMonster);
export const getMonster = createScope().include(ids);
// Tag.
export const updateTag = createScope();
export const createTag = createScope().include(ids, updateTag);
export const getTag = createScope().include(ids);
// HeroTag.
export const createHeroTag = createScope().include(ids);
export const getHeroTag = createScope().include(ids);
// BattleMapTag.
export const createBattleMapTag = createScope().include(ids);
export const getBattleMapTag = createScope().include(ids);
// JournalEntry.
export const updateJournalEntry = createScope();
export const createJournalEntry = createScope().include(ids, updateJournalEntry);
export const getJournalEntry = createScope().include(ids);
// MarkerItem.
export const updateMarkerItem = createScope();
export const createMarkerItem = createScope().include(ids, updateMarkerItem);
export const getMarkerItem = createScope().include(ids);
// StashItem.
export const updateStashItem = createScope();
export const createStashItem = createScope().include(ids, updateStashItem);
export const getStashItem = createScope().include(ids);
// Stash.
export const updateStash = createScope();
export const createStash = createScope().include(ids, updateStash);
export const getStash = createScope().include(ids);
