import { component, factory } from "tsdi";
import { Connection } from "typeorm";

@component
export class DatabaseFactory {
    @factory
    public getConnection(): Connection { return null; }
}
