import { component, factory } from "tsdi";
import { ConfigInterface } from "pnp-common";

@component
export class ConfigFactory {
    @factory({ name: "config" })
    public getConfig(): ConfigInterface { return null; }
}
