import * as React from "react";
import { PageName } from "pnp-common";

export type PathFactory = (...args: string[]) => string;

export enum Parent {
    GROUP = "group",
    PRESET = "preset",
}

export type HeroPane =
    | "general"
    | "skills"
    | "battle"
    | "inventory"
    | "notes"
    | "master";

export interface Route {
    path: PathFactory;
    pattern: string;
    component: React.ComponentClass;
    unauthorized?: false;
    navbar?: boolean;
    icon?: string;
    title?: string;
    masterOnly?: boolean;
    parent: Parent[];
    global?: boolean;
    livePage?: PageName;
}

export const routes: Route[] = [];

export function addRoute(route: Route) {
    routes.push(route);
}

export interface RouteProps<T> {
    readonly match: {
        readonly params: T;
        readonly url: string;
    };
}
