import { Field } from "hyrest-mobx";

interface FormFieldProps {
    error?: boolean;
}

export function formItem(field: Field<any>): FormFieldProps {
    if (field.invalid) {
        return { error: true };
    }
    return {};
}
