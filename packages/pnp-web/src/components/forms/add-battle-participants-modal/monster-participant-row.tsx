import * as React from "react";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import { external, inject } from "tsdi";
import { Image, Grid, Checkbox, Input } from "semantic-ui-react";
import { hasFields } from "hyrest-mobx";
import { StoreMonsters, StoreTemplateMonsters } from "../../../stores";
import * as css from "./add-battle-participants-modal.scss";

export interface MonsterParticipantRowProps {
    onParticipateChange: (participating: boolean) => void;
    participating: boolean;
    onInitiativeChange: (initiative: number) => void;
    initiative: number;
    id: string;
}

@observer @external @hasFields()
export class MonsterParticipantRow extends React.Component<MonsterParticipantRowProps> {
    @inject private monsters: StoreMonsters;
    @inject private templateMonsters: StoreTemplateMonsters;

    @computed private get monster() {
        return this.monsters.byId(this.props.id);
    }

    @computed private get templateMonster() {
        return this.templateMonsters.byId(this.monster.templateMonster.id);
    }

    @action.bound private handleParticipatingChange() {
        this.props.onParticipateChange(!this.props.participating);
    }

    @action.bound private handleInitiativeChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        const { value } = evt.currentTarget;
        this.props.onInitiativeChange(isNaN(Number(value)) ? 0 : Number(value));
    }

    public render() {
        return (
            <Grid.Row>
                <Grid.Column computer={1} mobile={4}>
                    <Image
                        className={css.icon}
                        inline
                        circular
                        size="mini"
                        src={this.templateMonster.imageUrl}
                    />
                </Grid.Column>
                <Grid.Column computer={4} mobile={12}>{this.templateMonster.name}</Grid.Column>
                <Grid.Column computer={2} mobile={4}>
                    <Checkbox
                        className={css.checkbox}
                        toggle
                        checked={this.props.participating}
                        onChange={this.handleParticipatingChange}
                    />
                </Grid.Column>
                <Grid.Column computer={2} mobile={4}>
                    <Input
                        className={css.input}
                        type="number"
                        onChange={this.handleInitiativeChange}
                        value={typeof this.props.initiative === "number" ? this.props.initiative : ""}
                        disabled={!this.props.participating}
                    />
                </Grid.Column>
            </Grid.Row>
        );
    }
}
