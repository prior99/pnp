import * as React from "react";
import { observer } from "mobx-react";
import { action, computed, observable, autorun } from "mobx";
import { external, inject, initialize } from "tsdi";
import { Modal, Grid, Button } from "semantic-ui-react";
import { hasFields } from "hyrest-mobx";
import { StoreHeroes, StoreBattleMaps, StoreMonsters } from "../../../stores";
import { StoreCurrentBattle } from "../../../stores/current-battle";
import { HeroParticipantRow } from "./hero-participant-row";
import { bind } from "lodash-decorators";
import { MonsterParticipantRow } from "./monster-participant-row";

@observer @external @hasFields()
export class AddBattleParticipantsModal extends React.Component<{ battleMapId: string }> {
    @inject private heroes: StoreHeroes;
    @inject private monsters: StoreMonsters;
    @inject private battleMaps: StoreBattleMaps;
    @inject private currentBattle: StoreCurrentBattle;

    @observable private initiativeHeroes = new Map<string, number>();
    @observable private initiativeMonsters = new Map<string, number>();

    @initialize
    protected initialize() {
        autorun(() => {
            this.allHeroes.forEach(hero => {
                if (hero.initiative !== null) {
                    this.initiativeHeroes.set(hero.id, hero.initiative);
                    return;
                }
                this.initiativeHeroes.delete(hero.id);
            });
            this.allMonsters.forEach(monster => {
                if (monster.initiative !== null) {
                    this.initiativeMonsters.set(monster.id, monster.initiative);
                    return;
                }
                this.initiativeMonsters.delete(monster.id);
            });
        });
    }

    @action.bound private handleClose() {
        this.currentBattle.addParticipantsModalVisible = false;
    }

    @bind public handleToggleHero(heroId: string) {
        return action(() => {
            if (!this.heroParticipates(heroId)) {
                this.initiativeHeroes.set(heroId, 0);
                return;
            }
            this.initiativeHeroes.delete(heroId);
        });
    }

    @bind public handleHeroInitiativeChange(heroId: string) {
        return action((value: number) => {
            if (!this.heroParticipates(heroId)) { return; }
            this.initiativeHeroes.set(heroId, value);
        });
    }

    @bind private heroParticipates(heroId: string) {
        return this.initiativeHeroes.has(heroId);
    }

    @bind public handleToggleMonster(monsterId: string) {
        return action(() => {
            if (!this.monsterParticipates(monsterId)) {
                this.initiativeMonsters.set(monsterId, 0);
                return;
            }
            this.initiativeMonsters.delete(monsterId);
        });
    }

    @bind public handleMonsterInitiativeChange(monsterId: string) {
        return action((value: number) => {
            if (!this.monsterParticipates(monsterId)) { return; }
            this.initiativeMonsters.set(monsterId, value);
        });
    }

    @bind private monsterParticipates(monsterId: string) {
        return this.initiativeMonsters.has(monsterId);
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.battleMapId);
    }

    @computed private get allHeroes() {
        return this.currentBattle.allHeroes(this.battleMap.id);
    }

    @computed private get allMonsters() {
        return this.currentBattle.allMonsters(this.battleMap.id);
    }

    @computed private get changedHeroes() {
        return this.allHeroes.filter(hero => {
            if (hero.initiative === null) { return this.initiativeHeroes.has(hero.id); }
            return this.initiativeHeroes.get(hero.id) !== hero.initiative;
        });
    }

    @computed private get changedMonsters() {
        return this.allMonsters.filter(monster => {
            if (monster.initiative === null) { return this.initiativeMonsters.has(monster.id); }
            return this.initiativeMonsters.get(monster.id) !== monster.initiative;
        });
    }

    @action.bound private async handleSubmit() {
        await Promise.all([
            ...this.changedMonsters.map(async monster => {
                if (!this.monsterParticipates(monster.id)) {
                    await this.monsters.update(monster.id, { initiative: null });
                    return;
                }
                await this.monsters.update(monster.id, { initiative: this.initiativeMonsters.get(monster.id) });
            }),
            ...this.changedHeroes.map(async hero => {
                if (!this.heroParticipates(hero.id)) {
                    await this.heroes.update(hero.id, { initiative: null });
                    return;
                }
                await this.heroes.update(hero.id, { initiative: this.initiativeHeroes.get(hero.id) });
            }),
        ]);
        this.currentBattle.addParticipantsModalVisible = false;
    }

    @computed private get valid() {
        return this.changedHeroes.length > 0 || this.changedMonsters.length > 0;
    }

    public render() {
        return (
            <Modal closeIcon open={this.currentBattle.addParticipantsModalVisible} onClose={this.handleClose}>
                <Modal.Header>Change participants</Modal.Header>
                <Modal.Content>
                    <Grid>
                        <Grid.Row style={{ fontWeight: "bold" }}>
                            <Grid.Column computer={5} mobile={16}>Hero / Monster</Grid.Column>
                            <Grid.Column computer={2} mobile={4}>Active</Grid.Column>
                            <Grid.Column computer={2} mobile={4}>Initiative</Grid.Column>
                        </Grid.Row>
                        {
                            this.allHeroes.map(hero => (
                                <HeroParticipantRow
                                    id={hero.id}
                                    onParticipateChange={this.handleToggleHero(hero.id)}
                                    onInitiativeChange={this.handleHeroInitiativeChange(hero.id)}
                                    participating={this.heroParticipates(hero.id)}
                                    initiative={this.initiativeHeroes.get(hero.id)}
                                />
                            ))
                        }
                        {
                            this.allMonsters.map(monster => (
                                <MonsterParticipantRow
                                    id={monster.id}
                                    onParticipateChange={this.handleToggleMonster(monster.id)}
                                    onInitiativeChange={this.handleMonsterInitiativeChange(monster.id)}
                                    participating={this.monsterParticipates(monster.id)}
                                    initiative={this.initiativeMonsters.get(monster.id)}
                                />
                            ))
                        }
                        <Grid.Row>
                            <Grid.Column computer={16} mobile={16}>
                                <Button
                                    disabled={!this.valid}
                                    onClick={this.handleSubmit}
                                    icon="check"
                                    content="Change participants"
                                    primary
                                    fluid
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Content>
            </Modal>
        );
    }
}
