import * as React from "react";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import { external, inject } from "tsdi";
import { Image, Grid, Checkbox, Input } from "semantic-ui-react";
import { hasFields } from "hyrest-mobx";
import { StoreHeroes } from "../../../stores";
import * as css from "./add-battle-participants-modal.scss";

export interface HeroParticipantRowProps {
    onParticipateChange: (participating: boolean) => void;
    participating: boolean;
    onInitiativeChange: (initiative: number) => void;
    initiative: number;
    id: string;
}

@observer @external @hasFields()
export class HeroParticipantRow extends React.Component<HeroParticipantRowProps> {
    @inject private heroes: StoreHeroes;

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @action.bound private handleParticipatingChange() {
        this.props.onParticipateChange(!this.props.participating);
    }

    @action.bound private handleInitiativeChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        const { value } = evt.currentTarget;
        this.props.onInitiativeChange(isNaN(Number(value)) ? 0 : Number(value));
    }

    public render() {
        return (
            <Grid.Row>
                <Grid.Column computer={1} mobile={4}>
                    <Image
                        className={css.icon}
                        inline
                        circular
                        size="mini"
                        src={this.hero.avatarUrl}
                    />
                </Grid.Column>
                <Grid.Column computer={4} mobile={12}>{this.hero.name}</Grid.Column>
                <Grid.Column computer={2} mobile={4}>
                    <Checkbox
                        className={css.checkbox}
                        toggle
                        checked={this.props.participating}
                        onChange={this.handleParticipatingChange}
                    />
                </Grid.Column>
                <Grid.Column computer={2} mobile={4}>
                    <Input
                        className={css.input}
                        type="number"
                        onChange={this.handleInitiativeChange}
                        value={typeof this.props.initiative === "number" ? this.props.initiative : ""}
                        disabled={!this.props.participating}
                    />
                </Grid.Column>
            </Grid.Row>
        );
    }
}
