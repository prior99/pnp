import * as React from "react";
import { ImageUpload } from "pnp-common";
import { Image, Form, Checkbox, Button, Input, SemanticSIZES, Icon } from "semantic-ui-react";
import { observer } from "mobx-react";
import { observable, action } from "mobx";
import { bind } from "lodash-decorators";
import * as css from "./upload-image-form.scss";

export interface UploadImageFormProps {
    onUpload?: (image: ImageUpload) => Promise<void>;
    url?: string;
    editable?: boolean;
    size?: SemanticSIZES;
}

@observer
export class UploadImageForm extends React.Component<UploadImageFormProps> {
    @observable private useUrl = true;
    @observable private uploading = false;
    @observable private url: string;
    @observable private editing = false;

    private uploadRef: HTMLInputElement;

    @bind public handleUploadRef(input: HTMLInputElement) {
        this.uploadRef = input;
    }

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        if (this.useUrl) {
            await this.handleUpload();
        } else {
            this.uploadRef.click();
        }
        return;
    }

    private async imageUpload(): Promise<ImageUpload> {
        const { useUrl, url } = this;
        if (useUrl) {
            return { url } as ImageUpload;
        }
        const { files } = this.uploadRef;
        const file = files.item(0);
        const content = await new Promise(resolve => {
            const reader = new FileReader();
            reader.addEventListener("load", async () => resolve(btoa(reader.result as string)));
            reader.readAsBinaryString(file);
        });
        return { content } as ImageUpload;
    }

    @action.bound private async handleUpload() {
        this.uploading = true;
        await this.props.onUpload(await this.imageUpload());
        if (this.uploadRef) {
            this.uploadRef.files = null;
        }
        this.uploading = false;
        this.editing = false;
    }

    @action.bound private handleEditClick() {
        this.editing = true;
    }

    @action.bound private handleCancelClick(evt: React.SyntheticEvent<HTMLButtonElement>) {
        evt.preventDefault();
        this.editing = false;
    }

    public render() {
        if (!this.editing) {
            return (
                <div className={css.wrapper}>
                    <Image
                        src={this.props.url}
                        size={this.props.size}
                    />
                    {
                        this.props.editable && this.props.onUpload && (
                            <Icon
                                name="pencil"
                                color="grey"
                                onClick={this.handleEditClick}
                                className={css.icon}
                            />
                        )
                    }
                </div>
            );
        }
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Field>
                    <label>Upload per URL</label>
                    <Checkbox
                        toggle
                        checked={this.useUrl}
                        onChange={() => this.useUrl = !this.useUrl}
                    />
                </Form.Field>
                {
                    this.useUrl && (
                        <Form.Field>
                            <label>URL</label>
                            <Input
                                value={this.url}
                                onChange={evt => this.url = evt.currentTarget.value}
                            />
                        </Form.Field>
                    )
                }
                <Form.Field>
                    {
                        !this.useUrl && (
                            <input
                                ref={this.handleUploadRef}
                                type="file"
                                style={{ display: "none" }}
                                onChange={this.handleUpload}
                            />
                        )
                    }
                    <Button
                        htmlType="submit"
                        icon="upload"
                        content="Upload"
                        disabled={this.uploading || (this.useUrl && !this.url)}
                        loading={this.uploading}
                        size="mini"
                    />
                    <Button
                        icon="cancel"
                        content="Cancel"
                        disabled={this.uploading}
                        onClick={this.handleCancelClick}
                        size="mini"
                    />
                </Form.Field>
            </Form>
        );
    }
}
