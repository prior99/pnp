import * as React from "react";
import { Button, Dropdown, Form, Input } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreTemplateItems, StoreStashs, StoreStashItems } from "../../../stores";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";
import { TemplateItem, Stash } from "pnp-common";

export interface AddStashItemFormProps {
    templateItemId?: string;
    stashId?: string;
    onAdd?: () => void;
}

@external @observer
export class AddStashItemForm extends React.Component<AddStashItemFormProps> {
    @inject private templateItems: StoreTemplateItems;
    @inject private stashs: StoreStashs;
    @inject private stashItems: StoreStashItems;

    @observable private stashId: string;
    @observable private templateItemId: string;
    @observable private quantity = "1";

    @action.bound private async handleStashChange(_, { value }: { value: string }) {
        this.stashId = value;
    }

    @action.bound private async handleTemplateItemChange(_, { value }: { value: string }) {
        this.templateItemId = value;
    }

    @action.bound private async handleAdd() {
        await this.stashItems.create({
            stash: { id: this.stashId || this.props.stashId } as Stash,
            templateItem: { id: this.templateItemId || this.props.templateItemId } as TemplateItem,
            quantity: Number(this.quantity),
        });
        this.quantity = "1";
        this.stashId = undefined;
        this.templateItemId = undefined;
        if (this.props.onAdd) { this.props.onAdd(); }
    }

    @action.bound private handleQuantityChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.quantity = evt.currentTarget.value;
    }

    @computed private get valid() {
        return this.quantity !== "" && !isNaN(Number(this.quantity)) &&
            Boolean(this.stashId || this.props.stashId) &&
            Boolean(this.templateItemId || this.props.templateItemId);
    }

    @computed get templateItem() {
        if (!this.props.templateItemId) { return; }
        return this.templateItems.byId(this.props.templateItemId);
    }

    @computed get stash() {
        if (!this.props.stashId) { return; }
        return this.stashs.byId(this.props.stashId);
    }

    @computed private get stashDropDownOptions() {
        if (!this.templateItem) { return; }
        const groupId = this.templateItem.group && this.templateItem.group.id;
        const presetId = this.templateItem.preset && this.templateItem.preset.id;
        return this.stashs.dropDownOptions(groupId, presetId);
    }

    @computed private get templateItemDropDownOptions() {
        if (!this.stash) { return; }
        const groupId = this.stash.group && this.stash.group.id;
        const presetId = this.stash.preset && this.stash.preset.id;
        return this.templateItems.dropDownOptions(groupId, presetId);
    }

    public render () {
        return (
            <Form onSubmit={this.handleAdd}>
                {
                    !this.props.templateItemId && (
                        <Form.Field>
                            <label>Item</label>
                            <Dropdown
                                placeholder="Item"
                                search
                                selection
                                value={this.templateItemId}
                                options={this.templateItemDropDownOptions}
                                onChange={this.handleTemplateItemChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    !this.props.stashId && (
                        <Form.Field>
                            <label>Stash</label>
                            <Dropdown
                                placeholder="Stash"
                                search
                                selection
                                value={this.stashId}
                                options={this.stashDropDownOptions}
                                onChange={this.handleStashChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    this.templateItem && this.templateItem.quantifiable && (
                        <Form.Field error={this.quantity === ""}>
                            <label>Quantity</label>
                            <Input
                                type="number"
                                placeholder="quantity"
                                value={this.quantity}
                                onChange={this.handleQuantityChange}
                            />
                        </Form.Field>
                    )
                }
                <Form.Field>
                    <Button
                        disabled={!this.valid}
                        icon="check"
                        color="green"
                        size="mini"
                        content="Add"
                        htmlType="submit"
                    />
                </Form.Field>
            </Form>
        );
    }
}
