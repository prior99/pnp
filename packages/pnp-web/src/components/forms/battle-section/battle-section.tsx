import * as React from "react";
import { external, inject } from "tsdi";
import { Grid } from "semantic-ui-react";
import { observable, computed, action } from "mobx";
import { BattleStepper, BattleStep, PlayerBattleMode } from "./battle-stepper";
import { BattleSubSectionProps } from "./section-props";
import { observer } from "mobx-react";
import { complement, equals, isNil, uniq, __, prop } from "ramda";
import { SectionEquipment } from "./section-equipment";
import { SectionAttackManeuvers } from "./section-attack-maneuvers";
import { SectionAttackRoll } from "./section-attack-roll";
import { SectionDefenseRoll } from "./section-defense-roll";
import { SectionResult } from "./section-result";
import { StoreHeroes } from "../../../stores";
import { SectionDefenseManeuvers } from "./section-defense-maneuver";
import { StoreTemplateItems, StoreItems } from "../../../stores";
import { combatDictionary } from "pnp-common";

export enum SelectedBattleTab {
    ATTACK = "attack",
    DEFENSE = "defense",
    INSTANT = "instant",
}

export interface BattleSectionProps {
    id: string;
    selectedBattleTab: SelectedBattleTab;
}

@external @observer
export class BattleSection extends React.Component<BattleSectionProps> {
    @inject private heroes: StoreHeroes;
    @inject private templateItems: StoreTemplateItems;
    @inject private items: StoreItems;

    @observable private activeStep = BattleStep.EQUIPMENT;
    @observable private activeEquipment: string[] = [];
    @observable private selectedAttackManeuver: string | undefined;
    @observable private selectedDefenseManeuver: string | undefined;
    @observable private selectedCombatTrait: "Awareness" | "Aim" | "Focus" | undefined;
    @observable private combatTraitValue: number | undefined;
    @observable private power: number | undefined;
    @observable private swiftness: number | undefined;
    @observable private tension: number | undefined;

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get playerBattleMode() {
        if (this.props.selectedBattleTab === SelectedBattleTab.ATTACK) { return PlayerBattleMode.ATTACKER; }
        return PlayerBattleMode.DEFENDER;
    }

    @computed private get subSectionProps(): BattleSubSectionProps {
        const {
            activeEquipment,
            combatTraitValue,
            power,
            selectedCombatTrait,
            selectedAttackManeuver,
            selectedDefenseManeuver,
            swiftness,
            tension,
        } = this;
        const { id } = this.props;
        return {
            activeEquipment,
            combatTraitValue,
            id,
            power,
            selectedCombatTrait,
            selectedAttackManeuver,
            selectedDefenseManeuver,
            swiftness,
            tension,
        };
    }

    @computed private get defaultBattleStep() {
        switch (this.playerBattleMode) {
            case PlayerBattleMode.DEFENDER: return BattleStep.DEFENSE_MANEUVER;
            case PlayerBattleMode.ATTACKER: return BattleStep.ATTACK_MANEUVER;
        }
    }

    @action.bound private handleEquipmentChange(id: string) {
        if (this.activeEquipment.includes(id)) {
            this.activeEquipment = this.activeEquipment.filter(complement(equals(id)));
        } else {
            this.activeEquipment.push(id);
        }
        const combatTraits = uniq(this.activeEquipment
            .map(itemId => this.items.byId(itemId))
            .map(item => this.templateItems.byId(item.templateItem.id))
            .map(templateItem => templateItem.suggestedCombatTrait)
            .filter(complement(isNil))
            .filter(complement(equals("swiftness"))))
            .map(prop(__, combatDictionary));

        if (combatTraits.length === 1) {
            this.handleSelectedCombatTraitChange(combatTraits[0] as "Awareness" | "Aim" | "Focus");
        }
    }

    @action.bound private handleAttackManeuverChange(id: string) {
        if (this.selectedAttackManeuver === id) { this.selectedAttackManeuver = undefined; }
        else { this.selectedAttackManeuver = id; }
    }

    @action.bound private handleDefenseManeuverChange(id: string) {
        if (this.selectedDefenseManeuver === id) { this.selectedDefenseManeuver = undefined; }
        else { this.selectedDefenseManeuver = id; }
    }

    @action.bound private handleReset() {
        this.activeStep = this.defaultBattleStep;
        this.selectedAttackManeuver = undefined;
        this.combatTraitValue = undefined;
        this.power = undefined;
        this.swiftness = undefined;
        this.tension = undefined;
    }

    @action.bound private handleTensionChange(value: number) {
        this.tension = value;
    }

    @action.bound private handleSwiftnessChange(value: number) {
        this.swiftness = value;
    }

    @action.bound private handleSelectedCombatTraitChange(value: "Awareness" | "Aim" | "Focus") {
        this.selectedCombatTrait = value;
    }

    @action.bound private handleCombatTraitValueChange(value: number) {
        this.combatTraitValue = value;
    }

    @action.bound private handlePowerChange(value: number) {
        this.power = value;
    }

    @action.bound private handleStepChange(step: BattleStep) {
        this.activeStep = step;
    }

    public componentDidUpdate(props: BattleSectionProps) {
        if (this.props.selectedBattleTab !== props.selectedBattleTab) {
            this.activeStep = this.defaultBattleStep;
        }
    }

    private renderSection() {
        const { activeStep, subSectionProps } = this;
        switch (activeStep) {
            case BattleStep.EQUIPMENT: return (
                <SectionEquipment
                    {...subSectionProps}
                    onEquipmentClick={this.handleEquipmentChange}
                />
            );
            case BattleStep.ATTACK_MANEUVER: return (
                <SectionAttackManeuvers
                    {...subSectionProps}
                    onManeuverClick={this.handleAttackManeuverChange}
                />
            );
            case BattleStep.ATTACK_ROLL: return (
                <SectionAttackRoll
                    {...subSectionProps}
                    onPowerChange={this.handlePowerChange}
                    onCombatTraitValueChange={this.handleCombatTraitValueChange}
                    onSelectedCombatTraitChange={this.handleSelectedCombatTraitChange}
                />
            );
            case BattleStep.DEFENSE_MANEUVER: return (
                <SectionDefenseManeuvers
                    {...subSectionProps}
                    onManeuverClick={this.handleDefenseManeuverChange}
                />
            );
            case BattleStep.DEFENSE_ROLL: return (
                <SectionDefenseRoll
                    {...subSectionProps}
                    onSwiftnessChange={this.handleSwiftnessChange}
                    onTensionChange={this.handleTensionChange}
                />
            );
            case BattleStep.RESULT: return (
                <SectionResult
                    {...subSectionProps}
                    onReset={this.handleReset}
                />
            );
        }
    }

    public render() {
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column mobile={16} computer={6}>
                        <BattleStepper
                            id={this.hero.id}
                            activeStep={this.activeStep}
                            onStepChange={this.handleStepChange}
                            selectedAttackManeuver={this.selectedAttackManeuver}
                            selectedDefenseManeuver={this.selectedDefenseManeuver}
                            activeEquipment={this.activeEquipment}
                            combatTrait={this.selectedCombatTrait}
                            combatTraitValue={this.combatTraitValue}
                            power={this.power}
                            swiftness={this.swiftness}
                            tension={this.tension}
                            mode={this.playerBattleMode}
                        />
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={10}>{this.renderSection()}</Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}
