
export interface BattleSubSectionProps {
    activeEquipment: string[];
    id: string;
    selectedCombatTrait: "Awareness" | "Aim" | "Focus";
    selectedAttackManeuver: string | undefined;
    selectedDefenseManeuver: string | undefined;
    combatTraitValue: number | undefined;
    power: number | undefined;
    swiftness: number | undefined;
    tension: number | undefined;
}
