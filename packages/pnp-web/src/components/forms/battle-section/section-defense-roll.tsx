import * as React from "react";
import { external } from "tsdi";
import { observer } from "mobx-react";
import { action } from "mobx";
import { Grid, Form, Input } from "semantic-ui-react";
import { BattleSubSectionProps } from "./section-props";
import { Mosaic } from "../../elements";
import { CardLearnedManeuver, CardItem } from "../../cards";

export interface SectionDefenseRollProps extends BattleSubSectionProps {
    onSwiftnessChange: (value: number) => void;
    onTensionChange: (value: number) => void;
}

@external @observer
export class SectionDefenseRoll extends React.Component<SectionDefenseRollProps> {
    @action.bound private handleSwiftnessChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        const value = Number(evt.currentTarget.value);
        if (isNaN(value)) { return; }
        this.props.onSwiftnessChange(value);
    }

    @action.bound private handleTensionChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        const value = Number(evt.currentTarget.value);
        if (isNaN(value)) { return; }
        this.props.onTensionChange(value);
    }

    public render() {
        const { tension, swiftness } = this.props;
        return (
            <>
                <p>
                    Now it's your enemy's turn. They'll have to tell you their Swiftness
                    and Tension. That'll determine the amount of damage you'll deal.
                                        </p>
                <Grid>
                    <Grid.Row>
                        <Mosaic size="large">
                            {
                                this.props.selectedDefenseManeuver && (
                                    <Grid.Column mobile={16} tablet={8} computer={4}>
                                        <CardLearnedManeuver id={this.props.selectedDefenseManeuver} />
                                    </Grid.Column>
                                )
                            }
                        </Mosaic>
                        <Mosaic size="large">
                            {
                                this.props.activeEquipment.map(itemId => (
                                    <Grid.Column mobile={16} tablet={8} computer={4}>
                                        <CardItem id={itemId} />
                                    </Grid.Column>
                                ))
                            }
                        </Mosaic>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Form>
                                <Form.Field>
                                    <label>Enemy Swiftness</label>
                                    <Input
                                        value={swiftness === undefined ? "" : String(swiftness)}
                                        onChange={this.handleSwiftnessChange}
                                        type="number"
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>Enemy Tension</label>
                                    <Input
                                        value={tension === undefined ? "" : String(tension)}
                                        onChange={this.handleTensionChange}
                                        type="number"
                                    />
                                </Form.Field>
                            </Form>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </>

        );
    }
}
