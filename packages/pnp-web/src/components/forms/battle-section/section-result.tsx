import * as React from "react";
import { external } from "tsdi";
import { observer } from "mobx-react";
import { computed } from "mobx";
import { List, Button } from "semantic-ui-react";
import { BattleSubSectionProps } from "./section-props";

export interface SectionResultProps extends BattleSubSectionProps{
    onReset: () => void;
}

@external @observer
export class SectionResult extends React.Component<SectionResultProps> {
    @computed private get numbersValid() {
        return !isNaN(this.combatTraitValue) &&
            !isNaN(this.power) &&
            !isNaN(this.tension) &&
            !isNaN(this.swiftness);
    }

    @computed private get combatTraitValue() {
        return Number(this.combatTraitValue);
    }

    @computed private get power() {
        return Number(this.power);
    }

    @computed private get tension() {
        return Number(this.tension);
    }

    @computed private get swiftness() {
        return Number(this.swiftness);
    }

    public renderNumbersString() {
        if (!this.numbersValid) { return ""; }
        const comparison = this.combatTraitValue === this.swiftness
            ? "equal to"
            : this.combatTraitValue > this.swiftness
                ? "greater than"
                : "less than";
        return (
            <>
                <p>
                    Your result of <b>{this.combatTraitValue}</b> for <b>{this.props.selectedCombatTrait}</b> was{" "}
                    {comparison} your enemy's result of <b>{this.swiftness}</b> for <b>Swiftness</b>.
                </p>
                {
                    this.combatTraitValue > this.swiftness ? (
                        <p>
                            So, <b>{this.power} (Power) - {this.tension} (Tension) ={" "}
                                {Math.max(0, this.power - this.tension)}</b> damage{" "}
                            is dealt.
                        </p>
                    ) : <p>Hence, no damage was dealt.</p>
                }
            </>
        );
    }

    public render() {
        return (
            <>
                <p>The damage dealt is calculated like this:</p>
                <List ordered>
                    <List.Item>
                        Some maneuvers or items (e.g. ranged combat) might have additional
                        conditions in order to succeed. Make sure your roll passes.
                                        </List.Item>
                    <List.Item>
                        If your relevant combat trait (Awareness, Focus or Aim) was
                                            <b>greated than</b> your enemy's swiftness, damage is dealt.
                        Otherwise, nothing happens. Your attack missed.
                                        </List.Item>
                    <List.Item>
                        Subtract your enemy's roll on <b>Tension</b> from your roll on
                                            <b>Power</b>. The result is the damage you've dealt.
                                        </List.Item>
                </List>
                {this.renderNumbersString()}
                <Button onClick={this.props.onReset} icon="cross">Reset</Button>
            </>
        );
    }
}
