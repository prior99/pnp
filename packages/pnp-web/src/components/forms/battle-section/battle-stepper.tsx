import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Step, List } from "semantic-ui-react";
import { bind } from "lodash-decorators";
import { action, computed } from "mobx";
import { StoreManeuvers, StoreTemplateItems, StoreItems, StoreLearnedManeuvers } from "../../../stores";

export enum BattleStep {
    EQUIPMENT = "equipment",
    ATTACK_MANEUVER = "attack maneuver",
    ATTACK_ROLL = "attack roll",
    DEFENSE_MANEUVER = "defense maneuver",
    DEFENSE_ROLL = "defense roll",
    RESULT = "result",
}

export enum PlayerBattleMode {
    ATTACKER = "attacker",
    DEFENDER = "defender",
}

export interface BattleStepperProps {
    id: string;
    activeStep: BattleStep;
    onStepChange: (step: BattleStep) => void;
    activeEquipment: string[];
    selectedAttackManeuver: string | undefined;
    selectedDefenseManeuver: string | undefined;
    combatTrait: string;
    combatTraitValue: number;
    power: number | undefined;
    swiftness: number | undefined;
    tension: number | undefined;
    mode: PlayerBattleMode;
}

@observer @external
export class BattleStepper extends React.Component<BattleStepperProps> {
    @inject private maneuvers: StoreManeuvers;
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private templateItems: StoreTemplateItems;
    @inject private items: StoreItems;

    @bind private handleClick(step: BattleStep) {
        return action(() => this.props.onStepChange(step));
    }

    @computed private get attackManeuver() {
        if (!this.props.selectedAttackManeuver) { return; }
        return this.maneuvers.byId(this.learnedManeuvers.byId(this.props.selectedAttackManeuver).maneuver.id);
    }

    @computed private get defenseManeuver() {
        if (!this.props.selectedDefenseManeuver) { return; }
        return this.maneuvers.byId(this.learnedManeuvers.byId(this.props.selectedDefenseManeuver).maneuver.id);
    }

    @computed private get equippedTemplateItems() {
        return this.props.activeEquipment
            .map(itemId => this.items.byId(itemId))
            .map(item => this.templateItems.byId(item.templateItem.id));
    }

    @computed private get defenseManeuverStepDone() {
        return Boolean(this.defenseManeuver);
    }

    @computed private get defenseRollStepDone() {
        return this.props.swiftness !== undefined && this.props.tension !== undefined;
    }

    @computed private get attackRollStepDone() {
        return Boolean(this.props.combatTrait) &&
            this.props.combatTraitValue !== undefined &&
            this.props.power !== undefined;
    }

    @computed private get attackmaneuverStepDone() {
        return Boolean(this.attackManeuver);
    }

    @computed private get equipmentStepDone() {
        return this.equippedTemplateItems.length > 0;
    }

    @computed private get allDone() {
        return this.defenseRollStepDone &&
            this.attackRollStepDone &&
            this.attackmaneuverStepDone &&
            this.equipmentStepDone;
    }

    @computed private get equipmentStepActive() {
        if (this.equipmentStepDisabled) { return false; }
        return this.props.activeStep === BattleStep.EQUIPMENT;
    }

    @computed private get attackManeuverStepActive() {
        if (this.attackManeuverStepDisabled) { return false; }
        return this.props.activeStep === BattleStep.ATTACK_MANEUVER;
    }

    @computed private get attackRollStepActive() {
        if (this.attackRollStepDisabled) { return false; }
        return this.props.activeStep === BattleStep.ATTACK_ROLL;
    }

    @computed private get defenseManeuverStepActive() {
        if (this.defenseManeuverStepDisabled) { return false; }
        return this.props.activeStep === BattleStep.DEFENSE_MANEUVER;
    }

    @computed private get defenseRollStepActiv() {
        if (this.defenseRollStepDisabled) { return false; }
        return this.props.activeStep === BattleStep.DEFENSE_ROLL;
    }

    @computed private get equipmentStepDisabled() {
        return false;
    }

    @computed private get attackManeuverStepDisabled() {
        switch (this.props.mode) {
            case PlayerBattleMode.ATTACKER: return false;
            case PlayerBattleMode.DEFENDER: return true;
        }
    }

    @computed private get attackRollStepDisabled() {
        switch (this.props.mode) {
            case PlayerBattleMode.ATTACKER: return false;
            case PlayerBattleMode.DEFENDER: return true;
        }
    }

    @computed private get defenseManeuverStepDisabled() {
        switch (this.props.mode) {
            case PlayerBattleMode.ATTACKER: return true;
            case PlayerBattleMode.DEFENDER: return false;
        }
    }

    @computed private get defenseRollStepDisabled() {
        switch (this.props.mode) {
            case PlayerBattleMode.ATTACKER: return true;
            case PlayerBattleMode.DEFENDER: return false;
        }
    }

    private renderAttackManeuverDescription() {
        if (this.props.mode === PlayerBattleMode.DEFENDER) { return "Your enemy selects a defensive maneuver."; }
        if (this.attackManeuver) {
            return this.attackManeuver.name;
        }
        return "Select an offensive maneuver";
    }

    private renderAttackRollDescription() {
        if (this.props.mode === PlayerBattleMode.DEFENDER) { return "Your enemy rolls on swiftness and tension."; }
        if (this.props.combatTrait || this.props.power) {
            return (
                <List>
                    {
                        this.props.combatTrait && <List.Item>
                            <b>{this.props.combatTrait}:</b> {
                                this.props.combatTraitValue !== undefined
                                    ? this.props.combatTraitValue
                                    : <span style={{ opacity: 0.6 }}>???</span>
                                }
                        </List.Item>
                    }
                    {
                        this.props.power && <List.Item>
                            <b>Power:</b> {this.props.power}
                        </List.Item>
                    }
                </List>
            );
        }
        return "Roll on the combat trait relevant for your weapon and power.";
    }

    private renderDefenseManeuverDescription() {
        if (this.props.mode === PlayerBattleMode.ATTACKER) { return "Your enemy selects an offensive maneuver."; }
        if (this.defenseManeuver) {
            return this.defenseManeuver.name;
        }
        return "Select a defensive maneuver";
    }

    private renderDefenseRollDescription() {
        if (this.props.mode === PlayerBattleMode.ATTACKER) { return "Your enemy rolls on swiftness and tension."; }
        if (this.props.swiftness !== undefined || this.props.tension !== undefined) {
            return (
                <List>
                    {
                        this.props.swiftness && <List.Item>
                            <b>Swiftness:</b> {this.props.swiftness}
                        </List.Item>
                    }
                    {
                        this.props.tension && <List.Item>
                            <b>Tension:</b> {this.props.tension}
                        </List.Item>
                    }
                </List>
            );
        }
        return "Roll on the swiftness and tension.";
    }

    private renderEquipmentDescription() {
        if (this.equippedTemplateItems.length === 0) { return "Select equipment"; }
        return (
            <p>{this.equippedTemplateItems.map(templateItem => templateItem.name).join(", ")}</p>
        );
    }

    private renderResultDescription() {
        return "The master calculates the amount of damge done.";
    }

    public render() {
        return (
            <Step.Group ordered fluid vertical>
                <Step
                    onClick={this.handleClick(BattleStep.EQUIPMENT)}
                    active={this.equipmentStepActive}
                    completed={this.equipmentStepDone}
                    disabled={this.equipmentStepDisabled}
                    title="Equipment"
                    description={this.renderEquipmentDescription()}
                />
                <Step
                    onClick={this.handleClick(BattleStep.ATTACK_MANEUVER)}
                    active={this.attackManeuverStepActive}
                    completed={this.attackmaneuverStepDone}
                    disabled={this.attackManeuverStepDisabled}
                    title="Attack - Maneuver"
                    description={this.renderAttackManeuverDescription()}
                />
                <Step
                    onClick={this.handleClick(BattleStep.ATTACK_ROLL)}
                    active={this.attackRollStepActive}
                    completed={this.attackRollStepDone}
                    disabled={this.attackRollStepDisabled}
                    title="Attack - Roll"
                    description={this.renderAttackRollDescription()}
                />
                <Step
                    onClick={this.handleClick(BattleStep.DEFENSE_MANEUVER)}
                    active={this.defenseManeuverStepActive}
                    completed={this.defenseManeuverStepDone}
                    disabled={this.defenseManeuverStepDisabled}
                    title="Defense - Maneuver"
                    description={this.renderDefenseManeuverDescription()}
                />
                <Step
                    onClick={this.handleClick(BattleStep.DEFENSE_ROLL)}
                    active={this.defenseRollStepActiv}
                    completed={this.defenseRollStepDone}
                    disabled={this.defenseRollStepDisabled}
                    title="Defense - Roll"
                    description={this.renderDefenseRollDescription()}
                />
                <Step
                    onClick={this.handleClick(BattleStep.RESULT)}
                    completed={this.allDone}
                    disabled
                    title="Result"
                    description={this.renderResultDescription()}
                />
            </Step.Group>
        );
    }
}
