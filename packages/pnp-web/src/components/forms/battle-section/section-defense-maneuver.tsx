import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { StoreLearnedManeuvers, StoreManeuvers } from "../../../stores";
import { computed, action } from "mobx";
import { CardLearnedManeuver } from "../../cards";
import { bind } from "lodash-decorators";
import { BattleSubSectionProps } from "./section-props";
import { Mosaic } from "../../elements";

export interface SectionDefenseManeuversProps extends BattleSubSectionProps {
    onManeuverClick: (id: string) => void;
}

@external @observer
export class SectionDefenseManeuvers extends React.Component<SectionDefenseManeuversProps> {
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private maneuvers: StoreManeuvers;

    @computed private get allManeuvers() {
        return this.learnedManeuvers.forHero(this.props.id)
            .sort((a, b) => {
                const maneuverA = this.maneuvers.byId(a.maneuver.id);
                const maneuverB = this.maneuvers.byId(b.maneuver.id);
                if (!maneuverA || !maneuverB) { return 0; }
                return maneuverA.name.localeCompare(maneuverB.name);
            });
    }

    @bind private handleManeuverClick(id: string) {
        return action(() => this.props.onManeuverClick(id));
    }

    public render() {
        return (
            <>
                <p>
                    Choose the maneuver you want to perform.
                    Don't forget to reduce your active stamina by its stamina cost.
                </p>
                <Mosaic size="large">
                    {
                        this.allManeuvers.map(learnedManeuver => {
                            const maneuver = this.maneuvers.byId(learnedManeuver.maneuver.id);
                            const faded = maneuver.classification && maneuver.classification !== "defensive";
                            return (
                                <CardLearnedManeuver
                                    onClick={this.handleManeuverClick(learnedManeuver.id)}
                                    id={learnedManeuver.id}
                                    highlighted={this.props.selectedDefenseManeuver === learnedManeuver.id}
                                    faded={faded}
                                />
                            );
                        })
                    }
                </Mosaic>
            </>

        );
    }
}
