import * as React from "react";
import { external } from "tsdi";
import { observer } from "mobx-react";
import { action } from "mobx";
import { Grid, Form, Dropdown, Input } from "semantic-ui-react";
import { CardLearnedManeuver, CardItem } from "../../cards";
import { BattleSubSectionProps } from "./section-props";
import { Mosaic } from "../../elements";

export interface SectionAttackRollProps extends BattleSubSectionProps {
    onSelectedCombatTraitChange: (selectedCombatTrait: "Awareness" | "Aim" | "Focus") => void;
    onCombatTraitValueChange: (value: number) => void;
    onPowerChange: (value: number) => void;
}

@external @observer
export class SectionAttackRoll extends React.Component<SectionAttackRollProps> {

    @action.bound private handleSelectedCombatTraitChange(_, { value }: { value: string }) {
        this.props.onSelectedCombatTraitChange(value as any);
    }

    @action.bound private handleCombatTraitValueChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        const value = Number(evt.currentTarget.value);
        if (isNaN(value)) { return; }
        this.props.onCombatTraitValueChange(value);
    }

    @action.bound private handlePowerChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        const value = Number(evt.currentTarget.value);
        if (isNaN(value)) { return; }
        this.props.onPowerChange(value);
    }

    public render() {
        const { selectedCombatTrait, combatTraitValue, power } = this.props;
        return (
            <>
                <p>
                    Select the combat trait that matches your weapon and maneuver,
                    roll the dice according to the rules and enter the results below.
                </p>
                <Grid>
                    <Grid.Row>
                        <Mosaic size="large">
                            {
                                this.props.selectedAttackManeuver && (
                                    <Grid.Column mobile={16} tablet={8} computer={4}>
                                        <CardLearnedManeuver id={this.props.selectedAttackManeuver} />
                                    </Grid.Column>
                                )
                            }
                        </Mosaic>
                        <Mosaic size="large">
                            {
                                this.props.activeEquipment.map(itemId => (
                                    <Grid.Column mobile={16} tablet={8} computer={4}>
                                        <CardItem id={itemId} />
                                    </Grid.Column>
                                ))
                            }
                        </Mosaic>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Form>
                                <Form.Group widths="equal">
                                    <Form.Field>
                                        <label>Relevant combat trait</label>
                                        <Dropdown
                                            search
                                            selection
                                            value={selectedCombatTrait}
                                            onChange={this.handleSelectedCombatTraitChange}
                                            options={[
                                                { value: "Awareness", text: "Awareness" },
                                                { value: "Aim", text: "Aim" },
                                                { value: "Focus", text: "Focus" },
                                            ]}
                                        />
                                    </Form.Field>
                                    {
                                        selectedCombatTrait && <Form.Field>
                                            <label>{selectedCombatTrait}</label>
                                            <Input
                                                value={combatTraitValue === undefined ? "" : String(combatTraitValue)}
                                                onChange={this.handleCombatTraitValueChange}
                                                type="number"
                                            />
                                        </Form.Field>
                                    }
                                </Form.Group>
                                <Form.Field>
                                    <label>Power</label>
                                    <Input
                                        value={power === undefined ? "" : String(power)}
                                        onChange={this.handlePowerChange}
                                        type="number"
                                    />
                                </Form.Field>
                            </Form>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </>
        );
    }
}
