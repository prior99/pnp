import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { StoreItems, StoreTemplateItems } from "../../../stores";
import { computed, action } from "mobx";
import { CardItem } from "../../cards";
import { bind } from "lodash-decorators";
import { BattleSubSectionProps } from "./section-props";
import { Mosaic } from "../../elements";

export interface SectionEquipmentProps extends BattleSubSectionProps {
    onEquipmentClick: (id: string) => void;
}

@external @observer
export class SectionEquipment extends React.Component<SectionEquipmentProps> {
    @inject private items: StoreItems;
    @inject private templateItems: StoreTemplateItems;

    @computed private get allActivatableItems() {
        return this.items.forHero(this.props.id)
            .filter(item => this.templateItems.byId(item.templateItem.id).canBeActive)
            .sort((a, b) => {
                const templateItemA = this.templateItems.byId(a.templateItem.id);
                const templateItemB = this.templateItems.byId(b.templateItem.id);
                if (!templateItemA || !templateItemB) { return 0; }
                return templateItemA.name.localeCompare(templateItemB.name);
            });
    }

    @bind private handleEquipmentClick(id: string) {
        return action(() => this.props.onEquipmentClick(id));
    }

    public render() {
        return (
            <>
                <p>
                    Choose the equipment you want to use.
                    Typically, you can do this at the beginning of the combat and then keep the same
                    equipment for its duration.
                </p>
                <Mosaic size="large">
                    {
                        this.allActivatableItems.map(item => {
                            return (
                                <CardItem
                                    onClick={this.handleEquipmentClick(item.id)}
                                    id={item.id}
                                    highlighted={this.props.activeEquipment.includes(item.id)}
                                />
                            );
                        })
                    }
                </Mosaic>
            </>
        );
    }
}
