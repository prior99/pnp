import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { StoreLearnedManeuvers, StoreManeuvers, StoreTemplateItems, StoreItems } from "../../../stores";
import { computed, action } from "mobx";
import { CardLearnedManeuver } from "../../cards";
import { bind } from "lodash-decorators";
import { BattleSubSectionProps } from "./section-props";
import { Mosaic } from "../../elements";

export interface SectionAttackManeuversProps extends BattleSubSectionProps {
    onManeuverClick: (id: string) => void;
}

@external @observer
export class SectionAttackManeuvers extends React.Component<SectionAttackManeuversProps> {
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private maneuvers: StoreManeuvers;
    @inject private templateItems: StoreTemplateItems;
    @inject private items: StoreItems;

    @computed private get allManeuvers() {
        return this.learnedManeuvers.forHero(this.props.id)
            .sort((a, b) => {
                const maneuverA = this.maneuvers.byId(a.maneuver.id);
                const maneuverB = this.maneuvers.byId(b.maneuver.id);
                if (!maneuverA || !maneuverB) { return 0; }
                return maneuverA.name.localeCompare(maneuverB.name);
            });
    }

    @computed private get allTemplateItems() {
        return this.props.activeEquipment
            .map(itemId => this.items.byId(itemId))
            .map(item => this.templateItems.byId(item.templateItem.id));
    }

    private wrongCombatTrait(maneuverId: string) {
        const maneuver = this.maneuvers.byId(maneuverId);
        if (!maneuver.suggestedCombatTrait) { return false; }
        if (this.allTemplateItems.length === 0) { return false; }
        return !this.allTemplateItems.some(templateItem => !templateItem.suggestedCombatTrait ||
            templateItem.suggestedCombatTrait === maneuver.suggestedCombatTrait,
        );
    }

    private wrongClassification(maneuverId: string) {
        const maneuver = this.maneuvers.byId(maneuverId);
        return maneuver.classification && maneuver.classification !== "offensive";
    }

    @bind private handleManeuverClick(id: string) {
        return action(() => this.props.onManeuverClick(id));
    }

    public render() {
        return (
            <>
                <p>
                    Choose the maneuver you want to perform.
                    Don't forget to reduce your active stamina by its stamina cost.
                </p>
                <Mosaic size="large">
                    {
                        this.allManeuvers.map(learnedManeuver => {
                            const { id } = learnedManeuver.maneuver;
                            const faded = this.wrongClassification(id) || this.wrongCombatTrait(id);
                            return (
                                <CardLearnedManeuver
                                    onClick={this.handleManeuverClick(learnedManeuver.id)}
                                    id={learnedManeuver.id}
                                    highlighted={this.props.selectedAttackManeuver === learnedManeuver.id}
                                    faded={faded}
                                />
                            );
                        })
                    }
                </Mosaic>
            </>

        );
    }
}
