import * as React from "react";
import { inject, external } from "tsdi";
import { computed, action } from "mobx";
import { StoreBattleMaps, StoreMarkers, StoreLogin, MapProps } from "../../../stores";
import { Button } from "semantic-ui-react";
import { observer } from "mobx-react";

export interface BattleMapFormProps {
    id: string;
    createPosition: MapProps["center"];
}

@external @observer
export class BattleMapForm extends React.Component<BattleMapFormProps> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private markers: StoreMarkers;
    @inject private login: StoreLogin;

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @action.bound private async handleAddMarker() {
        const isMasterView = Boolean(this.battleMap.preset || this.login.isMaster(this.battleMap.group.id));

        this.markers.createCustomMarker(this.battleMap.id, isMasterView, undefined, this.props.createPosition);
    }

    @action.bound private async handleEnterPointerMode() {
        this.battleMaps.togglePointerMode(this.battleMap.id);
    }

    @action.bound private async handleDescriptionToggle() {
        this.battleMaps.toggleDescription(this.battleMap.id);
    }

    public render() {
        return (
            <Button.Group>
                <Button icon="map marker alternate" onClick={this.handleAddMarker} />
                <Button
                    active={this.battleMaps.isPointerMode(this.props.id)}
                    icon="mouse pointer" onClick={this.handleEnterPointerMode}
                />
                <Button
                    active={this.battleMaps.isDescriptionActive(this.props.id)}
                    icon="info" onClick={this.handleDescriptionToggle}
                />
            </Button.Group>
        );
    }
}
