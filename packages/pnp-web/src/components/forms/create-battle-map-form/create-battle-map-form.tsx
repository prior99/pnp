import * as React from "react";
import { Button, Form } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreBattleMaps } from "../../../stores";
import { action, observable } from "mobx";
import { bind } from "lodash-decorators";

@external
export class CreateBattleMapForm extends React.Component<{ presetId?: string, groupId?: string }> {
    private uploadRef: HTMLInputElement;

    @inject private battleMaps: StoreBattleMaps;

    @observable private uploading = false;

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        this.uploadRef.click();
        return;
    }

    @action.bound private async handleUpload(event: React.SyntheticEvent<HTMLInputElement>) {
        this.uploading = true;
        const { files } = event.currentTarget;
        for (let i = 0; i < files.length; ++i) {
            const file = files.item(i);
            await new Promise(resolve => {
                const reader = new FileReader();
                reader.addEventListener("load", async () => {
                    await this.battleMaps.upload({
                        content: btoa(reader.result as string),
                        name: file.name,
                        preset: this.props.presetId && { id: this.props.presetId },
                        group: this.props.groupId && { id: this.props.groupId },
                    });
                    resolve();
                });
                reader.readAsBinaryString(file);
            });
        }
        event.currentTarget.files = null;
        this.uploading = false;
    }

    @bind public handleUploadRef(input: HTMLInputElement) {
        this.uploadRef = input;
    }

    public render () {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Field>
                    <input
                        ref={this.handleUploadRef}
                        type="file"
                        style={{ display: "none" }}
                        multiple
                        onChange={this.handleUpload}
                    />
                </Form.Field>
                <Form.Field>
                    <Button
                        htmlType="submit"
                        icon="upload"
                        content="Upload"
                        disabled={this.uploading}
                        loading={this.uploading}
                    />
                </Form.Field>
            </Form>
        );
    }
}
