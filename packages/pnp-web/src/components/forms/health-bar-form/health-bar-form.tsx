import * as React from "react";
import { Grid } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { computed, action } from "mobx";
import { observer } from "mobx-react";
import { AdjustableValueBar } from "../../elements";
import { StoreHeroes } from "../../../stores";

export interface HealthBarFormProps {
    heroId: string;
}

@external @observer
export class HealthBarForm extends React.Component<HealthBarFormProps> {
    @inject private heroes: StoreHeroes;

    @computed private get hero() {
        return this.heroes.byId(this.props.heroId);
    }

    @computed private get constitution() {
        return this.hero.combatRank("constitution");
    }

    @computed private get stamina() {
        return this.hero.combatRank("stamina");
    }

    @action.bound private handleHealthChange(proposedHealth: number): void {
        const {constitution} = this;

        const newDamage = Math.min(constitution, Math.max(0, constitution - proposedHealth));

        if (newDamage !== this.hero.damage) {
            this.heroes.update(this.props.heroId, {
                damage: newDamage,
            });
        }
    }

    @action.bound private handleStaminaChange(proposedStamina: number): void {
        const {stamina} = this;

        const newSpentStamina = Math.min(stamina, Math.max(0, stamina - proposedStamina));

        if (newSpentStamina !== this.hero.spentStamina) {
            this.heroes.update(this.props.heroId, {
                spentStamina: newSpentStamina,
            });
        }
    }

    public render(): JSX.Element {
        const {constitution, stamina} = this;

        return (
            <Grid columns={2} padded="horizontally">
                <Grid.Row>
                    <Grid.Column>
                        <AdjustableValueBar
                            label="Health"
                            total={constitution}
                            current={constitution - this.hero.damage}
                            color="red"
                            onChangeValue={this.handleHealthChange}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <AdjustableValueBar
                            label="Stamina"
                            total={stamina}
                            current={stamina - this.hero.spentStamina}
                            color="green"
                            onChangeValue={this.handleStaminaChange}
                            resettable
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}
