import * as React from "react";
import { inject, external } from "tsdi";
import { computed, action } from "mobx";
import {
    StoreBattleMaps,
    StoreHeroes,
    StoreMarkers,
    StoreMonsters,
    StoreTemplateMonsters,
    StoreStashs,
} from "../../../stores";
import { Dropdown, Button } from "semantic-ui-react";
import { observer } from "mobx-react";
import { bind } from "lodash-decorators";

export interface BattleMapMasterFormProps {
    id: string;
}

@external @observer
export class BattleMapMasterForm extends React.Component<BattleMapMasterFormProps> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private heroes: StoreHeroes;
    @inject private monsters: StoreMonsters;
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private markers: StoreMarkers;
    @inject private stashs: StoreStashs;

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @bind private handleHeroChange(value: string) {
        return action(async () => {
            await this.markers.createForHero(this.battleMap.id, value, [0, 0]);
        });
    }

    @bind private handleTemplateMonsterChange(value: string) {
        return action(async () => {
            await this.monsters.createWithMarker(value, this.battleMap.id);
        });
    }

    @bind private handleLootChange(value: string) {
        return action(async () => {
            await this.markers.createLootMarkerFromStash(this.battleMap.id, value);
        });
    }

    @computed private get templateMonsterDropDownOptions() {
        if (!this.battleMap) { return; }
        const groupId = this.battleMap.group && this.battleMap.group.id;
        const presetId = this.battleMap.preset && this.battleMap.preset.id;
        return [...this.templateMonsters.dropDownOptions(groupId, presetId)];
    }

    @computed private get heroDropDownOptions() {
        if (!this.battleMap) { return; }
        const groupId = this.battleMap.group && this.battleMap.group.id;
        const presetId = this.battleMap.preset && this.battleMap.preset.id;
        return [...this.heroes.dropDownOptions(groupId, presetId)];
    }

    @computed private get lootDropDownOptions() {
        if (!this.battleMap) { return; }
        const groupId = this.battleMap.group && this.battleMap.group.id;
        const presetId = this.battleMap.preset && this.battleMap.preset.id;
        return [...this.stashs.dropDownOptions(groupId, presetId)];
    }

    public render() {
        return (
            <Button.Group vertical>
                <Dropdown text="Add Person" icon="child" button labeled floating className="icon">
                    <Dropdown.Menu>
                        {
                            this.heroDropDownOptions.map(option => (
                                <Dropdown.Item onClick={this.handleHeroChange(option.value)}>
                                    {option.text}
                                </Dropdown.Item>
                            ))
                        }
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown text="Add Monster" icon="bug" button labeled floating className="icon">
                    <Dropdown.Menu>
                        {
                            this.templateMonsterDropDownOptions.map(option => (
                                <Dropdown.Item onClick={this.handleTemplateMonsterChange(option.value)}>
                                    {option.text}
                                </Dropdown.Item>
                            ))
                        }
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown text="Add Loot" icon="bug" button labeled floating className="icon">
                    <Dropdown.Menu>
                        {
                            this.lootDropDownOptions.map(option => (
                                <Dropdown.Item onClick={this.handleLootChange(option.value)}>
                                    {option.text}
                                </Dropdown.Item>
                            ))
                        }
                    </Dropdown.Menu>
                </Dropdown>
            </Button.Group>
        );
    }
}
