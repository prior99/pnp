import * as React from "react";
import { Button, Dropdown, Form } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreTemplateMonsters, StoreMonsters, StoreBattleMaps } from "../../../stores";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";

export interface AddMonsterFormProps {
    templateMonsterId?: string;
    battleMapId?: string;
    onAdd?: () => void;
}

@external @observer
export class AddMonsterForm extends React.Component<AddMonsterFormProps> {
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private battleMaps: StoreBattleMaps;
    @inject private monsters: StoreMonsters;

    @observable private battleMapId: string;
    @observable private templateMonsterId: string;

    @action.bound private async handleBattleMapChange(_, { value }: { value: string }) {
        this.battleMapId = value;
    }

    @action.bound private async handleTemplateMonsterChange(_, { value }: { value: string }) {
        this.templateMonsterId = value;
    }

    @action.bound private async handleAdd() {
        await this.monsters.createWithMarker(
            this.templateMonsterId || this.props.templateMonsterId,
            this.battleMapId || this.props.battleMapId,
        );
        this.battleMapId = undefined;
        this.templateMonsterId = undefined;
        if (this.props.onAdd) { this.props.onAdd(); }
    }

    @computed private get valid() {
        return Boolean(this.battleMapId || this.props.battleMapId) &&
            Boolean(this.templateMonsterId || this.props.templateMonsterId);
    }

    @computed get templateMonster() {
        if (!this.props.templateMonsterId) { return; }
        return this.templateMonsters.byId(this.props.templateMonsterId);
    }

    @computed get battleMap() {
        if (!this.props.battleMapId) { return; }
        return this.battleMaps.byId(this.props.battleMapId);
    }

    @computed private get battleMapDropDownOptions() {
        if (!this.templateMonster) { return; }
        const groupId = this.templateMonster.group && this.templateMonster.group.id;
        const presetId = this.templateMonster.preset && this.templateMonster.preset.id;
        return this.battleMaps.dropDownOptions(groupId, presetId);
    }

    @computed private get templateMonsterDropDownOptions() {
        if (!this.battleMap) { return; }
        const groupId = this.battleMap.group && this.battleMap.group.id;
        const presetId = this.battleMap.preset && this.battleMap.preset.id;
        return this.templateMonsters.dropDownOptions(groupId, presetId);
    }

    public render () {
        return (
            <Form onSubmit={this.handleAdd}>
                {
                    !this.props.templateMonsterId && (
                        <Form.Field>
                            <label>Monster</label>
                            <Dropdown
                                placeholder="Monster"
                                search
                                selection
                                value={this.templateMonsterId}
                                options={this.templateMonsterDropDownOptions}
                                onChange={this.handleTemplateMonsterChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    !this.props.battleMapId && (
                        <Form.Field>
                            <label>BattleMap</label>
                            <Dropdown
                                placeholder="Map"
                                search
                                selection
                                value={this.battleMapId}
                                options={this.battleMapDropDownOptions}
                                onChange={this.handleBattleMapChange}
                            />
                        </Form.Field>
                    )
                }
                <Form.Field>
                    <Button
                        disabled={!this.valid}
                        icon="check"
                        color="green"
                        size="mini"
                        content="Add"
                        htmlType="submit"
                    />
                </Form.Field>
            </Form>
        );
    }
}
