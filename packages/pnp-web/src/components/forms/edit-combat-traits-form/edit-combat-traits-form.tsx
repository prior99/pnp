import * as React from "react";
import {
    TemplateMonster,
    CombatTrait,
    getCombatTraitsForCategory,
    combatTraitCostPerRank,
    allCombatTraitCategories,
    combatDictionary,
} from "pnp-common";
import { FieldSimple } from "hyrest-mobx";
import { Form, Header } from "semantic-ui-react";
import { action } from "mobx";
import { observer } from "mobx-react";
import { formItem } from "../../../hyrest-semantic-ui";
import { NumberInput } from "../../elements";

export interface EditCombatTraitsFormProps {
    editActive?: boolean;
    templateMonster: FieldSimple<TemplateMonster>;
}

@observer
export class EditCombatTraitsForm extends React.Component<EditCombatTraitsFormProps> {
    private handleTraitChange(trait: CombatTrait) {
        return action((newValue: number) =>
            this.props.templateMonster.nested[trait].update(newValue * combatTraitCostPerRank(trait)),
        );
    }

    public render(): JSX.Element {
        const { templateMonster, editActive } = this.props;

        const battleValueFields = allCombatTraitCategories.map(category => {
            const traits = getCombatTraitsForCategory(category);

            const inputs = [];

            for (let i = 0; i < traits.length; i += 2) {
                const trait = traits[i];
                const name = combatDictionary[trait];
                const field = templateMonster.nested[trait];
                const value = field.value / combatTraitCostPerRank(trait);

                const nextTrait = traits[i + 1];
                const nextName = combatDictionary[nextTrait];
                const nextField = templateMonster.nested[nextTrait];
                const nextValue = nextField.value / combatTraitCostPerRank(nextTrait);

                inputs.push(
                    <Form.Group>
                        <Form.Field width="8" {...formItem(field)}>
                            <label style={{ color: "rgba(40, 40, 40, .3)", fontWeight: "normal" }}>{name}</label>
                            {editActive ? (
                                <NumberInput
                                    nonNegative
                                    value={value}
                                    onChange={this.handleTraitChange(trait)}
                                />
                            ) : value}
                        </Form.Field>
                        {
                            nextTrait && (
                                <Form.Field width="8" {...formItem(nextField)}>
                                    <label
                                        style={{ color: "rgba(40, 40, 40, .3)", fontWeight: "normal" }}
                                    >
                                        {nextName}
                                    </label>
                                    {editActive ? (
                                        <NumberInput
                                            nonNegative
                                            value={nextValue}
                                            onChange={this.handleTraitChange(nextTrait)}
                                        />
                                    ) : nextValue}
                                </Form.Field>
                            )
                        }
                    </Form.Group>,
                );
            }

            return (
                <>
                    <Header size="tiny">
                        {combatDictionary[category]}
                        <span style={{ opacity: 0.5, fontSize: ".75em", marginLeft: "1em" }}>
                            Estimated level {templateMonster.value.level(category).toFixed(1)}
                        </span>
                    </Header>
                    {inputs}
                </>
            );
        });
        const levels = templateMonster.value.levelRange();
        const minLevel = levels.min.toFixed(1);
        const maxLevel = levels.max.toFixed(1);

        return (
            <>
                {battleValueFields}
                {
                    editActive
                    ? `Overall level range: ${minLevel === maxLevel ? minLevel : `${minLevel} - ${maxLevel}`}`
                    : null
                }
            </>
        );
    }
}
