import * as React from "react";
import { Button, Dropdown, Form } from "semantic-ui-react";
import { inject, external } from "tsdi";
import {
    StoreLearnedManeuvers,
    StoreManeuvers,
    StoreHeroes,
    StoreTemplateMonsters,
    StoreMonsterManeuvers,
} from "../../../stores";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";
import { Maneuver, Hero, TemplateMonster } from "pnp-common";

export interface AddManeuverFormProps {
    maneuverId?: string;
    heroId?: string;
    templateMonsterId?: string;
    onAdd?: () => void;
    target?: "monster" | "hero";
}

enum EntityType {
    MONSTER, HERO,
}

@external @observer
export class AddManeuverForm extends React.Component<AddManeuverFormProps> {
    @inject private maneuvers: StoreManeuvers;
    @inject private heroes: StoreHeroes;
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private monsterManeuvers: StoreMonsterManeuvers;
    @inject private templateMonsters: StoreTemplateMonsters;

    @observable private heroId: string;
    @observable private templateMonsterId: string;
    @observable private maneuverId: string;

    @action.bound private async handleHeroChange(_, { value }: { value: string }) {
        this.heroId = value;
    }

    @action.bound private async handleTemplateMonsterChange(_, { value }: { value: string }) {
        this.templateMonsterId = value;
    }

    @action.bound private async handleTemplateItemChange(_, { value }: { value: string }) {
        this.maneuverId = value;
    }

    @computed get entityType() {
        if (this.props.heroId) { return EntityType.HERO; }
        if (this.props.templateMonsterId) { return EntityType.MONSTER; }
        if (this.props.target === "monster") { return EntityType.MONSTER; }
        if (this.props.target === "hero") { return EntityType.HERO; }
        return EntityType.HERO;
    }

    @action.bound private async handleAdd() {
        switch (this.entityType) {
            case EntityType.HERO: {
                await this.learnedManeuvers.create({
                    hero: { id: this.heroId || this.props.heroId } as Hero,
                    maneuver: { id: this.maneuverId || this.props.maneuverId } as Maneuver,
                });
                break;
            }
            case EntityType.MONSTER: {
                await this.monsterManeuvers.create({
                    templateMonster: { id: this.templateMonsterId || this.props.templateMonsterId } as TemplateMonster,
                    maneuver: { id: this.maneuverId || this.props.maneuverId } as Maneuver,
                });
                break;
            }
        }
        this.heroId = undefined;
        this.templateMonsterId = undefined;
        this.maneuverId = undefined;
        if (this.props.onAdd) { this.props.onAdd(); }
    }

    @computed private get valid() {
        switch (this.entityType) {
            case EntityType.HERO:
                return Boolean(this.heroId || this.props.heroId) && Boolean(this.maneuverId || this.props.maneuverId);
            case EntityType.MONSTER:
                return Boolean(
                    this.templateMonsterId || this.props.templateMonsterId,
                ) && Boolean(this.maneuverId || this.props.maneuverId);
        }
    }

    @computed get maneuver() {
        if (!this.props.maneuverId) { return; }
        return this.maneuvers.byId(this.props.maneuverId);
    }

    @computed get templateMonster() {
        if (!this.props.templateMonsterId) { return; }
        return this.templateMonsters.byId(this.props.templateMonsterId);
    }

    @computed get hero() {
        if (!this.props.heroId) { return; }
        return this.heroes.byId(this.props.heroId);
    }

    @computed private get templateMonsterDropDownOptions() {
        if (!this.maneuver) { return; }
        const groupId = this.maneuver.group && this.maneuver.group.id;
        const presetId = this.maneuver.preset && this.maneuver.preset.id;
        return this.templateMonsters.dropDownOptions(groupId, presetId)
            .filter(templateMonster => {
                return !this.monsterManeuvers.forMonster(templateMonster.value)
                    .some(monsterManeuver => monsterManeuver.maneuver.id === this.props.maneuverId);
            });
    }

    @computed private get heroDropDownOptions() {
        if (!this.maneuver) { return; }
        const groupId = this.maneuver.group && this.maneuver.group.id;
        const presetId = this.maneuver.preset && this.maneuver.preset.id;
        return this.heroes.dropDownOptions(groupId, presetId)
            .filter(hero => {
                return !this.learnedManeuvers.forHero(hero.value)
                    .some(learnedManeuver => learnedManeuver.maneuver.id === this.props.maneuverId);
            });
    }

    @computed private get maneuverDropDownOptions() {
        switch (this.entityType) {
            case EntityType.HERO: {
                if (!this.hero) { return; }
                const groupId = this.hero.group && this.hero.group.id;
                const presetId = this.hero.preset && this.hero.preset.id;
                return this.maneuvers.dropDownOptions(groupId, presetId)
                    .filter(maneuver => {
                        return !this.learnedManeuvers.forHero(this.hero.id)
                            .some(learnedManeuver => learnedManeuver.maneuver.id === maneuver.value);
                    });
            }
            case EntityType.MONSTER: {
                if (!this.templateMonster) { return; }
                const groupId = this.templateMonster.group && this.templateMonster.group.id;
                const presetId = this.templateMonster.preset && this.templateMonster.preset.id;
                return this.maneuvers.dropDownOptions(groupId, presetId)
                    .filter(maneuver => {
                        return !this.monsterManeuvers.forMonster(this.templateMonster.id)
                            .some(monsterManeuver => monsterManeuver.maneuver.id === maneuver.value);
                    });
            }
        }
    }

    public render() {
        return (
            <Form onSubmit={this.handleAdd}>
                {
                    !this.props.maneuverId && (
                        <Form.Field>
                            <label>Maneuver</label>
                            <Dropdown
                                placeholder="Maneuver"
                                search
                                selection
                                value={this.maneuverId}
                                options={this.maneuverDropDownOptions}
                                onChange={this.handleTemplateItemChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    !this.props.templateMonsterId && this.entityType === EntityType.MONSTER && (
                        <Form.Field>
                            <label>Monster</label>
                            <Dropdown
                                placeholder="Monster"
                                search
                                selection
                                value={this.templateMonsterId}
                                options={this.templateMonsterDropDownOptions}
                                onChange={this.handleTemplateMonsterChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    !this.props.heroId && this.entityType === EntityType.HERO && (
                        <Form.Field>
                            <label>Hero</label>
                            <Dropdown
                                placeholder="Hero"
                                search
                                selection
                                value={this.heroId}
                                options={this.heroDropDownOptions}
                                onChange={this.handleHeroChange}
                            />
                        </Form.Field>
                    )
                }
                <Form.Field>
                    <Button
                        disabled={!this.valid}
                        icon="check"
                        color="green"
                        size="mini"
                        content="Add"
                        htmlType="submit"
                    />
                </Form.Field>
            </Form>
        );
    }
}
