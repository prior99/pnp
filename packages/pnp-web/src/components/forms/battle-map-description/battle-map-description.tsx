import * as React from "react";
import { observer } from "mobx-react";
import { external, inject } from "tsdi";
import { computed, action } from "mobx";
import { History } from "history";
import { Content } from "../../layout";
import { PaneBattleMapDescription, PaneBattleMapMaster } from "../../panes";
import { StoreBattleMaps, StoreLogin } from "../../../stores";
import { Tab } from "semantic-ui-react";
import { PaneBattleMapBattle } from "../../panes/pane-battle-map-battle";
import { routeBattleMap } from "../../../pages";

export type BattleMapPane =
    | "general"
    | "battle"
    | "master";

@observer @external
export class BattleMapDescription extends React.Component<{ id: string, pane: BattleMapPane }> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;
    @inject private history: History;

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @computed private get panes() {
        const panes: { menuItem: { key: string; icon: string; name: string }; render: () => JSX.Element }[] = [
            {
                menuItem: {
                    key: "general",
                    icon: "map",
                    name: "Description",
                },
                render: () => <Content><PaneBattleMapDescription id={this.battleMap.id} /></Content>,
            },
        ];
        if (this.battleMap.group) {
            panes.push({
                menuItem: {
                    key: "battle",
                    icon: "bomb",
                    name: "Battle",
                },
                render: () => <Content><PaneBattleMapBattle id={this.battleMap.id} /></Content>,
            });
        }
        if (!this.battleMap.group || this.login.isMaster(this.battleMap.group.id)) {
            panes.push({
                menuItem: {
                    key: "master",
                    icon: "magic",
                    name: "Master",
                },
                render: () => <Content><PaneBattleMapMaster id={this.battleMap.id} /></Content>,
            });
        }
        return panes;
    }

    @action.bound private handlePaneChange(_, { activeIndex }: { activeIndex: number }) {
        const groupId = this.battleMap.group && this.battleMap.group.id;
        const presetId = this.battleMap.preset && this.battleMap.preset.id;
        const paneName = this.panes[activeIndex].menuItem.key;
        this.history.push(routeBattleMap.path(groupId, presetId, this.battleMap.id, paneName));
    }

    @computed private get activeIndex() {
        const selectedIndex = this.panes.findIndex(pane => pane.menuItem.key === this.props.pane);
        if (selectedIndex === -1) {
            return 0;
        }
        return selectedIndex;
    }

    public render() {
        return (
            <div style={{ paddingLeft: 10, paddingBottom: 20 }}>
                <Tab
                    onTabChange={this.handlePaneChange}
                    menu={{ secondary: true, pointing: true }}
                    panes={this.panes}
                    activeIndex={this.activeIndex}
                />
            </div>
        );
    }
}
