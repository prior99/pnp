import * as React from "react";
import { Button, Dropdown, Form, Input } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreTemplateItems, StoreHeroes, StoreItems } from "../../../stores";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";
import { TemplateItem, Hero } from "pnp-common";

export interface AddItemFormProps {
    templateItemId?: string;
    heroId?: string;
    onAdd?: () => void;
}

@external @observer
export class AddItemForm extends React.Component<AddItemFormProps> {
    @inject private templateItems: StoreTemplateItems;
    @inject private heroes: StoreHeroes;
    @inject private items: StoreItems;

    @observable private heroId: string;
    @observable private templateItemId: string;
    @observable private quantity = "1";

    @action.bound private async handleHeroChange(_, { value }: { value: string }) {
        this.heroId = value;
    }

    @action.bound private async handleTemplateItemChange(_, { value }: { value: string }) {
        this.templateItemId = value;
    }

    @action.bound private async handleAdd() {
        await this.items.create({
            hero: { id: this.heroId || this.props.heroId } as Hero,
            templateItem: { id: this.templateItemId || this.props.templateItemId } as TemplateItem,
            quantity: Number(this.quantity),
        });
        this.quantity = "1";
        this.heroId = undefined;
        this.templateItemId = undefined;
        if (this.props.onAdd) { this.props.onAdd(); }
    }

    @action.bound private handleQuantityChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.quantity = evt.currentTarget.value;
    }

    @computed private get valid() {
        return this.quantity !== "" && !isNaN(Number(this.quantity)) &&
            Boolean(this.heroId || this.props.heroId) &&
            Boolean(this.templateItemId || this.props.templateItemId);
    }

    @computed get templateItem() {
        if (!this.props.templateItemId) { return; }
        return this.templateItems.byId(this.props.templateItemId);
    }

    @computed get hero() {
        if (!this.props.heroId) { return; }
        return this.heroes.byId(this.props.heroId);
    }

    @computed private get heroDropDownOptions() {
        if (!this.templateItem) { return; }
        const groupId = this.templateItem.group && this.templateItem.group.id;
        const presetId = this.templateItem.preset && this.templateItem.preset.id;
        return this.heroes.dropDownOptions(groupId, presetId);
    }

    @computed private get templateItemDropDownOptions() {
        if (!this.hero) { return; }
        const groupId = this.hero.group && this.hero.group.id;
        const presetId = this.hero.preset && this.hero.preset.id;
        return this.templateItems.dropDownOptions(groupId, presetId);
    }

    public render () {
        return (
            <Form onSubmit={this.handleAdd}>
                {
                    !this.props.templateItemId && (
                        <Form.Field>
                            <label>Item</label>
                            <Dropdown
                                placeholder="Item"
                                search
                                selection
                                value={this.templateItemId}
                                options={this.templateItemDropDownOptions}
                                onChange={this.handleTemplateItemChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    !this.props.heroId && (
                        <Form.Field>
                            <label>Hero</label>
                            <Dropdown
                                placeholder="Hero"
                                search
                                selection
                                value={this.heroId}
                                options={this.heroDropDownOptions}
                                onChange={this.handleHeroChange}
                            />
                        </Form.Field>
                    )
                }
                {
                    this.templateItem && this.templateItem.quantifiable && (
                        <Form.Field error={this.quantity === ""}>
                            <label>Quantity</label>
                            <Input
                                type="number"
                                placeholder="quantity"
                                value={this.quantity}
                                onChange={this.handleQuantityChange}
                            />
                        </Form.Field>
                    )
                }
                <Form.Field>
                    <Button
                        disabled={!this.valid}
                        icon="check"
                        color="green"
                        size="mini"
                        content="Add"
                        htmlType="submit"
                    />
                </Form.Field>
            </Form>
        );
    }
}
