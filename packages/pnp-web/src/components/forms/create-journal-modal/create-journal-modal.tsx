import * as React from "react";
import { observer } from "mobx-react";
import { computed, action, autorun, observable } from "mobx";
import { external, inject, initialize } from "tsdi";
import { Form, Input, Button, Checkbox, SemanticICONS, Dropdown, Modal } from "semantic-ui-react";
import { hasFields, field, Field } from "hyrest-mobx";
import { JournalEntry, Hero } from "pnp-common";
import ReactQuill from "react-quill";
import { formItem } from "../../../hyrest-semantic-ui";
import { parentIds } from "../../../parent-id";
import { IconSelector } from "../../elements";
import { StoreJournalEntries, StoreLogin, StoreHeroes } from "../../../stores";
import { Live } from "../../../live";

@observer @external @hasFields()
export class CreateJournalModal extends React.Component<{ groupId?: string, presetId?: string }> {
    @inject private journalEntries: StoreJournalEntries;
    @inject private heroes: StoreHeroes;
    @inject private login: StoreLogin;
    @inject private live: Live;

    @field(JournalEntry) private journalEntryField: Field<JournalEntry>;
    @observable private heroId: string;

    @initialize protected initialize() {
        autorun(() => {
            const claimedHeroId = this.live.myClaimedHeroId(this.props.groupId);
            if (claimedHeroId) {
                this.heroId = claimedHeroId;
            }
        });
    }

    @computed private get allEntries() {
        const { groupId, presetId } = this.props;

        return this.journalEntries
            .sorted((a, b) => b.sortKey - a.sortKey)
            .filter(journalEntry => {
                if (journalEntry.group && groupId !== journalEntry.group.id) { return false; }
                if (journalEntry.preset && presetId !== journalEntry.preset.id) {
                    return false;
                }
                return true;
            });
    }

    @computed private get maxSortKey() {
        if (this.allEntries.length === 0) { return 0; }
        return this.allEntries
            .map(journalEntry => journalEntry.sortKey)
            .reduce((result, current) => current > result ? current : result);
    }

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        await this.journalEntries.create({
            ...this.journalEntryField.value,
            ...parentIds(this.props),
            sortKey: this.journalEntries.previousJournalSortKey === undefined
                ? this.maxSortKey + 1
                : this.journalEntries.previousJournalSortKey,
            hero: this.heroId ? { id: this.heroId } as Hero : undefined,
        });
        this.journalEntries.modalVisible = false;
        this.journalEntries.previousJournalSortKey = undefined;
    }

    @action.bound private async handleIconChange(icon: string) {
        this.journalEntryField.nested.icon.update(icon);
    }

    @action.bound private handleDescriptionChange(value: string) {
        this.journalEntryField.nested.description.update(value);
    }

    @action.bound private handleMasterOnlyClick() {
        this.journalEntryField.nested.masterOnly.update(!this.journalEntryField.nested.masterOnly.value as any);
    }

    @action.bound private handleSecretClick() {
        this.journalEntryField.nested.secret.update(!this.journalEntryField.nested.secret.value as any);
    }

    @action.bound private handleHeroChange(_, { value }: { value: string }) {
        this.heroId = value;
    }

    @action.bound private handleClose() {
        this.journalEntries.modalVisible = false;
    }

    @computed private get isMaster() {
        return this.props.presetId || this.login.isMaster(this.props.groupId);
    }

    public render() {
        const { groupId, presetId } = this.props;
        const { description, meta, masterOnly, secret, icon, summary } = this.journalEntryField.nested;
        return (
            <Modal closeIcon open={this.journalEntries.modalVisible} onClose={this.handleClose}>
                <Modal.Header>Create new entry</Modal.Header>
                <Modal.Content>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Field>
                            <label>Summary</label>
                            <Input
                                placeholder="Summary"
                                {...summary.reactInput}
                            />
                        </Form.Field>
                        <Form.Field {...formItem(name)}>
                            <label>Content</label>
                            <ReactQuill
                                style={{
                                    border: "1px solid rgba(34,36,38,.15)",
                                    borderRadius: ".28571429rem",
                                }}
                                value={description.value || ""}
                                onChange={this.handleDescriptionChange}
                                modules={{
                                    toolbar: [
                                        ["bold", "italic", "underline", "strike"],
                                        [{ header: 1 }, { header: 2 }],
                                        ["blockquote", "code-block"],
                                        [{ color: [] }, { background: [] }],
                                        [{ list: "ordered" }, { list: "bullet" }],
                                    ],
                                }}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Location and date</label>
                            <Input
                                placeholder="Location and date"
                                {...meta.reactInput}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Hero</label>
                            <Dropdown
                                selection
                                search
                                options={this.heroes.dropDownOptions(groupId, presetId)}
                                value={this.heroId}
                                onChange={this.handleHeroChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Icon</label>
                            <IconSelector
                                value={icon.value as SemanticICONS}
                                onSelect={this.handleIconChange}
                                content="Select icon"
                            />
                        </Form.Field>
                        {
                            this.isMaster && <>
                                <Form.Field>
                                    <label>Master only</label>
                                    <Checkbox
                                        label="Master only"
                                        checked={masterOnly.value}
                                        onChange={this.handleMasterOnlyClick}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>Secret</label>
                                    <Checkbox
                                        label="Secret"
                                        checked={secret.value}
                                        onChange={this.handleSecretClick}
                                    />
                                </Form.Field>
                            </>
                        }
                        <Form.Field>
                            <Button type="primary" htmlType="submit">Create</Button>
                        </Form.Field>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}
