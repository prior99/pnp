import * as React from "react";
import { observer } from "mobx-react";
import { external, initialize, inject } from "tsdi";
import {
    StoreVisiting,
    StoreBattleMaps,
    StoreLogin,
    StoreHeroes,
    StoreMonsters,
} from "../../../stores";
import { computed, action } from "mobx";
import { CurrentBattleWidget } from "../../cards";
import { Button } from "semantic-ui-react";
import { AddBattleParticipantsModal } from "../../forms";
import { StoreCurrentBattle } from "../../../stores/current-battle";

@observer
@external
export class PaneBattleMapBattle extends React.Component<{ id: string }> {
    @inject private visiting: StoreVisiting;
    @inject private battleMaps: StoreBattleMaps;
    @inject private currentBattle: StoreCurrentBattle;
    @inject private heroes: StoreHeroes;
    @inject private monsters: StoreMonsters;
    @inject private login: StoreLogin;

    @initialize
    protected initialize() {
        this.visiting.setPaneForBattleMap(this.battleMap.id, "battle");
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @computed private get isMaster() {
        return this.login.isMaster(this.battleMap.group.id);
    }

    @action.bound private handleHeroesAdd() {
        this.currentBattle.addParticipantsModalVisible = true;
    }

    @computed private get allHeroes() {
        return this.currentBattle.allHeroes(this.battleMap.id);
    }

    @computed private get allMonsters() {
        return this.currentBattle.allMonsters(this.battleMap.id);
    }

    @action.bound private async handleReset() {
        await Promise.all(
            this.allHeroes.map(hero =>
                this.heroes.update(hero.id, {
                    currentCombatTurns: 0,
                    initiative: null,
                }),
            ),
        );
        await Promise.all(
            this.allMonsters.map(monster =>
                this.monsters.update(monster.id, {
                    currentCombatTurns: 0,
                    initiative: null,
                }),
            ),
        );
    }

    public render() {
        return (
            <>
                <CurrentBattleWidget battleMapId={this.battleMap.id} />
                <AddBattleParticipantsModal battleMapId={this.battleMap.id} />
                {this.isMaster && (
                    <Button.Group style={{ marginTop: 20 }} fluid>
                        <Button
                            style={{ marginTop: 20 }}
                            icon="add"
                            onClick={this.handleHeroesAdd}
                            content="Change participants"
                        />
                        <Button
                            style={{ marginTop: 20 }}
                            icon="undo"
                            onClick={this.handleReset}
                            content="Reset"
                        />
                    </Button.Group>
                )}
            </>
        );
    }
}
