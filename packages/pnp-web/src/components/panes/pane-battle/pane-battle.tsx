import * as React from "react";
import { inject, external, initialize } from "tsdi";
import { StoreHeroes, StoreVisiting, StoreManeuvers, StoreLearnedManeuvers, StoreUi } from "../../../stores";
import { computed, observable, action } from "mobx";
import { observer } from "mobx-react";
import { Grid, Message, Icon, Tab } from "semantic-ui-react";
import { allCombatTraitCategories, combatDictionary } from "pnp-common";
import { CardCombatTraits, CardLearnedManeuver } from "../../cards";
import { BattleSection, SelectedBattleTab } from "../../forms";
import { bind } from "lodash-decorators";
import * as classNames from "classnames/bind";
import { Mosaic } from "../../elements";
import * as css from "./pane-battle.scss";
import { Content } from "../../layout";

const cx = classNames.bind(css);

@external @observer
export class PaneBattle extends React.Component<{ id: string }> {
    @inject private heroes: StoreHeroes;
    @inject private visiting: StoreVisiting;
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private maneuvers: StoreManeuvers;
    @inject private ui: StoreUi;

    @observable private selectedBattleTab = SelectedBattleTab.ATTACK;

    @initialize public init(): void {
        this.visiting.setPaneForHero(this.props.id, "battle");
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get totalUnspent() {
        return allCombatTraitCategories.reduce((sum: number, category) => {
            return sum + this.hero.unspentCombatTrait(category);
        }, 0);
    }

    @computed private get activeIndex() {
        const selectedIndex = this.panes.findIndex(pane => pane.menuItem.key === this.selectedBattleTab);
        if (selectedIndex === -1) {
            return 0;
        }
        return selectedIndex;
    }

    @computed private get allManeuvers() {
        return this.learnedManeuvers.forHero(this.props.id)
            .sort((a, b) => {
                const maneuverA = this.maneuvers.byId(a.maneuver.id);
                const maneuverB = this.maneuvers.byId(b.maneuver.id);
                if (!maneuverA || !maneuverB) { return 0; }
                return maneuverA.name.localeCompare(maneuverB.name);
            });
    }

    private isInstant(learnedManeuverId: string) {
        const learnedManeuver = this.learnedManeuvers.byId(learnedManeuverId);
        const maneuver = this.maneuvers.byId(learnedManeuver.maneuver.id);
        return maneuver.classification === "instant" || !maneuver.classification;
    }

    @computed private get panes() {
        return [
            {
                menuItem: {
                    key: SelectedBattleTab.ATTACK,
                    icon: "fire",
                    name: "Attack",
                },
                render: this.renderAttackOrDefensePane,
            },
            {
                menuItem: {
                    key: SelectedBattleTab.DEFENSE,
                    icon: "shield alternative",
                    name: "Defenese",
                },
                render: this.renderAttackOrDefensePane,
            },
            {
                menuItem: {
                    key: SelectedBattleTab.INSTANT,
                    icon: "bolt",
                    name: "Instant",
                },
                render: this.renderInstantPane,
            },
        ];
    }

    @action.bound private handlePaneChange(_, { activeIndex }: { activeIndex: number }) {
        this.selectedBattleTab = this.panes[activeIndex].menuItem.key;
    }

    @bind private renderAttackOrDefensePane() {
        return (
            <Content className={css.content}>
                <Grid className={css.content}>
                    {this.renderCombatTraits()}
                    <Grid.Row>
                        <Grid.Column>
                            <BattleSection id={this.props.id} selectedBattleTab={this.selectedBattleTab} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Content>
        );
    }

    @bind private renderInstantPane() {
        return (
            <Content className={css.content}>
                <Grid className={css.content}>
                    {this.renderCombatTraits()}
                    <Grid.Row>
                        <Grid.Column>
                            <Mosaic>
                                {
                                    this.allManeuvers.map(learnedManeuver => {
                                        const faded = !this.isInstant(learnedManeuver.id);
                                        return <CardLearnedManeuver id={learnedManeuver.id} faded={faded} />;
                                    })
                                }
                            </Mosaic>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Content>
        );
    }

    @bind private renderCombatTraits() {
        const { hero, totalUnspent } = this;
        if (!hero) { return null; }
        return (
            <>
                <Grid.Row>
                    <Grid.Column>
                        {
                            totalUnspent > 0 && (
                                <Message icon>
                                    <Icon name="plus" />
                                    <Message.Content>
                                        <Message.Header>
                                            {`${totalUnspent} unspent points`}
                                        </Message.Header>
                                        <Message.List>
                                            {
                                                allCombatTraitCategories.reduce((list, category) => {
                                                    const unspent = hero.unspentCombatTrait(category);
                                                    if (unspent > 0) {
                                                        list.push(
                                                            <Message.Item>
                                                                <b>{unspent}</b>
                                                                {" "}
                                                                points to spend on {combatDictionary[category]}.
                                                            </Message.Item>,
                                                        );
                                                    }
                                                    return list;
                                                }, [])
                                            }
                                        </Message.List>
                                    </Message.Content>
                                </Message>
                            )
                        }
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    {
                        allCombatTraitCategories.map((category, index) => {
                            return (
                                <Grid.Column mobile={16} computer={index === 0 ? 6 : 5}>
                                    <CardCombatTraits
                                        hero={this.hero.id}
                                        category={category}
                                        highlightOffensive={this.selectedBattleTab === SelectedBattleTab.ATTACK}
                                        highlightDefensive={this.selectedBattleTab === SelectedBattleTab.DEFENSE}
                                    />
                                </Grid.Column>
                            );
                        })
                    }
                </Grid.Row>
            </>
        );
    }

    public render() {
        const { hero } = this;
        if (!hero) { return null; }
        const noSidebar = !this.ui.visible;
        return (
            <>
                <Tab
                    activeIndex={this.activeIndex}
                    onTabChange={this.handlePaneChange}
                    menu={{
                        className: cx({
                            tabMenu: true,
                            noSidebar,
                        }),
                        secondary: true,
                        pointing: true,
                    }}
                    panes={this.panes}
                />
            </>
        );
    }
}
