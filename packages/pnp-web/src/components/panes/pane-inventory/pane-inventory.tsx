import * as React from "react";
import { Grid, Header, Form, Input } from "semantic-ui-react";
import { observer } from "mobx-react";
import { Mosaic, EditableText } from "../../elements";
import { CardItem } from "../../cards";
import { computed, action, observable, autorun } from "mobx";
import { external, inject, initialize } from "tsdi";
import { AddItemForm } from "../../forms";
import { StoreItems, StoreTemplateItems, StoreLogin, StoreHeroes, StoreVisiting } from "../../../stores";
import { Content } from "../../layout";

@external @observer
export class PaneInventory extends React.Component<{ id: string }> {
    @inject private items: StoreItems;
    @inject private templateItems: StoreTemplateItems;
    @inject private login: StoreLogin;
    @inject private heroes: StoreHeroes;
    @inject private visiting: StoreVisiting;

    @observable private query = "";
    @observable private money: number | undefined;

    @initialize public init(): void {
        this.visiting.setPaneForHero(this.props.id, "inventory");
        autorun(() => this.money = this.hero.money);
    }

    @computed private get allItems() {
        return this.items.forHero(this.props.id)
            .sort((a, b) => {
                const templateItemA = this.templateItems.byId(a.templateItem.id);
                const templateItemB = this.templateItems.byId(b.templateItem.id);
                if (!templateItemA || !templateItemB) { return 0; }
                return templateItemA.name.localeCompare(templateItemB.name);
            })
            .filter(item => {
                const templateItem = this.templateItems.byId(item.templateItem.id);
                if (!templateItem) { return true; }
                return templateItem.name.toLowerCase().includes(this.query.toLowerCase()) ||
                    templateItem.description.toLowerCase().includes(this.query.toLowerCase());
            });
    }

    @action.bound private handleQueryChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.query = evt.currentTarget.value;
    }

    @action.bound private async handleMoneyChange(value: string) {
        if (value === "") {
            this.money = undefined;
            return;
        }
        this.money = isNaN(Number(value)) ? this.money : Number(value);
    }

    @action.bound private async handleSetMoney() {
        await this.heroes.update(this.hero.id, { money: this.money || this.hero.money });
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    public render() {
        return (
            <Content>
                <Grid>
                    <Grid.Column mobile={16} computer={3}>
                        <Header>Money</Header>
                        <EditableText
                            value={this.money === undefined ? "" : `${this.money}`}
                            onChange={this.handleMoneyChange}
                            onFinish={this.handleSetMoney}
                        />
                        <Header>Search</Header>
                        <Form>
                            <Form.Field>
                                <label>Query</label>
                                <Input
                                    placeholder="Query"
                                    onChange={this.handleQueryChange}
                                    value={this.query}
                                />
                            </Form.Field>
                        </Form>
                        {
                            (!this.hero.group || this.login.isMaster(this.hero.group.id)) && (
                                <>
                                    <Header>Add item</Header>
                                    <AddItemForm heroId={this.props.id} />
                                </>
                            )
                        }
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={12}>
                        <Header>Inventory</Header>
                        <Mosaic>
                            {
                                this.allItems.map(item => <CardItem key={item.id} id={item.id} />)
                            }
                        </Mosaic>
                    </Grid.Column>
                </Grid>
            </Content>
        );
    }
}
