import * as React from "react";
import { initialize, inject, external } from "tsdi";
import { StoreTags, StoreHeroTags, StoreHeroes, StoreVisiting, StoreLogin } from "../../../stores";
import { action, autorun, computed, observable } from "mobx";
import { observer } from "mobx-react";
import { Header, Table, Icon, Button, Label, Dropdown, Checkbox } from "semantic-ui-react";
import { Hero, Tag, ImageUpload } from "pnp-common";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { EditableText, fromEditableTextField, TagLabel } from "../../elements";
import { Content } from "../../layout";
import { Live } from "../../../live";
import { extractParentIds } from "../../../parent-id";
import { bind } from "lodash-decorators";
import { UploadImageForm } from "../../forms/upload-image-form";

@external @observer @hasFields()
export class PaneGeneral extends React.Component<{ id: string }> {
    @inject private heroes: StoreHeroes;
    @inject private visiting: StoreVisiting;
    @inject private live: Live;
    @inject private tags: StoreTags;
    @inject private heroTags: StoreHeroTags;
    @inject private login: StoreLogin;

    @field(Hero) private heroField: FieldSimple<Hero>;

    @observable private editTags = false;
    @observable private tagsLoading = false;

    @initialize
    protected initialize() {
        this.visiting.setPaneForHero(this.props.id, "battle");
        autorun(() => this.hero && this.heroField.update(this.hero));
    }

    @action.bound private async handleNameChange(name: string) {
        await this.heroes.update(this.props.id, { name: this.heroField.nested.name.value });
    }

    @action.bound private async handleDescriptionChange(description: string) {
        await this.heroes.update(this.props.id, { description: this.heroField.nested.description.value });
    }

    @action.bound private async handleStatusChange() {
        await this.heroes.update(this.props.id, { isNpc: !this.heroField.nested.isNpc.value });
    }

    @action.bound private handleClaim() {
        this.live.claimHero(this.hero.id, this.hero.group.id);
    }

    @action.bound private handleUnclaim() {
        this.live.unclaimHero(this.hero.id, this.hero.group.id);
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.heroes.update(this.hero.id, {
            masterOnly: !this.hero.masterOnly,
        });
    }

    @action.bound private handleStartEditTags() { this.editTags = true; }

    @action.bound private handleFinishEditTags() { this.editTags = false; }

    @bind private handleUntag(tagId: string) {
        return action(async () => {
            this.tagsLoading = true;
            const { id } = this.heroTags.forHero(this.hero.id).find(heroTag => heroTag.tag.id === tagId);
            await this.heroTags.delete(id);
            this.tagsLoading = false;
        });
    }

    @action.bound private async handleAddTagChange(_: React.SyntheticEvent, { value }: { value: string }) {
        this.tagsLoading = true;
        const tag = await this.tags.getOrCreate(value, ...extractParentIds(this.hero));
        await this.heroTags.create({
            tag: { id: tag.id } as Tag,
            hero: { id: this.hero.id } as Hero,
        });
        this.tagsLoading = false;
    }

    @action.bound private async handleUpload(upload: ImageUpload) {
        await this.heroes.uploadAvatar(this.hero.id, upload);
    }

    @computed private get isMaster() {
        if (this.hero.preset) { return true; }
        return this.login.isMaster(this.hero.group.id);
    }

    @computed private get tagList() {
        return this.heroTags
            .forHero(this.hero.id)
            .map(heroTag => heroTag.tag.id)
            .map(this.tags.byId)
            .filter(tag => this.isMaster || !tag.masterOnly);
    }

    @computed private get dropdownOptions() {
        return this.tags.dropDownOptions(...extractParentIds(this.hero))
            .filter(option => !this.tagList.some(tag => tag.id === option.value));
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get isClaimed() {
        if (!this.hero.group) { return false; }
        return this.live.isHeroClaimed(this.hero.group.id, this.hero.id);
    }

    @computed private get claimedByMe() {
        if (!this.hero.group) { return false; }
        return this.live.isHeroClaimedByMe(this.hero.group.id, this.hero.id);
    }

    public render() {
        if (!this.heroField) { return null; }
        const { name, description, level, isNpc } = this.heroField.nested;
        return (
            <Content>
                <Table basic="very">
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell style={{ width: 200 }}>
                                <Header as="h4">Name</Header>
                            </Table.Cell>
                            <Table.Cell>
                                <EditableText
                                    {...fromEditableTextField(name)}
                                    onFinish={this.handleNameChange}
                                />
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell style={{ width: 200 }}>
                                <Header as="h4">Avatar</Header>
                            </Table.Cell>
                            <Table.Cell>
                                <UploadImageForm
                                    onUpload={this.handleUpload}
                                    url={this.hero.avatarUrl}
                                    editable
                                    size="small"
                                />
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Header as="h4">Description</Header>
                            </Table.Cell>
                            <Table.Cell>
                                <EditableText
                                    area
                                    {...fromEditableTextField(description)}
                                    onFinish={this.handleDescriptionChange}
                                    label="Description"
                                />
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Header as="h4">Level</Header>
                            </Table.Cell>
                            <Table.Cell>
                                {level.value}
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <Header as="h4">Status</Header>
                            </Table.Cell>
                            <Table.Cell>
                                {isNpc.value ? "NPC" : "Hero"}
                                {
                                    this.isMaster && (
                                        <Icon
                                            size="small"
                                            name="exchange"
                                            color="grey"
                                            link
                                            onClick={this.handleStatusChange}
                                        />
                                    )
                                }
                            </Table.Cell>
                        </Table.Row>
                        {
                            this.isMaster && <Table.Row>
                                <Table.Cell>
                                    <Header as="h4">Master only</Header>
                                </Table.Cell>
                                <Table.Cell>
                                    <Checkbox
                                        label="Master only"
                                        onClick={this.handleMasterOnlyChange}
                                        toggle
                                        checked={this.hero.masterOnly}
                                    />
                                </Table.Cell>
                            </Table.Row>

                        }
                        <Table.Row>
                            <Table.Cell>
                                <Header as="h4">Tags</Header>
                            </Table.Cell>
                            <Table.Cell>
                                <Label.Group>
                                    {
                                        this.tagList.map(tag => (
                                            <TagLabel
                                                key={tag.id}
                                                id={tag.id}
                                                onRemove={this.handleUntag(tag.id)}
                                                editing={this.editTags}
                                            />
                                        ))
                                    }
                                    {
                                        this.isMaster && (
                                            !this.editTags ? (
                                                <Icon
                                                    name="pencil"
                                                    color="grey"
                                                    link
                                                    onClick={this.handleStartEditTags}
                                                />
                                            ) : (
                                                    <Icon
                                                        name="checkmark"
                                                        color="grey"
                                                        link
                                                        onClick={this.handleFinishEditTags}
                                                    />
                                                )
                                        )
                                    }
                                </Label.Group>
                                {
                                    this.editTags && <>
                                        <Dropdown
                                            placeholder="Add Tag"
                                            search
                                            selection
                                            selectOnNavigation={false}
                                            selectOnBlur={false}
                                            options={this.dropdownOptions}
                                            onChange={this.handleAddTagChange}
                                            loading={this.tagsLoading}
                                            disabled={this.tagsLoading}
                                            value={undefined}
                                            closeOnChange={false}
                                            allowAdditions
                                        />{" "}
                                    </>
                                }

                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
                {
                    this.isClaimed
                        ? <Button
                            disabled={!this.claimedByMe || Boolean(this.hero.preset)}
                            content="Unclaim"
                            onClick={this.handleUnclaim}
                            icon="hand paper outline"
                        />
                        : <Button content="Claim" onClick={this.handleClaim} icon="hand rock outline" />
                }
            </Content>
        );
    }
}
