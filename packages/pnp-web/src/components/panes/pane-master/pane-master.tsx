import * as React from "react";
import { History } from "history";
import { computed, action, observable } from "mobx";
import { inject, external, initialize } from "tsdi";
import { StoreHeroes, StoreVisiting } from "../../../stores";
import { observer } from "mobx-react";
import { CardSkillDifficulty } from "../../cards";
import { Button, Card, Ref, Popup } from "semantic-ui-react";
import { AddManeuverForm } from "../../forms";
import { bind } from "lodash-decorators";
import { routeHero, routeGroupDashboard, routePresetDashboard } from "../../../pages";
import { Content } from "../../layout";

@external @observer
export class PaneMaster extends React.Component<{ id: string }> {
    @inject private heroes: StoreHeroes;
    @inject private visiting: StoreVisiting;
    @inject private history: History;

    @observable private addingManeuver = false;
    @observable private archiving = false;
    @observable private deleting = false;

    private maneuverButtonRef: HTMLButtonElement;
    private archiveRef: HTMLButtonElement;
    private deleteRef: HTMLButtonElement;

    @initialize public init(): void {
        this.visiting.setPaneForHero(this.props.id, "master");
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @action.bound private async handleClone() {
        const newHero = await this.heroes.clone(this.hero.id);
        const { group, preset } = this.hero;
        this.history.push(routeHero.path(group && group.id, preset && preset.id, newHero.id));
    }

    @action.bound private async handleLevelUp() {
        await this.heroes.update(this.hero.id, {
            level: Number(this.hero.level) + 1,
        });
    }

    @action.bound private handleManeuverStart() {
        this.addingManeuver = true;
    }

    @action.bound private handleManeuver() {
        this.addingManeuver = false;
    }

    @action.bound private handleManeuverClose() {
        this.addingManeuver = false;
    }

    @bind public handleManeuverButtonRef(button: HTMLButtonElement) {
        this.maneuverButtonRef = button;
    }

    @bind public handleArchiveRef(button: HTMLButtonElement) {
        this.archiveRef = button;
    }

    @bind public handleDeleteRef(button: HTMLButtonElement) {
        this.deleteRef = button;
    }

    @action.bound private async handleArchive() {
        if (this.hero.group) { this.history.push(routeGroupDashboard.path(this.hero.group.id)); }
        if (this.hero.preset) { this.history.push(routePresetDashboard.path(undefined, this.hero.preset.id)); }
        await this.heroes.archive(this.hero.id);
        this.archiving = false;
    }

    @action.bound private async handleDelete() {
        if (this.hero.group) { this.history.push(routeGroupDashboard.path(this.hero.group.id)); }
        if (this.hero.preset) { this.history.push(routePresetDashboard.path(undefined, this.hero.preset.id)); }
        await this.heroes.delete(this.hero.id);
        this.deleting = false;
    }

    @action.bound private handleArchiveStart() {
        this.archiving = true;
    }

    @action.bound private handleArchiveClose() {
        this.archiving = false;
    }

    @action.bound private handleDeleteStart() {
        this.deleting = true;
    }

    @action.bound private handleDeleteClose() {
        this.deleting = false;
    }

    public render() {
        if (!this.hero) { return null; }
        return (
            <Content>
                <Card fluid>
                    <Card.Content>
                        <Card.Header>Actions</Card.Header>
                        <Button.Group style={{ marginBottom: 10 }}>
                            <Button
                                color="green"
                                content="Award Level"
                                icon="star"
                                onClick={this.handleLevelUp}
                                label={`Currently: ${this.hero.level}`}
                            />
                        </Button.Group>
                        <br />
                        <Button.Group>
                            <Ref innerRef={this.handleManeuverButtonRef}>
                                <Button
                                    icon="student"
                                    onClick={this.handleManeuverStart}
                                    content="Teach maneuver"
                                />
                            </Ref>
                            <Popup
                                open={this.addingManeuver}
                                context={this.maneuverButtonRef}
                                onClose={this.handleManeuverClose}
                            >
                                <Popup.Header>Teach maneuver</Popup.Header>
                                <Popup.Content>
                                    <AddManeuverForm
                                        onAdd={this.handleManeuver}
                                        heroId={this.props.id}
                                    />
                                </Popup.Content>
                            </Popup>
                            <Button
                                content="Clone"
                                icon="clone"
                                onClick={this.handleClone}
                            />
                            <Ref innerRef={this.handleArchiveRef}>
                                <Button
                                    icon="archive"
                                    content="Archive"
                                    onClick={this.handleArchiveStart}
                                />
                            </Ref>
                            <Popup
                                flowing
                                hoverable
                                context={this.archiveRef}
                                position="left center"
                                open={this.archiving}
                                onClose={this.handleArchiveClose}
                            >
                                <Button
                                    icon="checkmark"
                                    color="red"
                                    onClick={this.handleArchive}
                                    content="Archive"
                                    size="mini"
                                />
                            </Popup>
                            <Ref innerRef={this.handleDeleteRef}>
                                <Button
                                    icon="trash alternative"
                                    content="Delete permanently"
                                    onClick={this.handleDeleteStart}
                                />
                            </Ref>
                            <Popup
                                flowing
                                hoverable
                                context={this.deleteRef}
                                position="left center"
                                open={this.deleting}
                                onClose={this.handleDeleteClose}
                            >
                                <Button
                                    icon="checkmark"
                                    color="red"
                                    onClick={this.handleDelete}
                                    content="Delete forever"
                                    size="mini"
                                />
                            </Popup>
                        </Button.Group>
                    </Card.Content>
                </Card>
                <CardSkillDifficulty id={this.hero.id} />
            </Content>
        );
    }
}
