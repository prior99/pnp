import * as React from "react";
import { Header, Button } from "semantic-ui-react";
import { external, inject, initialize } from "tsdi";
import { observer } from "mobx-react";
import { computed, action } from "mobx";
import { hasFields } from "hyrest-mobx";
import { StoreNotes, StoreVisiting, StoreHeroes, StoreLogin } from "../../../stores";
import { CardNote } from "../../cards";
import { Mosaic } from "../../elements";
import { Hero } from "pnp-common";
import { Content } from "../../layout";

@external @observer @hasFields()
export class PaneNotes extends React.Component<{id: string }> {
    @inject private notes: StoreNotes;
    @inject private visiting: StoreVisiting;
    @inject private heroes: StoreHeroes;
    @inject private login: StoreLogin;

    @initialize public init(): void {
        this.visiting.setPaneForHero(this.props.id, "notes");
    }

    @computed private get allNotes() {
        return this.notes.forHero(this.props.id)
            .filter(note => !note.masterOnly || this.isMaster)
            .sort((a, b) => {
                return a.created.getTime() - b.created.getTime();
            });
    }

    @action.bound private handleAdd() {
        this.notes.create({
            hero: { id: this.props.id } as Hero,
        });
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get isMaster() {
        return Boolean(this.hero.preset) || this.login.isMaster(this.hero.group.id);
    }

    public render () {
        return (
            <Content>
                <Header>Notes</Header>
                <Mosaic size="large">
                    {this.allNotes.map(note => <CardNote id={note.id} key={note.id} />)}
                </Mosaic>
                <Button icon="add" color="green" onClick={this.handleAdd} content="Create new note" />
            </Content>
        );
    }
}
