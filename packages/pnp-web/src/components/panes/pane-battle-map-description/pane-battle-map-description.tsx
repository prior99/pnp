import * as React from "react";
import { external, inject, initialize } from "tsdi";
import { observer } from "mobx-react";
import { StoreBattleMaps, StoreLogin, StoreTags, StoreBattleMapTags, StoreVisiting } from "../../../stores";
import { computed, autorun, action, observable } from "mobx";
import { EditableText, fromEditableTextField, TagLabel } from "../../elements";
import { hasFields, field, FieldSimple } from "hyrest-mobx";
import { BattleMap, Tag } from "pnp-common";
import * as css from "./pane-battle-map-description.scss";
import { Label, Icon, Dropdown } from "semantic-ui-react";
import { extractParentIds } from "../../../parent-id";
import { bind } from "lodash-decorators";

@observer @external @hasFields()
export class PaneBattleMapDescription extends React.Component<{ id: string }> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;
    @inject private tags: StoreTags;
    @inject private battleMapTags: StoreBattleMapTags;
    @inject private visiting: StoreVisiting;

    @observable private editTags = false;
    @observable private tagsLoading = false;

    @field(BattleMap) private battleMapField: FieldSimple<BattleMap>;

    @initialize
    protected initialize() {
        autorun(() => this.battleMap && this.battleMapField.update(this.battleMap));
        this.visiting.setPaneForBattleMap(this.battleMap.id, "general");
    }

    @action.bound private async handleNameChange() {
        await this.battleMaps.update(this.props.id, { name: this.battleMapField.nested.name.value });
    }

    @action.bound private async handleDescriptionChange() {
        await this.battleMaps.update(this.props.id, { description: this.battleMapField.nested.description.value });
    }

    @action.bound private handleStartEditTags() { this.editTags = true; }

    @action.bound private handleFinishEditTags() { this.editTags = false; }

    @bind private handleUntag(tagId: string) {
        return action(async () => {
            this.tagsLoading = true;
            const { id } = this.battleMapTags.forBattleMap(this.battleMap.id).find(battleMapTag => {
                return battleMapTag.tag.id === tagId;
            });
            await this.battleMapTags.delete(id);
            this.tagsLoading = false;
        });
    }

    @action.bound private async handleAddTagChange(_: React.SyntheticEvent, { value }: { value: string }) {
        this.tagsLoading = true;
        const tag = await this.tags.getOrCreate(value, ...extractParentIds(this.battleMap));
        await this.battleMapTags.create({
            tag: { id: tag.id } as Tag,
            battleMap: { id: this.battleMap.id } as BattleMap,
        });
        this.tagsLoading = false;
    }

    @computed private get tagList() {
        return this.battleMapTags.forBattleMap(this.battleMap.id)
            .map(battleMapTag => battleMapTag.tag.id)
            .map(this.tags.byId)
            .filter(tag => this.canEdit || !tag.masterOnly);
    }

    @computed private get dropdownOptions() {
        return this.tags.dropDownOptions(...extractParentIds(this.battleMap))
            .filter(option => !this.tagList.some(tag => tag.id === option.value));
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @computed private get canEdit() {
        if (this.battleMap.preset) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    public render() {
        const { name, description } = this.battleMapField.nested;
        return (
            <div className={css.description}>
                <EditableText
                    disabled={!this.canEdit}
                    {...fromEditableTextField(name)}
                    onFinish={this.handleNameChange}
                    as="h2"
                />

                <Label.Group>
                    {
                        this.tagList.map(tag => (
                            <TagLabel
                                key={tag.id}
                                id={tag.id}
                                onRemove={this.handleUntag(tag.id)}
                                editing={this.editTags}
                            />
                        ))
                    }
                    {
                        this.canEdit && (
                            !this.editTags ? (
                                <Icon
                                    name="pencil"
                                    color="grey"
                                    link
                                    onClick={this.handleStartEditTags}
                                />
                            ) : (
                                    <Icon
                                        name="checkmark"
                                        color="grey"
                                        link
                                        onClick={this.handleFinishEditTags}
                                    />
                                )
                        )
                    }
                </Label.Group>
                {
                    this.editTags && <>
                        <Dropdown
                            placeholder="Add Tag"
                            search
                            selection
                            selectOnNavigation={false}
                            selectOnBlur={false}
                            options={this.dropdownOptions}
                            onChange={this.handleAddTagChange}
                            loading={this.tagsLoading}
                            disabled={this.tagsLoading}
                            value={undefined}
                            closeOnChange={false}
                            allowAdditions
                        />{" "}
                    </>
                }
                <EditableText
                    formatted
                    area
                    disabled={!this.canEdit}
                    {...fromEditableTextField(description)}
                    onFinish={this.handleDescriptionChange}
                    extendedToolbar
                    label="Description"
                />
            </div>
        );
    }
}
