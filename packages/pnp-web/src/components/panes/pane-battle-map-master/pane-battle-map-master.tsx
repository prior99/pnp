import * as React from "react";
import { History } from "history";
import { external, inject, initialize } from "tsdi";
import { observer } from "mobx-react";
import { StoreBattleMaps, StoreLogin, StoreVisiting } from "../../../stores";
import { computed, autorun, action, observable } from "mobx";
import { EditableText, fromEditableTextField } from "../../elements";
import { hasFields, field, FieldSimple } from "hyrest-mobx";
import { BattleMap } from "pnp-common";
import { Checkbox, Button, Ref, Popup } from "semantic-ui-react";
import { routeGroupDashboard, routePresetDashboard } from "../../../pages";
import { bind } from "lodash-decorators";

@observer @external @hasFields()
export class PaneBattleMapMaster extends React.Component<{ id: string }> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;
    @inject private visiting: StoreVisiting;
    @inject private history: History;

    @field(BattleMap) private battleMapField: FieldSimple<BattleMap>;

    @observable private archiving = false;
    @observable private deleting = false;

    private archiveRef: HTMLButtonElement;
    private deleteRef: HTMLButtonElement;

    @initialize
    protected initialize() {
        autorun(() => this.battleMap && this.battleMapField.update(this.battleMap));
        this.visiting.setPaneForBattleMap(this.battleMap.id, "master");
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.battleMaps.update(this.props.id, { masterOnly: !this.battleMap.masterOnly });
    }

    @action.bound private async handleMasterNotesChange() {
        await this.battleMaps.update(this.props.id, { masterNotes: this.battleMapField.nested.masterNotes.value });
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @computed private get canEdit() {
        if (this.battleMap.preset) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @bind public handleArchiveRef(button: HTMLButtonElement) {
        this.archiveRef = button;
    }

    @bind public handleDeleteRef(button: HTMLButtonElement) {
        this.deleteRef = button;
    }

    @action.bound private async handleArchive() {
        if (this.battleMap.group) {
            this.history.push(routeGroupDashboard.path(this.battleMap.group.id));
        }
        if (this.battleMap.preset) {
            this.history.push(routePresetDashboard.path(undefined, this.battleMap.preset.id));
        }
        await this.battleMaps.archive(this.battleMap.id);
        this.archiving = false;
    }

    @action.bound private async handleDelete() {
        if (this.battleMap.group) { this.history.push(routeGroupDashboard.path(this.battleMap.group.id)); }
        if (this.battleMap.preset) {
            this.history.push(routePresetDashboard.path(undefined, this.battleMap.preset.id));
        }
        await this.battleMaps.delete(this.battleMap.id);
        this.deleting = false;
    }

    @action.bound private handleArchiveStart() {
        this.archiving = true;
    }

    @action.bound private handleArchiveClose() {
        this.archiving = false;
    }

    @action.bound private handleDeleteStart() {
        this.deleting = true;
    }

    @action.bound private handleDeleteClose() {
        this.deleting = false;
    }

    public render() {
        const { masterNotes } = this.battleMapField.nested;
        return (
            <div>
                <Button.Group style={{ display: "block", marginBottom: 10 }}>
                    <Ref innerRef={this.handleArchiveRef}>
                        <Button
                            icon="archive"
                            content="Archive"
                            onClick={this.handleArchiveStart}
                        />
                    </Ref>
                    <Popup
                        flowing
                        hoverable
                        context={this.archiveRef}
                        position="left center"
                        open={this.archiving}
                        onClose={this.handleArchiveClose}
                    >
                        <Button
                            icon="checkmark"
                            color="red"
                            onClick={this.handleArchive}
                            content="Archive"
                            size="mini"
                        />
                    </Popup>
                    <Ref innerRef={this.handleDeleteRef}>
                        <Button
                            icon="trash alternative"
                            content="Delete permanently"
                            onClick={this.handleDeleteStart}
                        />
                    </Ref>
                    <Popup
                        flowing
                        hoverable
                        context={this.deleteRef}
                        position="left center"
                        open={this.deleting}
                        onClose={this.handleDeleteClose}
                    >
                        <Button
                            icon="checkmark"
                            color="red"
                            onClick={this.handleDelete}
                            content="Delete forever"
                            size="mini"
                        />
                    </Popup>
                </Button.Group>
                <Checkbox
                    toggle
                    checked={!this.battleMap.masterOnly}
                    onChange={this.handleMasterOnlyChange}
                    label="Visible to players"
                    style={{ marginBottom: 10 }}
                />
                <EditableText
                    formatted
                    area
                    disabled={!this.canEdit}
                    {...fromEditableTextField(masterNotes)}
                    onFinish={this.handleMasterNotesChange}
                    extendedToolbar
                    label="Master notes"
                />
            </div>
        );
    }
}
