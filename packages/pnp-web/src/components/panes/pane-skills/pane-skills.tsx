import * as React from "react";
import { inject, external, initialize } from "tsdi";
import { StoreHeroes, StoreVisiting } from "../../../stores";
import { computed } from "mobx";
import { observer } from "mobx-react";
import { CardSkills } from "../../cards";
import { Grid, Message } from "semantic-ui-react";
import { allBaseSkills } from "pnp-common";
import { Content } from "../../layout";

@external @observer
export class PaneSkills extends React.Component<{id: string }> {
    @inject private heroes: StoreHeroes;
    @inject private visiting: StoreVisiting;

    @initialize public init(): void {
        this.visiting.setPaneForHero(this.props.id, "skills");
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    public render () {
        const { hero } = this;
        if (!hero) { return null; }
        return (
            <Content>
                {
                    hero.unspentSkill > 0 && (
                        <Message
                            header={`${hero.unspentSkill} unspent skill points`}
                            content={
                                "Spend the skill points on perks below to increase their " +
                                "value as well as the base skill's value."
                            }
                            icon="plus"
                        />
                    )
                }
                <Grid>
                    {
                        allBaseSkills.map(skill => {
                            return (
                                <Grid.Column mobile={16} tablet={8} computer={4}>
                                    <CardSkills hero={hero.id} skill={skill} />
                                </Grid.Column>
                            );
                        })
                    }
                </Grid>
            </Content>
        );
    }
}
