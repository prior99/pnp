import * as React from "react";
import { History } from "history";
import { List, Icon } from "semantic-ui-react";
import { observer } from "mobx-react";
import { action } from "mobx";
import { inject, external } from "tsdi";
import { StoreGroups, StoreLogin } from "../../../stores";
import { Group } from "pnp-common";
import { bind } from "lodash-decorators";
import { routeGroupDashboard, routeJoinGroup } from "../../../pages";

@observer @external
export class GroupList extends React.Component {
    @inject private groups: StoreGroups;
    @inject private history: History;
    @inject private login: StoreLogin;

    @bind private handleClick(group: Group) {
        if (this.login.hasGroup(group.id)) {
            return action(() => this.history.push(routeGroupDashboard.path(group.id)));
        }
        return action(() => this.history.push(routeJoinGroup.path(group.name)));
    }

    public render() {
        return (
            <List selection verticalAlign="middle">
                {
                    this.groups
                        .sorted((a, b) => a.name.localeCompare(b.name))
                        .map(group => {
                            return (
                                <List.Item
                                    key={group.id}
                                    onClick={this.handleClick(group)}
                                    content={group.name}
                                    icon={<Icon name={this.login.hasGroup(group.id) ? "eye" : "sign in"} />}
                                />
                            );
                        })
                }
            </List>
        );
    }
}
