import * as React from "react";
import { History } from "history";
import { List, Image } from "semantic-ui-react";
import { observer } from "mobx-react";
import { action } from "mobx";
import { inject, external } from "tsdi";
import { StoreBattleMaps, StoreVisiting, StoreLogin } from "../../../stores";
import { BattleMap } from "pnp-common";
import { bind } from "lodash-decorators";
import { routeBattleMap } from "../../../pages";
import * as css from "./battle-map-list.scss";

@observer
@external
export class BattleMapList extends React.Component<{
    presetId?: string;
    groupId?: string;
}> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private history: History;
    @inject private visiting: StoreVisiting;
    @inject private login: StoreLogin;

    @bind private handleClick(battleMap: BattleMap) {
        const { presetId, groupId } = this.props;
        return action(() => {
            this.history.push(
                routeBattleMap.path(
                    groupId,
                    presetId,
                    battleMap.id,
                    this.visiting.paneForBattleMap(battleMap.id),
                ),
            );
        });
    }

    public render() {
        const { groupId, presetId } = this.props;
        return (
            <List selection verticalAlign="middle">
                {this.battleMaps
                    .sorted((a, b) => a.name.localeCompare(b.name))
                    .filter(
                        battleMap =>
                            !presetId ||
                            (battleMap.preset &&
                                battleMap.preset.id === presetId),
                    )
                    .filter(
                        battleMap =>
                            !groupId ||
                            (battleMap.group && battleMap.group.id === groupId),
                    )
                    .filter(
                        battleMap =>
                            !groupId ||
                            this.login.isMaster(groupId) ||
                            !battleMap.masterOnly,
                    )
                    .map(battleMap => {
                        return (
                            <List.Item
                                key={battleMap.id}
                                onClick={this.handleClick(battleMap)}
                                content={battleMap.name}
                                icon={
                                    <Image
                                        src={battleMap.previewUrl}
                                        circular
                                        inline
                                        size="mini"
                                        className={css.icon}
                                    />
                                }
                            />
                        );
                    })}
            </List>
        );
    }
}
