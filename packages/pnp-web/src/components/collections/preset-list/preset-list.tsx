import * as React from "react";
import { History } from "history";
import { List, Icon } from "semantic-ui-react";
import { observer } from "mobx-react";
import { action } from "mobx";
import { inject, external } from "tsdi";
import { StorePresets, StoreLogin } from "../../../stores";
import { Preset } from "pnp-common";
import { bind } from "lodash-decorators";
import { routePresetDashboard, routeJoinPreset } from "../../../pages";

@observer @external
export class PresetList extends React.Component {
    @inject private presets: StorePresets;
    @inject private history: History;
    @inject private login: StoreLogin;

    @bind private handleClick(preset: Preset) {
        if (this.login.hasPreset(preset.id)) {
            return action(() => this.history.push(routePresetDashboard.path(undefined, preset.id)));
        }
        return action(() => this.history.push(routeJoinPreset.path(preset.name)));
    }

    public render() {
        return (
            <List selection verticalAlign="middle">
                {
                    this.presets
                        .sorted((a, b) => a.name.localeCompare(b.name))
                        .map(preset => {
                            return (
                                <List.Item
                                    key={preset.id}
                                    onClick={this.handleClick(preset)}
                                    content={preset.name}
                                    icon={<Icon name={this.login.presetIds.includes(preset.id) ? "eye" : "sign in"} />}
                                />
                            );
                        })
                }
            </List>
        );
    }
}
