import * as React from "react";
import { History } from "history";
import { List, Image } from "semantic-ui-react";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import { inject, external } from "tsdi";
import { StoreHeroes, StoreLogin, StoreVisiting } from "../../../stores";
import { Hero } from "pnp-common";
import { bind } from "lodash-decorators";
import { routeHero } from "../../../pages";
import * as css from "./hero-list.scss";

export interface HeroListProps {
    list: "hero" | "NPC";
    presetId?: string;
    groupId?: string;
}

@observer @external
export class HeroList extends React.Component<HeroListProps> {
    @inject private heroes: StoreHeroes;
    @inject private history: History;
    @inject private login: StoreLogin;
    @inject private visiting: StoreVisiting;

    @bind private handleClick(hero: Hero) {
        const { groupId, presetId } = this.props;
        return action(() => {
            this.history.push(routeHero.path(groupId, presetId, hero.id, this.visiting.paneForHero(hero.id)));
        });
    }

    @computed private get isMaster() {
        if (this.props.presetId) { return true; }
        return this.login.isMaster(this.props.groupId);
    }

    public render() {
        const { list, presetId, groupId } = this.props;
        return (
            <List selection verticalAlign="middle">
                {
                    this.heroes.all
                        .filter(hero => !presetId || hero.preset && hero.preset.id === presetId)
                        .filter(hero => !groupId || hero.group && hero.group.id === groupId)
                        .filter(hero => hero.isNpc === (list === "NPC"))
                        .filter(hero => this.isMaster || !hero.masterOnly)
                        .sort((a, b) => a.name.localeCompare(b.name))
                        .map(hero => {
                            const style = {
                                opacity: hero.damage >= hero.combatRank("constitution") ? 0.3 : 1,
                            };
                            return (
                                <List.Item
                                    style={style}
                                    key={hero.id}
                                    onClick={this.handleClick(hero)}
                                    icon={
                                        <Image
                                            src={hero.avatarUrl}
                                            size="mini"
                                            circular
                                            inline
                                            className={css.icon}
                                        />
                                    }
                                    content={hero.name}
                                />
                            );
                        })
                }
            </List>
        );
    }
}
