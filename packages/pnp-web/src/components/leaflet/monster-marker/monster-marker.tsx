import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Marker as LeafletMarker, Popup } from "react-leaflet";
import * as Leaflet from "leaflet";
import { computed, action } from "mobx";
import {
    StoreMarkers,
    StoreMonsters,
    StoreBattleMaps,
    StoreLogin,
    StoreTemplateMonsters,
    StoreManeuvers,
    StoreMonsterManeuvers,
} from "../../../stores";
import * as css from "./monster-marker.scss";
import * as classNames from "classnames/bind";
import { ButtonConfirm, AdjustableValueBar, EditableText } from "../../elements";
import { Marker, Monster, combatDictionary, getCombatTraitsForCategory, combatTraitCostPerRank } from "pnp-common";
import { Grid, Segment, Checkbox } from "semantic-ui-react";

export interface MonsterMarkerProps {
    id: string;
    onDrag: (evt: Leaflet.DragEndEvent) => void;
}

const iconSize = parseInt(css.markerSize, 10);

const cx = classNames.bind(css);

@observer @external
export class MonsterMarker extends React.Component<MonsterMarkerProps> {
    @inject private markers: StoreMarkers;
    @inject private monsters: StoreMonsters;
    @inject private monsterManeuvers: StoreMonsterManeuvers;
    @inject private maneuvers: StoreManeuvers;
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;

    @computed private get marker() {
        return this.markers.byId(this.props.id);
    }

    @computed private get templateMonster() {
        return this.templateMonsters.byId(this.monster.templateMonster.id);
    }

    @computed private get monster() {
        return this.monsters.byId(this.marker.monster.id);
    }

    @computed private get icon() {
        const { templateMonster } = this;
        return new Leaflet.DivIcon({
            html: `<img src=${templateMonster.imageUrl}>`,
            iconSize: [iconSize, iconSize],
            iconAnchor: [iconSize / 2, iconSize / 2],
            popupAnchor: [0, -iconSize / 2],
            className: cx({ monsterMarker: true, masterOnly: this.marker.masterOnly }),
        });
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.marker.battleMap.id);
    }

    @computed private get canEdit() {
        if (!this.battleMap.group) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @computed private get allManeuvers() {
        return this.monsterManeuvers.forMonster(this.templateMonster.id)
            .map(monsterManeuver => this.maneuvers.byId(monsterManeuver.maneuver.id));
    }

    @action.bound private async handleKill(marker: Marker) {
        if (!marker) { return; }
        if (this.templateMonster.stash) {
            await this.markers.createLootMarkerFromStash(
                marker.battleMap.id,
                this.templateMonster.stash.id,
                {
                    lat: marker.latitude,
                    lng: marker.longitude,
                },
            );
        }
        await this.markers.delete(marker.id);
        await this.monsters.delete(marker.monster.id);
    }

    @action.bound private async handleMarkerDelete(marker: Marker) {
        if (!marker) { return; }
        await this.markers.delete(marker.id);
        await this.monsters.delete(marker.monster.id);
    }

    @action.bound private async handleStaminaChange(value: number) {
        const { stamina } = this;
        await this.monsters.update(this.monster.id, {
            spentStamina: Math.min(stamina, Math.max(0, stamina - value)),
        } as Monster);
    }

    @action.bound private async handleHealthChange(value: number) {
        const { constitution } = this;
        await this.monsters.update(this.monster.id, {
            damage: Math.min(constitution, Math.max(0, constitution - value)),
        } as Monster);
    }

    @computed private get stamina() {
        return this.templateMonster.stamina / combatTraitCostPerRank("stamina");
    }

    @computed private get constitution() {
        return this.templateMonster.constitution / combatTraitCostPerRank("constitution");
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.markers.update(this.props.id, { masterOnly: !this.marker.masterOnly });
    }

    public render() {
        const { marker, templateMonster, monster, icon, stamina, constitution } = this;

        return (
            <LeafletMarker
                key={marker.id}
                icon={icon as any}
                position={[marker.latitude, marker.longitude]}
                draggable
                onDragEnd={this.props.onDrag}
            >
                <Popup className={css.popup}>
                    <div className={css.content}>
                        <h1>{templateMonster.name}</h1>
                        {
                            this.canEdit ? <>
                                <Grid>
                                    <Grid.Row>
                                        <Grid.Column computer={16} mobile={16}>
                                            <h3>Fitness</h3>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row className={css.staminaRow}>
                                        <Grid.Column mobile={16} computer={16}>
                                            <AdjustableValueBar
                                                label="Stamina"
                                                color="green"
                                                current={stamina - monster.spentStamina}
                                                total={stamina}
                                                onChangeValue={this.handleStaminaChange}
                                                mini
                                                resettable
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column mobile={16} computer={16}>
                                            <AdjustableValueBar
                                                label="Health"
                                                color="red"
                                                current={constitution - monster.damage}
                                                total={constitution}
                                                onChangeValue={this.handleHealthChange}
                                                mini
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column computer={16} mobile={16}>
                                            <h3>Combat Traits</h3>
                                        </Grid.Column>
                                        {
                                            [
                                                ...getCombatTraitsForCategory("combatStyle"),
                                                ...getCombatTraitsForCategory("body"),
                                            ].map(combatTrait => (
                                                <Grid.Column mobile={4} computer={2}>
                                                    <p>
                                                        <b>
                                                            {combatDictionary[combatTrait].substr(0, 3).toUpperCase()}
                                                        </b>
                                                    </p>
                                                    <p>
                                                        {
                                                            templateMonster[combatTrait] /
                                                            combatTraitCostPerRank(combatTrait)
                                                        }
                                                    </p>
                                                </Grid.Column>
                                            ))
                                        }
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column computer={16} mobile={16}><h3>Maneuvers</h3></Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row className={css.maneuvers}>
                                        {
                                            this.allManeuvers.map(maneuver => (
                                                <Grid.Column className={css.maneuverColumn} mobile={8} computer={8}>
                                                    <Segment fluid>
                                                        <p><b>{maneuver.name}</b></p>
                                                        <EditableText
                                                            formatted
                                                            disabled
                                                            area
                                                            value={maneuver.effect}
                                                            label="Rules"
                                                        />
                                                    </Segment>
                                                </Grid.Column>
                                            ))
                                        }
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column computer={16} mobile={16}>
                                            <h3>Actions</h3>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <ButtonConfirm size="mini" item={marker} onConfirm={this.handleMarkerDelete} />
                                        {
                                            this.battleMap.group && (
                                                <ButtonConfirm
                                                    icon="frown"
                                                    content="Kill"
                                                    size="mini"
                                                    item={marker}
                                                    onConfirm={this.handleKill}
                                                />
                                            )
                                        }
                                        <Checkbox
                                            toggle
                                            checked={!this.marker.masterOnly}
                                            onChange={this.handleMasterOnlyChange}
                                            label="Visible"
                                        />
                                    </Grid.Row>
                                </Grid>
                            </> : <EditableText
                                    formatted
                                    disabled
                                    area
                                    value={templateMonster.description}
                                    label="Description"
                                />
                        }
                    </div>
                </Popup>
            </LeafletMarker>
        );
    }
}
