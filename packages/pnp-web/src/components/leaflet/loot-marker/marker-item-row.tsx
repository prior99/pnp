import * as React from "react";
import { observer } from "mobx-react";
import {
    StoreTemplateItems,
    StoreMarkerItems,
    StoreHeroes,
    StoreItems,
} from "../../../stores";
import { inject, external } from "tsdi";
import { Grid, Button, Ref, Popup, Dropdown, Form } from "semantic-ui-react";
import { computed, action, observable } from "mobx";
import { Live } from "../../../live";
import { bind } from "lodash-decorators";
import { pick } from "ramda";
import { Hero } from "pnp-common";

@external
@observer
export class MarkerItemRow extends React.Component<{
    id: string;
    onDisappear?: (id: string) => void;
}> {
    @inject private markerItems: StoreMarkerItems;
    @inject private items: StoreItems;
    @inject private templateItems: StoreTemplateItems;
    @inject private heroes: StoreHeroes;
    @inject private live: Live;

    @observable private taking = false;
    @observable private selectedHeroId: string | undefined;

    private takeButtonRef: HTMLButtonElement;

    @computed private get markerItem() {
        return this.markerItems.byId(this.props.id);
    }

    @computed private get templateItem() {
        return this.templateItems.byId(this.markerItem.templateItem.id);
    }

    @action.bound private async handleTakeStart() {
        const claimedHeroId = this.live.myClaimedHeroId(
            this.templateItem.group.id,
        );
        if (!this.templateItem.group || !claimedHeroId) {
            this.taking = true;
            return;
        }
        await this.items.create({
            ...pick(["templateItem", "quantity", "notes"], this.markerItem),
            hero: { id: claimedHeroId } as Hero,
        });
        await this.markerItems.delete(this.markerItem.id);

        if (this.props.onDisappear) {
            this.props.onDisappear(this.props.id);
        }
    }

    @action.bound private handleTakeClose() {
        this.taking = false;
    }

    @action.bound private async handleTake() {
        await this.items.create({
            ...pick(["templateItem", "quantity", "notes"], this.markerItem),
            hero: { id: this.selectedHeroId } as Hero,
        });
        await this.markerItems.delete(this.markerItem.id);
        this.taking = false;
        if (this.props.onDisappear) {
            this.props.onDisappear(this.props.id);
        }
    }

    @bind public handleTakeButtonRef(button: HTMLButtonElement) {
        this.takeButtonRef = button;
    }

    @action.bound private async handleDelete() {
        await this.markerItems.delete(this.props.id);
        if (this.props.onDisappear) {
            this.props.onDisappear(this.props.id);
        }
    }

    @action.bound private handleSelectedHeroChange(
        _,
        { value }: { value: string },
    ) {
        this.selectedHeroId = value;
    }

    public render() {
        const groupId = this.templateItem.group && this.templateItem.group.id;
        const presetId =
            this.templateItem.preset && this.templateItem.preset.id;
        return (
            <Grid.Row>
                <Grid.Column computer={6}>{this.templateItem.name}</Grid.Column>
                <Grid.Column computer={2}>
                    {this.templateItem.quantifiable && (
                        <>
                            {this.markerItem.quantity}
                            {this.templateItem.unit
                                ? this.templateItem.unit
                                : ""}
                        </>
                    )}
                </Grid.Column>
                <Grid.Column computer={8}>
                    <Button
                        size="mini"
                        icon="trash"
                        onClick={this.handleDelete}
                    />
                    {this.templateItem.group && (
                        <Ref innerRef={this.handleTakeButtonRef}>
                            <Button
                                size="mini"
                                icon="hand paper outline"
                                onClick={this.handleTakeStart}
                            />
                        </Ref>
                    )}
                    <Popup
                        open={this.taking}
                        context={this.takeButtonRef}
                        onClose={this.handleTakeClose}
                    >
                        <Popup.Header>Give item to</Popup.Header>
                        <Popup.Content>
                            <Form onSubmit={this.handleTake}>
                                <Form.Field>
                                    <label>Hero</label>
                                    <Dropdown
                                        selection
                                        search
                                        options={this.heroes.dropDownOptions(
                                            groupId,
                                            presetId,
                                        )}
                                        value={this.selectedHeroId}
                                        onChange={this.handleSelectedHeroChange}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <Button
                                        primary
                                        fluid
                                        htmlType="submit"
                                        content="Give"
                                        icon="hand paper outline"
                                        disabled={!this.selectedHeroId}
                                    />
                                </Form.Field>
                            </Form>
                        </Popup.Content>
                    </Popup>
                </Grid.Column>
            </Grid.Row>
        );
    }
}
