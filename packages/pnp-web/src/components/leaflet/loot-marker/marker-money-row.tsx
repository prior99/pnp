import * as React from "react";
import { observer } from "mobx-react";
import { inject, external } from "tsdi";
import { StoreMarkers, StoreBattleMaps, StoreHeroes } from "../../../stores";
import { Live } from "../../../live";
import { observable, computed, action } from "mobx";
import {
    Ref,
    Button,
    Form,
    Dropdown,
    Popup as SemanticPopup,
} from "semantic-ui-react";
import { bind } from "lodash-decorators";

@observer
@external
export class MarkerMoneyRow extends React.Component<{
    markerId: string;
    onDisappear: () => void;
}> {
    @inject private markers: StoreMarkers;
    @inject private battleMaps: StoreBattleMaps;
    @inject private heroes: StoreHeroes;
    @inject private live: Live;

    @observable private takingMoney = false;
    @observable private selectedHeroId: string | undefined;

    private takeMoneyButtonRef: HTMLButtonElement;

    @computed private get marker() {
        return this.markers.byId(this.props.markerId);
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.marker.battleMap.id);
    }

    @computed private get claimedHeroes() {
        if (!this.battleMap.group) {
            return [];
        }
        return this.live.claimedHeroes(this.battleMap.group.id);
    }

    @action.bound private async handleTakeMoneyStart() {
        const claimedHeroId = this.live.myClaimedHeroId(
            this.battleMap.group.id,
        );
        if (!this.battleMap.group || !claimedHeroId) {
            this.takingMoney = true;
            return;
        }
        const hero = this.heroes.byId(claimedHeroId);
        await this.heroes.update(claimedHeroId, {
            money: hero.money + this.marker.money,
        });
        await this.markers.update(this.marker.id, { money: 0 });
    }

    @action.bound private handleTakeMoneyClose() {
        this.takingMoney = false;
    }

    @action.bound private async handleTakeMoney() {
        await this.heroes.update(this.selectedHeroId, {
            money: this.marker.money,
        });
        await this.markers.update(this.marker.id, { money: 0 });
        this.takingMoney = false;
        if (this.props.onDisappear) {
            this.props.onDisappear();
        }
    }

    @action.bound private async handleSplitMoney() {
        const heroIds = this.claimedHeroes;
        const part = Math.ceil(this.marker.money / heroIds.length);
        await Promise.all(
            heroIds.map(async heroId => {
                await this.heroes.update(heroId, {
                    money: this.heroes.byId(heroId).money + part,
                });
            }),
        );
        await this.markers.update(this.marker.id, { money: 0 });
        if (this.props.onDisappear) {
            this.props.onDisappear();
        }
    }

    @action.bound private handleSelectedHeroChange(
        _,
        { value }: { value: string },
    ) {
        this.selectedHeroId = value;
    }

    @bind public handleTakeButtonRef(button: HTMLButtonElement) {
        this.takeMoneyButtonRef = button;
    }

    public render(): JSX.Element {
        const { marker, claimedHeroes } = this;
        const groupId = this.battleMap.group && this.battleMap.group.id;
        const presetId = this.battleMap.preset && this.battleMap.preset.id;

        return (
            <>
                <p>There is a total amount of {marker.money}</p>
                <Ref innerRef={this.handleTakeButtonRef}>
                    <Button
                        size="mini"
                        icon="hand paper outline"
                        onClick={this.handleTakeMoneyStart}
                    />
                </Ref>
                <SemanticPopup
                    open={this.takingMoney}
                    context={this.takeMoneyButtonRef}
                    onClose={this.handleTakeMoneyClose}
                >
                    <SemanticPopup.Header>Give item to</SemanticPopup.Header>
                    <SemanticPopup.Content>
                        <Form onSubmit={this.handleTakeMoney}>
                            <Form.Field>
                                <label>Hero</label>
                                <Dropdown
                                    selection
                                    search
                                    options={this.heroes.dropDownOptions(
                                        groupId,
                                        presetId,
                                    )}
                                    value={this.selectedHeroId}
                                    onChange={this.handleSelectedHeroChange}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Button
                                    primary
                                    fluid
                                    htmlType="submit"
                                    content="Give"
                                    icon="hand paper outline"
                                    disabled={!this.selectedHeroId}
                                />
                            </Form.Field>
                        </Form>
                    </SemanticPopup.Content>
                </SemanticPopup>
                {claimedHeroes.length > 0 &&
                    marker.money > claimedHeroes.length && (
                        <Button
                            icon="handshake outline"
                            size="mini"
                            onClick={this.handleSplitMoney}
                        />
                    )}
            </>
        );
    }
}
