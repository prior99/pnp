import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Marker as LeafletMarker, Popup } from "react-leaflet";
import * as Leaflet from "leaflet";
import { computed, action } from "mobx";
import {
    StoreMarkers,
    StoreBattleMaps,
    StoreMarkerItems,
    StoreTemplateItems,
    StoreLogin,
} from "../../../stores";
import * as css from "./loot-marker.scss";
import * as classNames from "classnames/bind";
import { Marker } from "pnp-common";
import { MarkerItemRow } from "./marker-item-row";
import { Grid, Checkbox } from "semantic-ui-react";
import { ButtonConfirm } from "../../elements";
import { MarkerMoneyRow } from "./marker-money-row";

export interface LootMarkerProps {
    id: string;
    onDrag: (evt: Leaflet.DragEndEvent) => void;
}

const iconSize = parseInt(css.markerSize, 10);

const cx = classNames.bind(css);

@observer
@external
export class LootMarker extends React.Component<LootMarkerProps> {
    @inject private markers: StoreMarkers;
    @inject private battleMaps: StoreBattleMaps;
    @inject private markerItems: StoreMarkerItems;
    @inject private templateItems: StoreTemplateItems;
    @inject private login: StoreLogin;

    @computed private get marker() {
        return this.markers.byId(this.props.id);
    }

    @computed private get allMarkerItems() {
        return this.markerItems.forMarker(this.marker.id).sort((a, b) => {
            const templateItemA = this.templateItems.byId(a.templateItem.id);
            const templateItemB = this.templateItems.byId(b.templateItem.id);
            return templateItemA.name.localeCompare(templateItemB.name);
        });
    }

    @computed private get icon() {
        return new Leaflet.DivIcon({
            html: `
                <i
                    class="dollar icon fitted"
                    style="color: #ff8c00; margin-left: 7px !important; font-size: 18px"
                >
                </i>
            `,
            iconSize: [iconSize, iconSize],
            iconAnchor: [iconSize / 2, iconSize / 2],
            popupAnchor: [0, -iconSize / 2],
            className: cx({
                lootMarker: true,
                masterOnly: this.marker.masterOnly,
            }),
        });
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.marker.battleMap.id);
    }

    @action.bound private async handleMarkerDelete(marker: Marker) {
        if (!marker) {
            return;
        }
        await this.markers.delete(marker.id);
    }

    @action.bound private handleTake(): void {
        if (this.marker.money || this.allMarkerItems.length) {
            return;
        }

        this.markers.delete(this.marker.id);
    }

    @computed private get isMaster() {
        if (!this.battleMap.group) {
            return true;
        }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.markers.update(this.props.id, {
            masterOnly: !this.marker.masterOnly,
        });
    }

    public render() {
        const { marker, icon } = this;

        return (
            <LeafletMarker
                key={marker.id}
                icon={icon as any}
                position={[marker.latitude, marker.longitude]}
                draggable
                onDragEnd={this.props.onDrag}
            >
                <Popup className={css.popup}>
                    <div className={css.content}>
                        <h1>Loot</h1>
                        {this.marker.money ? (
                            <div>
                                <h3>Money</h3>
                                <MarkerMoneyRow
                                    markerId={marker.id}
                                    onDisappear={this.handleTake}
                                />
                            </div>
                        ) : (
                            <></>
                        )}
                        {this.allMarkerItems.length > 0 && (
                            <div>
                                <h3>Items</h3>
                                <Grid style={{ margin: 0 }}>
                                    {this.allMarkerItems.map(markerItem => (
                                        <MarkerItemRow
                                            key={markerItem.id}
                                            id={markerItem.id}
                                            onDisappear={this.handleTake}
                                        />
                                    ))}
                                </Grid>
                            </div>
                        )}
                        <ButtonConfirm
                            size="mini"
                            item={marker}
                            onConfirm={this.handleMarkerDelete}
                        />
                        {this.isMaster && (
                            <Checkbox
                                toggle
                                checked={!this.marker.masterOnly}
                                onChange={this.handleMasterOnlyChange}
                                label="Visible"
                            />
                        )}
                    </div>
                </Popup>
            </LeafletMarker>
        );
    }
}
