import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import * as Leaflet from "leaflet";
import { computed, action } from "mobx";
import { StoreMarkers, StoreMarkerItems, StoreLogin, StoreBattleMaps } from "../../../stores";
import { MonsterMarker } from "../monster-marker";
import { HeroMarker } from "../hero-marker";
import { CustomMarker } from "../custom-marker";
import { LootMarker } from "../loot-marker";

enum MarkerType {
    HERO = "hero",
    MONSTER = "monster",
    CUSTOM = "custom",
    LOOT = "loot",
}

@observer @external
export class PnPMarker extends React.Component<{ id: string }> {
    @inject private markers: StoreMarkers;
    @inject private markerItems: StoreMarkerItems;
    @inject private login: StoreLogin;
    @inject private battleMaps: StoreBattleMaps;

    @computed private get battleMap() {
        return this.battleMaps.byId(this.marker.battleMap.id);
    }

    @computed private get isMaster() {
        if (!this.battleMap.group) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @computed private get marker() {
        return this.markers.byId(this.props.id);
    }

    @computed private get markerType(): MarkerType {
        if (this.marker.hero) { return MarkerType.HERO; }
        if (this.marker.monster) { return MarkerType.MONSTER; }
        if (this.markerItems.forMarker(this.marker.id).length > 0 || typeof this.marker.money === "number") {
            return MarkerType.LOOT;
        }
        return MarkerType.CUSTOM;
    }

    @action.bound private handleDrag(evt: Leaflet.DragEndEvent) {
        const leafletMarker: Leaflet.Marker = evt.target;
        const { lat, lng } = leafletMarker.getLatLng();
        this.markers.move(this.marker.id, [lat, lng]);
    }

    public render() {
        if (this.marker.masterOnly && !this.isMaster) { return <></>; }
        switch (this.markerType) {
            case MarkerType.MONSTER: return <MonsterMarker id={this.marker.id} onDrag={this.handleDrag} />;
            case MarkerType.HERO: return <HeroMarker id={this.marker.id} onDrag={this.handleDrag} />;
            case MarkerType.CUSTOM: return <CustomMarker id={this.marker.id} onDrag={this.handleDrag} />;
            case MarkerType.LOOT: return <LootMarker id={this.marker.id} onDrag={this.handleDrag} />;
        }
    }
}
