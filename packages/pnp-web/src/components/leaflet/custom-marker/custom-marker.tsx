import * as React from "react";
import { external, inject, initialize } from "tsdi";
import { observer } from "mobx-react";
import { Marker as LeafletMarker, Popup as LeafletPopup } from "react-leaflet";
import * as Leaflet from "leaflet";
import { Checkbox, SemanticICONS } from "semantic-ui-react";
import { computed, action, autorun } from "mobx";
import { StoreMarkers, StoreBattleMaps, StoreLogin } from "../../../stores";
import * as css from "./custom-marker.scss";
import * as classNames from "classnames/bind";
import { ButtonConfirm, IconSelector } from "../../elements";
import { Marker } from "pnp-common";
import { EditableText, fromEditableTextField } from "../../elements";
import { field, FieldSimple, hasFields } from "hyrest-mobx";

export interface CustomMarkerProps {
    id: string;
    onDrag: (evt: Leaflet.DragEndEvent) => void;
}

const iconSize = parseInt(css.customMarkerSize, 10);
const cx = classNames.bind(css);

@observer @external @hasFields()
export class CustomMarker extends React.Component<CustomMarkerProps> {
    @inject private markers: StoreMarkers;
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;

    @field(Marker) private markerField: FieldSimple<Marker>;

    @initialize
    protected initialize() {
        autorun(() => this.marker && this.markerField.update(this.marker));
    }

    @computed private get marker() {
        return this.markers.byId(this.props.id);
    }

    @computed private get icon() {
        return new Leaflet.DivIcon({
            html: `
                <i
                    class="${this.marker.icon} icon fitted"
                    style="font-size: 15px"
                >
                </i>
            `,
            iconSize: [iconSize, iconSize],
            iconAnchor: [iconSize / 2, iconSize / 2],
            popupAnchor: [0, -iconSize / 2],
            className: cx({ customMarker: true, masterOnly: this.marker.masterOnly }),
        });
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.marker.battleMap.id);
    }

    @computed private get isMasterView() {
        if (!this.battleMap.group) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @action.bound private async handleIconChange(icon: SemanticICONS) {
        await this.markers.update(this.props.id, { icon });
    }

    @action.bound private async handleMarkerDelete(marker: Marker) {
        if (!marker) { return; }
        this.markers.delete(marker.id);
    }

    @action.bound private async handleNameChange() {
        await this.markers.update(this.props.id, { name: this.markerField.nested.name.value });
    }

    @action.bound private async handleMasterDescriptionChange() {
        await this.markers.update(this.props.id, {
            masterDescription: this.markerField.nested.masterDescription.value,
        });
    }

    @action.bound private async handleDescriptionChange() {
        await this.markers.update(this.props.id, { description: this.markerField.nested.description.value });
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.markers.update(this.props.id, { masterOnly: !this.markerField.nested.masterOnly.value });
    }

    public render() {
        const { markerField, marker, icon } = this;

        return (
            <LeafletMarker
                key={marker.id}
                icon={icon as any}
                position={[marker.latitude, marker.longitude]}
                draggable
                onDragEnd={this.props.onDrag}
            >
                <LeafletPopup className={css.popup}>
                    <div className={css.headerRow}>
                        <IconSelector
                            value={markerField.nested.icon.value as SemanticICONS}
                            onSelect={this.handleIconChange}
                            size="mini"
                        />
                        <b className={css.name}>
                            <EditableText
                                {...fromEditableTextField(markerField.nested.name)}
                                onFinish={this.handleNameChange}
                            />
                        </b>
                    </div>
                    {
                        this.isMasterView && <>
                            <br />
                            <EditableText
                                area
                                formatted
                                {...fromEditableTextField(markerField.nested.masterDescription)}
                                onFinish={this.handleMasterDescriptionChange}
                                label="Master description"
                            />
                        </>
                    }
                    <br />
                    <EditableText
                        area
                        formatted
                        {...fromEditableTextField(markerField.nested.description)}
                        onFinish={this.handleDescriptionChange}
                        label="Description"
                    />
                    {
                        this.isMasterView && <>
                            <br />
                            <ButtonConfirm size="mini" item={markerField.value} onConfirm={this.handleMarkerDelete} />
                            <Checkbox
                                toggle
                                checked={!this.markerField.nested.masterOnly.value}
                                onChange={this.handleMasterOnlyChange}
                                label="Visible"
                            />
                        </>
                    }
                </LeafletPopup>
            </LeafletMarker>
        );
    }
}
