import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Marker as LeafletMarker, Popup } from "react-leaflet";
import * as Leaflet from "leaflet";
import { computed, action } from "mobx";
import { StoreMarkers, StoreBattleMaps, StoreLogin, StoreHeroes, StoreItems } from "../../../stores";
import * as css from "./hero-marker.scss";
import * as classNames from "classnames/bind";
import { ButtonConfirm } from "../../elements";
import { Marker } from "pnp-common";
import { Checkbox } from "semantic-ui-react";

export interface HeroMarkerProps {
    id: string;
    onDrag: (evt: Leaflet.DragEndEvent) => void;
}

const iconSize = parseInt(css.markerSize, 10);

const cx = classNames.bind(css);

@observer @external
export class HeroMarker extends React.Component<HeroMarkerProps> {
    @inject private markers: StoreMarkers;
    @inject private heroes: StoreHeroes;
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;
    @inject private items: StoreItems;

    @computed private get marker() {
        return this.markers.byId(this.props.id);
    }

    @computed private get hero() {
        return this.heroes.byId(this.marker.hero.id);
    }

    @computed private get icon() {
        const { hero } = this;
        return new Leaflet.DivIcon({
            html: `<img src=${hero.avatarUrl}>`,
            iconSize: [iconSize, iconSize],
            iconAnchor: [iconSize / 2, iconSize / 2],
            popupAnchor: [0, -iconSize / 2],
            className: cx({
                heroMarker: true,
                npc: hero.isNpc,
                masterOnly: this.marker.masterOnly,
            }),
        });
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.marker.battleMap.id);
    }

    @computed private get canEdit() {
        if (!this.battleMap.group) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @action.bound private async handleMarkerDelete(marker: Marker) {
        if (!marker) { return; }
        this.markers.delete(marker.id);
    }

    @action.bound private async handleKill(marker: Marker) {
        if (!marker) { return; }
        await this.markers.createLootMarkerFromHero(
            marker.battleMap.id,
            this.hero.id,
            {
                lat: marker.latitude,
                lng: marker.longitude,
            },
        );
        await Promise.all(this.items.forHero(this.hero.id).map(item => this.items.delete(item.id)));
        await this.heroes.update(this.hero.id, {
            damage: this.hero.combatRank("constitution"),
            money: 0,
        });
        await this.markers.delete(marker.id);
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.markers.update(this.props.id, { masterOnly: !this.marker.masterOnly });
    }

    public render() {
        const { marker, hero, icon } = this;

        return (
            <LeafletMarker
                key={marker.id}
                icon={icon as any}
                position={[marker.latitude, marker.longitude]}
                draggable
                onDragEnd={this.props.onDrag}
            >
                <Popup>
                    <b>{hero.name}</b>
                    {
                        this.canEdit && <>
                            <br />
                            <ButtonConfirm size="mini" item={marker} onConfirm={this.handleMarkerDelete} />
                            {
                                this.battleMap.group && (
                                    <ButtonConfirm
                                        icon="frown"
                                        content="Kill"
                                        size="mini"
                                        item={marker}
                                        onConfirm={this.handleKill}
                                    />
                                )
                            }
                            <Checkbox
                                toggle
                                checked={!this.marker.masterOnly}
                                onChange={this.handleMasterOnlyChange}
                                label="Visible"
                            />
                        </>
                    }
                </Popup>
            </LeafletMarker>
        );
    }
}
