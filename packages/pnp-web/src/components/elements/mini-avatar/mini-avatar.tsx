import * as React from "react";
import { Image } from "semantic-ui-react";
import * as css from "./mini-avatar.scss";

export interface MiniAvatarProps {
    src: string;
    circular?: boolean;
}

export class MiniAvatar extends React.Component<MiniAvatarProps> {
    public render() {
        return (
            <Image
                size="tiny"
                src={this.props.src}
                className={css.icon}
                circular={this.props.circular}
            />
        );
    }
}
