import * as React from "react";
import * as css from "./help-text.scss";

export class HelpText extends React.Component<{ text: string }> {
    public render() {
        const { text } = this.props;
        return (
            <div
                className={css.helpText}
                dangerouslySetInnerHTML={{ __html: text }}
            />
        );
    }
}
