import * as React from "react";
import { SemanticCOLORS, Form, Button, Popup, PopupProps, Input, Ref } from "semantic-ui-react";
import { action, computed, observable } from "mobx";
import { bind } from "lodash-decorators";
import { observer } from "mobx-react";
import { SemanticSIZES } from "semantic-ui-react/dist/commonjs/generic";

export interface QuantityButtonProps<Item> {
    item?: Item;
    popupPosition?: PopupProps["position"];
    mode: "remove" | "set" | "add" | "reset";
    currentQuanitity: number;
    onChange: (newQuantity: number, item?: Item) => void;
    color?: SemanticCOLORS;
    mini?: boolean;
    size?: SemanticSIZES;
}

@observer
export class QuantityButton<Item> extends React.Component<QuantityButtonProps<Item>> {
    private buttonRef: HTMLButtonElement;

    @observable private opened = false;
    @observable private value = "1";

    constructor(props: QuantityButtonProps<Item>) {
        super(props);

        if (this.props.mode === "set") {
            this.value = `${this.props.currentQuanitity}`;
        }
    }

    public componentWillReceiveProps(newProps: QuantityButtonProps<Item>): void {
        if (newProps.mode === "set") {
            this.value = `${newProps.currentQuanitity}`;
        }
    }

    @computed private get header() {
        switch (this.props.mode) {
            case "add": return "Increase quantity";
            case "set": return "Set quantity";
            case "remove": return "Decrease quantity";
            default: return;
        }
    }

    @computed private get icon() {
        switch (this.props.mode) {
            case "add": return "plus";
            case "set": return "refresh";
            case "remove": return "minus";
            default: return;
        }
    }

    @action.bound private async handleConfirm() {
        this.opened = false;
        this.props.onChange(this.newQuantity, this.props.item);
    }

    @computed private get newQuantity() {
        const amount = Number(this.value);
        const oldAmount = this.props.currentQuanitity;
        switch (this.props.mode) {
            case "add": return oldAmount + amount;
            case "set": return amount;
            case "remove": return oldAmount - amount;
            default: return;
        }
    }

    @action.bound private handleValueChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.value = evt.currentTarget.value;
    }

    @action.bound private handleOpen() {
        this.opened = true;
    }

    @action.bound private handleClose() {
        this.opened = false;
    }

    @bind public handleButtonRef(button: HTMLButtonElement) {
        this.buttonRef = button;
    }

    public render () {
        return (
            <>
                <Ref innerRef={this.handleButtonRef}>
                    <Button
                        icon={this.icon}
                        onClick={this.handleOpen}
                        size={this.props.mini ? "tiny" : this.props.size}
                        color={this.props.color}
                    />
                </Ref>
                <Popup
                    context={this.buttonRef}
                    onClose={this.handleClose}
                    open={this.opened}
                    position={this.props.popupPosition}
                >
                    <Popup.Header>{this.header}</Popup.Header>
                    <Popup.Content>
                        <Form onSubmit={this.handleConfirm}>
                            <Form.Field>
                                <label>Amount</label>
                                <Input
                                    value={this.value}
                                    onChange={this.handleValueChange}
                                    placeholder="Amount"
                                    labelPosition="right"
                                    type="number"
                                    label={
                                        <Button
                                            htmlType="submit"
                                            icon="checkmark"
                                            color="green"
                                        />
                                    }
                                />
                            </Form.Field>
                        </Form>
                    </Popup.Content>
                </Popup>
            </>
        );
    }
}
