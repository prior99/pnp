import * as React from "react";
import { Button, Segment, Progress, Popup, SemanticCOLORS, Ref, Icon } from "semantic-ui-react";
import { action, observable } from "mobx";
import { observer } from "mobx-react";
import * as css from "./segment-level-value.scss";
import * as classNames from "classnames/bind";
import { bind } from "lodash-decorators";
import { HelpText } from "../../elements";
import { SemanticICONS } from "semantic-ui-react/dist/commonjs/generic";

const cx = classNames.bind(css);

export interface SegmentLevelValueProps {
    humanReadable: string;
    rank: number;
    rankProgress: number;
    skillToNextRank: number;
    color: SemanticCOLORS;
    dice?: string;
    inPreview?: boolean;
    spending?: boolean;
    canSpend?: boolean;
    valueName?: string;
    onPreviewStart?: (valueName: string) => void;
    onPreviewAbort?: (valueName: string) => void;
    onSpend?: (valueName: string) => void;
    onReduce?: (valueName: string) => void;
    editable?: boolean;
    highlight?: boolean;
    loading?: boolean;
    help?: string;
    masterMode?: boolean;
    faded?: boolean;
    icon?: SemanticICONS;
}

@observer
export class SegmentLevelValue extends React.Component<SegmentLevelValueProps> {
    private previewButtonRef: HTMLButtonElement;

    @observable private previewJustOpened = false;

    @action.bound private async handleSpend() {
        this.previewJustOpened = false;
        if (this.props.onSpend) { this.props.onSpend(this.props.valueName); }
    }

    @action.bound private async handleReduce() {
        if (this.props.onReduce) { this.props.onReduce(this.props.valueName); }
    }

    @action.bound private handlePreviewStart() {
        this.previewJustOpened = true;
        if (this.props.onPreviewStart) { this.props.onPreviewStart(this.props.valueName); }
        if (this.props.masterMode) {
            this.handleSpend();
        }
    }

    @action.bound private handlePreviewAbort() {
        if (this.props.onPreviewAbort) { this.props.onPreviewAbort(this.props.valueName); }
    }

    @action.bound private handlePreviewClose() {
        if (this.previewJustOpened) { this.handlePreviewAbort(); }
    }

    @bind public handlePreviewRef(button: HTMLButtonElement) {
        this.previewButtonRef = button;
    }

    public render() {
        const {
            spending,
            humanReadable,
            rank,
            dice,
            rankProgress,
            skillToNextRank,
            inPreview,
            color,
            canSpend,
            highlight,
            loading,
            editable,
            help,
            faded,
            icon,
        } = this.props;
        return (
            <Segment basic attached className={cx({ "segment-with-progress": canSpend, faded })}>
                <div className={cx("flex", { highlight })}>
                    <div>
                        {icon && <Icon name={icon} />}
                        {humanReadable}
                        {
                            help && <>
                                <Popup
                                    flowing
                                    on="click"
                                    trigger={
                                        <Icon
                                            size="small"
                                            name="help"
                                            color="grey"
                                            style={{
                                                cursor: "pointer",
                                                marginLeft: 8,
                                            }}
                                        />
                                    }
                                    position="top center"
                                    className={css.popup}
                                >
                                    <HelpText text={help} />
                                </Popup>
                            </>
                        }
                    </div>
                    {
                        this.props.masterMode && editable && (
                            <Button
                                icon="minus"
                                color={color}
                                size="mini"
                                circular
                                onClick={this.handleReduce}
                            />
                        )
                    }
                    {
                        canSpend && editable && <>
                            <Ref innerRef={this.handlePreviewRef}>
                                <Button
                                    icon="plus"
                                    color={color}
                                    size="mini"
                                    circular
                                    onClick={this.handlePreviewStart}
                                />
                            </Ref>
                            <Popup
                                flowing
                                hoverable
                                context={this.previewButtonRef}
                                onClose={this.handlePreviewClose}
                                position="left center"
                                className={css.popup}
                                disabled={loading}
                                open={spending}
                            >
                                <Button
                                    color={color}
                                    icon="checkmark"
                                    onClick={this.handleSpend}
                                    content="Spend Point"
                                    size="mini"
                                    disabled={loading}
                                />
                            </Popup>
                        </>
                    }
                    <div>{rank} {dice && <span className={css.dice}>{dice}</span>}</div>
                </div>
                {
                    canSpend && (
                        <Progress
                            className={css.progress}
                            color={color}
                            percent={100 * rankProgress}
                            active={Boolean(inPreview)}
                            size="tiny"
                            label={
                                `${skillToNextRank} to level up`
                            }
                        />
                    )
                }
            </Segment>
        );
    }
}
