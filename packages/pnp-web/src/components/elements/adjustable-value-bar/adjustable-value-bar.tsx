import * as React from "react";
import { Button, SemanticCOLORS, Progress, Popup } from "semantic-ui-react";
import { QuantityButton } from "../quantity-button";
import * as classNames from "classnames/bind";
import * as css from "./adjustable-value-bar.scss";

const cx = classNames.bind(css);

export interface AdjustableValueBarProps {
    label: string;
    color: SemanticCOLORS;
    current: number;
    total: number;
    onChangeValue: (newValue: number) => void;
    resettable?: boolean;
    mini?: boolean;
}

export class AdjustableValueBar extends React.PureComponent<AdjustableValueBarProps> {
    public render(): JSX.Element {
        const { mini, label, color, current, total, onChangeValue } = this.props;
        return (
            <div className={cx({ container: true, mini })}>
                <Button.Group size="mini" style={{ width: "100%" }}>
                    <QuantityButton
                        mode="remove"
                        popupPosition="bottom right"
                        currentQuanitity={current}
                        onChange={onChangeValue}
                        color="black"
                        mini={mini}
                    />
                    <Popup
                        trigger={
                            <Progress
                                className={css.progress}
                                color={color}
                                total={total}
                                value={current}
                                progress="ratio"
                                size={this.props.mini ? "tiny" : undefined}
                            />
                        }
                        position="bottom center"
                    >
                        {label} ({current}/{total})
                    </Popup>
                    {
                        this.props.resettable ? (
                            <Button
                                onClick={() => onChangeValue(total)}
                                color="black"
                                icon="remove"
                            />
                        ) : (
                            <QuantityButton
                                mode="set"
                                popupPosition="bottom right"
                                currentQuanitity={current}
                                onChange={onChangeValue}
                                color="black"
                                mini={mini}
                            />
                        )
                    }
                    <QuantityButton
                        mode="add"
                        popupPosition="bottom right"
                        currentQuanitity={current}
                        onChange={onChangeValue}
                        color="black"
                        mini={mini}
                    />
                </Button.Group>
            </div>
        );
    }
}
