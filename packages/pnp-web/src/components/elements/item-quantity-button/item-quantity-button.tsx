import * as React from "react";
import { inject, external } from "tsdi";
import { StoreItems, StoreMarkerItems, StoreStashItems } from "../../../stores";
import { action, computed } from "mobx";
import { observer } from "mobx-react";
import { QuantityButton } from "../quantity-button";
import { SemanticSIZES } from "semantic-ui-react/dist/commonjs/generic";

export interface ItemQuantityButtonProps {
    mode: "remove" | "set" | "add";
    id: string;
    entityType?: "marker item" | "stash item" | "item";
    size?: SemanticSIZES;
}

@external @observer
export class ItemQuantityButton extends React.Component<ItemQuantityButtonProps> {
    @inject private items: StoreItems;
    @inject private stashItems: StoreStashItems;
    @inject private markerItems: StoreMarkerItems;

    @computed private get entity() {
        switch (this.props.entityType) {
            case "marker item":
                return this.markerItems.byId(this.props.id);
            case "stash item":
                return this.stashItems.byId(this.props.id);
            default:
            case "item":
                return this.items.byId(this.props.id);
        }
    }

    @computed private get value() {
        return this.entity.quantity;
    }

    @action.bound private async handleChange(newQuantity: number) {
        switch (this.props.entityType) {
            case "marker item":
                await this.markerItems.update(this.props.id, { quantity: newQuantity });
                return;
            case "stash item":
                await this.stashItems.update(this.props.id, { quantity: newQuantity });
                return;
            default:
            case "item":
                await this.items.update(this.props.id, { quantity: newQuantity });
                return;
        }
    }

    public render () {
        const { mode, size } = this.props;
        return (
            <QuantityButton size={size} mode={mode} currentQuanitity={this.value} onChange={this.handleChange}/>
        );
    }
}
