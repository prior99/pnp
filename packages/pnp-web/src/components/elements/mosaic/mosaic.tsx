import * as React from "react";
import * as css from "./mosaic.scss";
import * as classNames from "classnames/bind";

const cx = classNames.bind(css);

export interface MosaicProps {
    size?: "small" | "large" | "twice";
}

export class Mosaic extends React.Component<MosaicProps> {
    protected static defaultProps = {
        size: "small",
    };

    private get children() {
        if (Array.isArray(this.props.children)) {
            return this.props.children.map((child, index) => (
                <div className={css.column} key={index}>
                    {child}
                </div>
            ));
        }
        return (
            <div className={css.column}>
                {this.props.children}
            </div>
        );
    }

    public render() {
        return (
            <div className={cx("grid", this.props.size)}>
                {this.children}
            </div>
        );
    }
}
