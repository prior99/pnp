import * as React from "react";
import { SemanticICONS, SemanticCOLORS } from "semantic-ui-react/dist/commonjs/generic";
import { Button, Popup, SemanticSIZES } from "semantic-ui-react";
import { action, observable } from "mobx";
import { observer } from "mobx-react";

const validIcons: SemanticICONS[] = [
    // map
    "map marker",
    "map marker alternate",
    "map pin",
    "thumbtack",
    "map signs",
    "compass",
    "globe",
    "dot circle",
    "flag",
    "flag checkered",
    // chess
    "chess pawn",
    "chess bishop",
    "chess knight",
    "chess queen",
    "chess king",
    "chess rook",
    "chess",
    // person
    "male",
    "female",
    "child",
    "user secret",
    "user md",
    "users",
    "podcast",
    "id badge outline",
    // communication
    "handshake",
    "talk",
    "comment",
    "comments",
    "exclamation",
    "exclamation triangle",
    "info",
    "question",
    "times",
    "ban",
    "search",
    // notes
    "clipboard",
    "file",
    "sticky note",
    "envelope",
    "envelope open",
    "newspaper",
    "quote right",
    // creature
    "bug",
    "paw",
    "crosshairs",
    "bullseye",
    "shield",
    "bomb",
    // pointers
    "arrow left",
    "arrow right",
    "arrow up",
    "arrow down",
    "arrows alternate",
    "location arrow",
    // buildings
    "industry",
    "home",
    "warehouse",
    "building",
    "hospital",
    "image",
    "university",
    "road",
    "ship",
    "anchor",
    "stethoscope",
    "balance scale",
    // occupations
    "beer",
    "coffee",
    "birthday cake",
    "utensils",
    "bed",
    "magic",
    "quidditch",
    "flask",
    "gavel",
    "graduation cap",
    "music",
    "paint brush",
    "cut",
    "wrench",
    // loot
    "box",
    "boxes",
    "archive",
    "cube",
    "gift",
    "suitcase",
    "medkit",
    "trophy",
    "gem",
    "money",
    // weather
    "umbrella",
    "cloud",
    "bolt",
    "tint",
    "fire",
    "snowflake",
    "thermometer half",
    // nature
    "sun",
    "moon",
    "star",
    "tree",
    "leaf",
    "lemon",
    // other
    "key",
    "puzzle piece",
    "lock",
    "unlock",
    "lock open",
    "zoom-in",
    "zoom-out",
    "certificate",
    "clock",
    "stopwatch",
    "eye",
    "trash",
    "cogs",
    "binoculars",
    "life ring",
    "globe",
    "magnet",
    "futbol",
    "baseball ball",
    "heart",
    "check",
    "lightbulb",
    "hourglass",
    "bell",
    "sync",
    "recycle",
];

export interface IconSelectorProps {
    onSelect: (icon: SemanticICONS) => void;
    value: SemanticICONS;
    size?: SemanticSIZES;
    content?: string;
    color?: SemanticCOLORS;
}

@observer
export class IconSelector extends React.Component<IconSelectorProps> {
    @observable private isChaningIcon = false;

    @action.bound private startChangeIcon(evt: React.SyntheticEvent) {
        evt.preventDefault();
        this.isChaningIcon = true;
    }

    @action.bound private stopChangeIcon() {
        this.isChaningIcon = false;
    }

    @action.bound private handleIconChange(_, { icon }) {
        this.isChaningIcon = false;
        this.props.onSelect(icon);
    }

    public render() {
        return (
            <Popup
                trigger={
                    <Button
                        size={this.props.size}
                        content={this.props.content}
                        color={this.props.color}
                        icon={this.props.value}
                        onClick={evt => evt.preventDefault()}
                    />
                }
                position="bottom left"
                open={this.isChaningIcon}
                onOpen={this.startChangeIcon}
                onClose={this.stopChangeIcon}
                openOnTriggerClick
                openOnTriggerMouseEnter={false}
                closeOnTriggerMouseLeave={false}
            >
                {validIcons.map(iconName => {
                    return (
                        <Button
                            size="mini"
                            compact
                            icon={iconName}
                            color={this.props.value === iconName ? "olive" : undefined}
                            onClick={this.handleIconChange}
                        />
                    );
                })}
            </Popup>
        );
    }
}
