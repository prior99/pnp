import * as React from "react";
import { Label, Icon } from "semantic-ui-react";
import { action, computed } from "mobx";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { StoreTags } from "../../../stores";
import * as css from "./tag-label.scss";

export interface TagLabelProps {
    id: string;
    onClick?: () => void;
    onRemove?: () => void;
    editing?: boolean;
}

@external @observer
export class TagLabel extends React.Component<TagLabelProps> {
    @inject private tags: StoreTags;

    @action.bound private async handleMasterOnlyToggle() {
        await this.tags.update(this.props.id, { masterOnly: !this.tag.masterOnly });
    }

    @action.bound private async handlePinToggle() {
        await this.tags.update(this.props.id, { pinned: !this.tag.pinned });
    }

    @action.bound private handleClick() {
        if (this.props.onClick) { this.props.onClick(); }
    }

    @action.bound private handleRemove(event: React.MouseEvent<HTMLDivElement>) {
        event.stopPropagation();
        if (this.props.onRemove) { this.props.onRemove(); }
    }

    @computed private get tag() {
        return this.tags.byId(this.props.id);
    }

    @computed private get eyeIcon() {
        if (this.tag.masterOnly) { return "eye slash"; }
        return "eye";
    }

    @computed private get pinIcon() {
        if (this.tag.pinned) { return "bookmark"; }
        return "bookmark outline";
    }

    public render() {
        const { editing } = this.props;
        const { tag } = this;
        return (
            <Label onClick={this.handleClick}>
                {tag.name}
                {
                    editing && <>
                        <Icon name="delete" onClick={this.handleRemove} />
                        <Icon className={css.icon} name={this.eyeIcon} onClick={this.handleMasterOnlyToggle} />
                        <Icon className={css.icon} name={this.pinIcon} onClick={this.handlePinToggle} />
                    </>
                }
            </Label>
        );
    }
}
