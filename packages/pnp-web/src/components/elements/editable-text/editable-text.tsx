import * as React from "react";
import { observable, action } from "mobx";
import { observer } from "mobx-react";
import {
    TextArea,
    Icon,
    Input,
    Button,
    Form,
    DropdownItemProps,
    Dropdown,
} from "semantic-ui-react";
import * as css from "./editable-text.scss";
import { FieldSimple } from "hyrest-mobx";
import { Quill } from "../quill";

export interface EditableTextInputProps {
    value: string;
    onChange?: (value: string) => Promise<void>;
    number?: boolean;
    options?: DropdownItemProps[];
}

export interface EditableTextProps extends EditableTextInputProps {
    area?: boolean;
    onFinish?: (value: string) => Promise<void>;
    disabled?: boolean;
    formatted?: boolean;
    as?: any;
    extendedToolbar?: boolean;
    label?: string;
    inline?: boolean;
}

export function fromEditableTextField(
    target: FieldSimple<string>,
): EditableTextInputProps {
    return {
        value: target.value,
        onChange: (value: string) => target.update(value),
    };
}

export function fromEditableNumberField(
    target: FieldSimple<number>,
): EditableTextInputProps {
    return {
        number: true,
        value:
            target.value === null || target.value === undefined
                ? undefined
                : String(target.value),
        onChange: (value: string) => target.update(Number(value)),
    };
}

@observer
export class EditableText extends React.Component<EditableTextProps> {
    @observable private edit = false;
    @observable private loading = false;
    @observable private initialValue: string;

    @action.bound private async handleFinishEdit() {
        this.loading = true;
        if (this.props.onFinish) {
            await this.props.onFinish(this.props.value);
        }
        this.loading = false;
        this.initialValue = undefined;
        this.edit = false;
    }

    @action.bound private handleStringChange(value: string) {
        if (!this.props.onChange) {
            return;
        }
        this.props.onChange(value);
    }

    @action.bound private async handleDropdownChange(value: any) {
        if (!this.props.onChange) {
            return;
        }
        await this.props.onChange(value);
        await this.handleFinishEdit();
    }

    @action.bound private handleChange(
        event: React.SyntheticEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) {
        if (!this.props.onChange) {
            return;
        }
        this.props.onChange(event.currentTarget.value);
    }

    @action.bound private handleStartEdit() {
        this.edit = true;
        this.initialValue = this.props.value;
    }

    @action.bound private handleAbortEdit() {
        if (this.props.onChange) {
            this.props.onChange(this.initialValue);
        }
        this.initialValue = undefined;
        this.edit = false;
    }

    @action.bound private handleKeyDown(
        event: React.KeyboardEvent<HTMLInputElement>,
    ) {
        if (this.props.area) {
            return;
        }
        switch (event.key) {
            case "Enter":
                this.handleFinishEdit();
                break;
            case "Esc":
            case "Escape":
            case "Tab":
                this.handleAbortEdit();
                break;
            default:
                break;
        }
    }

    private renderIcon() {
        if (this.props.disabled) {
            return null;
        }
        return (
            <Icon
                size="small"
                name="pencil"
                color="grey"
                link
                onClick={this.handleStartEdit}
            />
        );
    }

    public renderContent() {
        const { edit, props } = this;
        if (props.formatted) {
            if (edit) {
                return (
                    <Form>
                        <Quill
                            value={this.props.value || ""}
                            onChange={this.handleStringChange}
                            readOnly={this.loading}
                            modules={
                                this.props.extendedToolbar
                                    ? {
                                          toolbar: [
                                              [
                                                  "bold",
                                                  "italic",
                                                  "underline",
                                                  "strike",
                                              ],
                                              [{ header: 1 }, { header: 2 }],
                                              ["blockquote", "code-block"],
                                              [
                                                  { color: [] },
                                                  { background: [] },
                                              ],
                                              [
                                                  { list: "ordered" },
                                                  { list: "bullet" },
                                              ],
                                          ],
                                      }
                                    : {
                                          toolbar: [
                                              [
                                                  "bold",
                                                  "italic",
                                                  "underline",
                                                  { color: [] },
                                                  { list: "bullet" },
                                              ],
                                          ],
                                      }
                            }
                        />
                        <br />
                        <br />
                        <Button
                            icon="checkmark"
                            onClick={this.handleFinishEdit}
                            loading={this.loading}
                            disabled={this.loading}
                            color="green"
                            fluid
                        />
                    </Form>
                );
            }
            return (
                <>
                    <Quill
                        value={this.props.value || ""}
                        readOnly
                        modules={{ toolbar: false }}
                        style={{}}
                    />
                    {this.renderIcon()}
                </>
            );
        }
        if (edit) {
            if (props.area) {
                return (
                    <Form>
                        <TextArea
                            ref={element => element && element.focus()}
                            disabled={this.loading}
                            rows={5}
                            value={this.props.value}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDown}
                        />
                        <br />
                        <br />
                        <Button
                            icon="checkmark"
                            onClick={this.handleFinishEdit}
                            loading={this.loading}
                            disabled={this.loading}
                            color="green"
                            fluid
                        />
                    </Form>
                );
            }
            if (props.options) {
                return (
                    <Dropdown
                        selection
                        options={props.options.map(option => ({
                            ...option,
                            onClick: () =>
                                this.handleDropdownChange(option.value),
                        }))}
                        disabled={this.loading}
                        fluid
                        value={this.props.value}
                        onKeyDown={this.handleKeyDown}
                        type={props.number ? "number" : undefined}
                    />
                );
            }
            return (
                <Input
                    label={
                        <Button
                            icon="checkmark"
                            onClick={this.handleFinishEdit}
                            loading={this.loading}
                            disabled={this.loading}
                            color="green"
                        />
                    }
                    ref={element => element && element.focus()}
                    disabled={this.loading}
                    labelPosition="right"
                    fluid
                    value={this.props.value}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    type={props.number ? "number" : undefined}
                />
            );
        }
        if (this.props.area) {
            return (
                <pre className={css.container}>
                    {`${this.props.value} `}
                    {this.renderIcon()}
                </pre>
            );
        }
        const Component: any = this.props.as || "span";
        return (
            <div className={css.container}>
                <Component>
                    {props.value === null || props.value === undefined
                        ? ""
                        : props.options
                        ? props.options.find(
                              option => option.value === props.value,
                          ).text
                        : props.value}
                </Component>
                {this.renderIcon()}
            </div>
        );
    }

    public render() {
        const { label, inline } = this.props;
        return (
            <Form.Field
                style={{
                    marginBottom: 5,
                    marginTop: 5,
                    display: inline ? "inline-block" : "block",
                    minWidth: 100,
                }}
            >
                {label && (
                    <label style={{ color: "rgba(40, 40, 40, .3)" }}>
                        {label}
                    </label>
                )}
                {this.renderContent()}
            </Form.Field>
        );
    }
}
