import * as React from "react";
import * as css from "./color-borders.scss";

export interface ColorBordersProps {
    colors: string[];
}

export class ColorBorders extends React.Component<ColorBordersProps> {
    public render() {
        return (
            <div className={css.wrapper}>
                {
                    this.props.colors.map(color => <div
                        style={{ background: color }}
                        key={color}
                        className={css.border}
                    />)
                }
            </div>
        );
    }
}
