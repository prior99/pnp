import * as React from "react";
import { external } from "tsdi";
import { bind } from "lodash-decorators";
import { Button, Popup, SemanticSIZES } from "semantic-ui-react";
import { SemanticICONS } from "semantic-ui-react/dist/commonjs/generic";

export interface ButtonDeleteProps<Item> {
    item?: Item;
    onConfirm: (item?: Item) => void;
    size?: SemanticSIZES;
    icon?: SemanticICONS;
    content?: string;
}

@external
export class ButtonConfirm<Item> extends React.Component<ButtonDeleteProps<Item>> {
    @bind private handleDelete(): void {
        this.props.onConfirm(this.props.item);
    }

    public render(): JSX.Element {
        const { size } = this.props;

        return (
            <Popup
                trigger={
                    <Button
                        size={size}
                        icon={this.props.icon || "trash alternate"}
                        content={this.props.content || "Delete"}
                    />
                }
                on="click"
            >
                <Popup.Header>Are you sure?</Popup.Header>
                <Popup.Content>
                    <Button
                        icon="check"
                        color="green"
                        size="mini"
                        content="Delete"
                        onClick={this.handleDelete}
                    />
                </Popup.Content>
            </Popup>
        );
    }
}
