import * as React from "react";
import { Input, Button } from "semantic-ui-react";
import { bind } from "lodash-decorators";
import { FieldSimple } from "hyrest-mobx";
import * as css from "./number-input.scss";
import { observer } from "mobx-react";
import { observable } from "mobx";

export interface NumberInputProps {
    value: number;
    nonNegative?: boolean;
    onChange: (newValue: number) => void;
}

export function fromNumberInputField(target: FieldSimple<number>): NumberInputProps {
    return {
        value: target.value || 0,
        onChange: (value: number) => {
            target.update(value);
            return;
        },
    };
}

@observer
export class NumberInput extends React.Component<NumberInputProps> {
    @observable private value = "0";

    constructor(props: NumberInputProps) {
        super(props);

        this.value = `${props.value}`;
    }

    public componentWillReceiveProps(newProps: NumberInputProps): void {
        this.value = `${newProps.value}`;
    }

    @bind public handlePlus(): void {
        this.props.onChange(this.props.value + 1);
    }

    @bind public handleMinus(): void {
        const newValue = this.props.value - 1;
        if (!this.props.nonNegative || newValue >= 0) {
            this.props.onChange(newValue);
        }
    }

    @bind public handleInputChange(_evt, { value }): void {
        if (!value) {
            this.value = "";
        }

        const newValue = parseInt(value, 10);

        if (!isNaN(newValue) && (!this.props.nonNegative || newValue >= 0)) {
            this.value = `${newValue}`;

            this.props.onChange(newValue);
        }
    }

    public render(): JSX.Element {
        return (
            <div className={css.container}>
                <Button className={css.button} size="mini" icon="minus" attached="left" onClick={this.handleMinus}/>
                <Input className={css.input} value={this.value} type="number" onChange={this.handleInputChange}/>
                <Button className={css.button} size="mini" icon="plus" attached="right" onClick={this.handlePlus}/>
            </div>
        );
    }
}
