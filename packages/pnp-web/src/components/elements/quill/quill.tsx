import ReactQuill from "react-quill";
import * as React from "react";
import { FieldSimple } from "hyrest-mobx";
import * as css from "./quill.scss";

export type QuillProps = Partial<JSX.LibraryManagedAttributes<typeof ReactQuill, ReactQuill["props"]>>;

export function fromQuillField(target: FieldSimple<string>): QuillProps {
    return {
        value: target.value || "",
        onChange: (value: string) => {
            target.update(value);
            return;
        },
    };
}

export class Quill extends React.Component<QuillProps> {
    public render() {
        return <ReactQuill
            className={this.props.readOnly ? css.quill : undefined}
            modules={{
                toolbar: [
                    ["bold", "italic", "underline", { color: [] }, { list: "bullet" }],
                ],
            }}
            style={{
                border: "1px solid rgba(34,36,38,.15)",
                borderRadius: ".28571429rem",
            }}
            {...this.props}
        />;
    }
}
