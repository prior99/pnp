import * as React from "react";
import { Ref, Card, Button, Icon, Popup } from "semantic-ui-react";
import { inject, external, initialize } from "tsdi";
import { StoreTemplateItems } from "../../../stores";
import { action, observable, autorun, computed } from "mobx";
import { observer } from "mobx-react";
import { TemplateItem, CombatTrait, ImageUpload } from "pnp-common";
import * as css from "./card-template-item.scss";
import { AddItemForm, AddStashItemForm } from "../../forms";
import { bind } from "lodash-decorators";
import { EditableText, fromEditableTextField, fromEditableNumberField, MiniAvatar } from "../../elements";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { UploadImageForm } from "../../forms/upload-image-form";

@external @observer @hasFields()
export class CardTemplateItem extends React.Component<{ id: string }> {
    @inject private templateItems: StoreTemplateItems;

    @field(TemplateItem) private templateItemField: FieldSimple<TemplateItem>;

    @observable private opened = false;
    @observable private addingItem = false;
    @observable private addingStashItem = false;

    private addItemButtonRef: HTMLButtonElement;
    private addStashItemButtonRef: HTMLButtonElement;

    @initialize
    protected initialize() {
        autorun(() => this.templateItem && this.templateItemField.update(this.templateItem));
    }

    @computed private get templateItem() {
        return this.templateItems.byId(this.props.id);
    }

    @action.bound private async handleNameChange(name: string) {
        await this.templateItems.update(this.props.id, { name: this.templateItemField.nested.name.value });
    }

    @action.bound private async handleDescriptionChange(description: string) {
        await this.templateItems.update(this.props.id, {
            description: this.templateItemField.nested.description.value || "",
        });
    }

    @action.bound private async handleEffectChange(effect: string) {
        await this.templateItems.update(this.props.id, { effect: this.templateItemField.nested.effect.value || "" });
    }

    @action.bound private handleOpen() {
        this.opened = !this.opened;
    }

    @action.bound private async handleDelete() {
        await this.templateItems.delete(this.props.id);
    }

    @action.bound private handleAddItemStart() {
        this.addingItem = true;
    }

    @action.bound private handleAddItem() {
        this.addingItem = false;
    }

    @action.bound private handleAddItemClose() {
        this.addingItem = false;
    }

    @action.bound private handleAddStashItemStart() {
        this.addingStashItem = true;
    }

    @action.bound private handleAddStashItem() {
        this.addingStashItem = false;
    }

    @action.bound private handleAddStashItemClose() {
        this.addingStashItem = false;
    }

    @action.bound private async handleSuggestedCombatTraitChange() {
        const suggestedCombatTrait: string = this.templateItemField.nested.suggestedCombatTrait.value;
        await this.templateItems.update(this.props.id, {
            suggestedCombatTrait: suggestedCombatTrait === "none" ? null : suggestedCombatTrait as CombatTrait,
        });
    }

    @action.bound private async handleSuggestedStaminaCostChange() {
        const suggestedStaminaCost = this.templateItemField.nested.suggestedStaminaCost.value;
        await this.templateItems.update(this.props.id, {
            suggestedStaminaCost: !suggestedStaminaCost ? null : suggestedStaminaCost,
        });
    }

    @bind public handleAddItemButtonRef(button: HTMLButtonElement) {
        this.addItemButtonRef = button;
    }

    @bind public handleAddStashItemButtonRef(button: HTMLButtonElement) {
        this.addStashItemButtonRef = button;
    }

    @action.bound private async handleUpload(upload: ImageUpload) {
        await this.templateItems.uploadImage(this.templateItem.id, upload);
    }

    public render() {
        const {
            name,
            description,
            unit,
            quantifiable,
            effect,
            suggestedCombatTrait,
            suggestedStaminaCost,
        } = this.templateItemField.nested;
        return (
            <Card fluid>
                <Card.Content>
                    <Icon
                        name={this.opened ? "angle up" : "angle down"}
                        onClick={this.handleOpen}
                        className={css.toggle}
                        size="large"
                    />
                    <Card.Header>
                        <MiniAvatar src={this.templateItem.imageUrl} />
                        <EditableText
                            inline
                            disabled={!this.opened}
                            {...fromEditableTextField(name)}
                            onFinish={this.handleNameChange}
                        />
                    </Card.Header>
                    <Card.Description>
                        {
                            this.opened && (
                                <>
                                    <UploadImageForm
                                        onUpload={this.handleUpload}
                                        url={this.templateItem.imageUrl}
                                        editable
                                    />
                                    <EditableText
                                        formatted
                                        disabled={!this.opened}
                                        area
                                        {...fromEditableTextField(description)}
                                        onFinish={this.handleDescriptionChange}
                                        label="Description"
                                    />
                                </>
                            )
                        }
                        <EditableText
                            formatted
                            disabled={!this.opened}
                            area
                            {...fromEditableTextField(effect)}
                            onFinish={this.handleEffectChange}
                            label="Rules"
                        />
                    </Card.Description>
                </Card.Content>
                {
                    this.opened && (
                        <>
                            <Card.Content>
                                <EditableText
                                    {...fromEditableNumberField(suggestedStaminaCost)}
                                    number
                                    onFinish={this.handleSuggestedStaminaCostChange}
                                    label="Suggested stamina cost"
                                />
                                <EditableText
                                    {...fromEditableTextField(suggestedCombatTrait)}
                                    options={[
                                        { key: "focus", value: "focus", text: "Focus" },
                                        { key: "aim", value: "aim", text: "Aim" },
                                        { key: "awareness", value: "awareness", text: "Awareness" },
                                        { key: "swiftness", value: "swiftness", text: "Swiftness" },
                                        { key: "none", value: "none", text: "None" },
                                    ]}
                                    onFinish={this.handleSuggestedCombatTraitChange}
                                    label="Corresponding combat trait"
                                />
                            </Card.Content>
                            {
                                quantifiable.value && (
                                    <Card.Content extra>
                                        Quantifiable in {unit.value}.
                                    </Card.Content>
                                )
                            }
                            <Card.Content extra>
                                <Button.Group size="mini">
                                    <Ref innerRef={this.handleAddStashItemButtonRef}>
                                        <Button
                                            icon={
                                                <Icon.Group>
                                                    <Icon name="suitcase" />
                                                    <Icon name="add" corner />
                                                </Icon.Group>
                                            }
                                            size="mini"
                                            onClick={this.handleAddStashItemStart}
                                        />
                                    </Ref>
                                    <Popup
                                        open={this.addingStashItem}
                                        context={this.addStashItemButtonRef}
                                        onClose={this.handleAddStashItemClose}
                                    >
                                        <Popup.Header>Add to stash</Popup.Header>
                                        <Popup.Content>
                                            <AddStashItemForm
                                                onAdd={this.handleAddStashItem}
                                                templateItemId={this.props.id}
                                            />
                                        </Popup.Content>
                                    </Popup>
                                    <Ref innerRef={this.handleAddItemButtonRef}>
                                        <Button
                                            icon="dolly flatbed"
                                            size="mini"
                                            onClick={this.handleAddItemStart}
                                        />
                                    </Ref>
                                    <Popup
                                        open={this.addingItem}
                                        context={this.addItemButtonRef}
                                        onClose={this.handleAddItemClose}
                                    >
                                        <Popup.Header>Add to inventory</Popup.Header>
                                        <Popup.Content>
                                            <AddItemForm onAdd={this.handleAddItem} templateItemId={this.props.id} />
                                        </Popup.Content>
                                    </Popup>
                                    <Popup
                                        trigger={<Button icon="trash alternate" size="mini" />}
                                        on="click"
                                    >
                                        <Popup.Header>Are you sure?</Popup.Header>
                                        <Popup.Content>
                                            <p>All items of this type will also be deleted from hero inventories.</p>
                                            <Button
                                                icon="check"
                                                color="green"
                                                size="mini"
                                                content="Delete"
                                                onClick={this.handleDelete}
                                            />
                                        </Popup.Content>
                                    </Popup>
                                </Button.Group>
                            </Card.Content>
                        </>
                    )
                }
            </Card>
        );
    }
}
