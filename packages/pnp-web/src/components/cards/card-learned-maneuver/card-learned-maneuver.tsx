import * as React from "react";
import { Card, Button, Icon, Popup } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreLearnedManeuvers, StoreManeuvers, StoreHeroes } from "../../../stores";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";
import * as classNames from "classnames/bind";
import * as css from "./card-learned-maneuver.scss";
import { Quill } from "../../elements";

const cx = classNames.bind(css);

export interface CardLearnedManeuverProps {
    id: string;
    onClick?: () => void;
    highlighted?: boolean;
    faded?: boolean;
}

@external @observer
export class CardLearnedManeuver extends React.Component<CardLearnedManeuverProps> {
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private maneuvers: StoreManeuvers;
    @inject private heroes: StoreHeroes;

    @observable private opened = false;

    @computed private get maneuver() {
        return this.maneuvers.byId(this.learnedManeuver.maneuver.id);
    }

    @computed private get hero() {
        return this.heroes.byId(this.learnedManeuver.hero.id);
    }

    @computed private get learnedManeuver() {
        return this.learnedManeuvers.byId(this.props.id);
    }

    @action.bound private handleOpen() {
        this.opened = !this.opened;
    }

    @action.bound private async handleDelete() {
        await this.learnedManeuvers.delete(this.props.id);
    }

    @action.bound private async handleStaminaClick(evt: React.SyntheticEvent<HTMLButtonElement>) {
        evt.stopPropagation();
        evt.preventDefault();
        this.heroes.update(this.hero.id, {
            spentStamina: this.hero.spentStamina + this.maneuver.suggestedStaminaCost,
        });
        return false;
    }

    public render() {
        const { faded } = this.props;
        return (
            <Card
                fluid
                onClick={this.props.onClick}
                color={this.props.highlighted ? "blue" : undefined}
                className={cx({ faded })}
            >
                <Card.Content>
                    {
                        this.maneuver && (
                            <>
                                <Card.Header>{this.maneuver.name}</Card.Header>
                                <Card.Description>
                                    {
                                        this.opened && <Quill
                                            value={this.maneuver.description}
                                            readOnly
                                            modules={{ toolbar: false }}
                                            style={{}}
                                        />
                                    }
                                    <Quill
                                        value={this.maneuver.effect}
                                        readOnly
                                        modules={{ toolbar: false }}
                                        style={{}}
                                    />
                                </Card.Description>
                                <div className={css.icon}>
                                    <Icon
                                        name={this.opened ? "angle up" : "angle down"}
                                        onClick={this.handleOpen}
                                        size="large"
                                        style={{ cursor: "pointer" }}
                                    />
                                </div>
                            </>
                        )
                    }
                </Card.Content>
                {
                    this.opened && (
                        <Card.Content extra>
                            <Button.Group size="mini">
                                <Popup
                                    trigger={<Button icon="trash alternate" size="mini" />}
                                    on="click"
                                >
                                    <Popup.Header>Are you sure?</Popup.Header>
                                    <Popup.Content>
                                        <Button
                                            icon="check"
                                            color="green"
                                            size="mini"
                                            content="Delete"
                                            onClick={this.handleDelete}
                                        />
                                    </Popup.Content>
                                </Popup>
                                {
                                    this.maneuver.suggestedStaminaCost && <Button
                                        icon={
                                            <Icon.Group>
                                                <Icon name="heartbeat" />
                                                <Icon corner name="minus circle" />
                                            </Icon.Group>
                                        }
                                        onClick={this.handleStaminaClick}
                                        size="mini"
                                    />
                                }
                            </Button.Group>
                        </Card.Content>
                    )
                }
            </Card>
        );
    }
}
