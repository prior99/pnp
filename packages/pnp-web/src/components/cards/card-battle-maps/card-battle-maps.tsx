import * as React from "react";
import { Card } from "semantic-ui-react";
import { BattleMapList } from "../../collections";
import { CreateBattleMapForm } from "../../forms";
import { observer } from "mobx-react";

@observer
export class CardBattleMaps extends React.Component<{ groupId?: string, presetId?: string}> {
    public render () {
        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>Battle maps</Card.Header>
                    <BattleMapList {...this.props} />
                </Card.Content>
                <Card.Content extra>
                    <CreateBattleMapForm {...this.props} />
                </Card.Content>
            </Card>
        );
    }
}
