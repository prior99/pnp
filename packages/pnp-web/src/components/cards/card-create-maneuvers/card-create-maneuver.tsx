import * as React from "react";
import { Input, Form, Button, Header, Dropdown } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreManeuvers } from "../../../stores";
import { action } from "mobx";
import { observer } from "mobx-react";
import { Maneuver, CombatTraitClassification, CombatTrait } from "pnp-common";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { formItem } from "../../../hyrest-semantic-ui";
import { Quill, fromQuillField } from "../../elements";
import { parentIds } from "../../../parent-id";

@external @observer @hasFields()
export class CardCreateManeuver extends React.Component<{ presetId?: string, groupId?: string }> {
    @inject private maneuvers: StoreManeuvers;

    @field(Maneuver) private maneuver: FieldSimple<Maneuver>;

    @action.bound private async handleSubmit() {
        await this.maneuvers.create({
            ...this.maneuver.value,
            ...parentIds(this.props),
        });
    }

    @action.bound private handleCategoryChange(_, { value }: { value: CombatTraitClassification }) {
        this.maneuver.nested.classification.update(value as any);
    }

    @action.bound private handleSuggestedCombatTraitChange(_, { value }: { value: CombatTrait }) {
        this.maneuver.nested.suggestedCombatTrait.update(value as any);
    }

    public render() {
        const {
            name,
            description,
            effect,
            suggestedCombatTrait,
            suggestedStaminaCost,
            classification,
        } = this.maneuver.nested;
        return (
            <>
                <Header>Create new maneuver</Header>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Field {...formItem(name)}>
                        <label>Name</label>
                        <Input
                            placeholder="Name"
                            {...name.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(description)}>
                        <label>Description</label>
                        <Quill {...fromQuillField(description)} />
                    </Form.Field>
                    <Form.Field {...formItem(effect)}>
                        <label>Rules</label>
                        <Quill {...fromQuillField(effect)} />
                    </Form.Field>
                    <Form.Field {...formItem(classification)}>
                        <label>Category</label>
                        <Dropdown
                            fluid
                            placeholder="Category"
                            onChange={this.handleCategoryChange}
                            value={classification.value}
                            selection
                            options={[
                                { icon: "shield alternative", key: "offensive", value: "offensive", text: "Attack" },
                                { icon: "fire", key: "defensive", value: "defensive", text: "Defense" },
                                { icon: "bolt", key: "instant", value: "instant", text: "Instant" },
                            ]}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(suggestedCombatTrait)}>
                        <label>Combat trait</label>
                        <Dropdown
                            fluid
                            onChange={this.handleSuggestedCombatTraitChange}
                            placeholder="Corresponding combat trait"
                            value={suggestedCombatTrait.value}
                            selection
                            options={[
                                { key: "focus", value: "focus", text: "Focus" },
                                { key: "aim", value: "aim", text: "Aim" },
                                { key: "awareness", value: "awareness", text: "Awareness" },
                                { key: "swiftness", value: "swiftness", text: "Swiftness" },
                            ]}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(suggestedStaminaCost)}>
                        <label>Stamina cost</label>
                        <Input
                            type="number"
                            placeholder="Suggested stamina cost"
                            value={
                                suggestedStaminaCost.value === null || suggestedStaminaCost.value === undefined
                                    ? undefined
                                    : String(suggestedStaminaCost.value)
                            }
                            onChange={evt => suggestedStaminaCost.update(Number(evt.currentTarget.value))}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Button
                            disabled={!this.maneuver.valid}
                            primary
                            htmlType="submit"
                            icon="plus"
                            content="Create"
                        />
                    </Form.Field>
                </Form>
            </>
        );
    }
}
