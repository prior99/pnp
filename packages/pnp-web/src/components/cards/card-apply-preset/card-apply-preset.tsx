import * as React from "react";
import { Card, Button, Form, Dropdown, Header } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreGroups, StorePresets } from "../../../stores";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";

@external @observer
export class CardApplyPreset extends React.Component<{ groupId: string }> {
    @inject private presets: StorePresets;
    @inject private groups: StoreGroups;

    @observable private presetId: string;

    @action.bound private async handlePresetChange(_, { value }: { value: string }) {
        this.presetId = value;
    }

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        await this.groups.applyPreset(this.props.groupId, this.presetId);
    }

    @computed private get valid() {
        return Boolean(this.presetId);
    }

    public render () {
        return (
            <Card fluid>
                <Card.Content>
                    <Header>Apply Preset</Header>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Field>
                            <label>Preset</label>
                            <Dropdown
                                placeholder="Preset"
                                search
                                selection
                                value={this.presetId}
                                options={this.presets.dropDownOptions}
                                onChange={this.handlePresetChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Button
                                disabled={!this.valid}
                                icon="check"
                                color="green"
                                size="mini"
                                content="Apply"
                                htmlType="submit"
                            />
                        </Form.Field>
                    </Form>
                </Card.Content>
            </Card>
        );
    }
}
