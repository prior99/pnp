import * as React from "react";
import { Ref, Card, Button, Icon, Popup, Form, Header } from "semantic-ui-react";
import { inject, external, initialize } from "tsdi";
import { StoreTemplateMonsters, StoreMonsterManeuvers, StoreStashs } from "../../../stores";
import { action, observable, autorun, computed } from "mobx";
import { observer } from "mobx-react";
import { pick } from "ramda";
import {
    TemplateMonster,
    allCombatTraits,
    ImageUpload,
} from "pnp-common";
import * as css from "./card-template-monster.scss";
import { bind } from "lodash-decorators";
import { EditableText, fromEditableTextField, MiniAvatar } from "../../elements";
import { EditCombatTraitsForm, AddManeuverForm, AddMonsterForm } from "../../forms";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { CardManeuver } from "../card-maneuver";
import { UploadImageForm } from "../../forms/upload-image-form";

@external @observer @hasFields()
export class CardTemplateMonster extends React.Component<{ id: string }> {
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private monsterManeuvers: StoreMonsterManeuvers;
    @inject private stashs: StoreStashs;

    @field(TemplateMonster) private templateMonsterField: FieldSimple<TemplateMonster>;

    @observable private opened = false;
    @observable private spawning = false;
    @observable private addingManeuver = false;
    @observable private editTraits = false;
    @observable private stashId: string | undefined;

    private spawnButtonRef: HTMLButtonElement;
    private maneuverButtonRef: HTMLButtonElement;

    @initialize
    protected initialize() {
        autorun(() => this.templateMonster && this.templateMonsterField.update(this.templateMonster));
        autorun(() => this.stashId = this.templateMonster.stash && this.templateMonster.stash.id);
    }

    @action.bound private async handleNameChange(name: string) {
        await this.templateMonsters.update(this.props.id, { name: this.templateMonsterField.nested.name.value });
    }

    @action.bound private async handleDescriptionChange(description: string) {
        await this.templateMonsters.update(this.props.id, {
            description: this.templateMonsterField.nested.description.value || "",
        });
    }

    @computed private get templateMonster() {
        return this.templateMonsters.byId(this.props.id);
    }

    @action.bound private handleOpen() {
        this.opened = !this.opened;
    }

    @action.bound private async handleDelete() {
        await this.templateMonsters.delete(this.props.id);
    }

    @action.bound private handleManeuverStart() {
        this.addingManeuver = true;
    }

    @action.bound private handleManeuver() {
        this.addingManeuver = false;
    }

    @action.bound private handleManeuverClose() {
        this.addingManeuver = false;
    }

    @action.bound private handleSpawn() {
        this.spawning = false;
    }

    @action.bound private handleSpawnStart() {
        this.spawning = true;
    }

    @action.bound private handleSpawnClose() {
        this.spawning = false;
    }

    @action.bound private handleStartEditTraits() {
        this.editTraits = true;
    }

    @action.bound private async handleFinishEditTraits() {
        this.editTraits = false;
        await this.templateMonsters.update(this.props.id, pick(allCombatTraits, this.templateMonsterField.value));
    }

    @action.bound private async handleStashChange(id: string) {
        this.stashId = id;
    }

    @action.bound private async handleFinishStashChange(id: string) {
        if (!this.stashId) {
            await this.templateMonsters.update(this.props.id, { stash: null });
            return;
        }
        await this.templateMonsters.update(this.props.id, {
            stash: { id: this.stashId },
        });
    }

    @bind public handleManeuverButtonRef(button: HTMLButtonElement) {
        this.maneuverButtonRef = button;
    }

    @bind public handleSpawnButtonRef(button: HTMLButtonElement) {
        this.spawnButtonRef = button;
    }

    @computed private get allMonsterManeuvers() {
        return this.monsterManeuvers.forMonster(this.templateMonster.id);
    }

    private renderCardContent(): JSX.Element {
        return (
            <Card.Content extra>
                <Form>
                    <Header size="small" as="h4">
                        Combat Traits {
                            !this.editTraits
                                ? <Icon
                                    size="small"
                                    name="pencil"
                                    color="grey"
                                    link
                                    onClick={this.handleStartEditTraits}
                                    style={{ fontSize: ".74em" }}
                                />
                                : null
                        }
                    </Header>
                    <EditCombatTraitsForm templateMonster={this.templateMonsterField} editActive={this.editTraits} />
                    <Form.Field>
                        {this.editTraits ? (
                            <Button
                                disabled={!this.templateMonsterField.valid}
                                icon="checkmark"
                                onClick={this.handleFinishEditTraits}
                                color="green"
                                fluid
                            />
                        ) : null}
                    </Form.Field>
                </Form>
            </Card.Content>
        );
    }

    @bind private handleManeuverDelete(maneuverId: string) {
        return action(() => {
            this.monsterManeuvers.delete(maneuverId);
        });
    }

    @action.bound private async handleUpload(upload: ImageUpload) {
        await this.templateMonsters.uploadImage(this.templateMonster.id, upload);
    }

    public render() {
        const { name, description } = this.templateMonsterField.nested;
        const levels = this.templateMonsterField.value.levelRange();
        const minLevel = Math.max(0, levels.min).toFixed(1);
        const maxLevel = levels.max.toFixed(1);
        const groupId = this.templateMonster.group && this.templateMonster.group.id;
        const presetId = this.templateMonster.preset && this.templateMonster.preset.id;

        return (
            <Card fluid>
                <Card.Content>
                    <Icon
                        name={this.opened ? "angle up" : "angle down"}
                        onClick={this.handleOpen}
                        className={css.toggle}
                        size="large"
                    />
                    <Card.Header>
                        <MiniAvatar src={this.templateMonster.imageUrl} />
                        <EditableText
                            inline
                            disabled={!this.opened}
                            {...fromEditableTextField(name)}
                            onFinish={this.handleNameChange}
                        />
                    </Card.Header>
                    <Card.Meta>
                        Levels: {minLevel === maxLevel ? minLevel : `${minLevel} - ${maxLevel}`}
                    </Card.Meta>
                    {
                        this.opened && <>
                            <UploadImageForm
                                onUpload={this.handleUpload}
                                url={this.templateMonster.imageUrl}
                                editable
                            />
                            <EditableText
                                formatted
                                disabled={!this.opened}
                                area
                                {...fromEditableTextField(description)}
                                onFinish={this.handleDescriptionChange}
                                label="Description"
                            />
                            {
                                <EditableText
                                    value={this.stashId}
                                    label="Loot stash"
                                    options={this.stashs.dropDownOptions(groupId, presetId, true)}
                                    onChange={this.handleStashChange}
                                    onFinish={this.handleFinishStashChange}
                                />
                            }
                        </>
                    }
                </Card.Content>
                {this.opened && this.renderCardContent()}
                {this.opened && this.allMonsterManeuvers.length > 0 &&
                    <Card.Content extra>
                        <Header size="small" as="h4">Maneuvers</Header>
                        {this.allMonsterManeuvers.map(monsterManeuver => (
                            <CardManeuver
                                id={monsterManeuver.maneuver.id}
                                onMonster
                                onDelete={this.handleManeuverDelete(monsterManeuver.id)}
                            />
                        ))
                        }
                    </Card.Content>
                }
                {
                    this.opened && (
                        <Card.Content extra>
                            <Button.Group size="mini">
                                <Ref innerRef={this.handleManeuverButtonRef}>
                                    <Button
                                        icon="student"
                                        size="mini"
                                        onClick={this.handleManeuverStart}
                                    />
                                </Ref>
                                <Popup
                                    open={this.addingManeuver}
                                    context={this.maneuverButtonRef}
                                    onClose={this.handleManeuverClose}
                                >
                                    <Popup.Header>Teach maneuver</Popup.Header>
                                    <Popup.Content>
                                        <AddManeuverForm
                                            onAdd={this.handleManeuver}
                                            templateMonsterId={this.props.id}
                                        />
                                    </Popup.Content>
                                </Popup>
                                <Ref innerRef={this.handleSpawnButtonRef}>
                                    <Button
                                        icon={
                                            <Icon.Group>
                                                <Icon name="bug" />
                                                <Icon corner name="add" />
                                            </Icon.Group>
                                        }
                                        size="mini"
                                        onClick={this.handleSpawnStart}
                                    />
                                </Ref>
                                <Popup
                                    open={this.spawning}
                                    context={this.spawnButtonRef}
                                    onClose={this.handleSpawnClose}
                                >
                                    <Popup.Header>Spawn Monster</Popup.Header>
                                    <Popup.Content>
                                        <AddMonsterForm
                                            onAdd={this.handleSpawn}
                                            templateMonsterId={this.props.id}
                                        />
                                    </Popup.Content>
                                </Popup>
                                <Popup
                                    trigger={<Button icon="trash alternate" size="mini" />}
                                    on="click"
                                >
                                    <Popup.Header>Are you sure?</Popup.Header>
                                    <Popup.Content>
                                        <p>All active monsters of this type will also be deleted.</p>
                                        <Button
                                            icon="check"
                                            color="green"
                                            size="mini"
                                            content="Delete"
                                            onClick={this.handleDelete}
                                        />
                                    </Popup.Content>
                                </Popup>
                            </Button.Group>
                        </Card.Content>
                    )
                }
            </Card>
        );
    }
}
