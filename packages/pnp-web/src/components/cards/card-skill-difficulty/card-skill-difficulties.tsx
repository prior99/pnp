import * as React from "react";
import { Card, Dropdown, Table, Header } from "semantic-ui-react";
import { ResponsiveLine } from "@nivo/line";
import { inject, external } from "tsdi";
import { StoreHeroes } from "../../../stores";
import { action, computed, observable } from "mobx";
import { observer } from "mobx-react";
import { P as prob } from "dice-probabilities";
import {
    allBaseSkills,
    getSubSkills,
    skillDictionary,
    Skill,
    SKILL_AWARDED_PER_LEVEL,
    SKILL_COST_PER_RANK,
} from "pnp-common";
import { bind } from "lodash-decorators";

const AnyLineChart = ResponsiveLine as any;

@external @observer
export class CardSkillDifficulty extends React.Component<{ id: string }> {
    @inject private heroes: StoreHeroes;

    @observable private selectedSkill: Skill;

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get data() {
        const offset = this.selectedSkill ? this.hero.rank(this.selectedSkill) : 0;
        return this.calculateDistribution(offset);
    }

    @computed private get defaultDistribution() {
        return this.calculateDistribution(0);
    }

    private calculateDistribution(offset = 0) {
        const data = [];
        let sum = 0;
        for (let x = 4; x < 30; ++x) {
            sum += prob(Math.max(4, x - offset), 4, 6);
            data.push({
                x,
                y: 1 - sum,
            });
        }
        return data;
    }

    @bind private handleSkillSelect(value?: Skill) {
        return action(() => this.selectedSkill = value);
    }

    private get skillOptions() {
        return allBaseSkills.reduce((list, baseSkill) => {
            return [
                ...list,
                <Dropdown.Item>
                    <Dropdown
                        text={`${skillDictionary[baseSkill]} (${this.hero.rank(baseSkill)})`}
                        pointing
                        icon={null}
                    >
                        <Dropdown.Menu>
                            <Dropdown.Item key={baseSkill} onClick={this.handleSkillSelect(baseSkill)}>
                                <b>{skillDictionary[baseSkill]} ({this.hero.rank(baseSkill)})</b>
                            </Dropdown.Item>
                            {
                                getSubSkills(baseSkill).reduce((result, subSkill) => {
                                    return [
                                        ...result,
                                        <Dropdown.Item key={subSkill} onClick={this.handleSkillSelect(subSkill)}>
                                            {skillDictionary[subSkill]} ({this.hero.rank(subSkill)})
                                        </Dropdown.Item>,
                                    ];
                                }, [])
                            }
                        </Dropdown.Menu>
                    </Dropdown>
                </Dropdown.Item>,
            ];
        }, []);
    }

    private valueForProbability(probability: number) {
        const adjustment: number = Math.floor(this.hero.level / (2 * SKILL_COST_PER_RANK / SKILL_AWARDED_PER_LEVEL));
        return adjustment + this.defaultDistribution.find(({ y }) => y < probability).x as number;
    }

    public render () {
        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>Skill probabilities</Card.Header>
                    <br />
                    <Dropdown
                        text={
                            this.selectedSkill ?
                                `${skillDictionary[this.selectedSkill]} (${this.hero.rank(this.selectedSkill)})` :
                                "Select skill"
                        }
                        button
                        labeled
                        pointing
                    >
                        <Dropdown.Menu>
                            <Dropdown.Item onClick={this.handleSkillSelect()}>
                                <b>None</b>
                            </Dropdown.Item>
                            {this.skillOptions}
                        </Dropdown.Menu>
                    </Dropdown>
                    <div style={{ height: 400 }}>
                        <AnyLineChart
                            data={[
                                {
                                    id: "Probability",
                                    data: this.data,
                                },
                            ]}
                            curve="monotoneX"
                            colors="category10"
                            margin={{
                                top: 20,
                                right: 0,
                                bottom: 60,
                                left: 35,
                            }}
                            xScale={{
                                type: "linear",
                                min: "auto",
                                max: "auto",
                            }}
                            yScale={{
                                type: "linear",
                                min: "auto",
                                max: "auto",
                            }}
                            axisBottom={{
                                legend: "Probability",
                                legendOffset: 36,
                                legendPosition: "middle",
                            }}
                            axisLeft={{
                                format: data => `${Math.round(data * 100)}%`,
                            }}
                            tooltipFormat={data => `${Math.round(data * 100)}%`}
                        />
                    </div>
                </Card.Content>
                <Card.Content>
                    <Card.Header>Suggested values at level {this.hero.level}</Card.Header>
                    <Table basic="very">
                        <Table.Body>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as="h4">Easy</Header>
                                </Table.Cell>
                                <Table.Cell>
                                    {this.valueForProbability(0.8)}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as="h4">Normal</Header>
                                </Table.Cell>
                                <Table.Cell>
                                    {this.valueForProbability(0.7)}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as="h4">Challenging</Header>
                                </Table.Cell>
                                <Table.Cell>
                                    {this.valueForProbability(0.4)}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as="h4">Hard</Header>
                                </Table.Cell>
                                <Table.Cell>
                                    {this.valueForProbability(0.3)}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as="h4">Impossible</Header>
                                </Table.Cell>
                                <Table.Cell>
                                    {this.valueForProbability(0.2)}
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                </Card.Content>
            </Card>
        );
    }
}
