import * as React from "react";
import { Card, SemanticCOLORS, SemanticICONS } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreHeroes, StoreLogin } from "../../../stores";
import { computed, action, observable } from "mobx";
import { observer } from "mobx-react";
import {
    getCombatTraitsForCategory,
    CombatTraitCategory,
    combatDictionary,
    CombatTrait,
    isOffensive,
    isDefensive,
} from "pnp-common";
import * as css from "./card-combat-traits.scss";
import { SegmentLevelValue } from "../../elements";
import * as help from "../../../texts";

export const baseCombatTraitColors: { [key in CombatTraitCategory]: SemanticCOLORS } = {
    combatStyle: "red",
    body: "blue",
    fitness: "green",
};

export interface CardCombatTraitsProps {
    category: CombatTraitCategory;
    hero: string;
    highlightOffensive: boolean;
    highlightDefensive: boolean;
}

export function getCombatTraitIcon(trait: CombatTrait): SemanticICONS {
    switch (trait) {
        case "focus": return "fire";
        case "aim": return "fire";
        case "awareness": return "fire";
        case "swiftness": return "shield alternative" as SemanticICONS;
        case "tension": return "shield alternative" as SemanticICONS;
        case "power": return "fire";
        case "stamina": return "heartbeat";
        case "constitution": return "heart";
        default: return;
    }
}

@external @observer
export class CardCombatTraits extends React.Component<CardCombatTraitsProps> {
    @inject private heroes: StoreHeroes;
    @inject private login: StoreLogin;

    @observable private inPreview: CombatTrait;
    @observable private loading = false;

    private get hero() {
        return this.heroes.byId(this.props.hero);
    }

    @action.bound private async handleReduce(name: CombatTrait) {
        this.loading = true;
        this.hero[name]--;
        await this.heroes.update(this.hero.id, {
            [name]: this.hero[name],
        });
        this.loading = false;
        this.inPreview = undefined;
    }

    @computed private get canSpendCombatTrait() {
        return this.hero.unspentCombatTrait(this.props.category) > 0;
    }

    @action.bound private async handleSpendCombatTrait() {
        this.loading = true;
        await this.heroes.update(this.hero.id, {
            [this.inPreview]: this.hero[this.inPreview],
        });
        this.loading = false;
        this.inPreview = undefined;
    }

    @action.bound private handlePreviewStart(trait: CombatTrait) {
        this.inPreview = trait;
        this.hero[trait]++;
    }

    @action.bound private handlePreviewAbort() {
        this.hero[this.inPreview]--;
        this.inPreview = undefined;
    }

    @computed private get color() {
        return baseCombatTraitColors[this.props.category];
    }

    @computed private get masterMode() {
        if (this.hero.preset) { return true; }
        if (this.hero.isNpc) { return true; }
        if (this.login.isMaster(this.hero.group.id)) { return true; }
        return false;
    }

    public render () {
        const { hero, props, canSpendCombatTrait, color } = this;
        const { category, highlightDefensive, highlightOffensive } = props;
        const hasHighlight = highlightDefensive || highlightOffensive;
        return (
            <Card fluid color={color} className={css.segment}>
                <Card.Content>
                    <Card.Header>{combatDictionary[category]}</Card.Header>
                </Card.Content>
                {
                    getCombatTraitsForCategory(category).map(trait => {
                        const faded = hasHighlight && (
                            (!isOffensive(trait) && highlightOffensive) ||
                            (!isDefensive(trait) && highlightDefensive)
                        );
                        const icon = getCombatTraitIcon(trait);
                        return (
                            <SegmentLevelValue
                                humanReadable={combatDictionary[trait]}
                                rank={hero.combatRank(trait)}
                                rankProgress={hero.combatTraitProgress(trait)}
                                skillToNextRank={hero.pointsToNextCombatTraitRank(trait)}
                                canSpend={canSpendCombatTrait}
                                inPreview={Boolean(this.inPreview)}
                                color={color}
                                valueName={trait}
                                onPreviewStart={this.handlePreviewStart}
                                onSpend={this.handleSpendCombatTrait}
                                onPreviewAbort={this.handlePreviewAbort}
                                spending={this.inPreview === trait}
                                loading={this.loading}
                                help={help.combat[category][trait]}
                                editable
                                masterMode={this.masterMode}
                                onReduce={this.handleReduce}
                                faded={faded}
                                icon={icon}
                            />
                        );
                    })
                }
            </Card>
        );
    }
}
