import * as React from "react";
import { Segment, SemanticCOLORS } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreHeroes, StoreLogin } from "../../../stores";
import { computed, action, observable } from "mobx";
import { observer } from "mobx-react";
import { getSubSkills, BaseSkill, skillDictionary, SubSkill } from "pnp-common";
import * as css from "./card-skills.scss";
import { SegmentLevelValue } from "../../elements";
import * as help from "../../../texts";

export const baseSkillColors: { [key in BaseSkill]: SemanticCOLORS } = {
    mind: "yellow",
    stealth: "violet",
    dexterity: "grey",
    strength: "red",
    agility: "blue",
    charisma: "orange",
    medicine: "green",
    senses: "pink",
};

@external @observer
export class CardSkills extends React.Component<{ skill: BaseSkill, hero: string }> {
    @inject private heroes: StoreHeroes;
    @inject private login: StoreLogin;

    @observable private inPreview: SubSkill;
    @observable private loading = false;

    private get hero() {
        return this.heroes.byId(this.props.hero);
    }

    @computed private get canSpendSkill() {
        return this.hero.unspentSkill > 0;
    }

    @action.bound private async handleReduce(name: SubSkill) {
        this.loading = true;
        this.hero[name]--;
        await this.heroes.update(this.hero.id, {
            [name]: this.hero[name],
        });
        this.loading = false;
        this.inPreview = undefined;
    }

    @action.bound private async handleSpendSkill() {
        this.loading = true;
        await this.heroes.update(this.hero.id, {
            [this.inPreview]: this.hero[this.inPreview],
        });
        this.loading = false;
        this.inPreview = undefined;
    }

    @action.bound private handlePreviewStart(subSkill: SubSkill) {
        this.inPreview = subSkill;
        this.hero[subSkill]++;
    }

    @action.bound private handlePreviewAbort() {
        this.hero[this.inPreview]--;
        this.inPreview = undefined;
    }

    @computed private get color() {
        return baseSkillColors[this.props.skill];
    }

    @computed private get masterMode() {
        if (this.hero.preset) { return true; }
        if (this.hero.isNpc) { return true; }
        if (this.login.isMaster(this.hero.group.id)) { return true; }
        return false;
    }

    public render () {
        const { hero, props, canSpendSkill, color } = this;
        const { skill } = props;
        return (
            <Segment color={color} className={css.segment}>
                <SegmentLevelValue
                    humanReadable={skillDictionary[skill]}
                    rank={hero.rank(skill)}
                    dice="+4D6"
                    rankProgress={hero.rankProgress(skill)}
                    skillToNextRank={hero.pointsToNextSkillRank(skill)}
                    canSpend={canSpendSkill}
                    inPreview={Boolean(this.inPreview)}
                    color={color}
                    highlight
                    masterMode={this.masterMode}
                    onReduce={this.handleReduce}
                    help={help.skills[skill]}
                />
                {
                    getSubSkills(this.props.skill).map(subSkill => {
                        return (
                            <SegmentLevelValue
                                humanReadable={skillDictionary[subSkill]}
                                rank={hero.rank(subSkill)}
                                dice="+4D6"
                                rankProgress={hero.rankProgress(subSkill)}
                                skillToNextRank={hero.pointsToNextSkillRank(subSkill)}
                                canSpend={this.canSpendSkill}
                                inPreview={Boolean(this.inPreview)}
                                color={color}
                                valueName={subSkill}
                                onPreviewStart={this.handlePreviewStart}
                                onSpend={this.handleSpendSkill}
                                onPreviewAbort={this.handlePreviewAbort}
                                spending={this.inPreview === subSkill}
                                loading={this.loading}
                                editable
                                masterMode={this.masterMode}
                                onReduce={this.handleReduce}
                                help={help.skills[subSkill]}
                            />
                        );
                    })
                }
            </Segment>
        );
    }
}
