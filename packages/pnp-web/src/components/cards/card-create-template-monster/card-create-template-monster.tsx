import * as React from "react";
import { Input, Form, Button, Header } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreTemplateMonsters } from "../../../stores";
import { action } from "mobx";
import { observer } from "mobx-react";
import {
    TemplateMonster,
    allCombatTraits,
    INITIAL_MONSTER_CONSTITUTION_POINTS,
    INITIAL_MONSTER_STAMINA_POINTS,
} from "pnp-common";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { formItem } from "../../../hyrest-semantic-ui";
import { Quill, fromQuillField } from "../../elements";
import { EditCombatTraitsForm } from "../../forms";
import { parentIds } from "../../../parent-id";

@external @observer @hasFields()
export class CardCreateTemplateMonster extends React.Component<{ presetId?: string, groupId?: string }> {
    @inject private templateMonsters: StoreTemplateMonsters;

    @field(TemplateMonster) private templateMonster: FieldSimple<TemplateMonster>;

    public componentDidMount(): void {
        allCombatTraits.forEach(trait => {
            this.templateMonster.nested[trait].update(0);
        });

        this.templateMonster.nested.constitution.update(INITIAL_MONSTER_CONSTITUTION_POINTS);

        this.templateMonster.nested.stamina.update(INITIAL_MONSTER_STAMINA_POINTS);
    }

    @action.bound private async handleSubmit() {
        await this.templateMonsters.create({
            ...this.templateMonster.value,
            ...parentIds(this.props),
        });

        this.templateMonster.reset();
    }

    public render () {
        const { name, description } = this.templateMonster.nested;

        return (
            <>
                    <Header>Create new monster</Header>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Field {...formItem(name)}>
                            <label>Name</label>
                            <Input
                                placeholder="Name"
                                {...name.reactInput}
                            />
                        </Form.Field>
                        <Form.Field {...formItem(description)}>
                            <label>Description</label>
                            <Quill {...fromQuillField(description)} />
                        </Form.Field>
                        <EditCombatTraitsForm templateMonster={this.templateMonster} editActive/>
                        <Form.Field>
                            <Button
                                disabled={!this.templateMonster.valid}
                                primary
                                htmlType="submit"
                                icon="plus"
                                content="Create"
                            />
                        </Form.Field>
                    </Form>
            </>
        );
    }
}
