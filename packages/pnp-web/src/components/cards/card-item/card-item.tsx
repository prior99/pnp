import * as React from "react";
import { Card, Button, Icon, Popup } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreTemplateItems, StoreItems, StoreHeroes } from "../../../stores";
import { action, computed, observable } from "mobx";
import * as classNames from "classnames/bind";
import { observer } from "mobx-react";
import * as css from "./card-item.scss";
import { ItemQuantityButton, Quill, MiniAvatar } from "../../elements";
import { UploadImageForm } from "../../forms";

const cx = classNames.bind(css);

export interface CardItemProps {
    id: string;
    onClick?: () => void;
    highlighted?: boolean;
    faded?: boolean;
}

@external
@observer
export class CardItem extends React.Component<CardItemProps> {
    @inject private templateItems: StoreTemplateItems;
    @inject private items: StoreItems;
    @inject private heroes: StoreHeroes;

    @observable private opened = false;

    @computed private get templateItem() {
        return this.templateItems.byId(this.item.templateItem.id);
    }

    @computed private get item() {
        return this.items.byId(this.props.id);
    }

    @computed private get hero() {
        return this.heroes.byId(this.item.hero.id);
    }

    @action.bound private handleOpen() {
        this.opened = !this.opened;
    }

    @action.bound private async handleDelete() {
        await this.items.delete(this.props.id);
    }

    @action.bound private async handleStaminaClick(
        evt: React.SyntheticEvent<HTMLButtonElement>,
    ) {
        evt.stopPropagation();
        evt.preventDefault();
        this.heroes.update(this.hero.id, {
            spentStamina:
                this.hero.spentStamina + this.templateItem.suggestedStaminaCost,
        });
        return false;
    }

    public render() {
        const { faded } = this.props;
        return (
            <Card
                fluid
                onClick={this.props.onClick}
                color={this.props.highlighted ? "blue" : undefined}
                className={cx({ faded })}
            >
                <Card.Content>
                    {this.templateItem && (
                        <>
                            <Card.Header>
                                <div className={css.header}>
                                    <div className={css.avatar}>
                                        <MiniAvatar
                                            src={this.templateItem.imageUrl}
                                        />
                                    </div>
                                    <div className={css.texts}>
                                        <div className={css.text}>
                                            {this.templateItem.name}
                                        </div>
                                        {this.templateItem.quantifiable && (
                                            <div
                                                className={`${css.text} ${css.quantity}`}
                                            >
                                                {this.item.quantity}
                                                {this.templateItem.unit || ""}
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className={css.expand}>
                                    <Icon
                                        name={
                                            this.opened
                                                ? "angle up"
                                                : "angle down"
                                        }
                                        onClick={this.handleOpen}
                                        size="large"
                                        style={{ cursor: "pointer" }}
                                        fitted
                                    />
                                </div>
                            </Card.Header>
                            <Card.Description>
                                {this.opened && (
                                    <>
                                        <UploadImageForm
                                            url={this.templateItem.imageUrl}
                                        />
                                        <Quill
                                            value={
                                                this.templateItem.description
                                            }
                                            readOnly
                                            modules={{ toolbar: false }}
                                            style={{}}
                                        />
                                    </>
                                )}
                                <Quill
                                    value={this.templateItem.effect}
                                    readOnly
                                    modules={{ toolbar: false }}
                                    style={{}}
                                />
                            </Card.Description>
                        </>
                    )}
                </Card.Content>
                {this.opened && (
                    <Card.Content extra>
                        {this.templateItem.quantifiable && (
                            <Button.Group
                                size="mini"
                                style={{ marginRight: 5 }}
                            >
                                <ItemQuantityButton
                                    mode="remove"
                                    id={this.props.id}
                                />
                                <ItemQuantityButton
                                    mode="set"
                                    id={this.props.id}
                                />
                                <ItemQuantityButton
                                    mode="add"
                                    id={this.props.id}
                                />
                            </Button.Group>
                        )}
                        <Popup
                            trigger={
                                <Button icon="trash alternate" size="mini" />
                            }
                            on="click"
                        >
                            <Popup.Header>Are you sure?</Popup.Header>
                            <Popup.Content>
                                <Button
                                    icon="check"
                                    color="green"
                                    size="mini"
                                    content="Delete"
                                    onClick={this.handleDelete}
                                />
                            </Popup.Content>
                        </Popup>
                        {this.templateItem.suggestedStaminaCost && (
                            <Button
                                icon={
                                    <Icon.Group>
                                        <Icon name="heartbeat" />
                                        <Icon corner name="minus circle" />
                                    </Icon.Group>
                                }
                                onClick={this.handleStaminaClick}
                                size="mini"
                            />
                        )}
                    </Card.Content>
                )}
            </Card>
        );
    }
}
