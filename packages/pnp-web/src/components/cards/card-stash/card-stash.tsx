import * as React from "react";
import { Card, Button, Icon, Popup, Ref, Grid } from "semantic-ui-react";
import { inject, external, initialize } from "tsdi";
import { StoreStashs, StoreStashItems, StoreTemplateItems } from "../../../stores";
import { action, observable, autorun, computed } from "mobx";
import { observer } from "mobx-react";
import { Stash } from "pnp-common";
import * as css from "./card-stash.scss";
import { EditableText, fromEditableTextField } from "../../elements";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { AddStashItemForm } from "../../forms";
import { bind } from "lodash-decorators";
import { StashItemRow } from "./stash-item-row";

@external @observer @hasFields()
export class CardStash extends React.Component<{ id: string }> {
    @inject private stashs: StoreStashs;
    @inject private stashItems: StoreStashItems;
    @inject private templateItems: StoreTemplateItems;

    @field(Stash) private stashField: FieldSimple<Stash>;

    @observable private opened = false;
    @observable private adding = false;
    @observable private money: number | undefined;

    private buttonRef: HTMLButtonElement;

    @initialize
    protected initialize() {
        autorun(() => this.stash && this.stashField.update(this.stash));
        autorun(() => this.money = this.stash.money);
    }

    @computed private get stash() {
        return this.stashs.byId(this.props.id);
    }

    @computed private get allStashItems() {
        return this.stashItems.forStash(this.stash.id).sort((a, b) => {
            const templateItemA = this.templateItems.byId(a.templateItem.id);
            const templateItemB = this.templateItems.byId(b.templateItem.id);
            return templateItemA.name.localeCompare(templateItemB.name);
        });
    }

    @action.bound private async handleNameChange(name: string) {
        await this.stashs.update(this.props.id, { name: this.stashField.nested.name.value });
    }

    @action.bound private async handleSetMoney() {
        await this.stashs.update(this.stash.id, { money: this.money || this.stash.money });
    }

    @action.bound private async handleMoneyChange(value: string) {
        if (value === "") {
            this.money = undefined;
            return;
        }
        this.money = isNaN(Number(value)) ? this.money : Number(value);
    }

    @action.bound private handleOpen() {
        this.opened = !this.opened;
    }

    @action.bound private async handleDelete() {
        await this.stashs.delete(this.props.id);
    }

    @action.bound private handleAddStart() {
        this.adding = true;
    }

    @action.bound private handleAdd() {
        this.adding = false;
    }

    @action.bound private handleClose() {
        this.adding = false;
    }

    @bind public handleButtonRef(button: HTMLButtonElement) {
        this.buttonRef = button;
    }

    public render() {
        const { name } = this.stashField.nested;
        return (
            <Card fluid>
                <Card.Content>
                    <Icon
                        name={this.opened ? "angle up" : "angle down"}
                        onClick={this.handleOpen}
                        className={css.toggle}
                        size="large"
                    />
                    <Card.Header>
                        <EditableText
                            disabled={!this.opened}
                            {...fromEditableTextField(name)}
                            onFinish={this.handleNameChange}
                        />
                    </Card.Header>
                </Card.Content>
                {
                    this.opened && (
                        <>
                            <Card.Content>
                                <EditableText
                                    label="Money"
                                    inline
                                    value={this.money === undefined ? "" : `${this.money}`}
                                    onChange={this.handleMoneyChange}
                                    onFinish={this.handleSetMoney}
                                />
                            </Card.Content>
                            <Card.Content extra>
                                <Grid>
                                    {this.allStashItems.map(stashItem => (
                                        <StashItemRow key={stashItem.id} id={stashItem.id} />
                                    ))}
                                </Grid>
                            </Card.Content>
                            <Card.Content extra>
                                <Button.Group size="mini">
                                    <Popup
                                        open={this.adding}
                                        context={this.buttonRef}
                                        onClose={this.handleClose}
                                    >
                                        <Popup.Header>Add to inventory</Popup.Header>
                                        <Popup.Content>
                                            <AddStashItemForm onAdd={this.handleAdd} stashId={this.props.id} />
                                        </Popup.Content>
                                    </Popup>
                                    <Ref innerRef={this.handleButtonRef}>
                                        <Button
                                            icon={
                                                <Icon.Group>
                                                    <Icon name="box" />
                                                    <Icon name="add" corner />
                                                </Icon.Group>
                                            }
                                            size="mini"
                                            onClick={this.handleAddStart}
                                        />
                                    </Ref>
                                    <Popup
                                        trigger={<Button icon="trash alternate" size="mini" />}
                                        on="click"
                                    >
                                        <Popup.Header>Are you sure?</Popup.Header>
                                        <Popup.Content>
                                            <Button
                                                icon="check"
                                                color="green"
                                                size="mini"
                                                content="Delete"
                                                onClick={this.handleDelete}
                                            />
                                        </Popup.Content>
                                    </Popup>
                                </Button.Group>
                            </Card.Content>
                        </>
                    )
                }
            </Card>
        );
    }
}
