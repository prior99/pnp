import * as React from "react";
import { observer } from "mobx-react";
import { StoreStashItems, StoreTemplateItems } from "../../../stores";
import { inject, external } from "tsdi";
import { Grid, Button } from "semantic-ui-react";
import { computed, action } from "mobx";
import { ItemQuantityButton } from "../../elements";

@external @observer
export class StashItemRow extends React.Component<{ id: string }> {
    @inject private stashItems: StoreStashItems;
    @inject private templateItems: StoreTemplateItems;

    @computed private get stashItem() {
        return this.stashItems.byId(this.props.id);
    }

    @computed private get templateItem() {
        return this.templateItems.byId(this.stashItem.templateItem.id);
    }

    @action.bound private async handleDelete() {
        await this.stashItems.delete(this.props.id);
    }

    public render() {
        return (
            <Grid.Row>
                <Grid.Column computer={7}>{this.templateItem.name}</Grid.Column>
                <Grid.Column computer={3}>
                    {
                        this.templateItem.quantifiable && <>
                            {this.stashItem.quantity}
                            {this.templateItem.unit ? this.templateItem.unit : ""}
                        </>
                    }
                </Grid.Column>
                <Grid.Column computer={6} style={{ display: "flex", justifyContent: "flex-end" }}>
                    <Button size="mini" icon="trash" onClick={this.handleDelete} />
                    {
                        this.templateItem.quantifiable && (
                            <Button.Group size="mini" style={{ marginRight: 5 }}>
                                <ItemQuantityButton
                                    size="mini"
                                    mode="remove"
                                    id={this.props.id}
                                    entityType="stash item"
                                />
                                <ItemQuantityButton size="mini" mode="set" id={this.props.id} entityType="stash item" />
                                <ItemQuantityButton size="mini" mode="add" id={this.props.id} entityType="stash item" />
                            </Button.Group>
                        )
                    }
                </Grid.Column>
            </Grid.Row>
        );
    }
}
