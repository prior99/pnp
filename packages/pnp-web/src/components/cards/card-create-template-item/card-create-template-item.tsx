import * as React from "react";
import { Checkbox, Input, Form, Button, Header, Dropdown } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { StoreTemplateItems } from "../../../stores";
import { action } from "mobx";
import { observer } from "mobx-react";
import { TemplateItem, CombatTrait } from "pnp-common";
import { field, FieldSimple, hasFields } from "hyrest-mobx";
import { formItem } from "../../../hyrest-semantic-ui";
import { Quill, fromQuillField } from "../../elements";
import { parentIds } from "../../../parent-id";

@external @observer @hasFields()
export class CardCreateTemplateItem extends React.Component<{ presetId?: string, groupId?: string }> {
    @inject private templateItems: StoreTemplateItems;

    @field(TemplateItem) private templateItem: FieldSimple<TemplateItem>;

    @action.bound private async handleSubmit() {
        await this.templateItems.create({
            ...this.templateItem.value,
            ...parentIds(this.props),
        });
    }

    @action.bound private handleQuantifiableChange(_, { checked }: { checked: boolean }) {
        return (this.templateItem.nested.quantifiable.update as any)(checked);
    }

    @action.bound private handleCanBeActiveChange(_, { checked }: { checked: boolean }) {
        return (this.templateItem.nested.canBeActive.update as any)(checked);
    }

    @action.bound private handleSuggestedCombatTraitChange(_, { value }: { value: CombatTrait }) {
        this.templateItem.nested.suggestedCombatTrait.update(value as any);
    }

    public render () {
        const {
            name,
            description,
            unit,
            quantifiable,
            canBeActive,
            effect,
            suggestedCombatTrait,
            suggestedStaminaCost,
        } = this.templateItem.nested;
        return (
            <>
                    <Header>Create new item</Header>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Field {...formItem(name)}>
                            <label>Name</label>
                            <Input
                                placeholder="Name"
                                {...name.reactInput}
                            />
                        </Form.Field>
                        <Form.Field {...formItem(description)}>
                            <label>Description</label>
                            <Quill {...fromQuillField(description)} />
                        </Form.Field>
                        <Form.Field {...formItem(effect)}>
                            <label>Rules</label>
                            <Quill {...fromQuillField(effect)} />
                        </Form.Field>
                        <Form.Field {...formItem(quantifiable)}>
                            <Checkbox
                                label="Is equipment"
                                checked={canBeActive.value}
                                onChange={this.handleCanBeActiveChange}
                            />
                        </Form.Field>
                        <Form.Field {...formItem(suggestedCombatTrait)}>
                            <label>Combat trait</label>
                            <Dropdown
                                fluid
                                onChange={this.handleSuggestedCombatTraitChange}
                                placeholder="Corresponding combat trait"
                                value={suggestedCombatTrait.value}
                                selection
                                options={[
                                    { key: "focus", value: "focus", text: "Focus" },
                                    { key: "aim", value: "aim", text: "Aim" },
                                    { key: "awareness", value: "awareness", text: "Awareness" },
                                    { key: "swiftness", value: "swiftness", text: "Swiftness" },
                                ]}
                            />
                        </Form.Field>
                        <Form.Field {...formItem(suggestedStaminaCost)}>
                            <label>Stamina cost</label>
                            <Input
                                type="number"
                                placeholder="Suggested stamina cost"
                                value={
                                    suggestedStaminaCost.value === null || suggestedStaminaCost.value === undefined
                                        ? undefined
                                        : String(suggestedStaminaCost.value)
                                }
                                onChange={evt => suggestedStaminaCost.update(Number(evt.currentTarget.value))}
                            />
                        </Form.Field>
                        <Form.Field {...formItem(quantifiable)}>
                            <Checkbox
                                label="Quantifiable"
                                checked={quantifiable.value}
                                onChange={this.handleQuantifiableChange}
                            />
                        </Form.Field>
                        {
                            quantifiable.value && (
                                <Form.Field {...formItem(unit)}>
                                    <label>Unit</label>
                                    <Input
                                        placeholder="Unit"
                                        {...unit.reactInput}
                                    />
                                </Form.Field>
                            )
                        }
                        <Form.Field>
                            <Button
                                disabled={!this.templateItem.valid}
                                primary
                                htmlType="submit"
                                icon="plus"
                                content="Create"
                            />
                        </Form.Field>
                    </Form>
            </>
        );
    }
}
