import * as React from "react";
import { History } from "history";
import { Card, Button } from "semantic-ui-react";
import { inject, external } from "tsdi";
import { PresetList } from "../../collections";
import { routeCreatePreset } from "../../../pages";
import { bind } from "lodash-decorators";

@external
export class CardPresets extends React.Component {
    @inject private history: History;

    @bind private handleCreate() {
        this.history.push(routeCreatePreset.path());
    }

    public render () {
        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>Presets</Card.Header>
                    <PresetList />
                </Card.Content>
                <Card.Content extra>
                    <Button onClick={this.handleCreate} icon="plus" content="Create" />
                </Card.Content>
            </Card>
        );
    }
}
