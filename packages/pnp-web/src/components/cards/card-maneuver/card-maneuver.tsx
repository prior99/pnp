import * as React from "react";
import { Ref, Card, Button, Icon, Popup } from "semantic-ui-react";
import { inject, external, initialize } from "tsdi";
import { StoreManeuvers } from "../../../stores";
import { action, observable, autorun, computed } from "mobx";
import { observer } from "mobx-react";
import { Maneuver, CombatTraitClassification, CombatTrait } from "pnp-common";
import * as css from "./card-maneuver.scss";
import { AddManeuverForm } from "../../forms";
import { bind } from "lodash-decorators";
import { EditableText, fromEditableTextField, fromEditableNumberField } from "../../elements";
import { field, FieldSimple, hasFields } from "hyrest-mobx";

export interface CardManeuverProps {
    id: string;
    onDelete?: () => void;
    onMonster?: boolean;
    highlighted?: boolean;
}

@external @observer @hasFields()
export class CardManeuver extends React.Component<CardManeuverProps> {
    @inject private maneuvers: StoreManeuvers;

    @field(Maneuver) private maneuverField: FieldSimple<Maneuver>;

    @observable private openedManeuver = false;
    @observable private addingHero = false;
    @observable private addingMonster = false;

    private buttonHero: HTMLButtonElement;
    private buttonMonster: HTMLButtonElement;

    @initialize
    protected initialize() {
        autorun(() => this.maneuver && this.maneuverField.update(this.maneuver));
    }

    @computed private get maneuver() {
        return this.maneuvers.byId(this.props.id);
    }

    @action.bound private async handleNameChange(name: string) {
        await this.maneuvers.update(this.props.id, { name: this.maneuverField.nested.name.value });
    }

    @action.bound private async handleDescriptionChange(description: string) {
        await this.maneuvers.update(this.props.id, {
            description: this.maneuverField.nested.description.value || "",
        });
    }

    @action.bound private async handleEffectChange(effect: string) {
        await this.maneuvers.update(this.props.id, { effect: this.maneuverField.nested.effect.value || "" });
    }

    @action.bound private async handleCategoryChange() {
        const classification: string = this.maneuverField.nested.classification.value;
        await this.maneuvers.update(this.props.id, {
            classification: classification === "none" ? null : classification as CombatTraitClassification,
        });
    }

    @action.bound private async handleSuggestedCombatTraitChange() {
        const suggestedCombatTrait: string = this.maneuverField.nested.suggestedCombatTrait.value;
        await this.maneuvers.update(this.props.id, {
            suggestedCombatTrait: suggestedCombatTrait === "none" ? null : suggestedCombatTrait as CombatTrait,
        });
    }

    @action.bound private async handleSuggestedStaminaCostChange() {
        const suggestedStaminaCost = this.maneuverField.nested.suggestedStaminaCost.value;
        await this.maneuvers.update(this.props.id, {
            suggestedStaminaCost: !suggestedStaminaCost ? null : suggestedStaminaCost,
        });
    }

    @action.bound private handleMonsterAddStart() {
        this.addingMonster = true;
    }

    @action.bound private handleMonsterAdd() {
        this.addingMonster = false;
    }

    @action.bound private handleMonsterClose() {
        this.addingMonster = false;
    }

    @bind public handleMonsterButtonRef(button: HTMLButtonElement) {
        this.buttonMonster = button;
    }

    @action.bound private handleHeroOpen() {
        this.openedManeuver = !this.openedManeuver;
    }

    @action.bound private async handleDelete() {
        await this.maneuvers.delete(this.props.id);
    }

    @action.bound private handleHeroAddStart() {
        this.addingHero = true;
    }

    @action.bound private handleHeroAdd() {
        this.addingHero = false;
    }

    @action.bound private handleHeroClose() {
        this.addingHero = false;
    }

    @bind public handleHeroButtonRef(button: HTMLButtonElement) {
        this.buttonHero = button;
    }

    public render() {
        const {
            name,
            description,
            effect,
            suggestedCombatTrait,
            suggestedStaminaCost,
            classification,
        } = this.maneuverField.nested;
        return (
            <Card fluid color={this.props.highlighted ? "blue" : undefined}>
                <Card.Content>
                    <Icon
                        name={this.openedManeuver ? "angle up" : "angle down"}
                        onClick={this.handleHeroOpen}
                        className={css.toggle}
                        size="large"
                    />
                    <Card.Header>
                        <EditableText
                            disabled={!this.openedManeuver}
                            {...fromEditableTextField(name)}
                            onFinish={this.handleNameChange}
                        />
                    </Card.Header>
                    <Card.Description>
                        {
                            this.openedManeuver && (
                                <EditableText
                                    formatted
                                    disabled={!this.openedManeuver}
                                    area
                                    {...fromEditableTextField(description)}
                                    onFinish={this.handleDescriptionChange}
                                    label="Description"
                                />
                            )
                        }
                        <EditableText
                            formatted
                            disabled={!this.openedManeuver}
                            area
                            {...fromEditableTextField(effect)}
                            onFinish={this.handleEffectChange}
                                    label="Rules"
                        />
                    </Card.Description>
                </Card.Content>
                {
                    this.openedManeuver && <Card.Content extra>
                        <EditableText
                            {...fromEditableNumberField(suggestedStaminaCost)}
                            number
                            onFinish={this.handleSuggestedStaminaCostChange}
                            label="Suggested stamina cost"
                        />
                        <EditableText
                            {...fromEditableTextField(suggestedCombatTrait)}
                            options={[
                                { key: "focus", value: "focus", text: "Focus" },
                                { key: "aim", value: "aim", text: "Aim" },
                                { key: "awareness", value: "awareness", text: "Awareness" },
                                { key: "swiftness", value: "swiftness", text: "Swiftness" },
                                { key: "none", value: "none", text: "None" },
                            ]}
                            onFinish={this.handleSuggestedCombatTraitChange}
                            label="Corresponding combat trait"
                        />
                        <EditableText
                            {...fromEditableTextField(classification)}
                            options={[
                                {
                                    icon: "shield alternative",
                                    key: "offensive",
                                    value: "offensive",
                                    text: "Attack",
                                },
                                { icon: "fire", key: "defensive", value: "defensive", text: "Defense" },
                                { icon: "bolt", key: "instant", value: "instant", text: "Instant" },
                                { icon: "cross", key: "none", value: "none", text: "None" },
                            ]}
                            onFinish={this.handleCategoryChange}
                            label="Category"
                        />
                    </Card.Content>
                }
                {
                    this.openedManeuver && this.props.onDelete && (
                        <Card.Content extra>
                            <Button icon="trash alternate" size="mini" onClick={this.props.onDelete} />
                        </Card.Content>
                    )
                }
                {
                    this.openedManeuver && !this.props.onMonster && (
                        <Card.Content extra>
                            <Button.Group size="mini">
                                <Ref innerRef={this.handleHeroButtonRef}>
                                    <Button
                                        icon={
                                            <Icon.Group>
                                                <Icon name="student" />
                                                <Icon corner name="child" />
                                            </Icon.Group>
                                        }
                                        size="mini"
                                        onClick={this.handleHeroAddStart}
                                    />
                                </Ref>
                                <Popup
                                    open={this.addingHero}
                                    context={this.buttonHero}
                                    onClose={this.handleHeroClose}
                                >
                                    <Popup.Header>Teach maneuver</Popup.Header>
                                    <Popup.Content>
                                        <AddManeuverForm
                                            target="hero"
                                            onAdd={this.handleHeroAdd}
                                            maneuverId={this.props.id}
                                        />
                                    </Popup.Content>
                                </Popup>
                                <Ref innerRef={this.handleMonsterButtonRef}>
                                    <Button
                                        icon={
                                            <Icon.Group>
                                                <Icon name="student" />
                                                <Icon corner name="bug" />
                                            </Icon.Group>
                                        }
                                        size="mini"
                                        onClick={this.handleMonsterAddStart}
                                    />
                                </Ref>
                                <Popup
                                    open={this.addingMonster}
                                    context={this.buttonMonster}
                                    onClose={this.handleMonsterClose}
                                >
                                    <Popup.Header>Teach maneuver</Popup.Header>
                                    <Popup.Content>
                                        <AddManeuverForm
                                            target="monster"
                                            onAdd={this.handleMonsterAdd}
                                            maneuverId={this.props.id}
                                        />
                                    </Popup.Content>
                                </Popup>
                                <Popup
                                    trigger={<Button icon="trash alternate" size="mini" />}
                                    on="click"
                                >
                                    <Popup.Header>Are you sure?</Popup.Header>
                                    <Popup.Content>
                                        <p>All maneuvers of this type will also be deleted from all heroes.</p>
                                        <Button
                                            icon="check"
                                            color="green"
                                            size="mini"
                                            content="Delete"
                                            onClick={this.handleDelete}
                                        />
                                    </Popup.Content>
                                </Popup>
                            </Button.Group>
                        </Card.Content>
                    )
                }
            </Card>
        );
    }
}
