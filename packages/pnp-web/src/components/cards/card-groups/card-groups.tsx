import * as React from "react";
import { History } from "history";
import { Card, Button } from "semantic-ui-react";
import { GroupList } from "../../collections";
import { inject, external } from "tsdi";
import { routeCreateGroup } from "../../../pages";
import { bind } from "lodash-decorators";

@external
export class CardGroups extends React.Component {
    @inject private history: History;

    @bind private handleCreate() {
        this.history.push(routeCreateGroup.path());
    }

    public render () {
        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>Groups</Card.Header>
                    <GroupList />
                </Card.Content>
                <Card.Content extra>
                    <Button onClick={this.handleCreate} icon="plus" content="Create" />
                </Card.Content>
            </Card>
        );
    }
}
