import * as React from "react";
import { Card, Button } from "semantic-ui-react";
import { HeroList } from "../../collections";
import { inject, external } from "tsdi";
import { StoreHeroes } from "../../../stores";
import { action } from "mobx";
import { parentIds } from "../../../parent-id";

@external
export class CardNpcs extends React.Component<{ groupId?: string, presetId?: string }> {
    @inject private heroes: StoreHeroes;

    @action.bound private async handleCreate() {
        await this.heroes.create({
            isNpc: true,
            ...parentIds(this.props),
        });
    }

    public render () {
        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>NPCs</Card.Header>
                    <HeroList list="NPC" {...this.props} />
                </Card.Content>
                <Card.Content extra>
                    <Button onClick={this.handleCreate} icon="plus" content="Create" />
                </Card.Content>
            </Card>
        );
    }
}
