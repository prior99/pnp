import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { computed } from "mobx";
import {
    StoreBattleMaps,
    Participant,
    StoreCurrentBattle,
    ParticipantType,
} from "../../../stores";
import { Grid } from "semantic-ui-react";
import { BattleParticipantMonster } from "./battle-participant-monster";
import { BattleParticipantHero } from "./battle-participant-hero";

@observer
@external
export class CurrentBattleWidget extends React.Component<{
    battleMapId: string;
}> {
    @inject private currentBattle: StoreCurrentBattle;
    @inject private battleMaps: StoreBattleMaps;

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.battleMapId);
    }

    @computed private get sortedParticipants(): Participant[] {
        return this.currentBattle.sortedParticipants(this.battleMap.id);
    }

    public render() {
        return (
            <Grid>
                <Grid.Row style={{ fontWeight: "bold" }}>
                    <Grid.Column computer={6} mobile={6}></Grid.Column>
                    <Grid.Column computer={2} mobile={2}>
                        INI
                    </Grid.Column>
                    <Grid.Column computer={2} mobile={2}>
                        Turns
                    </Grid.Column>
                </Grid.Row>
                {this.sortedParticipants.length === 0 && (
                    <Grid.Row>
                        <Grid.Column stretched textAlign="center">
                            No battle in progress
                        </Grid.Column>
                    </Grid.Row>
                )}
                {this.sortedParticipants.map(({ type, participant }, index) => {
                    switch (type) {
                        case ParticipantType.HERO:
                            return (
                                <BattleParticipantHero
                                    next={index === 0}
                                    id={participant.id}
                                    battleMapId={this.props.battleMapId}
                                />
                            );
                        case ParticipantType.MONSTER:
                            return (
                                <BattleParticipantMonster
                                    next={index === 0}
                                    id={participant.id}
                                    battleMapId={this.props.battleMapId}
                                />
                            );
                    }
                })}
            </Grid>
        );
    }
}
