import * as React from "react";
import { observer } from "mobx-react";
import { computed, action } from "mobx";
import { external, inject } from "tsdi";
import { Image, Grid, Icon, Button, Progress, Card } from "semantic-ui-react";
import { hasFields } from "hyrest-mobx";
import {
    StoreCurrentBattle,
    StoreMonsters,
    StoreTemplateMonsters,
    StoreBattleMaps,
    StoreLogin,
} from "../../../stores";
import * as css from "./current-battle-widget.scss";
import { combatTraitCostPerRank } from "pnp-common";

export interface BattleParticipantMonsterProps {
    id: string;
    battleMapId: string;
    next: boolean;
}

@observer
@external
@hasFields()
export class BattleParticipantMonster extends React.Component<
    BattleParticipantMonsterProps
> {
    @inject private monsters: StoreMonsters;
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private currentBattle: StoreCurrentBattle;
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;

    @computed private get monster() {
        return this.monsters.byId(this.props.id);
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.battleMapId);
    }

    @computed private get templateMonster() {
        return this.templateMonsters.byId(this.monster.templateMonster.id);
    }

    @computed private get isMaster() {
        return this.login.isMaster(this.battleMap.group.id);
    }

    @computed private get hasMadeTurn() {
        if (this.currentBattle.allSameTurn(this.props.battleMapId)) {
            return false;
        }
        return (
            this.currentBattle.highestTurn(this.props.battleMapId) ===
            this.monster.currentCombatTurns
        );
    }

    @computed private get healthPercent() {
        const constitution =
            this.templateMonster.constitution /
            combatTraitCostPerRank("constitution");
        return Math.floor(
            ((constitution - this.monster.damage) / constitution) * 100,
        );
    }

    @computed private get staminaPercent() {
        const stamina =
            this.templateMonster.stamina / combatTraitCostPerRank("stamina");
        return Math.floor(
            ((stamina - this.monster.spentStamina) / stamina) * 100,
        );
    }

    @action.bound private async handleNextTurn() {
        await this.monsters.update(this.monster.id, {
            currentCombatTurns: this.monster.currentCombatTurns + 1,
        });
    }

    public render() {
        return (
            <Card raised={this.props.next} fluid>
                <Progress
                    attached="top"
                    size="tiny"
                    color="red"
                    percent={this.healthPercent}
                />
                <Card.Content>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column computer={1} mobile={1}>
                                <Image
                                    className={css.icon}
                                    inline
                                    circular
                                    size="mini"
                                    src={this.templateMonster.imageUrl}
                                />
                            </Grid.Column>
                            <Grid.Column computer={5} mobile={5}>
                                {this.templateMonster.name}
                            </Grid.Column>
                            <Grid.Column computer={2} mobile={2}>
                                {this.monster.initiative}
                            </Grid.Column>
                            <Grid.Column computer={2} mobile={2}>
                                {this.monster.currentCombatTurns || 0}
                            </Grid.Column>
                            <Grid.Column computer={2} mobile={2}>
                                <Icon
                                    color={this.hasMadeTurn ? "green" : "red"}
                                    name={this.hasMadeTurn ? "check" : "cancel"}
                                />
                            </Grid.Column>
                            {this.isMaster && (
                                <Grid.Column computer={3} mobile={3}>
                                    <Button
                                        onClick={this.handleNextTurn}
                                        icon="angle double right"
                                        size="mini"
                                    />
                                </Grid.Column>
                            )}
                        </Grid.Row>
                    </Grid>
                </Card.Content>
                <Progress
                    attached="bottom"
                    size="tiny"
                    color="green"
                    percent={this.staminaPercent}
                />
            </Card>
        );
    }
}
