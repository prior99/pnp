import * as React from "react";
import { History } from "history";
import { observer } from "mobx-react";
import { computed, action } from "mobx";
import { external, inject } from "tsdi";
import { Image, Grid, Icon, Button, Card, Progress } from "semantic-ui-react";
import { hasFields } from "hyrest-mobx";
import {
    StoreHeroes,
    StoreCurrentBattle,
    StoreVisiting,
    StoreBattleMaps,
    StoreLogin,
} from "../../../stores";
import * as css from "./current-battle-widget.scss";
import { routeHero } from "../../../pages";

export interface BattleParticipantHeroProps {
    id: string;
    battleMapId: string;
    next: boolean;
}

@observer
@external
@hasFields()
export class BattleParticipantHero extends React.Component<
    BattleParticipantHeroProps
> {
    @inject private heroes: StoreHeroes;
    @inject private currentBattle: StoreCurrentBattle;
    @inject private history: History;
    @inject private visiting: StoreVisiting;
    @inject private battleMaps: StoreBattleMaps;
    @inject private login: StoreLogin;

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.battleMapId);
    }

    @computed private get hasMadeTurn() {
        if (this.currentBattle.allSameTurn(this.props.battleMapId)) {
            return false;
        }
        return (
            this.currentBattle.highestTurn(this.props.battleMapId) ===
            this.hero.currentCombatTurns
        );
    }

    @computed private get isMaster() {
        return this.login.isMaster(this.battleMap.group.id);
    }

    @action.bound private async handleNextTurn() {
        await this.heroes.update(this.hero.id, {
            currentCombatTurns: this.hero.currentCombatTurns + 1,
        });
    }

    @action.bound private async handleHeroClick() {
        const groupId = this.hero.group.id;
        this.history.push(
            routeHero.path(
                groupId,
                undefined,
                this.hero.id,
                this.visiting.paneForHero(this.hero.id),
            ),
        );
    }

    @computed private get healthPercent() {
        const constitution = this.hero.combatRank("constitution");
        return Math.floor(
            ((constitution - this.hero.damage) / constitution) * 100,
        );
    }

    @computed private get staminaPercent() {
        const stamina = this.hero.combatRank("stamina");
        return Math.floor(((stamina - this.hero.spentStamina) / stamina) * 100);
    }

    public render() {
        return (
            <Card raised={this.props.next} fluid>
                <Progress
                    attached="top"
                    size="tiny"
                    color="red"
                    percent={this.healthPercent}
                />
                <Card.Content>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column computer={1} mobile={1}>
                                <a onClick={this.handleHeroClick}>
                                    <Image
                                        className={css.icon}
                                        inline
                                        circular
                                        size="mini"
                                        src={this.hero.avatarUrl}
                                    />
                                </a>
                            </Grid.Column>
                            <Grid.Column computer={5} mobile={5}>
                                <a onClick={this.handleHeroClick}>
                                    {this.hero.name}
                                </a>
                            </Grid.Column>
                            <Grid.Column computer={2} mobile={2}>
                                {this.hero.initiative}
                            </Grid.Column>
                            <Grid.Column computer={2} mobile={2}>
                                {this.hero.currentCombatTurns || 0}
                            </Grid.Column>
                            <Grid.Column computer={2} mobile={2}>
                                <Icon
                                    color={this.hasMadeTurn ? "green" : "red"}
                                    name={this.hasMadeTurn ? "check" : "cancel"}
                                />
                            </Grid.Column>
                            {this.isMaster && (
                                <Grid.Column computer={3} mobile={3}>
                                    <Button
                                        onClick={this.handleNextTurn}
                                        icon="angle double right"
                                        size="mini"
                                    />
                                </Grid.Column>
                            )}
                        </Grid.Row>
                    </Grid>
                </Card.Content>
                <Progress
                    attached="bottom"
                    size="tiny"
                    color="green"
                    percent={this.staminaPercent}
                />
            </Card>
        );
    }
}
