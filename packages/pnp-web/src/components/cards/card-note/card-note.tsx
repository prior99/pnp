import * as React from "react";
import { Card, Button, Checkbox } from "semantic-ui-react";
import { inject, external, initialize } from "tsdi";
import { StoreNotes, StoreHeroes, StoreLogin } from "../../../stores";
import { action, observable, autorun, computed } from "mobx";
import { observer } from "mobx-react";
import ReactQuill from "react-quill";
import * as css from "./card-note.scss";
import "react-quill/dist/quill.snow.css";
import { ButtonConfirm } from "../../elements";
import { hasFields, field, Field } from "hyrest-mobx";
import { Note } from "pnp-common";
import { pick } from "ramda";

@external @observer @hasFields()
export class CardNote extends React.Component<{ id: string }> {
    @inject private notes: StoreNotes;
    @inject private heroes: StoreHeroes;
    @inject private login: StoreLogin;

    @observable private editing = false;
    @field(Note) private noteField: Field<Note>;

    @initialize protected initialize() {
        autorun(() => this.note && this.noteField.update(this.note));
    }

    @computed private get hero() {
        return this.heroes.byId(this.notes.byId(this.props.id).hero.id);
    }

    @computed private get note() {
        return this.notes.byId(this.props.id);
    }

    @computed private get obfuscated() {
        return this.notes.isObfuscated(this.note.id);
    }

    @computed private get isMaster() {
        return Boolean(this.hero.preset) || this.login.isMaster(this.hero.group.id);
    }

    @action.bound private handleEdit() {
        this.editing = !this.editing;
    }

    @action.bound private handleChange(value: string) {
        this.noteField.nested.note.update(value);
    }

    @action.bound private async handleToggleSecret() {
        this.notes.unobfuscate(this.props.id);
        this.noteField.nested.secret.update(!this.noteField.nested.secret.value as any);
    }

    @action.bound private async handleToggleMasterOnly() {
        this.noteField.nested.masterOnly.update(!this.noteField.nested.masterOnly.value as any);
    }

    @action.bound private async handleSubmit() {
        await this.notes.update(this.props.id, pick(["note", "secret", "masterOnly"], this.noteField.value));
        this.editing = false;
    }

    @action.bound private async handleDelete() {
        await this.notes.delete(this.props.id);
    }

    private renderText() {
        const { note } = this.noteField.nested;
        if (note.value && note.value.length === 0 && !this.editing) {
            return (
                <ReactQuill
                    value="<i>No content yet.</i>"
                    readOnly={!this.editing}
                    modules={{ toolbar: false }}
                />
            );
        }
        return (
            <ReactQuill
                value={note.value}
                onChange={this.handleChange}
                readOnly={!this.editing}
                className={this.obfuscated && css.obfuscated}
                modules={{
                    toolbar: this.editing ? [
                        ["bold", "italic", "underline", "strike"],
                        [{ header: 1 }, { header: 2 }],
                        ["blockquote", "code-block"],
                        [{ color: [] }, { background: [] }],
                        [{ list: "ordered" }, { list: "bullet" }],
                    ] : false,
                }}
            />
        );
    }

    @action.bound private handleObfuscate() {
        this.notes.toggleObfuscated(this.note.id);
    }

    private renderButtons() {
        const buttons: JSX.Element[] = [];
        if (!this.obfuscated) {
            if (this.editing) {
                buttons.push(<Button
                    icon="check"
                    onClick={this.handleSubmit}
                    color="green"
                    content="Save"
                />);
            } else {
                buttons.push(<Button icon="pencil" onClick={this.handleEdit} content="Edit" />);
            }
            buttons.push(<ButtonConfirm onConfirm={this.handleDelete} />);
        }
        return <>
            {
                this.editing && <div>
                    <p>
                        <Checkbox
                            onClick={this.handleToggleSecret}
                            checked={this.noteField.nested.secret.value}
                            label="Secret"
                        />
                    </p>
                    <p>
                        {
                            this.isMaster && <Checkbox
                                onClick={this.handleToggleMasterOnly}
                                checked={this.noteField.nested.masterOnly.value}
                                label="Master only"
                            />
                        }
                    </p>
                    <br />
                </div>
            }
            <Button.Group>{buttons}</Button.Group>
            {
                this.note.secret && !this.editing && <Checkbox
                    className={!this.obfuscated && css.checkBox}
                    toggle
                    checked={!this.obfuscated}
                    label="Show content"
                    onClick={this.handleObfuscate}
                />
            }
        </>;
    }

    public render() {
        return (
            <Card fluid>
                {this.renderText()}
            <Card.Content>
                        {this.renderButtons()}
                </Card.Content>
            </Card>
        );
    }
}
