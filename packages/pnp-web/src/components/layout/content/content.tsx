import * as React from "react";

import * as css from"./content.scss";

export class Content extends React.Component<{ title?: string, className?: string }> {
    public render() {
        return (
            <div className={[css.content, this.props.className].join(" ")}>
                {this.props.title && <h1>{this.props.title}</h1>}
                {this.props.children}
            </div>
        );
    }
}
