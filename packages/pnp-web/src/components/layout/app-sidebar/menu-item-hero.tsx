import * as React from "react";
import { History } from "history";
import { Menu, Image } from "semantic-ui-react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import * as Color from "color";
import { StoreUi, StoreHeroes, StoreVisiting } from "../../../stores";
import { routeHero } from "../../../pages";
import * as css from "./app-sidebar.scss";
import { Live } from "../../../live";
import { PageName } from "pnp-common";
import { ColorBorders } from "../../elements";
import { extractParentIds } from "../../../parent-id";

@external @observer
export class MenuItemHero extends React.Component<{ id: string }> {
    @inject private heroes: StoreHeroes;
    @inject private history: History;
    @inject private ui: StoreUi;
    @inject private visiting: StoreVisiting;
    @inject private live: Live;

    @action.bound private handleClick() {
        this.ui.toggleVisibility();
        this.history.push(
            routeHero.path(...[
                ...extractParentIds(this.hero),
                this.hero.id,
                this.visiting.paneForHero(this.hero.id),
            ] as Parameters<typeof routeHero["path"]>),
        );
    }

    @computed private get hero() {
        return this.heroes.byId(this.props.id);
    }

    @computed private get color() {
        const { hero } = this;
        return (hero.group && this.live.getColor(hero.group.id, hero.id)) || "transparent";
    }

    @computed private get backgroundColor() {
        return this.color ? Color(this.color).mix(Color("white"), 0.8) : "white";
    }

    @computed private get style() {
        return {
            borderRight: `4px solid ${this.color}`,
            background: this.backgroundColor,
            opacity: this.hero.damage >= this.hero.combatRank("constitution") ? 0.3 : 1,
        };
    }

    @computed private get navigationColors() {
        const [ groupId ] = extractParentIds(this.hero);
        return groupId ? this.live.getColorForNavigationTarget(groupId, {
            page: PageName.HERO,
            entityId: this.hero.id,
        }) : [];
    }

    public render() {
        const { hero, handleClick, style, navigationColors } = this;
        const { name, avatarUrl, id } = hero;
        return (
            <div className={css.menuItemWrapper}>
                <Menu.Item
                    icon={
                        <Image
                            className={css.icon}
                            inline
                            circular
                            size="mini"
                            src={avatarUrl}
                        />
                    }
                    key={id}
                    name={name}
                    onClick={handleClick}
                    style={style}
                />
                <ColorBorders colors={navigationColors} />
            </div>
        );
    }
}
