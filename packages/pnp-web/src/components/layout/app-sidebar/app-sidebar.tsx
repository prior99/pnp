import * as React from "react";
import { Sidebar, Menu } from "semantic-ui-react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import { StoreLogin, StoreUi, StoreTags } from "../../../stores";
import { routes, Parent } from "../../../routing";
import { MenuItemRoute } from "./menu-item-route";
import { TagSubmenu } from "./tag-submenu";

@external @observer
export class AppSidebar extends React.Component<{ groupId?: string, presetId?: string }> {
    @inject private login: StoreLogin;
    @inject private ui: StoreUi;
    @inject private tags: StoreTags;

    @action.bound private handleLogout() {
        this.login.logout();
    }

    @computed private get parent() {
        const { groupId, presetId } = this.props;
        if (groupId && presetId) { throw new Error("Both preset and group id were specified."); }
        if (!groupId && !presetId) { throw new Error("Neither preset and group id were specified."); }
        if (groupId) { return Parent.GROUP; }
        return Parent.PRESET;
    }

    @computed private get routeItems() {
        return routes
            .filter(route => route.navbar)
            .filter(route => route.parent.includes(this.parent))
            .filter(route => !this.props.groupId || this.login.isMaster(this.props.groupId) || !route.masterOnly)
            .map(route => <MenuItemRoute route={route} {...this.props} />);
    }

    @computed private get isMaster() {
        if (this.props.presetId) { return true; }
        return this.login.isMaster(this.props.groupId);
    }

    @computed private get pinnedTags() {
        return this.tags.all
            .filter(tag => tag.pinned)
            .filter(tag => this.isMaster || !tag.masterOnly)
            .sort((a, b) => a.name.localeCompare(b.name));
    }

    public render() {
        return (
            <Sidebar
                as={Menu}
                animation="uncover"
                visible={this.ui.visible}
                vertical
                style={{ borderTop: "none", paddingTop: 44 }}
            >
                {this.routeItems}
                {this.pinnedTags.map(tag => <TagSubmenu id={tag.id} {...this.props} />)}
                <Menu.Item
                    onClick={this.handleLogout}
                    icon="sign out alternate"
                    content="Logout"
                />
            </Sidebar>
        );
    }
}
