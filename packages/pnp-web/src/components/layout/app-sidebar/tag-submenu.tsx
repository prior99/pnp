import * as React from "react";
import { Menu, Icon } from "semantic-ui-react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import {
    StoreLogin,
    StoreHeroes,
    StoreBattleMaps,
    StoreHeroTags,
    StoreTags,
    StoreBattleMapTags,
} from "../../../stores";
import * as css from "./tag-submenu.scss";
import { MenuItemBattleMap } from "./menu-item-battle-map";
import { MenuItemHero } from "./menu-item-hero";

@external @observer
export class TagSubmenu extends React.Component<{ id?: string, groupId?: string, presetId?: string }> {
    @inject private login: StoreLogin;
    @inject private heroes: StoreHeroes;
    @inject private battleMaps: StoreBattleMaps;
    @inject private tags: StoreTags;
    @inject private heroTags: StoreHeroTags;
    @inject private battleMapTags: StoreBattleMapTags;

    @computed private get includedBattleMaps() {
        const { groupId, presetId } = this.props;
        return this.battleMaps
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(battleMap => !groupId || battleMap.group && battleMap.group.id === groupId)
            .filter(battleMap => !presetId || battleMap.preset && battleMap.preset.id === presetId)
            .filter(battleMap => this.isMaster || !battleMap.masterOnly)
            .filter(battleMap => this.battleMapTags.forBattleMap(battleMap.id)
                .map(battleMapTag => battleMapTag.tag.id)
                .some(tagId => tagId === this.props.id),
            );
    }

    @computed private get includedPCs() {
        return this.includedHeroes.filter(hero => !hero.isNpc);
    }

    @computed private get includedNPCs() {
        return this.includedHeroes.filter(hero => hero.isNpc);
    }

    @computed private get includedHeroes() {
        const { groupId, presetId } = this.props;
        return this.heroes.all
            .filter(hero => !groupId || hero.group && hero.group.id === groupId)
            .filter(hero => !presetId || hero.preset && hero.preset.id === presetId)
            .filter(hero => this.isMaster || !hero.masterOnly)
            .sort((a, b) => a.name.localeCompare(b.name))
            .filter(hero => this.heroTags.forHero(hero.id)
                .map(heroTag => heroTag.tag.id)
                .some(tagId => tagId === this.props.id),
            );
    }

    @computed private get tag() {
        return this.tags.byId(this.props.id);
    }

    @computed private get eyeIcon() {
        if (this.tag.masterOnly) { return "eye slash"; }
        return "eye";
    }

    @computed private get pinIcon() {
        if (this.tag.pinned) { return "bookmark"; }
        return "bookmark outline";
    }

    @computed private get openIcon() {
        return this.opened ? "angle up" : "angle down";
    }

    @computed private get isMaster() {
        if (this.props.presetId) { return true; }
        return this.login.isMaster(this.props.groupId);
    }

    @computed private get opened() {
        return this.tags.tagOpened(this.tag.id);
    }

    @action.bound private handleOpen() {
        this.tags.toggleOpen(this.tag.id);
    }

    @action.bound private async handleMasterOnlyToggle() {
        await this.tags.update(this.props.id, { masterOnly: !this.tag.masterOnly });
    }

    @action.bound private async handlePinToggle() {
        await this.tags.update(this.props.id, { pinned: !this.tag.pinned });
    }

    public render() {
        const { name } = this.tag;
        return (
            <Menu.Item>
                <Menu.Header>
                    <div className={css.wrapper}>
                        <div style={{ opacity: this.tag.masterOnly ? 0.5 : 1 }}>{name}</div>
                        <div className={css.icons}>
                            {
                                this.isMaster && <>
                                    <Icon
                                        className={css.icon}
                                        name={this.pinIcon}
                                        onClick={this.handlePinToggle}
                                    />
                                    <Icon
                                        className={css.icon}
                                        name={this.eyeIcon}
                                        onClick={this.handleMasterOnlyToggle}
                                    />
                                </>
                            }
                            <Icon
                                className={css.icon}
                                name={this.openIcon}
                                onClick={this.handleOpen}
                            />
                        </div>
                    </div>
                </Menu.Header>
                {
                    this.opened && <Menu.Menu>
                        {
                            this.includedHeroes.length > 0 && <Menu.Item>
                                <Menu.Header>Heroes</Menu.Header>
                                <Menu.Menu>
                                    {this.includedPCs.map(({ id }) => <MenuItemHero id={id} key={id} />)}
                                    {this.includedNPCs.map(({ id }) => <MenuItemHero id={id} key={id} />)}
                                </Menu.Menu>
                            </Menu.Item>
                        }
                        {
                            this.includedBattleMaps.length > 0 && <Menu.Item>
                                <Menu.Header>Battle Maps</Menu.Header>
                                <Menu.Menu>
                                    {this.includedBattleMaps.map(({ id }) => <MenuItemBattleMap id={id} key={id} />)}
                                </Menu.Menu>
                            </Menu.Item>
                        }
                    </Menu.Menu>
                }
            </Menu.Item>
        );
    }
}
