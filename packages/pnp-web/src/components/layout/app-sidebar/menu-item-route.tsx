import * as React from "react";
import { History } from "history";
import { Menu } from "semantic-ui-react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { action } from "mobx";
import { StoreUi } from "../../../stores";
import { Route } from "../../../routing";

@external @observer
export class MenuItemRoute extends React.Component<{ route: Route, groupId?: string, presetId?: string }> {
    @inject private history: History;
    @inject private ui: StoreUi;

    @action.bound private handleClick() {
        const { path } = this.props.route;
        this.ui.toggleVisibility();
        // All route path factories always take group id and then preset id.
        this.history.push(path(this.props.groupId, this.props.presetId));
    }

    public render() {
        const { icon, title, path } = this.props.route;
        return (
            <Menu.Item
                key={path()}
                onClick={this.handleClick}
                icon={icon}
                content={title}
            />
        );
    }
}
