import * as React from "react";
import { History } from "history";
import { Menu, Image } from "semantic-ui-react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import { StoreUi, StoreBattleMaps, StoreVisiting } from "../../../stores";
import { routeBattleMap } from "../../../pages";
import * as css from "./app-sidebar.scss";
import { Live } from "../../../live";
import { PageName } from "pnp-common";
import { ColorBorders } from "../../elements";
import { extractParentIds } from "../../../parent-id";

@external @observer
export class MenuItemBattleMap extends React.Component<{ id: string }> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private history: History;
    @inject private ui: StoreUi;
    @inject private visiting: StoreVisiting;
    @inject private live: Live;

    @action.bound private handleClick() {
        this.ui.toggleVisibility();
        this.history.push(
            routeBattleMap.path(...[
                ...extractParentIds(this.battleMap),
                this.battleMap.id,
                this.visiting.paneForBattleMap(this.battleMap.id),
            ] as Parameters<typeof routeBattleMap["path"]>),
        );
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.props.id);
    }

    @computed private get navigationColors() {
        const [ groupId ] = extractParentIds(this.battleMap);
        return groupId ? this.live.getColorForNavigationTarget(groupId, {
            page: PageName.HERO,
            entityId: this.battleMap.id,
        }) : [];
    }

    public render() {
        const { battleMap, handleClick, navigationColors } = this;
        const { name, previewUrl, id } = battleMap;
        return (
            <div className={css.menuItemWrapper}>
                <Menu.Item
                    icon={
                        <Image
                            className={css.icon}
                            inline
                            circular
                            size="mini"
                            src={previewUrl}
                        />
                    }
                    key={id}
                    name={name}
                    onClick={handleClick}
                />
                <ColorBorders colors={navigationColors} />
            </div>
        );
    }
}
