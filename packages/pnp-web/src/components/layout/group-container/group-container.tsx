import * as React from "react";
import { Sidebar, Dimmer, Icon, Progress, Header } from "semantic-ui-react";
import { Route } from "react-router-dom";
import { external, inject, initialize } from "tsdi";
import { observer } from "mobx-react";
import {
    StoreGroups,
    StoreHeroes,
    StoreTemplateItems,
    StoreBattleMaps,
    StoreManeuvers,
    StoreItems,
    StoreLearnedManeuvers,
    StoreMarkers,
    StoreAreas,
    StoreUi,
    StoreLogin,
    StoreTemplateMonsters,
    StoreMonsterManeuvers,
    StoreMonsters,
    StoreNotes,
    StoreJournalEntries,
    StoreStashs,
    StoreMarkerItems,
    StoreStashItems,
} from "../../../stores";
import { AppBar } from "../app-bar";
import { isProductionEnvironment } from "pnp-common";
import * as css from "./group-container.scss";
import * as classNames from "classnames/bind";
import { routes, RouteProps, Parent } from "../../../routing";
import { observable, computed, action } from "mobx";
import { AppSidebar } from "../app-sidebar";
import { StoreTags } from "../../../stores/tags";
import { StoreBattleMapTags } from "../../../stores/battle-map-tags";
import { StoreHeroTags } from "../../../stores/hero-tags";

declare const SOFTWARE_VERSION: string;

const cx = classNames.bind(css);

export type GroupContainerProps = RouteProps<{
    groupId: string,
    page: "hero" | "template-items" | "maneuvers" | "battle-map",
    subId?: string,
}>;

@observer @external
export class GroupContainer extends React.Component<GroupContainerProps> {
    public static path = "/group/:groupId/:page/:subId?";

    @inject private ui: StoreUi;
    @inject private groups: StoreGroups;
    @inject private heroes: StoreHeroes;
    @inject private templateItems: StoreTemplateItems;
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private battleMaps: StoreBattleMaps;
    @inject private maneuvers: StoreManeuvers;
    @inject private items: StoreItems;
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private markers: StoreMarkers;
    @inject private areas: StoreAreas;
    @inject private login: StoreLogin;
    @inject private monsterManeuvers: StoreMonsterManeuvers;
    @inject private monsters: StoreMonsters;
    @inject private notes: StoreNotes;
    @inject private tags: StoreTags;
    @inject private heroTags: StoreHeroTags;
    @inject private battleMapTags: StoreBattleMapTags;
    @inject private journalEntries: StoreJournalEntries;
    @inject private stashs: StoreStashs;
    @inject private markerItems: StoreMarkerItems;
    @inject private stashItems: StoreStashItems;

    @observable private loaded = false;
    @observable private currentLoadStep = "Nothing";
    @observable private totalToLoad = 0;
    @observable private totalLoaded = 0;
    @observable private currentToLoad = 0;
    @observable private currentLoaded = 0;

    @initialize
    public async loadRecursive() {
        this.resetCurrentLoading("Group", 7);
        const group = await this.groups.get(this.props.match.params.groupId);
        if (!group) { return; }
        this.totalToLoad = 12; // 8 categories, 3 times the sub entities and 1 to keep it below 100%.
        this.totalLoaded = 0;
        await this.loadEntities(() => this.templateItems.loadForGroup(group.id), "List of template items");
        await this.loadEntities(() => this.journalEntries.loadForGroup(group.id), "Journal");
        await this.loadEntities(() => this.tags.loadForGroup(group.id), "Tags");
        await this.loadEntities(() => this.maneuvers.loadForGroup(group.id), "Maneuvers");
        const heroes = await this.loadEntities(() => this.heroes.loadForGroup(group.id), "List of heroes");
        const battleMaps = await this.loadEntities(() => this.battleMaps.loadForGroup(group.id), "List of battle maps");
        const templateMonsters = await this.loadEntities(
            () => this.templateMonsters.loadForGroup(group.id),
            "List of battle maps",
        );
        const stashs = await this.loadEntities(() => this.stashs.loadForGroup(group.id), "Stashs");

        // Load all template monsters.
        this.resetCurrentLoading("Template monsters", 2 * templateMonsters.length);
        await this.loadSubEntities(templateMonsters, templateMonster => {
            return this.monsterManeuvers.loadForTemplateMonster(templateMonster.id);
        });
        await this.loadSubEntities(templateMonsters, templateMonster => {
            return this.monsters.loadForTemplateMonster(templateMonster.id);
        });

        // Load all heroes.
        this.resetCurrentLoading("Heroes", 4 * heroes.length);
        await this.loadSubEntities(heroes, hero => this.items.loadForHero(hero.id));
        await this.loadSubEntities(heroes, hero => this.learnedManeuvers.loadForHero(hero.id));
        await this.loadSubEntities(heroes, hero => this.notes.loadForHero(hero.id));
        await this.loadSubEntities(heroes, hero => this.heroTags.loadForHero(hero.id));

        // Load all battle maps.
        this.resetCurrentLoading("Battle maps", 3 * battleMaps.length);
        await this.loadSubEntities(battleMaps, battleMap => this.areas.loadForBattleMap(battleMap.id));
        await this.loadSubEntities(battleMaps, async battleMap => {
            const markers = await this.markers.loadForBattleMap(battleMap.id);
            await Promise.all(markers.map(marker => this.markerItems.loadForMarker(marker.id)));
        });
        await this.loadSubEntities(battleMaps, battleMap => this.battleMapTags.loadForBattleMap(battleMap.id));

        // Load all stashs.
        this.resetCurrentLoading("Stashs", 1 * stashs.length);
        await this.loadSubEntities(stashs, stash => this.stashItems.loadForStash(stash.id));

        // Done.
        this.loaded = true;
    }

    @action.bound private async loadEntities<T>(handler: () => Promise<T>, name: string): Promise<T> {
        const result = await handler();
        this.currentLoadStep = name;
        this.currentLoaded++;
        this.totalLoaded++;
        return result;
    }

    @action.bound private async loadSubEntities<T>(entities: T[], handler: (entity: T) => Promise<any>) {
        const stride = 10;
        for (let i = 0; i < entities.length; i += stride) {
            const slice = entities.slice(i, Math.min(i + stride, entities.length));
            await Promise.all(slice.map(entity => handler(entity)));
            this.currentLoaded += stride;
        }
    }

    @action.bound private resetCurrentLoading(name: string, subEntities = 0) {
        this.currentLoadStep = name;
        this.currentLoaded = 0;
        this.currentToLoad = subEntities;
        this.totalLoaded++;
    }

    @computed private get group() {
        return this.groups.byId(this.props.match.params.groupId);
    }

    @computed private get pageBreadCrumb() {
        const { page, subId } = this.props.match.params;
        switch (page) {
            case "hero": {
                const hero = this.heroes.byId(subId);
                return {
                    key: "page",
                    content: <>
                        <Icon className={css.crumbTypeIcon} name={hero.isNpc ? "chess pawn" : "chess queen"} />
                        {hero.name}
                    </>,
                };
            }
            case "template-items": return {
                key: "page",
                content: "Items",
            };
            case "maneuvers": return {
                key: "page",
                content: "Maneuvers",
            };
            case "battle-map": return {
                key: "page",
                content: this.battleMaps.byId(subId).name,
            };
        }
    }

    @computed private get percentCurrent() {
        return Math.floor((this.currentLoaded / this.currentToLoad) * 100);
    }

    @computed private get percentTotal() {
        return Math.floor((this.totalLoaded / this.totalToLoad) * 100);
    }

    @computed private get isMaster() {
        return this.login.isMaster(this.group.id);
    }

    @computed private get breadCrumbs() {
        if (!this.loaded) { return []; }
        return [
            {
                key: "group",
                content: this.group.name,
            }, {
                key: "playerType",
                content: this.isMaster ? "Master" : "Player",
                icon: this.isMaster ? "chess king" : "child",
            },
            this.pageBreadCrumb,
        ];
    }

    public render() {
        const pageClasses = cx({
            pageSidebarActive: this.ui.visible,
        });
        if (!this.loaded) {
            return (
                <Dimmer active>
                    <div style={{ width: "80vw" }}>
                        <Header style={{ color: "white" }}>Loading {this.currentLoadStep}...</Header>
                        <br />
                        <br />
                        <Progress
                            percent={this.percentCurrent}
                            indicating
                            progress="percent"
                        />
                        <Progress
                            percent={this.percentTotal}
                            indicating
                            progress="percent"
                        />
                    </div>
                </Dimmer>
            );
        }
        return (
            <>
                <AppBar breadCrumbs={this.breadCrumbs} groupId={this.props.match.params.groupId} />
                <Sidebar.Pushable className={css.content}>
                    <AppSidebar groupId={this.props.match.params.groupId} />
                    <Sidebar.Pusher className={pageClasses}>
                        <div className={css.container}>
                            {
                                routes.filter(route => route.parent.includes(Parent.GROUP)).map((route, index) => (
                                    <Route
                                        key={index}
                                        path={`/group/:groupId${route.pattern}`}
                                        component={route.component}
                                    />
                                ))
                            }
                        </div>
                        {
                            isProductionEnvironment() && (
                                <div className={css.version}>{SOFTWARE_VERSION}</div>
                            )
                        }
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </>
        );
    }
}
