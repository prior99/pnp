import * as React from "react";
import { Sidebar, Icon, Dimmer, Loader } from "semantic-ui-react";
import { external, inject, initialize } from "tsdi";
import { Route } from "react-router-dom";
import { observer } from "mobx-react";
import {
    StoreHeroes,
    StoreTemplateItems,
    StoreBattleMaps,
    StoreManeuvers,
    StoreItems,
    StoreLearnedManeuvers,
    StoreMarkers,
    StoreAreas,
    StoreUi,
    StorePresets,
    StoreTemplateMonsters,
    StoreMonsterManeuvers,
    StoreMonsters,
    StoreNotes,
    StoreJournalEntries,
    StoreStashs,
    StoreStashItems,
    StoreMarkerItems,
} from "../../../stores";
import { AppBar } from "../app-bar";
import { isProductionEnvironment } from "pnp-common";
import * as css from "./preset-container.scss";
import * as classNames from "classnames/bind";
import { routes, RouteProps, Parent } from "../../../routing";
import { computed, observable } from "mobx";
import { AppSidebar } from "../app-sidebar";
import { StoreTags } from "../../../stores/tags";
import { StoreHeroTags } from "../../../stores/hero-tags";
import { StoreBattleMapTags } from "../../../stores/battle-map-tags";

declare const SOFTWARE_VERSION: string;

const cx = classNames.bind(css);

export type PresetContainerProps = RouteProps<{
    presetId: string,
    page: "hero" | "template-items" | "maneuvers" | "battle-map",
    subId?: string,
}>;

@external @observer
export class PresetContainer extends React.Component<PresetContainerProps> {
    public static path = "/preset/:presetId/:page/:subId?";

    @inject private presets: StorePresets;
    @inject private heroes: StoreHeroes;
    @inject private templateItems: StoreTemplateItems;
    @inject private templateMonsters: StoreTemplateMonsters;
    @inject private battleMaps: StoreBattleMaps;
    @inject private maneuvers: StoreManeuvers;
    @inject private items: StoreItems;
    @inject private learnedManeuvers: StoreLearnedManeuvers;
    @inject private markers: StoreMarkers;
    @inject private areas: StoreAreas;
    @inject private ui: StoreUi;
    @inject private monsterManeuvers: StoreMonsterManeuvers;
    @inject private monsters: StoreMonsters;
    @inject private notes: StoreNotes;
    @inject private tags: StoreTags;
    @inject private heroTags: StoreHeroTags;
    @inject private battleMapTags: StoreBattleMapTags;
    @inject private journalEntries: StoreJournalEntries;
    @inject private stashs: StoreStashs;
    @inject private markerItems: StoreMarkerItems;
    @inject private stashItems: StoreStashItems;

    @observable private loaded = false;

    @initialize
    public async loadRecursive() {
        const preset = await this.presets.get(this.props.match.params.presetId);
        if (!preset) { return; }
        await this.templateItems.loadForPreset(preset.id);
        await this.journalEntries.loadForPreset(preset.id);
        await this.tags.loadForPreset(preset.id);
        const templateMonsters = await this.templateMonsters.loadForPreset(preset.id);
        await Promise.all(templateMonsters.map(templateMonster => {
            return this.monsterManeuvers.loadForTemplateMonster(templateMonster.id);
        }));
        await Promise.all(templateMonsters.map(templateMonster => {
            return this.monsters.loadForTemplateMonster(templateMonster.id);
        }));
        await this.maneuvers.loadForPreset(preset.id);
        const heroes = await this.heroes.loadForPreset(preset.id);
        await Promise.all(heroes.map(hero => this.items.loadForHero(hero.id)));
        await Promise.all(heroes.map(hero => this.learnedManeuvers.loadForHero(hero.id)));
        await Promise.all(heroes.map(hero => this.notes.loadForHero(hero.id)));
        await Promise.all(heroes.map(hero => this.heroTags.loadForHero(hero.id)));
        const battleMaps = await this.battleMaps.loadForPreset(preset.id);
        await Promise.all(battleMaps.map(battleMap => this.areas.loadForBattleMap(battleMap.id)));
        await Promise.all(battleMaps.map(async battleMap => {
            const markers = await this.markers.loadForBattleMap(battleMap.id);
            await Promise.all(markers.map(marker => this.markerItems.loadForMarker(marker.id)));
        }));
        await Promise.all(battleMaps.map(battleMap => this.battleMapTags.loadForBattleMap(battleMap.id)));
        const stashs = await this.stashs.loadForPreset(preset.id);
        await Promise.all(stashs.map(stash => this.stashItems.loadForStash(stash.id)));
        this.loaded = true;
    }

    @computed private get preset() {
        return this.presets.byId(this.props.match.params.presetId);
    }

    @computed private get pageBreadCrumb() {
        const { page, subId } = this.props.match.params;
        switch (page) {
            case "hero": {
                const hero = this.heroes.byId(subId);
                return {
                    key: "page",
                    content: <>
                        <Icon className={css.crumbTypeIcon} name={hero.isNpc ? "chess pawn" : "chess queen"} />
                        {hero.name}
                    </>,
                };
            }
            case "template-items": return {
                key: "page",
                content: "Items",
            };
            case "maneuvers": return {
                key: "page",
                content: "Maneuvers",
            };
            case "battle-map": return {
                key: "page",
                content: this.battleMaps.byId(subId).name,
            };
        }
    }

    @computed private get breadCrumbs() {
        if (!this.loaded) { return []; }
        return [
            {
                key: "preset",
                content: this.preset.name,
            },
            this.pageBreadCrumb,
        ];
    }

    public render() {
        const pageClasses = cx({
            pageSidebarActive: this.ui.visible,
        });
        if (!this.loaded) {
            return (
                <Dimmer active>
                    <Loader />
                </Dimmer>
            );
        }
        return (
            <>
                <AppBar breadCrumbs={this.breadCrumbs} />
                <Sidebar.Pushable className={css.content}>
                    <AppSidebar presetId={this.props.match.params.presetId} />
                    <Sidebar.Pusher className={pageClasses}>
                        <div className={css.container}>
                            {
                                routes.filter(route => route.parent.includes(Parent.PRESET)).map((route, index) => (
                                    <Route
                                        key={index}
                                        path={`/preset/:presetId${route.pattern}`}
                                        component={route.component}
                                    />
                                ))
                            }
                        </div>
                        {
                            isProductionEnvironment() && (
                                <div className={css.version}>{SOFTWARE_VERSION}</div>
                            )
                        }
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </>
        );
    }
}
