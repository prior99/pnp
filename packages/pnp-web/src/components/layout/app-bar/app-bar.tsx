import * as React from "react";
import { History } from "history";
import { Menu, Dropdown, Breadcrumb } from "semantic-ui-react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { computed, action } from "mobx";
import { StoreUi, StoreLogin, StorePresets, StoreGroups } from "../../../stores";
import * as css from "./app-bar.scss";
import { hasFields } from "hyrest-mobx";
import { Preset } from "pnp-common";
import { routePresetDashboard, routeGroupDashboard, routeCreateGroup, routeLanding } from "../../../pages";
import { routeCreatePreset } from "../../../pages/create-preset";
import { Live } from "../../../live";
import * as Color from "color";

export interface AppBarProps {
    breadCrumbs?: { key: string, content: string | JSX.Element }[];
    groupId?: string;
}

@external @observer @hasFields()
export class AppBar extends React.Component<AppBarProps> {
    @inject private ui: StoreUi;
    @inject private login: StoreLogin;
    @inject private presets: StorePresets;
    @inject private history: History;
    @inject private groups: StoreGroups;
    @inject private live: Live;

    @computed private get allGroups(): Preset[] {
        return this.login.groupIds
            .map(groupId => this.groups.getLazy(groupId))
            .filter(group => Boolean(group))
            .sort((a, b) => a.name.localeCompare(b.name));
    }

    @computed private get allPresets(): Preset[] {
        return this.login.presetIds
            .map(presetId => this.presets.getLazy(presetId))
            .filter(preset => Boolean(preset))
            .sort((a, b) => a.name.localeCompare(b.name));
    }

    @computed private get sidebarButtonVisible() { return !this.ui.alwaysOpen; }

    @computed private get color() {
        if (!this.props.groupId) { return; }
        return this.live.getOwnColor(this.props.groupId);
    }

    private handlePresetClick(presetId: string) {
        return action(() => this.history.push(routePresetDashboard.path(undefined, presetId)));
    }

    public render() {
        const { breadCrumbs = [] } = this.props;
        const style = {
            borderLeft: `8px solid ${this.color}`,
            background: this.color ? Color(this.color).mix(Color("white"), 0.6) : "white",
            borderTop: "0px solid transparent",
            borderRight: "0px solid transparent",
            borderBottom: "0px solid transparent",
        };

        return (
            <Menu className={css.appBar} attached compact style={style}>
                <Menu.Menu position="left">
                    {this.sidebarButtonVisible && (
                        <Menu.Item
                            icon="bars"
                            onClick={this.ui.toggleVisibility}
                        />
                    )}
                    <Menu.Item header>
                        <Breadcrumb className={css.crumbs} icon="right angle" sections={breadCrumbs} />
                    </Menu.Item>
                </Menu.Menu>
                <Menu.Menu position="right">
                    <Dropdown item key="user" text={this.ui.alwaysOpen ? "Change Group" : undefined}>
                        <Dropdown.Menu>
                            <Dropdown.Item
                                content="Overview"
                                icon="list"
                                onClick={() => this.history.push(routeLanding.path())}
                            />
                            <Dropdown.Divider />
                            <Dropdown.Header>Groups</Dropdown.Header>
                            {
                                this.allGroups.map(group => (
                                    <Dropdown.Item
                                        key={group.id}
                                        content={group.name}
                                        icon="group"
                                        onClick={() => this.history.push(routeGroupDashboard.path(group.id))}
                                    />
                                ))
                            }
                            <Dropdown.Item
                                content="Create new group"
                                icon="add"
                                onClick={() => this.history.push(routeCreateGroup.path())}
                            />
                            <Dropdown.Divider />
                            <Dropdown.Header>Presets</Dropdown.Header>
                            {
                                this.allPresets.map(preset => (
                                    <Dropdown.Item
                                        key={preset.id}
                                        content={preset.name}
                                        icon="th"
                                        onClick={this.handlePresetClick(preset.id)}
                                    />
                                ))
                            }
                            <Dropdown.Item
                                content="Create new preset"
                                icon="add"
                                onClick={() => this.history.push(routeCreatePreset.path())}
                            />
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Menu>
            </Menu>
        );
    }
}
