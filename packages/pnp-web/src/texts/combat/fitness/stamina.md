# Stamina

Every action in combat, attacks as well as defense will use stamina points which are regenerated each turn.
The stamina attribute determines the maximum stamina points a character can have and also the amount of regenerated stamina points per turn.