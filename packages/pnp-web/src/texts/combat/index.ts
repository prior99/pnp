import * as bodyImport from "./body";
export const body = bodyImport;
import * as combatStyleImport from "./combat-style";
export const combatStyle = combatStyleImport;
import * as fitnessImport from "./fitness";
export const fitness = fitnessImport;
