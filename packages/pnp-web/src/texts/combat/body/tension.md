# Tension

When a character is attacked successfully, the tension of the attacked character reduces the amount of damage taken.