# Swiftness

Swiftness influences both movement speed and evasiveness.
The higher the swiftness attribute of a character, the higher the chance of evading an attack.
Swiftness also increases the distance a character can run per stamina point.