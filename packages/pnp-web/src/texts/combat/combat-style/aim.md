# Aim

Aim is the primary attribute for range-based combat.
It determines the chance to hit when fighting with range-based weapons. 