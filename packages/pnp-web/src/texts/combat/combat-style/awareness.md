# Awareness

Awareness is the main attribute for close combat.
For all close-combat style weapons, awareness determines the chance of a successfull attack.