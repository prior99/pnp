# Focus

Focus is the primary attribute for magical skill- and magic based combat.
It determines the amount of focus points a character can accumulate per turn.
Focus points will be used by rituals and other magical and non-magical skills.
When interrupted, a high focus also decreases the chance of loosing focus-points.