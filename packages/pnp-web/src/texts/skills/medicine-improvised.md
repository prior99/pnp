# Improvised medicine

Use this skill with items which you improvised as medicine (makeshift bandages from ripped clothing, salves from plants found nearby, smearing dirt in open wounds to stop bleeding, improvising antidote, removing and cauterizing lost limbs, splints, ...)

Your game master will decide on the effectiveness of such items based on how ridiculous your idea is.

*Typically this is much lower than with crafted medicine and might be around a quarter to half of your roll (which is 4D6) plus the skill's level.*

## Inspirations

* Makeshift bandages: Rip some textile clothing into makeshift bandages. Will regenerate **1/4** of your roll on **Improvised medicine**. Crafted instantly.
* Splints: Use a semi-straight stick to tie up a broken limb. Will regenerate **1/3** of your roll on **Improvised medicine**. Crafting takes one hour and a stick as well as some clothing is needed.
* Salve: Use some local fauna and water to craft a salve. Will regenerate **1/2** of your roll on **Improvised medicine**, but an additional roll before determines whether the healing is effective, useless or poisonous. Crafting takes an hour.
* Using mud to cool burns: Heals **1/5** of your roll on **Improvised medicine**.

## Effectiveness

Of course it's up to the master to decide how effective your idea of improvising medicine is.
In general, when using up any item, **1/4th** of your roll is a good guidelines. The longer the crafting takes, the better the results might end up.

## Dangers

Fumbling with your party's life might not be a good idea and is prone to go wrong:

* Plants might be poisonous
* Makeshift bandages might have been dirty and infect the wounds
* Splints can break
* Mud might have been contamined
* ...