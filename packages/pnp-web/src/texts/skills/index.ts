export { default as mind } from "./mind.md";
export { default as medicine } from "./medicine.md";
export { default as improvisedMedicine } from "./medicine-improvised.md";
export { default as craftedMedicine } from "./medicine-crafted.md";
export { default as nursing } from "./medicine-nursing.md";
