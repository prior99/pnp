# Nursing

Often your whole party will suffer from loss of health after a long day of battleing the evil (or good) in this world. They'll need to heal, remove negative status effects, etc.


*Typically this is much higher than crafted medicine and effects the whole party (including yourself). It may be around twice of your roll plus the skill's level.*

Nursing will determine your effecitveness with healing the whole party in your sleep **this includes yourself**.

## Environement

The game master will decide on the effectiveness of your nursing based on the environment and the duration. A nursing session in a clean tavern for eight hours should be a default point of reference.

## Items

Additionally, items might help you out here.

## Disturbances

Your nursing might get disturbed and this might, based on the current situation result in negative effects.

Some inspirations:

* If everybody is fast asleep as you sedated them and gave them some healing potion:
   
   Nothing happens, but likely you will have to face the *interrupter* alone.

* You're in the middle of amputating your friends left leg:

   You will have to roll repeatedly on **Nursing** again, in order to proove that you can keep focus, also, when interrupting the surgery, your friend might die.

* You're in the middle of having a deep, long, assisted sleep and are disturbed after 4 of 8 hours:

    Only half of the health is regenerated.