# Crafted medicine

Use this skill with items you obtained which are specifically meant to heal (health potions, salves, bandages, etc.). The item will describe how much health will be healed.

*Typically this is around the roll (which is 4D6) plus the skill's level.*


## Crafting medicine

It is not possible to craft professional medicine.

## Buying medicine

Medicine should be buyable in all major cities and has to be affordable.

### Inspirational prices

* Health potion: **100 Gold**, heals your roll on **Crafted medicine**.
* Stable potion: **120 Gold**, heals your roll on **Crafted medicine**, with your skill rank doubled.
* Unstable potion: **150 Gold**, heals your roll on **Crafted medicine**, with your roll doubled.
* Health injection: **200 Gold**, heals your roll on **Crafted medicine**, but doubled.
* Charm: **100 Gold**, heals your roll on **Crafted medicine** to the whole party. Needs to be thrown in a camp fire an inhaled for half an hour.
* Health spray: **250 Gold**, heals your roll on **Crafted medicine** to the whole party instantly.

## Using medicine in battles

Depending on the item, medicine should be applicable within battles. This will take up to 2 whole turns of you and your patient. Such items are generally much more expensive than normally.

### Inspirational prices

* Health pill: **200 Gold**, heals your roll on **Crafted medicine**. Takes 2 rounds for you and your patient to use.
* Stable pill: **250 Gold**, heals your roll on **Crafted medicine**, with your skill rank doubled. Takes 2 rounds for you and your patient to use.
* Unstable pill: **300 Gold**, heals your roll on **Crafted medicine**, with your roll doubled. Takes 2 rounds for you and your patient to use.