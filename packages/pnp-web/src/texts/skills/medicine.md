# Medicine

Your general ability to handle medicine.
It might be involved with rolls on healing, detecting anatomics, etc.

## Healing

In order to heal your party you have three options:

 * Improvised medicine
 * Crafted medicine
 * Nursing

These three skills can be used for different situations:

### Instant healing

In scenarios where you need instant healing (before, after and in battles) you may use one of these skills:

 * Crafted medicine
 * Improvised medicine

#### Crafted medicine

Use this skill with items you obtained which are specifically meant to heal (health potions, salves, bandages, etc.). The item will describe how much health will be healed.

*Typically this is around the roll (which is 4D6) plus the skill's level.*

#### Improvised medicine

Use this skill with items which you improvised as medicine (makeshift bandages from ripped clothing, salves from plants found nearby, smearing dirt in open wounds to stop bleeding, improvising antidote, removing and cauterizing lost limbs, splints, ...)

*Typically this is much lower than with crafted medicine and might be around a quarter to half of your roll (which is 4D6) plus the skill's level.*

### Nursing

Often your whole party will suffer from loss of health after a long day of battleing the evil (or good) in this world. They'll need to heal, remove negative status effects, etc.

Nursing will determine your effecitveness with healing the whole party in your sleep **this includes yourself**.

*Typically this is much higher than crafted medicine and effects the whole party (including yourself). It may be around twice of your roll plus the skill's level.*