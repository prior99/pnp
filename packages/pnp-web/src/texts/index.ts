import * as combatImport from "./combat";
export const combat = combatImport;

import * as skillsImport from "./skills";
export const skills = skillsImport;
