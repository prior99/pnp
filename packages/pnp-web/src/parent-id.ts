export function parentIds({ groupId, presetId }: { groupId?: string, presetId?: string }) {
    if (groupId && presetId) { throw new Error("Both preset and group id were specified."); }
    if (!groupId && !presetId) { throw new Error("Neither preset and group id were specified."); }
    if (groupId) {
        return { group: { id: groupId } };
    }
    return { preset: { id: presetId } };
}

interface ParentIdProvider {
    group?: { id?: string };
    preset?: { id?: string };
}

export function extractParentIds(entity: ParentIdProvider): [string | undefined, string | undefined] {
    const groupId = entity.group ? entity.group.id : undefined;
    const presetId = entity.preset ? entity.preset.id : undefined;
    return [groupId, presetId];
}
