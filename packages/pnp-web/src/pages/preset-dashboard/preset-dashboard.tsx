import * as React from "react";
import { addRoute, RouteProps, Parent } from "../../routing";
import { observer } from "mobx-react";
import { external } from "tsdi";
import {
    Content,
    CardHeroes,
    CardBattleMaps,
    CardNpcs,
    Mosaic,
} from "../../components";

@observer @external
export class PagePresetDashboard extends React.Component<RouteProps<{ presetId: string }>> {
    public render() {
        const { presetId } = this.props.match.params;
        return (
            <>
                <Content>
                    <Mosaic size="twice">
                        <CardHeroes presetId={presetId} />
                        <CardBattleMaps presetId={presetId} />
                        <CardNpcs presetId={presetId} />
                    </Mosaic>
                </Content>
            </>
        );
    }
}

export const routePresetDashboard = {
    path: (_groupId: string | undefined, presetId: string | undefined) => `/preset/${presetId}/dashboard`,
    pattern: "/dashboard",
    component: PagePresetDashboard,
    title: "Preset Dashboard",
    icon: "dashboard",
    parent: [Parent.PRESET],
    navbar: true,
};

addRoute(routePresetDashboard);
