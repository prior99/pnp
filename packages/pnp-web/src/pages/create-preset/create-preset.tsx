import * as React from "react";
import { action } from "mobx";
import { observer } from "mobx-react";
import { History } from "history";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Button } from "semantic-ui-react";
import { Icon } from "react-fa";
import { Preset } from "pnp-common";
import { external, inject } from "tsdi";
import { addRoute } from "../../routing";
import { StorePresets } from "../../stores";
import { formItem } from "../../hyrest-semantic-ui";
import { Content } from "../../components";
import { routePresetDashboard } from "../preset-dashboard";

@observer @external @hasFields()
export class PageCreatePreset extends React.Component {
    @inject private history: History;
    @inject private presets: StorePresets;

    @field(Preset) private preset: Field<Preset>;

    @action.bound private async handlePresetCreate(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        const newPreset = await this.presets.create(this.preset.value);
        if (!newPreset) { return; }
        this.history.push(routePresetDashboard.path(undefined, newPreset.id));
    }

    public render() {
        const { password, name, description } = this.preset.nested;
        return (
            <Content title="Create Preset">
                <Form onSubmit={this.handlePresetCreate}>
                    <Form.Field {...formItem(name)}>
                        <label>Name</label>
                        <Input
                            addonBefore={<Icon name="th" />}
                            placeholder="Name"
                            {...name.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(description)}>
                        <label>Description</label>
                        <Input
                            addonBefore={<Icon name="info" />}
                            placeholder="Description"
                            {...description.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(description)}>
                        <label>Password</label>
                        <Input
                            addonBefore={<Icon name="lock" />}
                            placeholder="Password"
                            type="password"
                            {...password.reactInput}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Button htmlType="submit" disabled={!this.preset.valid}>Create</Button>
                    </Form.Field>
                </Form>
            </Content>
        );
    }
}

export const routeCreatePreset = {
    path: () => "/create-preset",
    pattern: "/create-preset",
    component: PageCreatePreset,
    parent: [],
    global: true,
};

addRoute(routeCreatePreset);
