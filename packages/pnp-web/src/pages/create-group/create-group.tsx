import * as React from "react";
import { action } from "mobx";
import { observer } from "mobx-react";
import { History } from "history";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Button } from "semantic-ui-react";
import { Icon } from "react-fa";
import { Group } from "pnp-common";
import { external, inject } from "tsdi";
import { addRoute } from "../../routing";
import { StoreGroups } from "../../stores";
import { formItem } from "../../hyrest-semantic-ui";
import { Content } from "../../components";
import { routeGroupDashboard } from "../group-dashboard";

@observer @external @hasFields()
export class PageCreateGroup extends React.Component {
    @inject private history: History;
    @inject private groups: StoreGroups;

    @field(Group) private group: Field<Group>;

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        const newGroup = await this.groups.create(this.group.value);
        if (!newGroup) { return; }
        this.history.push(routeGroupDashboard.path(newGroup.id));
    }

    public render() {
        const { password, name, masterPassword } = this.group.nested;
        return (
            <Content title="Create Group">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Field {...formItem(name)}>
                        <label>Group name</label>
                        <Input
                            addonBefore={<Icon name="group" />}
                            placeholder="Group Name"
                            {...name.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(password)}>
                        <label>Shared group password</label>
                        <Input
                            addonBefore={<Icon name="lock" />}
                            placeholder="Shared Password"
                            type="password"
                            {...password.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(masterPassword)}>
                        <label>Dungeon master password</label>
                        <Input
                            addonBefore={<Icon name="lock" />}
                            placeholder="Dungeon master Password"
                            type="password"
                            {...masterPassword.reactInput}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Button disabled={!this.group.valid} type="primary" htmlType="submit">Create</Button>
                    </Form.Field>
                </Form>
            </Content>
        );
    }
}

export const routeCreateGroup = {
    path: () => "/create-group",
    pattern: "/create-group",
    component: PageCreateGroup,
    parent: [],
    global: true,
};

addRoute(routeCreateGroup);
