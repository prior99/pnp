import * as React from "react";
import { addRoute, Parent, RouteProps } from "../../routing";
import { Grid, Header, Form, Input } from "semantic-ui-react";
import { observer } from "mobx-react";
import { computed, action, observable } from "mobx";
import { external, inject } from "tsdi";
import { CardCreateManeuver, CardManeuver, Content, Mosaic } from "../../components";
import { StoreManeuvers } from "../../stores";
import * as css from "./maneuvers.scss";

@observer @external
export class PageManeuvers extends React.Component<RouteProps<{ groupId?: string, presetId?: string }>> {
    @inject private maneuvers: StoreManeuvers;

    @observable private query = "";

    @computed private get allManeuvers() {
        const { groupId, presetId } = this.props.match.params;

        return this.maneuvers
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(maneuver => {
                if (maneuver.group && groupId !== maneuver.group.id) { return false; }
                if (maneuver.preset && presetId !== maneuver.preset.id) {
                    return false;
                }
                return maneuver.name.toLowerCase().includes(this.query.toLowerCase()) ||
                    maneuver.description.toLowerCase().includes(this.query.toLowerCase());
            });
    }

    @action.bound private onQueryChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.query = evt.currentTarget.value;
    }

    public render() {
        return (
            <Content>
                <Grid>
                    <Grid.Column mobile={16} computer={3} className={css.leftColumn}>
                        <Header>Search</Header>
                        <Form>
                            <Form.Field>
                                <label>Query</label>
                                <Input
                                    placeholder="Query"
                                    onChange={this.onQueryChange}
                                    value={this.query}
                                />
                            </Form.Field>
                        </Form>
                        <CardCreateManeuver {...this.props.match.params} />
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={12}>
                        <Header>Maneuvers</Header>
                        <Mosaic>
                            {
                                this.allManeuvers.map(maneuver => (
                                    <CardManeuver key={maneuver.id} id={maneuver.id} />
                                ))
                            }
                        </Mosaic>
                    </Grid.Column>
                </Grid>
            </Content>
        );
    }
}

export const routeManeuvers = {
    path: (groupId: string | undefined, presetId: string | undefined) => groupId ?
        `/group/${groupId}/maneuvers` :
        `/preset/${presetId}/maneuvers`,
    pattern: "/maneuvers",
    component: PageManeuvers,
    navbar: true,
    icon: "chess",
    title: "Maneuvers",
    masterOnly: true,
    parent: [Parent.GROUP, Parent.PRESET],
};

addRoute(routeManeuvers);
