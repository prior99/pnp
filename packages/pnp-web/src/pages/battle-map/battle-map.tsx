import * as React from "react";
import { addRoute, RouteProps, Parent } from "../../routing";
import { inject, external, initialize } from "tsdi";
import { StoreBattleMaps, StoreMarkers, StoreLogin, StoreAreas, StoreVisiting } from "../../stores";
import { computed, action, observable, reaction } from "mobx";
import { observer } from "mobx-react";
import { SemanticWIDTHS, Segment, Grid } from "semantic-ui-react";
import { Map as LeafletMap, TileLayer, Rectangle, Polyline } from "react-leaflet";
import { BattleMap, PageName } from "pnp-common";
import * as Leaflet from "leaflet";
import { bind } from "lodash-decorators";
import * as CanvasLayer from "react-leaflet-canvas-layer";
import { PnPMarker, BattleMapMasterForm, BattleMapForm, BattleMapDescription, BattleMapPane } from "../../components";
import * as css from "./battle-map.scss";
import { Live, Pointer } from "../../live";
import { toPairs, groupBy, prop } from "ramda";

declare const baseUrl: string;

export type PageBattleMapProps = RouteProps<{
    id: string;
    pane: BattleMapPane;
}>;

@external @observer
export class PageBattleMap extends React.Component<PageBattleMapProps> {
    @inject private battleMaps: StoreBattleMaps;
    @inject private markers: StoreMarkers;
    @inject private areaStore: StoreAreas;
    @inject private login: StoreLogin;
    @inject private visiting: StoreVisiting;
    @inject private live: Live;

    @observable private clearingMouseDownPosition: [number, number];
    @observable private clearingMouseTmpPosition: [number, number];
    @observable private pointingMouseDownPosition: [number, number];
    @observable private pointingMouseTmpPosition: [number, number];
    @observable private clearing = false;

    private interval: any;

    @initialize protected async initialize() {
        reaction(() => this.areaStore.all, () => this.forceUpdate());
        if (this.canEdit) {
            document.addEventListener("keydown", this.keyDownListener);
            document.addEventListener("keyup", this.keyUpListener);
        }
        this.interval = setInterval(this.pointerInterval, 300);
    }

    public componentWillUnmount() {
        if (this.canEdit) {
            document.removeEventListener("keydown", this.keyDownListener);
            document.removeEventListener("keyup", this.keyUpListener);
        }
        clearInterval(this.interval);
    }

    @bind private pointerInterval() {
        if (!this.pointing || !this.pointingMouseDownPosition) { return; }
        if (!this.battleMap.group) { return; }
        this.live.point(this.battleMap.group.id, this.battleMap.id, this.pointingMouseTmpPosition);
    }

    @bind private keyUpListener(evt: KeyboardEvent) {
        if (evt.key !== "Control") { return; }
        this.clearingMouseDownPosition = undefined;
        this.clearingMouseTmpPosition = undefined;
        this.clearing = false;
    }

    @bind private keyDownListener(evt: KeyboardEvent) {
        if (evt.key !== "Control") { return; }
        this.clearing = true;
    }

    @computed private get id() {
        return this.props.match.params.id;
    }

    @computed private get battleMap() {
        return this.battleMaps.byId(this.id);
    }

    @computed private get areas() {
        return this.areaStore.forBattleMap(this.id);
    }

    @computed private get url() {
        return `${baseUrl}/battle-map/${this.battleMap.id}/tile/{z}/{x}/{y}`;
    }

    @computed private get canEdit() {
        if (!this.battleMap.group) { return true; }
        return this.login.isMaster(this.battleMap.group.id);
    }

    @computed private get markerList() {
        return this.markers.forBattleMap(this.id)
            .filter(marker => this.canEdit || this.areas.some(area => area.contains(marker.latitude, marker.longitude)))
            .map(marker => <PnPMarker key={marker.id} id={marker.id} />);
    }

    @computed private get pointing() {
        return this.battleMaps.isPointerMode(this.battleMap.id);
    }

    @computed get mapProps() {
        return this.visiting.propForMap(this.battleMap.id) || {
            zoom: 1,
            center: {
                lat: 0,
                lng: 0,
            },
        };
    }

    @computed private get customMarkerCreatePosition() {
        const { center } = this.mapProps;

        // The master can see covered areas, so we can always create the new marker at the cneter of the map
        if (this.canEdit) {
            return center;
        }

        const areaProps = this.areas.map(area => {
            const areaCenter = {
                lat: (area.bottom + area.top) / 2,
                lng: (area.right + area.left) / 2,
            };

            const distance = Math.sqrt((center.lat - areaCenter.lat) ** 2 + (center.lng - areaCenter.lng) ** 2);

            return {
                center: areaCenter,
                distance,
            };
        });

        if (areaProps.length === 0) { return { lat: 0, lng: 0 }; }

        const minDistance = areaProps.reduce((min, current) => {
            if (min.distance <= current.distance) {
                return min;
            }

            return current;
        }, areaProps[0]);

        return minDistance ? minDistance.center : { lat: 0, lng: 0 };
    }

    @computed private get descriptionVisible() {
        return this.battleMaps.isDescriptionActive(this.battleMap.id);
    }

    @bind private drawMethod(info: { canvas: HTMLCanvasElement, map: Leaflet.Map }) {
        const ctx = info.canvas.getContext("2d");
        ctx.clearRect(0, 0, info.canvas.width, info.canvas.height);
        ctx.fillStyle = this.canEdit ? "rgba(38, 38, 38, 0.7)" : "rgb(38, 38, 38)";
        ctx.fillRect(0, 0, info.canvas.width, info.canvas.height);
        this.areas.forEach(area => {
            const topLeft = info.map.latLngToContainerPoint([area.top, area.left]);
            const bottomRight = info.map.latLngToContainerPoint([area.bottom, area.right]);
            ctx.clearRect(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
        });
    }

    @action.bound private handleMouseMove(evt: Leaflet.LeafletMouseEvent) {
        if (this.clearing) { this.clearingMouseTmpPosition = [evt.latlng.lat, evt.latlng.lng]; }
        if (this.pointing) { this.pointingMouseTmpPosition = [evt.latlng.lat, evt.latlng.lng]; }
    }

    @action.bound private handleMouseDown(evt: Leaflet.LeafletMouseEvent) {
        if (this.clearing) { this.clearingMouseDownPosition = [evt.latlng.lat, evt.latlng.lng]; }
        if (this.pointing) {
            this.pointingMouseDownPosition = [evt.latlng.lat, evt.latlng.lng];
            this.pointingMouseTmpPosition = [evt.latlng.lat, evt.latlng.lng];
            this.live.point(this.battleMap.group.id, this.battleMap.id, this.pointingMouseTmpPosition);
        }
    }

    @action.bound private handleMouseUp(evt: Leaflet.LeafletMouseEvent) {
        if (this.clearing && this.clearingMouseDownPosition) {
            this.areaStore.create({
                battleMap: { id: this.id } as BattleMap,
                top: Math.min(this.clearingMouseDownPosition[0], evt.latlng.lat),
                bottom: Math.max(this.clearingMouseDownPosition[0], evt.latlng.lat),
                left: Math.min(this.clearingMouseDownPosition[1], evt.latlng.lng),
                right: Math.max(this.clearingMouseDownPosition[1], evt.latlng.lng),
            });
            this.clearingMouseDownPosition = undefined;
            this.clearingMouseTmpPosition = undefined;
            this.forceUpdate();
        }
        if (this.pointing && this.pointingMouseDownPosition) {
            this.pointingMouseDownPosition = undefined;
            this.pointingMouseTmpPosition = undefined;
        }
    }

    @computed private get points() {
        return toPairs(groupBy(prop("color"), this.live.pointers) as { [color: string]: Pointer[] })
            .map(([color, points]) => <Polyline
                key={color}
                color={color}
                positions={points.map(point => point.pos)}
            />);
    }

    @action.bound private handleMapNavigate(event: Leaflet.LeafletEvent) {
        this.visiting.setMapProps(this.battleMap.id, event.target.getCenter(), event.target.getZoom());
    }

    @computed private get mapColumnWidth(): SemanticWIDTHS {
        if (!this.descriptionVisible) { return 16; }
        return 11;
    }

    public render() {
        if (!this.battleMap) { return null; }

        return (
            <Grid>
                <Grid.Row>
                    {
                        this.descriptionVisible && <Grid.Column computer={5} mobile={16}>
                            <BattleMapDescription id={this.battleMap.id} pane={this.props.match.params.pane} />
                        </Grid.Column>
                    }
                    <Grid.Column mobile={16} computer={this.mapColumnWidth} className={css.container}>
                        <div className={css.tools}>
                            {
                                this.canEdit ? (
                                    <>
                                        <Segment className={css.segment}>
                                            <BattleMapMasterForm id={this.battleMap.id} />
                                            <br />
                                            <br />
                                            <BattleMapForm
                                                id={this.battleMap.id}
                                                createPosition={this.customMarkerCreatePosition}
                                            />
                                        </Segment>
                                        <p className={css.help}>Press CTRL to reveal areas.</p>
                                    </>
                                ) : (
                                        <Segment>
                                            <BattleMapForm
                                                id={this.battleMap.id}
                                                createPosition={this.customMarkerCreatePosition}
                                            />
                                        </Segment>
                                    )
                            }
                        </div>
                        <LeafletMap
                            zoom={this.mapProps.zoom}
                            center={this.mapProps.center}
                            className={css.map}
                            onmousemove={this.handleMouseMove}
                            onmousedown={this.handleMouseDown}
                            onmouseup={this.handleMouseUp}
                            onzoomend={this.handleMapNavigate}
                            ondragend={this.handleMapNavigate}
                            dragging={!this.clearing && !this.pointing}
                            attributionControl={false}
                        >
                            {
                                (this.clearingMouseTmpPosition && this.clearingMouseDownPosition) && (
                                    <Rectangle
                                        bounds={[this.clearingMouseDownPosition, this.clearingMouseTmpPosition]}
                                    />
                                )
                            }
                            <CanvasLayer drawMethod={this.drawMethod} />
                            <TileLayer
                                continuousWorld={false}
                                noWrap
                                url={this.url}
                            />
                            {this.markerList}
                            {this.points}
                        </LeafletMap>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export const routeBattleMap = {
    path: (groupId: string | undefined, presetId: string | undefined, id: string, pane = "general") => groupId ?
        `/group/${groupId}/battle-map/${id}/${pane}` :
        `/preset/${presetId}/battle-map/${id}/${pane}`,
    pattern: "/battle-map/:id/:pane",
    component: PageBattleMap,
    title: "Battle map",
    parent: [Parent.GROUP, Parent.PRESET],
    livePage: PageName.BATTLE_MAP,
};

addRoute(routeBattleMap);
