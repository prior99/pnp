import * as React from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import { History } from "history";
import { field, Field, hasFields } from "hyrest-mobx";
import { Input, Button, Form, Checkbox } from "semantic-ui-react";
import { Icon } from "react-fa";
import { Group } from "pnp-common";
import { external, inject } from "tsdi";
import { addRoute, RouteProps } from "../../routing";
import { StoreLogin } from "../../stores";
import { formItem } from "../..//hyrest-semantic-ui";
import { Content } from "../../components";
import { routeGroupDashboard } from "../group-dashboard";

@observer @external @hasFields()
export class PageJoinGroup extends React.Component<RouteProps<{ name: string }>> {
    @inject private history: History;
    @inject private login: StoreLogin;

    @observable private master = false;
    @field(Group) private group: Field<Group>;

    public componentDidMount() {
        this.group.nested.name.update(this.props.match.params.name);
    }

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        const groupId = await this.login.loginGroup(this.group.value, this.master);
        if (!groupId) { return; }
        this.history.push(routeGroupDashboard.path(groupId));
        location.reload();
    }

    @action.bound private handleMaster(e: React.SyntheticEvent<HTMLInputElement>) {
        this.master = !this.master;
    }

    @computed private get valid() {
        return this.group.valid && (!this.master || this.group.nested.masterPassword.valid);
    }

    public render () {
        const { name, password, masterPassword } = this.group.nested;
        return (
            <Content title="Join Group">
                <Form size="large" onSubmit={this.handleSubmit}>
                    <Form.Field {...formItem(name)}>
                        <label>Group name</label>
                        <Input
                            icon="envelope"
                            placeholder="Group name"
                            {...name.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(password)}>
                        <label>Password</label>
                        <Input
                            icon="lock"
                            placeholder="Password"
                            type="password"
                            {...password.reactInput}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Dungeon master</label>
                        <Checkbox
                            toggle
                            label="Join as dungeon master"
                            checked={this.master}
                            onChange={this.handleMaster}
                        />
                    </Form.Field>
                    {
                        this.master && (
                            <Form.Field {...formItem(masterPassword)} >
                                <label>Dungeon master password</label>
                                <Input
                                    addonBefore={<Icon name="lock" />}
                                    placeholder="Dungeon master password"
                                    type="password"
                                    {...masterPassword.reactInput}
                                />
                            </Form.Field>
                        )
                    }
                    <Form.Field>
                        <Button disabled={!this.valid} type="submit">Join</Button>
                    </Form.Field>
                </Form>
            </Content>
        );
    }
}

export const routeJoinGroup = {
    path: (name: string) => `/join-group/${name}`,
    pattern: "/join-group/:name",
    component: PageJoinGroup,
    parent: [],
    global: true,
};

addRoute(routeJoinGroup);
