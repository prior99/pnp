import * as React from "react";
import { external, inject } from "tsdi";
import { addRoute, Parent, RouteProps } from "../../routing";
import { StoreLogin } from "../../stores";
import { CardApplyPreset, CardBattleMaps, CardHeroes, CardNpcs, Content, Mosaic } from "../../components";

@external
export class PageGroupDashboard extends React.Component<RouteProps<{ groupId: string }>> {
    @inject public login: StoreLogin;

    public render() {
        const { groupId } = this.props.match.params;
        return (
            <>
                <Content>
                    <Mosaic size="twice">
                        <CardHeroes groupId={groupId} />
                        <CardBattleMaps groupId={groupId} />
                        <CardNpcs groupId={groupId} />
                        {
                            this.login.isMaster(this.props.match.params.groupId) &&
                            <CardApplyPreset groupId={this.props.match.params.groupId} />
                        }
                    </Mosaic>
                </Content>
            </>
        );
    }
}

export const routeGroupDashboard = {
    path: (groupId: string | undefined) => `/group/${groupId}/dashboard`,
    pattern: "/dashboard",
    component: PageGroupDashboard,
    navbar: true,
    icon: "dashboard",
    title: "Group Dashboard",
    parent: [Parent.GROUP],

};

addRoute(routeGroupDashboard);
