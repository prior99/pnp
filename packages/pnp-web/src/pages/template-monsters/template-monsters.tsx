import * as React from "react";
import { addRoute, Parent, RouteProps } from "../../routing";
import { Grid, Header, Form, Input } from "semantic-ui-react";
import { observer } from "mobx-react";
import { computed, action, observable } from "mobx";
import { external, inject } from "tsdi";
import { CardCreateTemplateMonster, CardTemplateMonster, Content, Mosaic } from "../../components";
import { StoreTemplateMonsters } from "../../stores";
import * as css from "./template-monsters.scss";

@observer @external
export class PageTemplateMonsters extends React.Component<RouteProps<{ groupId?: string, presetId?: string }>> {
    @inject private templateMonsters: StoreTemplateMonsters;

    @observable private query = "";

    @computed private get allMonsters() {
        const { groupId, presetId } = this.props.match.params;

        return this.templateMonsters
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(templateMonster => {
                if (templateMonster.group && groupId !== templateMonster.group.id) { return false; }
                if (templateMonster.preset && presetId !== templateMonster.preset.id) {
                    return false;
                }
                return templateMonster.name.toLowerCase().includes(this.query.toLowerCase()) ||
                    templateMonster.description.toLowerCase().includes(this.query.toLowerCase());
            });
    }

    @action.bound private onQueryChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.query = evt.currentTarget.value;
    }

    public render() {
        return (
            <Content>
                <Grid>
                    <Grid.Column mobile={16} computer={4} className={css.leftColumn}>
                        <Header>Search</Header>
                        <Form>
                            <Form.Field>
                                <label>Query</label>
                                <Input
                                    placeholder="Query"
                                    onChange={this.onQueryChange}
                                    value={this.query}
                                />
                            </Form.Field>
                        </Form>
                        <CardCreateTemplateMonster {...this.props.match.params} />
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={10}>
                        <Header>Monsters</Header>
                        <Mosaic size="twice">
                        {
                            this.allMonsters.map(item => <CardTemplateMonster key={item.id} id={item.id} />)
                        }
                        </Mosaic>
                    </Grid.Column>
                </Grid>
            </Content>
        );
    }
}

export const routeTemplateMonsters = {
    path: (groupId: string | undefined, presetId: string | undefined) => groupId ?
        `/group/${groupId}/template-monsters` :
        `/preset/${presetId}/template-monsters`,
    pattern: "/template-monsters",
    component: PageTemplateMonsters,
    navbar: true,
    icon: "bug",
    title: "Monsters",
    masterOnly: true,
    parent: [Parent.PRESET, Parent.GROUP],
};

addRoute(routeTemplateMonsters);
