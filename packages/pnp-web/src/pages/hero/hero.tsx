import * as React from "react";
import { addRoute, RouteProps, Parent, HeroPane } from "../../routing";
import { History } from "history";
import { inject, external } from "tsdi";
import { StoreHeroes, StoreLogin, StoreUi } from "../../stores";
import { computed, action } from "mobx";
import * as classNames from "classnames/bind";
import { observer } from "mobx-react";
import {
    PaneSkills,
    PaneMaster,
    PaneBattle,
    PaneGeneral,
    PaneInventory,
    PaneNotes,
    HealthBarForm,
} from "../../components";
import { Tab, Segment } from "semantic-ui-react";
import * as css from "./hero.scss";
import { PageName } from "pnp-common";

export type PageHeroProps = RouteProps<{
    groupId: string,
    presetId: string,
    id: string,
    pane: HeroPane,
}>;

const cx = classNames.bind(css);

@external @observer
export class PageHero extends React.Component<PageHeroProps> {
    @inject private heroes: StoreHeroes;
    @inject private login: StoreLogin;
    @inject private ui: StoreUi;
    @inject private history: History;

    @computed private get hero() {
        return this.heroes.byId(this.props.match.params.id);
    }

    @computed private get panes() {
        const nameDisplayed = this.ui.alwaysOpen;
        const panes: { menuItem: { key: HeroPane; icon: string; name: string }; render: () => JSX.Element }[] = [
            {
                menuItem: {
                    key: "general",
                    icon: "info circle",
                    name: nameDisplayed ? "General" : "",
                },
                render: () => <div className={css.content}><PaneGeneral id={this.hero.id} /></div>,
            },
            {
                menuItem: {
                    key: "notes",
                    icon: "clipboard",
                    name: nameDisplayed ? "Notes" : "",
                },
                render: () => <div className={css.content}><PaneNotes id={this.hero.id} /></div>,
            },
        ];
        if (!this.hero.group || this.login.isMaster(this.hero.group.id) || !this.hero.isNpc) {
            panes.push({
                menuItem: {
                    key: "skills",
                    icon: "gem",
                    name: nameDisplayed ? "Skills" : "",
                },
                render: () => <div className={css.content}><PaneSkills id={this.hero.id} /></div>,
            });
            panes.push({
                menuItem: {
                    key: "battle",
                    icon: "bomb",
                    name: nameDisplayed ? "Battle" : "",
                },
                render: () => <div className={css.content}><PaneBattle id={this.hero.id} /></div>,
            });
            panes.push({
                menuItem: {
                    key: "inventory",
                    icon: "box",
                    name: nameDisplayed ? "Inventory" : "",
                },
                render: () => <div className={css.content}><PaneInventory id={this.hero.id} /></div>,
            });
        }
        if (!this.hero.group || this.login.isMaster(this.hero.group.id)) {
            panes.push({
                menuItem: {
                    key: "master",
                    icon: "magic",
                    name: nameDisplayed ? "Master" : "",
                },
                render: () => <div className={css.content}><PaneMaster id={this.hero.id} /> </div>,
            });
        }
        return panes;
    }

    @action.bound private handlePaneChange(_, { activeIndex }: { activeIndex: number }) {
        const { groupId, presetId } = this.props.match.params;
        const paneName = this.panes[activeIndex].menuItem.key;
        this.history.push(routeHero.path(groupId, presetId, this.hero.id, paneName));
    }

    @computed private get activeIndex() {
        const selectedIndex = this.panes.findIndex(pane => pane.menuItem.key === this.props.match.params.pane);
        if (selectedIndex === -1) {
            return 0;
        }
        return selectedIndex;
    }

    public render() {
        if (!this.hero) { return null; }
        const noSidebar = !this.ui.visible;
        return (
            <>
                <Segment vertical className={cx({ healthBar: true, noSidebar })}>
                    <HealthBarForm heroId={this.hero.id} />
                </Segment>
                <Segment vertical className={css.wrapper}>
                    <Tab
                        onTabChange={this.handlePaneChange}
                        menu={{
                            className: cx({ tabMenu: true, noSidebar }),
                            secondary: true,
                            pointing: true,
                        }}
                        panes={this.panes}
                        activeIndex={this.activeIndex}
                    />
                </Segment>
            </>
        );
    }
}

export const routeHero = {
    path: (groupId: string | undefined, presetId: string | undefined, id: string, pane = "general") => groupId ?
        `/group/${groupId}/hero/${id}/${pane}` :
        `/preset/${presetId}/hero/${id}/${pane}`,
    pattern: "/hero/:id/:pane",
    component: PageHero,
    title: "Hero",
    parent: [Parent.GROUP, Parent.PRESET],
    livePage: PageName.HERO,
};

addRoute(routeHero);
