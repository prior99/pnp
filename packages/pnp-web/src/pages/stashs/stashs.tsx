import * as React from "react";
import { addRoute, Parent, RouteProps } from "../../routing";
import { observer } from "mobx-react";
import { external, inject } from "tsdi";
import { Content, Mosaic, CardStash } from "../../components";
import { StoreStashs } from "../../stores";
import { Header, Button } from "semantic-ui-react";
import { computed, action } from "mobx";
import { parentIds } from "../../parent-id";

@observer @external
export class PageStashs extends React.Component<RouteProps<{ groupId?: string, presetId?: string }>> {
    @inject private stashs: StoreStashs;

    @action.bound private async handleCreate() {
        await this.stashs.create({
            ...parentIds(this.props.match.params),
            name: "Unnamed stash",
            money: 0,
        });
    }

    @computed private get allStashs() {
        const { groupId, presetId } = this.props.match.params;

        return this.stashs
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(stash => {
                if (stash.group && groupId !== stash.group.id) { return false; }
                if (stash.preset && presetId !== stash.preset.id) { return false; }
                return true;
            });
    }

    public render() {
        return (
            <Content>
                <Header>Stashs</Header>
                <Mosaic size="twice">
                    {this.allStashs.map(item => <CardStash key={item.id} id={item.id} />)}
                </Mosaic>
                <Button onClick={this.handleCreate} content="Create new" icon="add" />
            </Content>
        );
    }
}

export const routeStashs = {
    path: (groupId: string | undefined, presetId: string | undefined) => groupId ?
        `/group/${groupId}/stashs` :
        `/preset/${presetId}/stashs`,
    pattern: "/stashs",
    component: PageStashs,
    navbar: true,
    icon: "suitcase",
    title: "Stashs",
    parent: [Parent.GROUP, Parent.PRESET],
    masterOnly: true,
};

addRoute(routeStashs);
