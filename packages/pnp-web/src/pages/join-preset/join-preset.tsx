import * as React from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import { History } from "history";
import { field, Field, hasFields } from "hyrest-mobx";
import { Input, Button, Form } from "semantic-ui-react";
import { Group, Preset } from "pnp-common";
import { external, inject } from "tsdi";
import { addRoute, RouteProps } from "../../routing";
import { StoreLogin } from "../../stores";
import { formItem } from "../../hyrest-semantic-ui";
import { Content } from "../../components";
import { routePresetDashboard } from "../preset-dashboard";

@observer @external @hasFields()
export class PageJoinPreset extends React.Component<RouteProps<{ name: string }>> {
    @inject private history: History;
    @inject private login: StoreLogin;

    @observable private master = false;
    @field(Group) private preset: Field<Preset>;

    public componentDidMount() {
        this.preset.nested.name.update(this.props.match.params.name);
    }

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        const presetId = await this.login.loginPreset(this.preset.value, this.master);
        if (!presetId) { return; }
        this.history.push(routePresetDashboard.path(undefined, presetId));
    }

    @computed private get valid() {
        return this.preset.valid;
    }

    public render () {
        const { name, password } = this.preset.nested;
        return (
            <Content title="Join Preset">
                <Form size="large" onSubmit={this.handleSubmit}>
                    <Form.Field {...formItem(name)}>
                        <label>Preset name</label>
                        <Input
                            icon="th"
                            placeholder="Preset name"
                            {...name.reactInput}
                        />
                    </Form.Field>
                    <Form.Field {...formItem(password)}>
                        <label>Password</label>
                        <Input
                            icon="lock"
                            placeholder="Password"
                            type="password"
                            {...password.reactInput}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Button disabled={!this.valid} type="submit">Join</Button>
                    </Form.Field>
                </Form>
            </Content>
        );
    }
}

export const routeJoinPreset = {
    path: (name: string) => `/join-preset/${name}`,
    pattern: "/join-preset/:name",
    component: PageJoinPreset,
    parent: [],
    global: true,
};

addRoute(routeJoinPreset);
