import * as React from "react";
import { addRoute, Parent, RouteProps } from "../../routing";
import { observer } from "mobx-react";
import { computed, action } from "mobx";
import { external, inject } from "tsdi";
import DraggableList from "react-draggable-list";
import { Content } from "../../components";
import { StoreJournalEntries, StoreLogin } from "../../stores";
import { Button, Header } from "semantic-ui-react";
import { FeedJournalEntry } from "./feed-journal-entry";
import { CreateJournalModal } from "../../components/forms";
import { JournalEntry } from "pnp-common";

@observer @external
export class PageJournal extends React.Component<RouteProps<{ groupId?: string, presetId?: string }>> {
    @inject private journalEntries: StoreJournalEntries;
    @inject private login: StoreLogin;

    @computed private get allEntries() {
        const { groupId, presetId } = this.props.match.params;

        return this.journalEntries
            .sorted((a, b) => b.sortKey - a.sortKey)
            .filter(journalEntry => {
                if (journalEntry.group && groupId !== journalEntry.group.id) { return false; }
                if (journalEntry.preset && presetId !== journalEntry.preset.id) {
                    return false;
                }
                return true;
            })
            .filter(journalEntry => this.isMaster || !journalEntry.masterOnly);
    }

    @computed private get isMaster() {
        if (this.props.match.params.presetId) { return true; }
        return this.login.isMaster(this.props.match.params.groupId);
    }

    @action.bound private handleCreateJournal() {
        this.journalEntries.modalVisible = true;
    }

    @action.bound private async handleListChange(
        newList: JournalEntry[],
        movedItem: JournalEntry,
        oldIndex: number,
        newIndex: number,
    ) {
        const newSortKey = this.allEntries[newIndex].sortKey;
        const oldSortKey = movedItem.sortKey;
        this.allEntries.forEach(entry => {
            if (entry.sortKey > oldSortKey) { entry.sortKey--; }
        });
        this.allEntries.forEach(entry => {
            if (entry.sortKey >= newSortKey) { entry.sortKey++; }
        });
        movedItem.sortKey = newSortKey;
        await this.journalEntries.update(movedItem.id, {
            sortKey: newSortKey,
        });
    }

    public render() {
        return (
            <Content>
                <Header>Journal</Header>
                <CreateJournalModal {...this.props.match.params} />
                <Button
                    content="Create entry"
                    icon="plus"
                    primary
                    onClick={this.handleCreateJournal}
                    style={{ marginBottom: 20 }}
                />
                <DraggableList
                    list={this.allEntries}
                    itemKey="id"
                    template={FeedJournalEntry}
                    onMoveEnd={this.handleListChange}
                />
            </Content>
        );
    }
}

export const routeJournal = {
    path: (groupId: string | undefined, presetId: string | undefined) => groupId ?
        `/group/${groupId}/journal` :
        `/preset/${presetId}/journal`,
    pattern: "/journal",
    component: PageJournal,
    navbar: true,
    icon: "newspaper outline",
    title: "Journal",
    parent: [Parent.GROUP, Parent.PRESET],
};

addRoute(routeJournal);
