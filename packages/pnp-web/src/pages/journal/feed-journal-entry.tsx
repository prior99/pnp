import * as React from "react";
import { observer } from "mobx-react";
import { History } from "history";
import { computed, autorun, action } from "mobx";
import { external, inject, initialize } from "tsdi";
import { StoreJournalEntries, StoreHeroes, StoreLogin } from "../../stores";
import { Feed, Icon, Image } from "semantic-ui-react";
import { EditableText, fromEditableTextField } from "../../components";
import { hasFields, field, Field } from "hyrest-mobx";
import { JournalEntry } from "pnp-common";
import * as classNames from "classnames/bind";
import { SemanticICONS } from "semantic-ui-react";
import * as css from "./feed-journal-entry.scss";
import { routeHero } from "../hero";
import { extractParentIds } from "../../parent-id";

const cx = classNames.bind(css);

export interface FeedJournalEntryProps {
    item: JournalEntry;
    itemSelected: number;
    dragHandleProps: Object;
}

@observer @external @hasFields()
export class FeedJournalEntry extends React.Component<FeedJournalEntryProps> {
    @inject private journalEntries: StoreJournalEntries;
    @inject private heroes: StoreHeroes;
    @inject private history: History;
    @inject private login: StoreLogin;

    @field(JournalEntry) private journalEntryField: Field<JournalEntry>;

    @initialize protected initialize() {
        autorun(() => this.journalEntry && this.journalEntryField.update(this.journalEntry));
    }

    @computed private get journalEntry() {
        return this.journalEntries.byId(this.props.item.id);
    }

    @computed private get hero() {
        if (!this.journalEntry.hero) { return; }
        return this.heroes.byId(this.journalEntry.hero.id);
    }

    @computed private get icon() {
        if (this.hero) { return; }
        return this.journalEntry.icon || "info";
    }

    @computed private get image() {
        if (!this.hero) { return; }
        return (
            <Icon.Group>
                <Image
                    inline
                    circular
                    size="mini"
                    src={this.hero.avatarUrl}
                />
                <Icon className={css.icon} corner name={this.journalEntry.icon as SemanticICONS} />
            </Icon.Group>
        );
    }

    @action.bound private async handleSummaryChange(summary: string) {
        await this.journalEntries.update(this.journalEntry.id, {
            summary: this.journalEntryField.nested.summary.value || "",
        });
    }

    @action.bound private async handleDescriptionChange(description: string) {
        await this.journalEntries.update(this.journalEntry.id, {
            description: this.journalEntryField.nested.description.value || "",
        });
    }

    @action.bound private handleHeroClick() {
        this.history.push(routeHero.path(...[
            ...extractParentIds(this.hero),
            this.hero.id,
        ] as Parameters<typeof routeHero["path"]>));
    }

    @action.bound private async handleDelete() {
        await this.journalEntries.delete(this.journalEntry.id);
    }

    @action.bound private handleAddBelow() {
        this.journalEntries.modalVisible = true;
        this.journalEntries.previousJournalSortKey = this.journalEntry.sortKey;
    }

    @action.bound private async handleMasterOnlyChange() {
        await this.journalEntries.update(this.journalEntry.id, {
            masterOnly: !this.journalEntry.masterOnly,
        });
    }

    @action.bound private async handleSecretChange() {
        await this.journalEntries.update(this.journalEntry.id, {
            secret: !this.journalEntry.secret,
        });
    }

    @computed private get isMaster() {
        if (this.journalEntry.preset) { return true; }
        return this.login.isMaster(this.journalEntry.group.id);
    }

    @computed private get blurred() {
        if (this.isMaster) { return false; }
        return this.journalEntry.secret;
    }

    public renderText() {
        const { blurred } = this;
        const { description } = this.journalEntryField.nested;
        return (
            <EditableText
                formatted
                area
                {...fromEditableTextField(description)}
                onFinish={this.handleDescriptionChange}
                disabled={blurred}
            />
        );
    }

    public render() {
        const { blurred } = this;
        const { summary } = this.journalEntryField.nested;
        return (
            <Feed className={css.feed}>
                <Feed.Event className={cx({ blurred })}>
                    <Feed.Label icon={this.icon} image={this.image} />
                    <Feed.Content>
                        <Feed.Date content={this.journalEntry.meta} />
                        <Feed.Summary>
                            {
                                this.hero && <Feed.User
                                    style={{ marginRight: "0.5em" }}
                                    onClick={this.handleHeroClick}
                                    content={this.hero.name}
                                />
                            }
                            <EditableText
                                inline
                                {...fromEditableTextField(summary)}
                                onFinish={this.handleSummaryChange}
                                disabled={blurred}
                            />
                        </Feed.Summary>
                        {this.renderText()}
                        {
                            !blurred && <Feed.Meta>
                                <Feed.Like
                                    content="Delete"
                                    icon="trash"
                                    onClick={this.handleDelete}
                                />
                                {
                                    this.isMaster && <>
                                        <Feed.Like
                                            icon={this.journalEntry.masterOnly ? "eye slash" : "eye"}
                                            onClick={this.handleMasterOnlyChange}
                                            content={
                                                this.journalEntry.masterOnly
                                                    ? "Only visible for Master"
                                                    : "Public"
                                            }
                                        />
                                        <Feed.Like
                                            icon={this.journalEntry.secret ? "lock" : "lock open"}
                                            onClick={this.handleSecretChange}
                                            content={this.journalEntry.secret ? "Secret" : "Public"}
                                        />
                                        <Feed.Like
                                            icon={
                                                <Icon.Group>
                                                    <Icon name="add" />
                                                    <Icon name="triangle down" corner />
                                                </Icon.Group>
                                            }
                                            onClick={this.handleAddBelow}
                                            content="Add below"
                                        />
                                    </>
                                }
                                <Icon
                                    name="ellipsis vertical"
                                    color="grey"
                                    className={css.dragHandle}
                                    {...this.props.dragHandleProps}
                                />
                            </Feed.Meta>
                        }
                    </Feed.Content>
                </Feed.Event>
            </Feed>
        );
    }
}
