import * as React from "react";
import { addRoute, Parent, RouteProps } from "../../routing";
import { Grid, Header, Form, Input } from "semantic-ui-react";
import { observer } from "mobx-react";
import { computed, action, observable } from "mobx";
import { external, inject } from "tsdi";
import { CardCreateTemplateItem, CardTemplateItem, Content, Mosaic } from "../../components";
import { StoreTemplateItems } from "../../stores";
import * as css from "./template-items.scss";

@observer @external
export class PageTemplateItems extends React.Component<RouteProps<{ groupId?: string, presetId?: string }>> {
    @inject private templateItems: StoreTemplateItems;

    @observable private query = "";

    @computed private get allItems() {
        const { groupId, presetId } = this.props.match.params;

        return this.templateItems
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(templateItem => {
                if (templateItem.group && groupId !== templateItem.group.id) { return false; }
                if (templateItem.preset && presetId !== templateItem.preset.id) {
                    return false;
                }
                return templateItem.name.toLowerCase().includes(this.query.toLowerCase()) ||
                    templateItem.description.toLowerCase().includes(this.query.toLowerCase());
            });
    }

    @action.bound private onQueryChange(evt: React.SyntheticEvent<HTMLInputElement>) {
        this.query = evt.currentTarget.value;
    }

    public render() {
        return (
            <Content>
                <Grid>
                    <Grid.Column mobile={16} computer={3} className={css.leftColumn}>
                        <Header>Search</Header>
                        <Form>
                            <Form.Field>
                                <label>Query</label>
                                <Input
                                    placeholder="Query"
                                    onChange={this.onQueryChange}
                                    value={this.query}
                                />
                            </Form.Field>
                        </Form>
                        <CardCreateTemplateItem {...this.props.match.params} />
                    </Grid.Column>
                    <Grid.Column mobile={16} computer={12}>
                        <Header>Items</Header>
                        <Mosaic>
                            {this.allItems.map(item => <CardTemplateItem key={item.id} id={item.id} />)}
                        </Mosaic>
                    </Grid.Column>
                </Grid>
            </Content>
        );
    }
}

export const routeTemplateItems = {
    path: (groupId: string | undefined, presetId: string | undefined) => groupId ?
        `/group/${groupId}/template-items` :
        `/preset/${presetId}/template-items`,
    pattern: "/template-items",
    component: PageTemplateItems,
    navbar: true,
    icon: "boxes",
    title: "Items",
    masterOnly: true,
    parent: [Parent.GROUP, Parent.PRESET],
};

addRoute(routeTemplateItems);
