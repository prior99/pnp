import * as React from "react";
import { Content, CardGroups, CardPresets } from "../../components";
import { addRoute } from "../../routing";
import { Grid } from "semantic-ui-react";

export class PageLanding extends React.Component {
    public render() {
        return (
            <Content>
                <Grid>
                    <Grid.Column computer={8} mobile={16}>
                        <CardGroups />
                    </Grid.Column>
                    <Grid.Column computer={8} mobile={16}>
                        <CardPresets />
                    </Grid.Column>
                </Grid>
            </Content>
        );
    }
}

export const routeLanding = {
    path: () => "/landing",
    pattern: "/landing",
    component: PageLanding,
    parent: [],
    global: true,
};

addRoute(routeLanding);
