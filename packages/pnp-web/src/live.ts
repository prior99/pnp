import { component, initialize, inject } from "tsdi";
import { ReadingStore } from "hyrest-mobx";
import { History } from "history";
import {
    LiveMessage,
    LiveAction,
    ModelName,
    LiveMessageType,
    LivePublishMessage,
    LivePointerMessage,
    LiveClaimMessage,
    LiveStateMessage,
    LiveMessageGroup,
    LiveUnclaimMessage,
    LiveConnectedMessage,
    LiveDisconnectedMessage,
    LiveNavigationTarget,
    LiveNavigationMessage,
    PageName,
} from "pnp-common";
import { StoreLogin } from "./stores";
import { action, observable, computed } from "mobx";
import { bind } from "lodash-decorators";
import { pick, complement, reverse, equals, uniq } from "ramda";
import * as pathToRegexp from "path-to-regexp";
import { routes } from "./routing";

declare const baseUrl: string;

export type SubscriptionHandler = (action: LiveAction, id: string) => void;

export interface Subscription {
    model: ModelName;
    handler: SubscriptionHandler;
}

export interface Pointer {
    pos: [number, number];
    battleMapId: string;
    color: string;
}

class LiveGroup {
    @observable public claimedHeroes: Map<string, string>;
    @observable public colors: Map<string, string>;
    @observable public groupId: string;
    @observable public navigationTargets: Map<string, LiveNavigationTarget>;

    constructor(group: LiveMessageGroup) {
        this.claimedHeroes = new Map(group.claimedHeroes);
        this.colors = new Map(group.colors);
        this.navigationTargets = new Map(group.navigationTargets);
        this.groupId = group.groupId;
    }

    @computed public get reverseClaimedHeroes(): Map<string, string> {
        return new Map(Array.from(this.claimedHeroes.entries()).map(entry => reverse(entry)) as [string, string][]);
    }

    public getClientsForNavigationTarget(target: LiveNavigationTarget) {
        return Array.from(this.navigationTargets.entries())
            .filter(([_, currentTarget]) => target.entityId === currentTarget.entityId)
            .map(([clientId, _]) => clientId);
    }

    public getColorsForNavigationTarget(target: LiveNavigationTarget) {
        return this.getClientsForNavigationTarget(target).map(clientId => this.colors.get(clientId));
    }
}

function extractEntityId(page: PageName, params: Map<string, string>) {
    switch (page) {
        case PageName.HERO:
        case PageName.BATTLE_MAP:
            return params.get("id");
        default:
            return;
    }
}

function extractGroupId(page: PageName, params: Map<string, string>) {
    switch (page) {
        case PageName.HERO:
        case PageName.BATTLE_MAP:
        default:
            return params.get("groupId");
    }
}

function createParams(match: RegExpExecArray, keys: pathToRegexp.Key[]): Map<string, string> {
    const result = new Map<string, string>();
    for (let i = 0; i < Math.min(keys.length, match.length - 1); ++i) {
        result.set(String(keys[i].name), match[i + 1]);
    }
    return result;
}

function getMatchingRoute(pathname: string) {
    const matchedRoute = routes
        .map(route => {
            const keys = [];
            const regexp = pathToRegexp(`/group/:groupId${route.pattern}`, keys);
            const match = regexp.exec(pathname);
            if (!match) { return; }
            return {
                match: createParams(match, keys),
                page: route.livePage || PageName.OTHER,
            };
        })
        .find(result => Boolean(result && result.match));
    if (matchedRoute) { return matchedRoute; }
    const groupKeys = [];
    const groupMatch = pathToRegexp("/group/:groupId/(.*)", groupKeys).exec(pathname);
    if (!groupMatch) { return; }
    return {
        match: createParams(groupMatch, groupKeys),
        page: PageName.OTHER,
    };
}

@component
export class Live {
    private ws: WebSocket;
    private subscriptions: Subscription[] = [];
    @observable public pointers: Pointer[] = [];
    @observable public groups = new Map<string, LiveGroup>();
    @observable public ownId: string;

    @inject private login: StoreLogin;
    @inject private history: History;

    @initialize
    protected initialize() {
        this.ws = new WebSocket(`${baseUrl.replace("http", "ws")}/live`);
        this.ws.addEventListener("message", this.handleMessage);
        this.ws.addEventListener("open", this.handleOpen);
        this.history.listen(location => this.handleLocationChange(location.pathname));
    }

    @bind private handleLocationChange(pathname: string) {
        const matchingRoute = getMatchingRoute(pathname);
        if (!matchingRoute) { return; }
        const { match, page } = matchingRoute;
        const entityId = extractEntityId(page, match);
        const groupId = extractGroupId(page, match);
        this.navigate(groupId, { page, entityId });
    }

    @bind private handleOpen() {
        this.send({ message: LiveMessageType.SUBSCRIBE, groupIds: this.login.groupIds });
        this.handleLocationChange(location.hash.substr(1));
    }

    @bind private handlePublishMessage(message: LivePublishMessage) {
        if (message.action === LiveAction.RELOAD_PAGE) {
            location.reload();
            return;
        }
        this.subscriptions
            .filter(subscription => subscription.model === message.model)
            .forEach(subscription => subscription.handler(message.action, message.id));
    }

    @bind private handlePointerMessage(message: LivePointerMessage) {
        const pointer = pick(["pos", "color", "battleMapId"], message) as Pointer;
        this.pointers.push(pointer);
        // Remove the point again after 10 seconds.
        setTimeout(() => this.pointers = this.pointers.filter(complement(equals(pointer))), 10000);
    }

    @bind private handleClaimMessage(message: LiveClaimMessage) {
        const group = this.groups.get(message.groupId);
        if (!group) { return; }
        group.claimedHeroes.set(message.clientId, message.heroId);
    }

    @bind private handleUnclaimMessage(message: LiveUnclaimMessage) {
        const group = this.groups.get(message.groupId);
        if (!group) { return; }
        group.claimedHeroes.delete(message.clientId);
    }

    @bind private handleStateMessage(message: LiveStateMessage) {
        this.ownId = message.ownId;
        message.groups.forEach(group => this.groups.set(group.groupId, new LiveGroup(group)));
    }

    @bind private handleConnectedMessage(message: LiveConnectedMessage) {
        message.colors.forEach(({ groupId, color }) => {
            const group = this.groups.get(groupId);
            if (!group) { return; }
            group.colors.set(message.clientId, color);
        });
    }

    @bind private handleNavigationMessage(message: LiveNavigationMessage) {
        this.groups.get(message.groupId).navigationTargets.set(message.clientId, message.target);
    }

    @bind private handleDisconnectedMessage(message: LiveDisconnectedMessage) {
        this.groups.forEach(group => group.colors.delete(message.clientId));
        this.groups.forEach(group => group.navigationTargets.delete(message.clientId));
    }

    @action.bound private handleMessage({ data }: MessageEvent) {
        const message: LiveMessage = JSON.parse(data);
        switch (message.message) {
            case LiveMessageType.PUBLISH: return this.handlePublishMessage(message);
            case LiveMessageType.POINTER: return this.handlePointerMessage(message);
            case LiveMessageType.CLAIM: return this.handleClaimMessage(message);
            case LiveMessageType.UNCLAIM: return this.handleUnclaimMessage(message);
            case LiveMessageType.STATE: return this.handleStateMessage(message);
            case LiveMessageType.CONNECTED: return this.handleConnectedMessage(message);
            case LiveMessageType.DISCONNECTED: return this.handleDisconnectedMessage(message);
            case LiveMessageType.NAVIGATE: return this.handleNavigationMessage(message);
        }
    }

    @bind private send(message: LiveMessage) {
        this.ws.send(JSON.stringify(message));
    }

    @bind public subscribe(model: ModelName, handler: SubscriptionHandler) {
        this.subscriptions.push({ model, handler });
    }

    @bind public point(groupId: string, battleMapId: string, pos: [number, number]) {
        this.send({ message: LiveMessageType.POINTER, groupId, battleMapId, pos });
    }

    @action.bound public claimHero(heroId: string, groupId: string) {
        this.send({ message: LiveMessageType.CLAIM, heroId, groupId });
    }

    @action.bound public unclaimHero(heroId: string, groupId: string) {
        this.send({ message: LiveMessageType.UNCLAIM, heroId, groupId });
    }

    @bind public isHeroClaimed(groupId: string, heroId: string) {
        const group = this.groups.get(groupId);
        if (!group) { return; }
        return group.reverseClaimedHeroes.has(heroId);
    }

    @bind public isHeroClaimedByMe(groupId: string, heroId: string) {
        const group = this.groups.get(groupId);
        if (!group) { return; }
        return group.reverseClaimedHeroes.get(heroId) === this.ownId;
    }

    @bind public claimedHeroes(groupId: string) {
        const group = this.groups.get(groupId);
        if (!group) { return []; }
        return uniq(Array.from(group.claimedHeroes.values()));
    }

    @bind public getColor(groupId: string, heroId: string) {
        const group = this.groups.get(groupId);
        if (!group) { return; }
        const clientId = group.reverseClaimedHeroes.get(heroId);
        return group.colors.get(clientId);
    }

    public myClaimedHeroId(groupId: string) {
        const group = this.groups.get(groupId);
        if (!group) { return; }
        return group.claimedHeroes.get(this.ownId);
    }

    public getOwnColor(groupId: string) {
        return this.groups.get(groupId).colors.get(this.ownId);
    }

    public getColorForNavigationTarget(groupId: string, target: LiveNavigationTarget) {
        return this.groups.get(groupId).getColorsForNavigationTarget(target);
    }

    public navigate(groupId: string, target: LiveNavigationTarget) {
        this.send({
            message: LiveMessageType.NAVIGATE,
            groupId,
            target,
        });
    }
}

export function handleLive(store: ReadingStore<any, any, any>) {
    return action(async (liveAction: LiveAction, id: string) => {
        switch (liveAction) {
            case LiveAction.DELETE:
                // `entities` is protected on the store but it should be there.
                (store as any).entities.delete(id);
                return;
            case LiveAction.CREATE:
            case LiveAction.UPDATE:
                store.get(id as any);
                return;
            default:
                return;
        }
    });
}
