import * as React from "react";
import * as ReactDOM from "react-dom";
import { controllers } from "pnp-common";
import { TSDI } from "tsdi";
import "semantic-ui-css-offline/semantic.min.css";
import { configureController } from "hyrest";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { StoreLogin, StoreErrors, StorePresets, StoreGroups } from "./stores";
import "./factories";
import "./global.scss";
import "leaflet/dist/leaflet.css";
import * as Leaflet from "leaflet";
import * as iconRetinaUrl from "leaflet/dist/images/marker-icon-2x.png";
import * as iconUrl from "leaflet/dist/images/marker-icon.png";
import * as shadowUrl from "leaflet/dist/images/marker-shadow.png";
import { PresetContainer, GroupContainer, Errors } from "./components";
import { routes } from "./routing";
import { routeLanding } from "./pages";

Leaflet.Icon.Default.mergeOptions({ iconRetinaUrl, iconUrl, shadowUrl });

// Setup controllers.
declare const baseUrl: string;
configureController(controllers, {
    baseUrl,
    errorHandler: err => {
        console.error(err);
        tsdi.get(StoreErrors).report({
            message: err.answer ? err.answer.message : err.message ? err.message : "Unknown error.",
            fatal: err.statusCode === 401,
        });
        if (err.statusCode === 401) { tsdi.get(StoreLogin).logout(); }
    },
    authorizationProvider: (headers: Headers) => {
        const { base64AuthToken } = tsdi.get(StoreLogin);
        headers.append("authorization", `Bearer ${base64AuthToken}`);
    },
    throwOnError: true,
});

// Setup dependency injection.
const tsdi = new TSDI();
tsdi.enableComponentScanner();

// Load initial set of presets and groups.
tsdi.get(StorePresets).list();
tsdi.get(StoreGroups).list();

// Start React.
ReactDOM.render(
    <>
        <Errors />
        <Router history={tsdi.get("history")}>
            <Switch>
                <Redirect exact from="/" to={routeLanding.path()} />
                <Route path={PresetContainer.path} component={PresetContainer} />
                <Route path={GroupContainer.path} component={GroupContainer} />
                {
                    routes.filter(route => route.global).map((route, index) => <Route
                        path={route.pattern}
                        component={route.component}
                        key={index}
                    />)
                }
            </Switch>
        </Router>
    </>,
    document.getElementById("root"),
);
