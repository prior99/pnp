import { Store } from "hyrest-mobx";
import { ControllerJournalEntries, ModelName, LiveAction } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action, observable } from "mobx";
import { Live } from "../live";

@component
export class StoreJournalEntries extends Store(ControllerJournalEntries) {
    @inject private live: Live;
    @inject protected controller: ControllerJournalEntries;

    @observable public modalVisible = false;
    @observable public previousJournalSortKey: undefined | number;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.JOURNAL_ENTRY, this.handleLive);
    }

    @action.bound private async handleLive(liveAction: LiveAction, id: string) {
        const changed = liveAction === LiveAction.DELETE ? this.byId(id) : await this.get(id);
        if (changed.preset) { await this.loadForPreset(changed.preset.id); }
        if (changed.group) { await this.loadForGroup(changed.preset.id); }
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    public async forGroup(groupId: string) {
        return this.all.filter(journalEntry => journalEntry.group && journalEntry.group.id === groupId);
    }

    public async forPreset(presetId: string) {
        return this.all.filter(journalEntry => journalEntry.preset && journalEntry.preset.id === presetId);
    }
}
