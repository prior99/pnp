import { Store } from "hyrest-mobx";
import { ControllerNotes, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action, observable } from "mobx";
import { Live, handleLive } from "../live";
import { bind } from "lodash-decorators";

const softwareVersion = 1;
const localStorageIdentifier = "pnp-notes";

interface LocalStorageApi {
    readonly storageVersion: number;
    readonly unobfuscatedNotes: string[];
}

@component
export class StoreNotes extends Store(ControllerNotes) {
    @inject private live: Live;
    @inject protected controller: ControllerNotes;

    @observable private unobfuscatedNotes: string[] = [];

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.NOTE, handleLive(this));
        this.load();
    }

    private clearStorage() {
        localStorage.removeItem(localStorageIdentifier);
    }

    private load() {
        const serialized = localStorage.getItem(localStorageIdentifier);
        if (serialized === null) { return; }
        let deserialized: LocalStorageApi;
        try {
            deserialized = JSON.parse(serialized);
            if (deserialized.storageVersion !== softwareVersion) {
                this.clearStorage();
                return;
            }
            this.unobfuscatedNotes = deserialized.unobfuscatedNotes;
        } catch (err) {
            console.error("Error deserializing local storage", err);
            this.clearStorage();
            return;
        }
    }

    private save() {
        const deserialized: LocalStorageApi = {
            storageVersion: softwareVersion,
            unobfuscatedNotes: this.unobfuscatedNotes,
        };
        const serialized = JSON.stringify(deserialized);
        localStorage.setItem(localStorageIdentifier, serialized);
    }

    @action.bound public async loadForHero(heroId: string) {
        this.search(heroId);
    }

    public forHero(heroId: string) {
        return this.all.filter(note => note.hero.id === heroId);
    }

    @bind public isObfuscated(id: string) {
        return this.byId(id).secret && !this.unobfuscatedNotes.includes(id);
    }

    @action.bound public unobfuscate(id: string) {
        if (this.unobfuscatedNotes.includes(id)) { return; }
        this.unobfuscatedNotes.push(id);
    }

    @action.bound public toggleObfuscated(id: string) {
        if (this.isObfuscated(id)) {
            this.unobfuscate(id);
        } else {
            this.unobfuscatedNotes = this.unobfuscatedNotes.filter(tagId => tagId !== id);
        }
        this.save();
    }
}
