import { Store } from "hyrest-mobx";
import { ControllerHeroTags, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreHeroTags extends Store(ControllerHeroTags) {
    @inject private live: Live;
    @inject protected controller: ControllerHeroTags;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.HERO_TAG, handleLive(this));
    }

    @action.bound public async loadForHero(heroId: string) {
        return await this.search(heroId);
    }

    public forHero(heroId: string) {
        return this.all.filter(heroTag => heroTag.hero.id === heroId);
    }
}
