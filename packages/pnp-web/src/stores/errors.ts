import { component } from "tsdi";
import { action, computed, observable } from "mobx";

interface ApiError {
    message: string;
    fatal?: boolean;
}

@component
export class StoreErrors {
    @observable public errors: ApiError[] = [];

    @action.bound public report(error: ApiError) {
        if (this.errors.find(other => other.message === error.message)) {
            return;
        }
        this.errors.push(error);
    }

    @action.bound public dismiss() {
        this.errors.pop();
    }

    @computed public get latestError() {
        return this.errors[this.errors.length - 1];
    }
}
