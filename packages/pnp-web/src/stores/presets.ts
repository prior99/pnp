import { Store } from "hyrest-mobx";
import { ControllerPresets, Preset } from "pnp-common";
import { component, inject } from "tsdi";
import { action } from "mobx";
import { StoreLogin } from "./login";

@component
export class StorePresets extends Store(ControllerPresets) {
    @inject protected controller: ControllerPresets;
    @inject protected login: StoreLogin;

    @action.bound public async create(preset: Preset): Promise<Preset> {
        try {
            const newPreset = await super.create(preset);
            await this.login.loginPreset({ ...preset, ...newPreset }, true);
            return newPreset;
        } catch (error) {
            console.error(error);
            return;
        }
    }

    public get dropDownOptions() {
        return this
            .sorted((a, b) => a.name.localeCompare(b.name))
            .map(preset => ({
                key: preset.id,
                value: preset.id,
                text: preset.name,
            }));
    }
}
