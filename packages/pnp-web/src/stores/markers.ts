import { Store } from "hyrest-mobx";
import { ControllerMarkers, Hero, BattleMap } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";
import { ModelName } from "pnp-common";
import { StoreMarkerItems } from "./marker-items";

@component
export class StoreMarkers extends Store(ControllerMarkers) {
    @inject protected controller: ControllerMarkers;
    @inject private markerItems: StoreMarkerItems;
    @inject private live: Live;

    @initialize protected initialize() {
        this.live.subscribe(ModelName.MARKER, handleLive(this));
    }

    @action.bound public async loadForBattleMap(mapId: string) {
        return await this.search(mapId);
    }

    @action.bound public async createForHero(mapId: string, heroId: string, position: [number, number] = [0, 0]) {
        return await this.create({
            battleMap: { id: mapId } as BattleMap,
            hero: { id: heroId } as Hero,
            latitude: position[0],
            longitude: position[1],
        });
    }

    @action.bound public async createCustomMarker(
        mapId: string,
        masterOnly: boolean,
        icon = "map marker alternate",
        position: { lat: number; lng: number } = { lat: 0, lng: 0 },
    ) {
        return await this.create({
            battleMap: { id: mapId } as BattleMap,
            latitude: position.lat,
            longitude: position.lng,
            icon,
            masterOnly,
        });
    }

    @action.bound public async createLootMarkerFromStash(
        mapId: string,
        stashId: string,
        position: { lat: number; lng: number } = { lat: 0, lng: 0 },
    ) {
        const marker = await this.create({
            battleMap: { id: mapId } as BattleMap,
            latitude: position.lat,
            longitude: position.lng,
        });
        await this.markerItems.fromStash(stashId, marker.id);
        await this.markerItems.loadForMarker(marker.id);
        return marker;
    }

    @action.bound public async createLootMarkerFromHero(
        mapId: string,
        heroId: string,
        position: { lat: number; lng: number } = { lat: 0, lng: 0 },
    ) {
        const marker = await this.create({
            battleMap: { id: mapId } as BattleMap,
            latitude: position.lat,
            longitude: position.lng,
        });
        await this.markerItems.fromHero(heroId, marker.id);
        await this.markerItems.loadForMarker(marker.id);
        return marker;
    }

    @action.bound public async move(id: string, position: [number, number] = [0, 0]) {
        return await this.update(id, {
            latitude: position[0],
            longitude: position[1],
        });
    }

    public forBattleMap(id: string) {
        return this.all.filter(marker => marker.battleMap.id === id);
    }

    @action.bound public evictForMonster(monsterId: string) {
        this.entities.forEach(marker => {
            if (marker.monster && marker.monster.id === monsterId) {
                this.entities.delete(marker.id);
            }
        });
    }

    @action.bound public evictForBattleMap(battleMapId: string) {
        this.entities.forEach(marker => {
            if (marker.battleMap.id === battleMapId) {
                this.entities.delete(marker.id);
                this.markerItems.evictForMarker(marker.id);
            }
        });
    }
}
