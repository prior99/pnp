import { Store } from "hyrest-mobx";
import { ControllerTemplateItems, ModelName, ImageUpload } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";
import { StoreItems } from "./items";
import { StoreMarkerItems } from "./marker-items";
import { StoreStashItems } from "./stash-items";

@component
export class StoreTemplateItems extends Store(ControllerTemplateItems) {
    @inject private live: Live;
    @inject protected controller: ControllerTemplateItems;
    @inject private items: StoreItems;
    @inject private markerItems: StoreMarkerItems;
    @inject private stashItems: StoreStashItems;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.TEMPLATE_ITEM, handleLive(this));
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    public async forGroup(groupId: string) {
        return this.all.filter(templateItem => templateItem.group && templateItem.group.id === groupId);
    }

    public async forPreset(presetId: string) {
        return this.all.filter(templateItem => templateItem.preset && templateItem.preset.id === presetId);
    }

    public dropDownOptions(groupId?: string, presetId?: string) {
        return this
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(templateItem => !presetId || templateItem.preset && templateItem.preset.id === presetId)
            .filter(templateItem => !groupId || templateItem.group && templateItem.group.id === groupId)
            .map(templateItem => ({
                key: templateItem.id,
                value: templateItem.id,
                text: templateItem.name,
            }));
    }

    public async delete(id: string) {
        this.items.evictForTemplateItem(id);
        this.markerItems.evictForTemplateItem(id);
        this.stashItems.evictForTemplateItem(id);
        return await super.delete(id);
    }

    public async uploadImage(templateItemId: string, upload: ImageUpload) {
        await this.controller.uploadImage(templateItemId, upload);
    }
}
