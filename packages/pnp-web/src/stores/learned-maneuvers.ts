import { Store } from "hyrest-mobx";
import { ControllerLearnedManeuvers, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreLearnedManeuvers extends Store(ControllerLearnedManeuvers) {
    @inject private live: Live;
    @inject protected controller: ControllerLearnedManeuvers;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.LEARNED_MANEUVER, handleLive(this));
    }

    public forHero(heroId: string) {
        return this.all.filter(learnedManeuver => learnedManeuver.hero.id === heroId);
    }

    @action.bound public async loadForHero(heroId: string) {
        return await this.search(heroId);
    }
}
