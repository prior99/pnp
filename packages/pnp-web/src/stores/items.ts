import { Store } from "hyrest-mobx";
import { ControllerItems, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreItems extends Store(ControllerItems) {
    @inject private live: Live;
    @inject protected controller: ControllerItems;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.ITEM, handleLive(this));
    }

    public forHero(heroId: string) {
        return this.all.filter(item => item.hero.id === heroId);
    }

    @action.bound public async loadForHero(heroId: string) {
        return await this.search(heroId);
    }

    @action.bound public evictForTemplateItem(templateItemId: string) {
        this.entities.forEach(markerItem => {
            if (markerItem.templateItem.id === templateItemId) {
                this.entities.delete(markerItem.id);
            }
        });
    }
}
