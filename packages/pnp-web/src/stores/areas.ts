import { Store } from "hyrest-mobx";
import { ControllerAreas, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreAreas extends Store(ControllerAreas) {
    @inject private live: Live;
    @inject protected controller: ControllerAreas;

    @initialize protected initialize() {
        this.live.subscribe(ModelName.AREA, handleLive(this));
    }

    @action.bound public async loadForBattleMap(mapId: string) {
        return await this.search(mapId);
    }

    public forBattleMap(id: string) {
        return this.all.filter(area => area.battleMap.id === id);
    }

    @action.bound public evictForBattleMap(battleMapId: string) {
        this.entities.forEach(area => {
            if (area.battleMap.id === battleMapId) {
                this.entities.delete(area.id);
            }
        });
    }
}
