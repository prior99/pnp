import { Store } from "hyrest-mobx";
import { ControllerMonsters, ModelName, Monster, Marker } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";
import { StoreMarkers } from "./markers";

@component
export class StoreMonsters extends Store(ControllerMonsters) {
    @inject private live: Live;
    @inject protected controller: ControllerMonsters;
    @inject private markers: StoreMarkers;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.MONSTER, handleLive(this));
    }

    public forTemplateMonster(templateMonsterId: string) {
        return this.all.filter(monster => monster.templateMonster.id === templateMonsterId);
    }

    @action.bound public async loadForTemplateMonster(templateMonsterId: string) {
        return await this.search(templateMonsterId);
    }

    @action.bound public async createWithMarker(
        templateMonsterId: string,
        battleMapId: string,
        position: [number, number] = [0, 0],
    ): Promise<{ marker: Marker, monster: Monster }> {
        const monster = await this.create({
            templateMonster: { id: templateMonsterId },
        } as Monster);
        const marker = await this.markers.create({
            battleMap: { id: battleMapId },
            monster: { id: monster.id },
            latitude: position[0],
            longitude: position[1],
        } as Marker);
        return { marker, monster };
    }

    @action.bound public evictForTemplateMonster(templateMonsterId: string) {
        this.entities.forEach(monster => {
            if (monster.templateMonster.id === templateMonsterId) {
                this.entities.delete(monster.id);
                this.markers.evictForMonster(monster.id);
            }
        });
    }

    @action.bound public evict(monsterId: string) {
        this.entities.delete(monsterId);
    }
}
