import { Store } from "hyrest-mobx";
import { ControllerPresetTokens } from "pnp-common";
import { component, inject } from "tsdi";

@component
export class StorePresetTokens extends Store(ControllerPresetTokens) {
    @inject protected controller: ControllerPresetTokens;
}
