import { Store } from "hyrest-mobx";
import { ControllerTags, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action, observable } from "mobx";
import { Live, handleLive } from "../live";
import { parentIds } from "../parent-id";
import { bind } from "lodash-decorators";

const softwareVersion = 1;
const localStorageIdentifier = "pnp-tags";

interface LocalStorageApi {
    readonly storageVersion: number;
    readonly openedTags: string[];
}

@component
export class StoreTags extends Store(ControllerTags) {
    @inject private live: Live;
    @inject protected controller: ControllerTags;

    @observable private openedTags: string[] = [];

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.TAG, handleLive(this));
        this.load();
    }

    private clearStorage() {
        localStorage.removeItem(localStorageIdentifier);
    }

    private load() {
        const serialized = localStorage.getItem(localStorageIdentifier);
        if (serialized === null) { return; }
        let deserialized: LocalStorageApi;
        try {
            deserialized = JSON.parse(serialized);
            if (deserialized.storageVersion !== softwareVersion) {
                this.clearStorage();
                return;
            }
            this.openedTags = deserialized.openedTags;
        } catch (err) {
            console.error("Error deserializing local storage", err);
            this.clearStorage();
            return;
        }
    }

    private save() {
        const deserialized: LocalStorageApi = {
            storageVersion: softwareVersion,
            openedTags: this.openedTags,
        };
        const serialized = JSON.stringify(deserialized);
        localStorage.setItem(localStorageIdentifier, serialized);
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    @action.bound public async getOrCreate(idOrName: string, groupId: string, presetId?: string) {
        const existing = this.byId(idOrName);
        if (existing) { return existing; }
        return await this.create({
            name: idOrName,
            ...parentIds({ groupId, presetId }),
        });
    }

    public dropDownOptions(groupId?: string, presetId?: string) {
        return this.all
            .filter(tag => !presetId || tag.preset && tag.preset.id === presetId)
            .filter(tag => !groupId || tag.group && tag.group.id === groupId)
            .map(tag => ({
                key: tag.id,
                value: tag.id,
                text: tag.name,
            }));
    }

    @bind public tagOpened(id: string) {
        return this.openedTags.includes(id);
    }

    @action.bound public toggleOpen(id: string) {
        if (this.tagOpened(id)) {
            this.openedTags = this.openedTags.filter(tagId => tagId !== id);
        } else {
            this.openedTags.push(id);
        }
        this.save();
    }
}
