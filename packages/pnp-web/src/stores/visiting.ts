import { component } from "tsdi";
import { observable, action } from "mobx";
import { HeroPane } from "../routing";
import { BattleMapPane } from "../components";

export interface MapProps {
    readonly center: {
        readonly lat: number;
        readonly lng: number;
    };
    readonly zoom: number;
}

@component
export class StoreVisiting {
    @observable private visibleHeroPane = new Map<string, HeroPane>();
    @observable private visibleBattleMapPane = new Map<string, BattleMapPane>();
    @observable private mapProps = new Map<string, MapProps>();

    public paneForBattleMap(id: string): BattleMapPane | undefined {
        return this.visibleBattleMapPane.get(id);
    }

    public paneForHero(id: string): HeroPane | undefined {
        return this.visibleHeroPane.get(id);
    }

    public propForMap(id: string): MapProps | undefined {
        return this.mapProps.get(id);
    }

    @action public setPaneForHero(id: string, pane: HeroPane) {
        this.visibleHeroPane.set(id, pane);
    }

    @action public setPaneForBattleMap(id: string, pane: BattleMapPane) {
        this.visibleBattleMapPane.set(id, pane);
    }

    @action public setMapProps(mapId: string, center: MapProps["center"], zoom: number) {
        this.mapProps.set(mapId, {
            center,
            zoom,
        });
    }
}
