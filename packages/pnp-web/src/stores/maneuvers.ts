import { Store } from "hyrest-mobx";
import { ControllerManeuvers, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreManeuvers extends Store(ControllerManeuvers) {
    @inject private live: Live;
    @inject protected controller: ControllerManeuvers;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.MANEUVER, handleLive(this));
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    public async forGroup(groupId: string) {
        return this.all.filter(maneuver => maneuver.group && maneuver.group.id === groupId);
    }

    public async forPreset(presetId: string) {
        return this.all.filter(maneuver => maneuver.preset && maneuver.preset.id === presetId);
    }

    public dropDownOptions(groupId?: string, presetId?: string) {
        return this
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(maneuver => !presetId || maneuver.preset && maneuver.preset.id === presetId)
            .filter(maneuver => !groupId || maneuver.group && maneuver.group.id === groupId)
            .map(maneuver => ({
                key: maneuver.id,
                value: maneuver.id,
                text: maneuver.name,
            }));
    }
}
