import { Store } from "hyrest-mobx";
import { ControllerBattleMapTags, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreBattleMapTags extends Store(ControllerBattleMapTags) {
    @inject private live: Live;
    @inject protected controller: ControllerBattleMapTags;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.BATTLE_MAP_TAG, handleLive(this));
    }

    @action.bound public async loadForBattleMap(BattleMapId: string) {
        return await this.search(BattleMapId);
    }

    public forBattleMap(battleMapId: string) {
        return this.all.filter(battleMapTag => battleMapTag.battleMap.id === battleMapId);
    }
}
