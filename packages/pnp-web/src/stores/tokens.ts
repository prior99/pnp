import { Store } from "hyrest-mobx";
import { ControllerTokens } from "pnp-common";
import { component, inject } from "tsdi";

@component
export class StoreTokens extends Store(ControllerTokens) {
    @inject protected controller: ControllerTokens;
}
