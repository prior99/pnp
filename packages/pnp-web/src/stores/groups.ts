import { Store } from "hyrest-mobx";
import { Group, ControllerGroups } from "pnp-common";
import { component, inject } from "tsdi";
import { action } from "mobx";
import { StoreLogin } from "./login";

@component
export class StoreGroups extends Store(ControllerGroups) {
    @inject protected controller: ControllerGroups;
    @inject protected login: StoreLogin;

    @action.bound public async create(group: Group): Promise<Group> {
        try {
            const newGroup = await super.create(group);
            await this.login.loginGroup({ ...group, ...newGroup }, true);
            return newGroup;
        } catch (error) {
            console.error(error);
            return;
        }
    }

    @action.bound public async applyPreset(groupId: string, presetId: string) {
        await this.controller.applyPreset(groupId, presetId);
    }
}
