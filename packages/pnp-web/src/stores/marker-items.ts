import { Store } from "hyrest-mobx";
import { ControllerMarkerItems, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreMarkerItems extends Store(ControllerMarkerItems) {
    @inject private live: Live;
    @inject protected controller: ControllerMarkerItems;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.MARKER_ITEM, handleLive(this));
    }

    @action.bound public async loadForMarker(markerId: string) {
        return await this.search(markerId);
    }

    public forMarker(markerId: string) {
        return this.all.filter(markerItem => markerItem.marker.id === markerId);
    }

    public async fromStash(stashId: string, markerId: string) {
        await this.controller.fromStash(stashId, markerId);
    }

    public async fromHero(heroId: string, markerId: string) {
        await this.controller.fromHero(heroId, markerId);
    }

    @action.bound public evictForTemplateItem(templateItemId: string) {
        this.entities.forEach(markerItem => {
            if (markerItem.templateItem.id === templateItemId) {
                this.entities.delete(markerItem.id);
            }
        });
    }

    @action.bound public evictForMarker(markerId: string) {
        this.entities.forEach(markerItem => {
            if (markerItem.marker.id === markerId) {
                this.entities.delete(markerItem.id);
            }
        });
    }
}
