import { Store } from "hyrest-mobx";
import { ControllerMonsterManeuvers, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreMonsterManeuvers extends Store(ControllerMonsterManeuvers) {
    @inject private live: Live;
    @inject protected controller: ControllerMonsterManeuvers;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.MONSTER_MANEUVER, handleLive(this));
    }

    public forMonster(templateMonsterId: string) {
        return this.all.filter(learnedManeuver => learnedManeuver.templateMonster.id === templateMonsterId);
    }

    @action.bound public async loadForTemplateMonster(monsterId: string) {
        return await this.search(monsterId);
    }

    @action.bound public evictForTemplateMonster(templateMonsterId: string) {
        this.entities.forEach(monsterManeuver => {
            if (monsterManeuver.templateMonster.id === templateMonsterId) {
                this.entities.delete(monsterManeuver.id);
            }
        });
    }
}
