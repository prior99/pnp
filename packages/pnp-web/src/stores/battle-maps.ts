import { Store } from "hyrest-mobx";
import { ControllerBattleMaps, BattleMapUpload, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action, observable } from "mobx";
import { Live, handleLive } from "../live";
import { equals, complement } from "ramda";
import { StoreMarkers } from "./markers";
import { StoreAreas } from "./areas";

@component
export class StoreBattleMaps extends Store(ControllerBattleMaps) {
    @inject private live: Live;
    @inject protected controller: ControllerBattleMaps;
    @inject private markers: StoreMarkers;
    @inject private areas: StoreAreas;

    @observable private pointerModeBattleMaps: string[] = [];
    @observable private descriptionHiddenBattleMaps: string[] = [];
    @observable private activeIndices = new Map<string, number>();

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.BATTLE_MAP, handleLive(this));
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    public async forGroup(groupId: string) {
        return this.all.filter(templateItem => templateItem.group && templateItem.group.id === groupId);
    }

    public async forPreset(presetId: string) {
        return this.all.filter(templateItem => templateItem.preset && templateItem.preset.id === presetId);
    }

    @action.bound public async upload(upload: BattleMapUpload): Promise<BattleMapUpload> {
        const battleMap = await this.controller.create(upload);
        this.entities.set(battleMap.id, battleMap);
        return battleMap;
    }

    public dropDownOptions(groupId?: string, presetId?: string) {
        return this
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(battleMap => !presetId || battleMap.preset && battleMap.preset.id === presetId)
            .filter(battleMap => !groupId || battleMap.group && battleMap.group.id === groupId)
            .map(battleMap => ({
                key: battleMap.id,
                value: battleMap.id,
                text: battleMap.name,
                icon: "map",
            }));
    }

    public togglePointerMode(battleMapId: string) {
        if (this.pointerModeBattleMaps.includes(battleMapId)) {
            this.pointerModeBattleMaps = this.pointerModeBattleMaps.filter(complement(equals(battleMapId)));
            return;
        }
        this.pointerModeBattleMaps.push(battleMapId);
    }

    public isPointerMode(battleMapId: string) {
        return this.pointerModeBattleMaps.includes(battleMapId);
    }

    public toggleDescription(battleMapId: string) {
        if (this.descriptionHiddenBattleMaps.includes(battleMapId)) {
            this.descriptionHiddenBattleMaps = this.descriptionHiddenBattleMaps.filter(complement(equals(battleMapId)));
            return;
        }
        this.descriptionHiddenBattleMaps.push(battleMapId);
    }

    public isDescriptionActive(battleMapId: string) {
        return !this.descriptionHiddenBattleMaps.includes(battleMapId);
    }

    public setActivePane(battleMapId: string, activeIndex: number) {
        return this.activeIndices.set(battleMapId, activeIndex);
    }

    public getActivePane(battleMapId: string) {
        return this.activeIndices.get(battleMapId) || 0;
    }

    public async delete(id: string) {
        this.markers.evictForBattleMap(id);
        this.areas.evictForBattleMap(id);
        await super.delete(id);
    }

    public async archive(id: string) {
        this.markers.evictForBattleMap(id);
        this.areas.evictForBattleMap(id);
        await this.controller.archive(id);
        this.entities.delete(id);
    }
}
