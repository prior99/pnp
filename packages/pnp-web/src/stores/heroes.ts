import { Store } from "hyrest-mobx";
import { ControllerHeroes, ModelName, Hero, ImageUpload } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";
import { StoreNotes } from "./notes";
import { StoreItems } from "./items";
import { StoreLearnedManeuvers } from "./learned-maneuvers";

@component
export class StoreHeroes extends Store(ControllerHeroes) {
    @inject private live: Live;
    @inject private notes: StoreNotes;
    @inject private items: StoreItems;
    @inject private learnedManeuvers: StoreLearnedManeuvers;

    @inject protected controller: ControllerHeroes;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.HERO, handleLive(this));
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    @action.bound public async clone(heroId: string): Promise<Hero> {
        const newHero = await this.controller.clone(heroId);
        this.entities.set(newHero.id, newHero);
        await this.notes.loadForHero(newHero.id);
        await this.items.loadForHero(newHero.id);
        await this.learnedManeuvers.loadForHero(newHero.id);
        return newHero;
    }

    public forGroup(groupId: string) {
        return this.all.filter(hero => hero.group && hero.group.id === groupId);
    }

    public forPreset(presetId: string) {
        return this.all.filter(hero => hero.preset && hero.preset.id === presetId);
    }

    public dropDownOptions(groupId?: string, presetId?: string) {
        return this
            // sort Hero to the top, NPCs to the bottom - otherwise alphabetically
            .sorted((a, b) => a.isNpc === b.isNpc ? a.name.localeCompare(b.name) : (a.isNpc ? 1 : -1))
            .filter(hero => !presetId || hero.preset && hero.preset.id === presetId)
            .filter(hero => !groupId || hero.group && hero.group.id === groupId)
            .map(hero => ({
                key: hero.id,
                value: hero.id,
                text: hero.name,
                icon: hero.isNpc ? "chess pawn" : "chess queen",
            }));
    }

    public async archive(heroId: string) {
        await this.controller.archive(heroId);
        this.entities.delete(heroId);
    }

    public async uploadAvatar(heroId: string, upload: ImageUpload) {
        await this.controller.uploadAvatar(heroId, upload);
    }
}
