import { component, inject, initialize } from "tsdi";
import { action, computed, observable } from "mobx";
import { pick } from "ramda";
import { Group, AuthToken, Preset } from "pnp-common";
import { StoreTokens } from "./tokens";
import { StorePresetTokens } from "./preset-tokens";

const softwareVersion = 3;
const localStorageIdentifier = "pnp-login";

interface LocalStorageApi {
    readonly storageVersion: number;
    readonly date: string;
    readonly authToken: AuthToken;
    readonly groupIds: string[];
    readonly presetIds: string[];
    readonly masterIds: string[];
}

@component({ eager: true })
export class StoreLogin {
    @inject private tokens: StoreTokens;
    @inject private presetTokens: StorePresetTokens;

    @observable private authToken: AuthToken;
    @observable public presetIds: string[] = [];
    @observable public groupIds: string[] = [];
    @observable public masterIds: string[] = [];
    @observable public presetTokenIds: string[] = [];

    @initialize protected initialize() {
        const serialized = localStorage.getItem(localStorageIdentifier);
        if (serialized === null) { return; }
        let deserialized: LocalStorageApi;
        try {
            deserialized = JSON.parse(serialized);
            if (deserialized.storageVersion !== softwareVersion) {
                this.clearStorage();
                return;
            }
            this.authToken = deserialized.authToken;
            this.groupIds = deserialized.groupIds || [];
            this.presetIds = deserialized.presetIds || [];
            this.masterIds = deserialized.masterIds || [];
        } catch (err) {
            console.error("Error deserializing local storage", err);
            this.clearStorage();
            return;
        }
    }

    @action.bound public hasPreset(presetId: string) {
        return this.presetIds.includes(presetId);
    }

    @action.bound public hasGroup(groupId: string) {
        return this.groupIds.includes(groupId);
    }

    @action.bound public async loginPreset(preset: Preset, master?: boolean) {
        try {
            const token = await this.presetTokens.create({ preset: pick(["password", "id"], preset) as Preset });
            if (!this.authToken) { this.authToken = {}; }
            if (!this.authToken.presetTokens) { this.authToken.presetTokens = []; }
            this.authToken.presetTokens.push(token.id);
            this.presetIds.push(token.preset.id);
            this.save();
            return token.preset.id;
        } catch (error) {
            return;
        }
    }

    @action.bound public async loginGroup(group: Group, master?: boolean) {
        try {
            const token = await this.tokens.create({
                group: pick(["password", "name", "masterPassword"], group) as Group,
                master,
            });
            if (!this.authToken) { this.authToken = {}; }
            if (!this.authToken.groupTokens) { this.authToken.groupTokens = []; }
            this.authToken.groupTokens.push(token.id);
            this.groupIds.push(token.group.id);
            if (token.master) {
                this.masterIds.push(token.group.id);
            }
            this.save();
            return token.group.id;
        } catch (error) {
            return;
        }
    }

    @computed public get base64AuthToken() {
        return btoa(JSON.stringify(this.authToken));
    }

    @action.bound public logout() {
        this.clearStorage();
        this.authToken = undefined;
        this.groupIds = [];
        this.presetIds = [];
        this.masterIds = [];
        window.location.href = "/";
    }

    public isMaster(groupId: string) {
        return this.masterIds.includes(groupId);
    }

    private save() {
        const deserialized: LocalStorageApi = {
            storageVersion: softwareVersion,
            date: new Date().toString(),
            authToken: this.authToken,
            presetIds: this.presetIds,
            groupIds: this.groupIds,
            masterIds: this.masterIds,
        };
        const serialized = JSON.stringify(deserialized);
        localStorage.setItem(localStorageIdentifier, serialized);
    }

    private clearStorage() {
        localStorage.removeItem(localStorageIdentifier);
    }
}
