import { Store } from "hyrest-mobx";
import { ControllerStashs, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreStashs extends Store(ControllerStashs) {
    @inject private live: Live;
    @inject protected controller: ControllerStashs;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.STASH, handleLive(this));
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    public forGroup(groupId: string) {
        return this.all.filter(stash => stash.group && stash.group.id === groupId);
    }

    public forPreset(presetId: string) {
        return this.all.filter(stash => stash.preset && stash.preset.id === presetId);
    }

    public dropDownOptions(groupId?: string, presetId?: string, nullable = false) {
        const result = this
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(stash => !presetId || stash.preset && stash.preset.id === presetId)
            .filter(stash => !groupId || stash.group && stash.group.id === groupId)
            .map(stash => ({
                key: stash.id,
                value: stash.id,
                text: stash.name,
                icon: "suitcase",
            }));
        if (nullable) {
            result.unshift({
                key: "null",
                value: null,
                text: "None",
                icon: "cancel",
            });
        }
        return result;
    }
}
