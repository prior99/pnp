import { component, inject } from "tsdi";
import { observable } from "mobx";
import { uniq } from "ramda";
import { StoreHeroes } from "./heroes";
import { StoreMonsters } from "./monsters";
import { StoreMarkers } from "./markers";
import { StoreBattleMaps } from ".";
import { Monster, Hero } from "pnp-common";

export enum ParticipantType {
    HERO = "hero",
    MONSTER = "monster",
}

export interface BaseParticipant {
    type: ParticipantType;
    participant: Hero | Monster;
}

export interface HeroParticipant extends BaseParticipant {
    type: ParticipantType.HERO;
    participant: Hero;
}

export interface MonsterParticipant extends BaseParticipant {
    type: ParticipantType.MONSTER;
    participant: Monster;
}

export type Participant = HeroParticipant | MonsterParticipant;

@component
export class StoreCurrentBattle {
    @observable public addParticipantsModalVisible = false;
    @inject private heroes: StoreHeroes;
    @inject private monsters: StoreMonsters;
    @inject private markers: StoreMarkers;
    @inject private battleMaps: StoreBattleMaps;

    public participatingHeroes(battleMapId: string) {
        const battleMap = this.battleMaps.byId(battleMapId);
        return this.heroes.forGroup(battleMap.group.id)
            .filter(hero => {
                return this.markers.forBattleMap(battleMap.id)
                    .some(marker => marker.hero && marker.hero.id === hero.id);
            })
            .filter(hero => hero.initiative !== null);
    }

    public participatingMonsters(battleMapId: string) {
        return this.markers.forBattleMap(battleMapId)
            .filter(marker => Boolean(marker.monster))
            .map(marker => this.monsters.byId(marker.monster.id))
            .filter(monster => monster.initiative !== null);
    }

    public sortedParticipants(battleMapId: string): Participant[] {
        return this.participants(battleMapId).sort((a, b) => {
            if (a.participant.currentCombatTurns > b.participant.currentCombatTurns) { return 1; }
            if (a.participant.currentCombatTurns < b.participant.currentCombatTurns) { return -1; }
            if (a.participant.initiative > b.participant.initiative) { return -1; }
            if (a.participant.initiative < b.participant.initiative) { return 1; }
            return 0;
        });
    }

    public participants(battleMapId: string): Participant[] {
        const allHeroes = this.participatingHeroes(battleMapId);
        const allMonsters = this.participatingMonsters(battleMapId);
        return [
            ...allHeroes.map((participant): Participant => ({ participant, type: ParticipantType.HERO })),
            ...allMonsters.map((participant): Participant => ({ participant, type: ParticipantType.MONSTER })),
        ];
    }

    public highestTurn(battleMapId: string) {
        return this.participants(battleMapId)
            .map(({ participant }) => participant.currentCombatTurns)
            .reduce((result, current) => result > current ? result : current, 0);
    }

    public allSameTurn(battleMapId: string) {
        return uniq(
            this.participants(battleMapId).map(({ participant }) => participant.currentCombatTurns),
        ).length === 1;
    }

    public allHeroes(battleMapId: string) {
        const battleMap = this.battleMaps.byId(battleMapId);
        return this.heroes.forGroup(battleMap.group.id)
            .filter(hero => {
                return this.markers.forBattleMap(battleMap.id)
                    .some(marker => marker.hero && marker.hero.id === hero.id);
            });
    }

    public allMonsters(battleMapId: string) {
        const battleMap = this.battleMaps.byId(battleMapId);
        return this.markers.forBattleMap(battleMap.id)
            .filter(marker => Boolean(marker.monster))
            .map(marker => marker.monster.id)
            .map(this.monsters.byId);
    }
}
