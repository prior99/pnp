import { component, initialize } from "tsdi";
import { action, computed, observable } from "mobx";
import { breakpointL } from "../breakpoints";

@component
export class StoreUi {
    @observable public visibilityToggled = false;
    @observable public alwaysOpen = this.calculateAlwaysOpen();

    private calculateAlwaysOpen() {
        return window.innerWidth >= breakpointL;
    }

    @action.bound private onWindowResize() {
        this.alwaysOpen = this.calculateAlwaysOpen();
    }

    @initialize public init() {
        window.addEventListener("resize", this.onWindowResize);
    }

    @computed public get visible() {
        return this.visibilityToggled || this.alwaysOpen;
    }

    @action.bound public toggleVisibility() {
        this.visibilityToggled = ! this.visibilityToggled;
    }
}
