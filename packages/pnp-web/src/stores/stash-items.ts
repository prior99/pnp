import { Store } from "hyrest-mobx";
import { ControllerStashItems, ModelName } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";

@component
export class StoreStashItems extends Store(ControllerStashItems) {
    @inject private live: Live;
    @inject protected controller: ControllerStashItems;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.STASH_ITEM, handleLive(this));
    }

    @action.bound public async loadForStash(stashId: string) {
        return await this.search(stashId);
    }

    public forStash(stashId: string) {
        return this.all.filter(stashItem => stashItem.stash.id === stashId);
    }

    @action.bound public evictForTemplateItem(templateItemId: string) {
        this.entities.forEach(markerItem => {
            if (markerItem.templateItem.id === templateItemId) {
                this.entities.delete(markerItem.id);
            }
        });
    }
}
