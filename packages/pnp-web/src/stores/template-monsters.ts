import { Store } from "hyrest-mobx";
import { ControllerTemplateMonsters, TemplateMonster, ModelName, ImageUpload } from "pnp-common";
import { component, inject, initialize } from "tsdi";
import { action } from "mobx";
import { Live, handleLive } from "../live";
import { StoreMonsters } from "./monsters";
import { StoreMonsterManeuvers } from "./monster-maneuvers";

@component
export class StoreTemplateMonsters extends Store(ControllerTemplateMonsters) {
    @inject private live: Live;
    @inject protected controller: ControllerTemplateMonsters;
    @inject private monsters: StoreMonsters;
    @inject private monsterManeuvers: StoreMonsterManeuvers;

    @initialize @action.bound protected async initialize() {
        this.live.subscribe(ModelName.TEMPLATE_MONSTER, handleLive(this));
    }

    @action.bound public async loadForGroup(groupId: string) {
        return await this.search(groupId);
    }

    @action.bound public async loadForPreset(presetId: string) {
        return await this.search(undefined, presetId);
    }

    public async forGroup(groupId: string) {
        return this.all.filter(templateMonster => templateMonster.group && templateMonster.group.id === groupId);
    }

    public async forPreset(presetId: string) {
        return this.all.filter(templateMonster => templateMonster.preset && templateMonster.preset.id === presetId);
    }

    public async create(templateMonster: Partial<TemplateMonster>): Promise<TemplateMonster> {
        return super.create(templateMonster);
    }

    public dropDownOptions(groupId?: string, presetId?: string) {
        return this
            .sorted((a, b) => a.name.localeCompare(b.name))
            .filter(templateMonster => !presetId || templateMonster.preset && templateMonster.preset.id === presetId)
            .filter(templateMonster => !groupId || templateMonster.group && templateMonster.group.id === groupId)
            .map(templateMonster => ({
                key: templateMonster.id,
                value: templateMonster.id,
                text: templateMonster.name,
            }));
    }

    public async delete(id: string) {
        this.monsters.evictForTemplateMonster(id);
        this.monsterManeuvers.evictForTemplateMonster(id);
        return await super.delete(id);
    }

    public async uploadImage(templateMonsterId: string, upload: ImageUpload) {
        await this.controller.uploadImage(templateMonsterId, upload);
    }
}
