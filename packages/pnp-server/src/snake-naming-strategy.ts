import { NamingStrategyInterface, Table } from "typeorm";
import { snakeCase } from "typeorm/util/StringUtils";

function adjustColumnNames(columnNames: string[]): string[] {
    return [ ...columnNames ]
        .sort()
        .map(columnName => snakeCase(columnName));
}

function adjustName(tableOrName: Table | string): string {
    const name = tableOrName instanceof Table ? tableOrName.name : tableOrName;
    return name.replace(/./, "_");
}

/**
 * Will tell Typeorm to name database related stuff in `snake_case`.
 * Adapted from: https://gist.github.com/recurrence/b6a4cb04a8ddf42eda4e4be520921bd2
 */
export class SnakeNamingStrategy implements NamingStrategyInterface {
    public tableName(className: string, customName: string): string {
        return customName ? customName : snakeCase(className);
    }

    public columnName(propertyName: string, customName: string, embeddedPrefixes: string[]): string {
        return snakeCase(embeddedPrefixes.join("_")) + (customName ? customName : snakeCase(propertyName));
    }

    public relationName(propertyName: string): string {
        return snakeCase(propertyName);
    }

    public joinColumnName(relationName: string, referencedColumnName: string): string {
        return snakeCase(relationName + "_" + referencedColumnName);
    }

    public joinTableName(
        firstTableName: string,
        secondTableName: string,
        firstPropertyName: string,
        secondPropertyName: string,
    ): string {
        return snakeCase(firstTableName + "_" + firstPropertyName.replace(/\./g, "_") + "_" + secondTableName);
    }

    public joinTableColumnName(tableName: string, propertyName: string, columnName?: string): string {
        return snakeCase(tableName + "_" + (columnName ? columnName : propertyName));
    }

    public classTableInheritanceParentColumnName(parentTableName: any, parentTableIdPropertyName: any): string {
        return snakeCase(`${parentTableName}_${parentTableIdPropertyName}`);
    }

    public primaryKeyName(name: string | Table, columnNames: string[]): string {
        return `pk_${adjustName(name)}_${adjustColumnNames(columnNames).join("_")}`;
    }

    public uniqueConstraintName(name: Table | string, columnNames: string[]): string {
        return `uq_${adjustName(name)}_${adjustColumnNames(columnNames).join("_")}`;
    }

    public relationConstraintName(name: Table | string, columnNames: string[], where?: string): string {
        let result = `rel_${adjustName(name)}_${adjustColumnNames(columnNames).join("_")}`;
        if (where) { result += `_${where}`; }
        return result;
    }

    public defaultConstraintName(name: Table | string, columnName: string): string {
        return `df_${adjustName(name)}_${snakeCase(columnName)}`;
    }

    public foreignKeyName(name: Table | string, columnNames: string[]): string {
        return `fk_${adjustName(name)}_${adjustColumnNames(columnNames).join("_")}`;
    }

    public indexName(name: Table | string, columnNames: string[], where?: string): string {
        let result = `idx_${adjustName(name)}_${adjustColumnNames(columnNames).join("_")}`;
        if (where) { result += `_${where}`; }
        return result;
    }

    public checkConstraintName(name: Table | string, expression: string): string {
        return `chk_${adjustName(name)}_${expression}`;
    }

    public joinTableColumnDuplicationPrefix(columnName: string, index: number): string {
        return `${snakeCase(columnName)}_${index}`;
    }

    public joinTableInverseColumnName(tableName: string, propertyName: string, columnName?: string): string {
        return this.joinTableColumnName(snakeCase(tableName), snakeCase(propertyName), snakeCase(columnName));
    }

    public closureJunctionTableName(originalClosureTableName: string): string {
        return snakeCase(originalClosureTableName) + "_closure";
    }

    public prefixTableName(prefix: string, tableName: string): string {
        return snakeCase(prefix) + snakeCase(tableName);
    }
}
