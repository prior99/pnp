import * as Express from "express";
import { Request, Response } from "express";
import * as expressWS from "express-ws";
import * as BodyParser from "body-parser";
import { info, error } from "winston";
import { hyrest } from "hyrest-express";
import { Connection } from "typeorm";
import { AuthorizationMode } from "hyrest";
import { component, initialize, inject, TSDI } from "tsdi";
import * as morgan from "morgan";
import {
    controllers,
    BattleMap,
    Hero,
    TemplateMonster,
    TemplateItem,
} from "pnp-common";
import { LiveServer } from "pnp-common";
import { cors } from "./middlewares";
import { Config } from "./config";
import { ServerContext } from "./server-context";
import { checkAuthorization } from "./check-authorization";
import { bind } from "lodash-decorators";
import * as sharp from "sharp";
import * as Path from "path";
import { createReadStream } from "fs-extra";
import * as Identicon from "identicon";
import { existsSync } from "fs";

const tileSize = 256;
const previewSize = 512;

@component({ eager: true })
export class Api {
    @inject private tsdi: TSDI;
    @inject("config") private config: Config;
    @inject private db: Connection;
    @inject private live: LiveServer;

    private app: Express.Application;

    @initialize
    public initialize() {
        this.app = Express();
        this.app.use(
            morgan("tiny", { stream: { write: msg => info(msg.trim()) } }),
        );
        this.app.use(BodyParser.json({ limit: "100mb", strict: false }));
        this.app.use(BodyParser.urlencoded({ limit: "100mb", extended: true }));
        this.app.use(cors);
        this.app.get("/battle-map/:id/tile/:z/:x/:y", this.battleMapTile);
        this.app.get("/battle-map/:id/preview", this.battleMapPreview);
        this.app.get("/hero/:id/avatar", this.heroAvatar);
        this.app.get("/template-monster/:id/image", this.templateMonsterImage);
        this.app.get("/template-item/:id/image", this.templateItemImage);
        expressWS(this.app).app.ws("/live", this.live.addClient);

        const controllerInstances = controllers.map(controllerClass =>
            this.tsdi.get(controllerClass),
        );
        const hyrestMiddleware = hyrest(...controllerInstances)
            .context(async request => new ServerContext(request))
            .defaultAuthorizationMode(AuthorizationMode.AUTH)
            .authorization(checkAuthorization);
        this.app.use(hyrestMiddleware);

        this.app.listen(this.config.port);
        info(`Server listening on port ${this.config.port}.`);
    }

    @bind public async battleMapPreview({ params }: Request, res: Response) {
        const battleMap = await this.db
            .getRepository(BattleMap)
            .findOne({ id: params.id });
        if (!battleMap) {
            return res.status(404).send();
        }

        res.setHeader(
            "Content-disposition",
            `filename='battle-map_${battleMap.id}-preview.png'`,
        );
        res.type("png");
        createReadStream(Path.join(this.config.battleMapsDir, battleMap.id))
            .pipe(
                sharp()
                    .resize(previewSize, previewSize)
                    .png(),
            )
            .pipe(res);
    }

    @bind public async battleMapTile({ params }: Request, res: Response) {
        const battleMap = await this.db.getRepository(BattleMap).findOne({
            id: params.id,
        });
        if (!battleMap) {
            return res.status(404).send();
        }
        const zoom = Number(params.z);
        const x = Number(params.x);
        const y = Number(params.y);
        if ([zoom, x, y].some(value => value < 0 || isNaN(value))) {
            res.status(404).send();
            return;
        }
        const tileCount = Math.pow(2, zoom);
        const zoomedSize = tileSize * tileCount;

        res.setHeader(
            "Content-disposition",
            `filename='battle-map_${battleMap.id}.png'`,
        );
        res.setHeader("Cache-Control", `max-age=${1 * 24 * 60 * 60}`);
        res.type("png");

        const path = Path.join(this.config.battleMapsDir, battleMap.id);
        const meta = await sharp(path).metadata();
        const aspect = meta.width / meta.height;
        const targetWidth = Math.floor(zoomedSize * aspect);
        const targetHeight = zoomedSize;
        const tileSizeX = Math.min(tileSize, targetWidth - tileSize * x);
        const tileSizeY = Math.min(tileSize, targetHeight - tileSize * y);
        try {
            const resizeAndCut = sharp()
                .resize(targetWidth, targetHeight, {
                    fit: "outside",
                })
                .extract({
                    left: tileSize * x,
                    top: tileSize * y,
                    width: tileSizeX < 0 ? targetWidth : tileSizeX,
                    height: tileSizeY < 0 ? targetHeight : tileSizeY,
                })
                .extend({
                    top: 0,
                    left: 0,
                    bottom: tileSize - tileSizeY,
                    right: tileSize - tileSizeX,
                    background: {
                        r: 0,
                        g: 0,
                        b: 0,
                        alpha: 0,
                    },
                })
                .png()
                .on("error", err => {
                    error(err);
                    res.status(404).send();
                });
            createReadStream(path)
                .pipe(resizeAndCut)
                .pipe(res);
        } catch (err) {
            error(err);
            res.status(404).send();
            return;
        }
    }

    @bind public async templateItemImage({ params }: Request, res: Response) {
        const templateItem = await this.db
            .getRepository(TemplateItem)
            .findOne({ id: params.id });
        if (!templateItem) {
            return res.status(404).send();
        }
        const { id } = templateItem;
        const path = Path.join(this.config.imagesDir, templateItem.id);
        res.setHeader(
            "Content-disposition",
            `filename='template-item_${id}.png'`,
        );
        res.type("png");
        if (existsSync(path)) {
            createReadStream(path)
                .pipe(
                    sharp()
                        .resize(previewSize, previewSize)
                        .png(),
                )
                .pipe(res);
            return;
        }
        res.send(
            await new Promise((resolve, reject) => {
                Identicon.generate({ id, size: 512 }, (err, buffer) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(buffer);
                });
            }),
        );
    }

    @bind public async heroAvatar({ params }: Request, res: Response) {
        const hero = await this.db
            .getRepository(Hero)
            .findOne({ id: params.id });
        if (!hero) {
            return res.status(404).send();
        }
        const { id } = hero;
        const path = Path.join(this.config.imagesDir, hero.id);
        res.setHeader("Content-disposition", `filename='hero_${id}.png'`);
        res.type("png");
        if (existsSync(path)) {
            createReadStream(path)
                .pipe(
                    sharp()
                        .resize(previewSize, previewSize)
                        .png(),
                )
                .pipe(res);
            return;
        }
        res.send(
            await new Promise((resolve, reject) => {
                Identicon.generate({ id, size: 512 }, (err, buffer) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(buffer);
                });
            }),
        );
    }

    @bind public async templateMonsterImage(
        { params }: Request,
        res: Response,
    ) {
        const templateMonster = await this.db
            .getRepository(TemplateMonster)
            .findOne({ id: params.id });
        if (!templateMonster) {
            return res.status(404).send();
        }
        const { id } = templateMonster;
        const path = Path.join(this.config.imagesDir, templateMonster.id);
        res.setHeader(
            "Content-disposition",
            `filename='template-monster_${id}.png'`,
        );
        res.type("png");
        if (existsSync(path)) {
            createReadStream(path)
                .pipe(
                    sharp()
                        .resize(previewSize, previewSize)
                        .png(),
                )
                .pipe(res);
            return;
        }
        res.send(
            await new Promise((resolve, reject) => {
                Identicon.generate({ id, size: 512 }, (err, buffer) => {
                    if (err) {
                        return reject(err);
                    }
                    sharp(buffer)
                        .tint("#d02b2b")
                        .toBuffer((sharpErr, data) => {
                            if (sharpErr) {
                                reject(err);
                            } else {
                                resolve(data);
                            }
                        });
                });
            }),
        );
    }
}
