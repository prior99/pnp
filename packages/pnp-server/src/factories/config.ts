import { component, factory } from "tsdi";
import { Config } from "../config";

@component
export class ConfigFactory {
    public config: Config;

    @factory({ name: "config" }) public getConfig(): Config { return this.config; }
}
