import { option, Options } from "clime";
import { pickBy } from "ramda";
import * as YAML from "yamljs";
import { error } from "winston";
import { ConfigInterface } from "pnp-common";

function loadConfigFile(filename: string): any {
    if (!filename) { return {}; }
    try {
        return YAML.load(filename);
    } catch (err) {
        error(`Unable to load config file "${filename}".: ${err}`);
    }
}

export class Config extends Options implements ConfigInterface {
    @option({ flag: "p", description: "Port to host the API on." })
    public port: number;

    @option({ flag: "c", description: "Path to the configuration file." })
    public configFile: string;

    @option({ description: "Name of the database to use." })
    public dbName: string;

    @option({ description: "Username to use to connect to the database" })
    public dbUsername: string;

    @option({ description: "Password to use to connect to the database." })
    public dbPassword: string;

    @option({ description: "Port the database runs on." })
    public dbPort: number;

    @option({ description: "Hostname of the server hosting the database." })
    public dbHost: string;

    @option({ description: "Whether to log all SQL queries executed." })
    public dbLogging: boolean;

    @option({ flag: "l", description: "Language used for fulltext search." })
    public language: string;

    @option({ flag: "b", description: "Directory to store battle maps in." })
    public battleMapsDir: string;

    @option({ flag: "i", description: "Directory to store images in." })
    public imagesDir: string;

    public load() {
        const defaults = {
            port: 4000,
            dbPort: 5432,
            dbLogging: false,
            dbName: "pnp",
            language: "german",
            battleMapsDir: "./battle-maps",
            imagesDir: "./images",
        };
        const config = pickBy(val => val !== undefined, loadConfigFile(this.configFile));
        const params = pickBy(val => val !== undefined, this);
        Object.assign(this, defaults, config, params);
    }
}
