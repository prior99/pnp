import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Money1561719996880 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Money");
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN money INT NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Money");
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN money`);
    }
}
