import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class ManeuversEquipmentEnhancement1560405887314 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Maneuvers Equipment Ehancements");
        await queryRunner.query(`ALTER TABLE "template_item" ADD COLUMN suggested_stamina_cost INT`);
        await queryRunner.query(`ALTER TABLE "template_item" ADD COLUMN suggested_combat_trait VARCHAR`);
        await queryRunner.query(`ALTER TABLE "maneuver" ADD COLUMN suggested_stamina_cost INT`);
        await queryRunner.query(`ALTER TABLE "maneuver" ADD COLUMN suggested_combat_trait VARCHAR`);
        await queryRunner.query(`ALTER TABLE "maneuver" ADD COLUMN classification VARCHAR NULL DEFAULT 'offensive'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Maneuvers Equipment Ehancements");
        await queryRunner.query(`ALTER TABLE "template_item" DROP COLUMN suggested_stamina_cost`);
        await queryRunner.query(`ALTER TABLE "template_item" DROP COLUMN suggested_combat_trait`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP COLUMN suggested_stamina_cost`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP COLUMN suggested_combat_trait`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP COLUMN classification'`);
    }
}
