import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Monsters1558882035540 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Monsters");

        // IMPORTANT: Keep the default values for stamina and constitution in sync with the ones in the model!
        await queryRunner.query(`
            CREATE TABLE "template_monster" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid,
                preset_id uuid,
                name VARCHAR NOT NULL,
                description TEXT NOT NULL DEFAULT '',
                focus INT NOT NULL DEFAULT 0,
                aim INT NOT NULL DEFAULT 0,
                awareness INT NOT NULL DEFAULT 0,
                swiftness INT NOT NULL DEFAULT 0,
                tension INT NOT NULL DEFAULT 0,
                power INT NOT NULL DEFAULT 0,
                stamina INT NOT NULL DEFAULT 20,
                constitution INT NOT NULL DEFAULT 40,
                CONSTRAINT pk_template_monster_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id),
                CONSTRAINT fk_preset_id FOREIGN KEY (preset_id) REFERENCES "preset"(id),
                CONSTRAINT chk_group_preset_id CHECK ((group_id IS NULL) <> (preset_id IS NULL))
            )
        `);

        await queryRunner.query(`
            CREATE TABLE "monster_maneuver" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                template_monster_id uuid NOT NULL,
                maneuver_id uuid NOT NULL,
                CONSTRAINT pk_monster_maneuver_id PRIMARY KEY (id),
                CONSTRAINT fk_template_monster_id FOREIGN KEY (template_monster_id) REFERENCES "template_monster"(id),
                CONSTRAINT fk_maneuver_id FOREIGN KEY (maneuver_id) REFERENCES "maneuver"(id)
            )
        `);

        await queryRunner.query(`
            CREATE TABLE "monster" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid NOT NULL,
                template_monster_id uuid NOT NULL,
                damage INT NOT NULL DEFAULT 0,
                spentStamina INT NOT NULL DEFAULT 0,
                CONSTRAINT pk_monster_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id),
                CONSTRAINT fk_template_monster_id FOREIGN KEY (template_monster_id) REFERENCES "template_monster"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Monsters");
        await queryRunner.query(`DROP TABLE monster`);
        await queryRunner.query(`DROP TABLE monster_maneuver`);
        await queryRunner.query(`DROP TABLE template_monster`);
    }
}
