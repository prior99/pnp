import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class MonsterPresets1559294546485  implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Monster presets");
        await queryRunner.query(`ALTER TABLE "template_monster" ADD COLUMN original_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "template_monster"
            ADD CONSTRAINT fk_original_id
            FOREIGN KEY (original_id)
            REFERENCES "template_monster"(id)
        `);
        await queryRunner.query(`ALTER TABLE "monster" ADD COLUMN original_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "monster"
            ADD CONSTRAINT fk_original_id
            FOREIGN KEY (original_id)
            REFERENCES "monster"(id)
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Monster presets");
        await queryRunner.query(`ALTER TABLE "template_monster" DROP CONSTRAINT fk_original_id`);
        await queryRunner.query(`ALTER TABLE "template_monster" DROP COLUMN original_id`);
        await queryRunner.query(`ALTER TABLE "monster" DROP CONSTRAINT fk_original_id`);
        await queryRunner.query(`ALTER TABLE "monster" DROP COLUMN original_id`);
    }
}
