import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Tags1561180840342 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Tags");
        await queryRunner.query(`
            CREATE TABLE "tag" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid,
                preset_id uuid,
                name VARCHAR NOT NULL,
                original_id uuid,
                CONSTRAINT pk_tag_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id),
                CONSTRAINT fk_preset_id FOREIGN KEY (preset_id) REFERENCES "preset"(id),
                CONSTRAINT fk_original_id FOREIGN KEY (original_id) REFERENCES "tag"(id),
                CONSTRAINT chk_group_preset_id CHECK ((group_id IS NULL) <> (preset_id IS NULL))
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "hero_tag" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                hero_id uuid NOT NULL,
                tag_id uuid NOT NULL,
                original_id uuid,
                CONSTRAINT pk_hero_tag_id PRIMARY KEY (id),
                CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES "hero"(id),
                CONSTRAINT fk_tag_id FOREIGN KEY (tag_id) REFERENCES "tag"(id),
                CONSTRAINT fk_original_id FOREIGN KEY (original_id) REFERENCES "hero_tag"(id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "battle_map_tag" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                battle_map_id uuid NOT NULL,
                tag_id uuid NOT NULL,
                original_id uuid,
                CONSTRAINT pk_battle_map_tag_id PRIMARY KEY (id),
                CONSTRAINT fk_battle_map_id FOREIGN KEY (battle_map_id) REFERENCES "battle_map"(id),
                CONSTRAINT fk_tag_id FOREIGN KEY (tag_id) REFERENCES "tag"(id),
                CONSTRAINT fk_original_id FOREIGN KEY (original_id) REFERENCES "battle_map_tag"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Tags");
        await queryRunner.query(`DROP TABLE "hero_tag"`);
        await queryRunner.query(`DROP TABLE "battle_map_tag"`);
        await queryRunner.query(`DROP TABLE "tag"`);
    }
}
