import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Maneuvers1558505248963 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Maneuvers");
        await queryRunner.query(`
            CREATE TABLE "maneuver" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid NOT NULL,
                name VARCHAR NOT NULL,
                description TEXT NOT NULL DEFAULT '',
                effect TEXT,
                CONSTRAINT pk_maneuver_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "learned_maneuver" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                hero_id uuid NOT NULL,
                maneuver_id uuid NOT NULL,
                CONSTRAINT pk_learned_maneuver_id PRIMARY KEY (id),
                CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES "hero"(id),
                CONSTRAINT fk_maneuver_id FOREIGN KEY (maneuver_id) REFERENCES "maneuver"(id)
            )
        `);
        await queryRunner.query(`ALTER TABLE "template_item" DROP COLUMN attack`);
        await queryRunner.query(`ALTER TABLE "template_item" DROP COLUMN defense`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Maneuvers");
        await queryRunner.query(`DROP TABLE "learned_maneuver"`);
        await queryRunner.query(`DROP TABLE "maneuver"`);
        await queryRunner.query(`ALTER TABLE "template_item" ADD COLUMN attack TEXT`);
        await queryRunner.query(`ALTER TABLE "template_item" ADD COLUMN defense TEXT`);
    }
}
