import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class MonsterMarkers1559996144992 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Monster markers");
        await queryRunner.query(`ALTER TABLE "monster" DROP CONSTRAINT fk_group_id`);
        await queryRunner.query(`ALTER TABLE "monster" DROP COLUMN group_id`);
        await queryRunner.query(`ALTER TABLE "monster" RENAME COLUMN spentStamina TO spent_stamina`);
        await queryRunner.query(`ALTER TABLE "marker" ADD COLUMN monster_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "marker"
            ADD CONSTRAINT fk_monster_id
            FOREIGN KEY (monster_id)
            REFERENCES "monster"(id)
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Monster markers");
        await queryRunner.query(`ALTER TABLE "monster" ADD COLUMN group_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "monster"
            ADD CONSTRAINT fk_group_id
            FOREIGN KEY (group_id)
            REFERENCES "group"(id)
        `);
        await queryRunner.query(`ALTER TABLE "monster" RENAME COLUMN spent_stamina TO spentStamina`);
        await queryRunner.query(`ALTER TABLE "marker" DROP CONSTRAINT fk_monster_id`);
        await queryRunner.query(`ALTER TABLE "marker" DROP COLUMN monster_id`);
    }
}
