import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Hero1541578791853 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Hero");
        await queryRunner.query(`
            CREATE TABLE "hero" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid NOT NULL,
                name VARCHAR NOT NULL,
                description TEXT NOT NULL DEFAULT '',
                level INT NOT NULL DEFAULT 0,
                research INT NOT NULL DEFAULT 0,
                nature INT NOT NULL DEFAULT 0,
                knowledge INT NOT NULL DEFAULT 0,
                people INT NOT NULL DEFAULT 0,
                disguise INT NOT NULL DEFAULT 0,
                stealing INT NOT NULL DEFAULT 0,
                hiding INT NOT NULL DEFAULT 0,
                sneaking INT NOT NULL DEFAULT 0,
                steady_hand INT NOT NULL DEFAULT 0,
                examining INT NOT NULL DEFAULT 0,
                captivating INT NOT NULL DEFAULT 0,
                mechanics INT NOT NULL DEFAULT 0,
                pushing INT NOT NULL DEFAULT 0,
                throwing INT NOT NULL DEFAULT 0,
                bashing INT NOT NULL DEFAULT 0,
                lifting INT NOT NULL DEFAULT 0,
                balance INT NOT NULL DEFAULT 0,
                running INT NOT NULL DEFAULT 0,
                climbing INT NOT NULL DEFAULT 0,
                swimming INT NOT NULL DEFAULT 0,
                bartering INT NOT NULL DEFAULT 0,
                art INT NOT NULL DEFAULT 0,
                seduction INT NOT NULL DEFAULT 0,
                questioning INT NOT NULL DEFAULT 0,
                improvised_medicine INT NOT NULL DEFAULT 0,
                poisons INT NOT NULL DEFAULT 0,
                crafted_medicine INT NOT NULL DEFAULT 0,
                nursing INT NOT NULL DEFAULT 0,
                vision INT NOT NULL DEFAULT 0,
                hearing INT NOT NULL DEFAULT 0,
                instinct INT NOT NULL DEFAULT 0,
                tracking INT NOT NULL DEFAULT 0,
                CONSTRAINT pk_hero_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Hero");
        await queryRunner.query(`DROP TABLE hero`);
    }
}
