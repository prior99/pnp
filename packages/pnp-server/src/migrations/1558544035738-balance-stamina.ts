import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class BalanceStamina1558544035738 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Balance Stamina");
        await queryRunner.query(`ALTER TABLE "hero" ALTER COLUMN stamina SET DEFAULT 20`);
        await queryRunner.query(`ALTER TABLE "hero" ALTER COLUMN constitution SET DEFAULT 40`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Balance Stamina");
        await queryRunner.query(`ALTER TABLE "hero" ALTER COLUMN stamina SET DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ALTER COLUMN constitution SET DEFAULT 10`);
    }
}
