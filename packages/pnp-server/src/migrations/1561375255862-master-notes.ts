import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class MasterNotes1561375255862
 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Master notes");
        await queryRunner.query(`ALTER TABLE "note" ADD COLUMN secret BOOLEAN NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "note" ADD COLUMN master_only BOOLEAN NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Master notes");
        await queryRunner.query(`ALTER TABLE "note" DROP DOLUMN secret`);
        await queryRunner.query(`ALTER TABLE "note" DROP DOLUMN master_only`);
    }
}
