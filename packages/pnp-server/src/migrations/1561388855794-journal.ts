import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Journal1561388855794 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Journal");
        await queryRunner.query(`
            CREATE TABLE "journal_entry" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid,
                preset_id uuid,
                hero_id uuid,
                original_id uuid,
                sort_key INT NOT NULL,
                description VARCHAR NOT NULL DEFAULT '',
                summary VARCHAR NOT NULL DEFAULT '',
                meta VARCHAR NOT NULL DEFAULT '',
                icon VARCHAR,
                master_only BOOLEAN NOT NULL DEFAULT false,
                secret BOOLEAN NOT NULL DEFAULT false,
                CONSTRAINT pk_journal_entry_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id),
                CONSTRAINT fk_preset_id FOREIGN KEY (preset_id) REFERENCES "preset"(id),
                CONSTRAINT fk_original_id FOREIGN KEY (original_id) REFERENCES "journal_entry"(id),
                CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES "hero"(id),
                CONSTRAINT chk_group_preset_id CHECK ((group_id IS NULL) <> (preset_id IS NULL))
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Journal");
        await queryRunner.query(`DROP TABLE "journal_entry"`);
    }
}
