import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Skills1561995384296 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Skills");
        await queryRunner.query(`ALTER TABLE "hero" RENAME COLUMN seduction TO deception`);
        await queryRunner.query(`ALTER TABLE "hero" RENAME COLUMN disguise TO burglary`);
        await queryRunner.query(`ALTER TABLE "hero" RENAME COLUMN people TO navigation`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Skills");
        await queryRunner.query(`ALTER TABLE "hero" RENAME COLUMN deception TO seduction`);
        await queryRunner.query(`ALTER TABLE "hero" RENAME COLUMN burglary TO disguise`);
        await queryRunner.query(`ALTER TABLE "hero" RENAME COLUMN navigation TO people`);
    }
}
