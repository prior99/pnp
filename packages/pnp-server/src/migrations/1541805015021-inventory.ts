import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Inventory1541805015021 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Inventory");
        await queryRunner.query(`
            CREATE TABLE "template_item" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid NOT NULL,
                name VARCHAR NOT NULL,
                description TEXT NOT NULL DEFAULT '',
                quantifiable BOOL NOT NULL DEFAULT false,
                unit VARCHAR NOT NULL DEFAULT '',
                can_be_active BOOL NOT NULL DEFAULT false,
                effect TEXT,
                attack TEXT,
                defense TEXT,
                CONSTRAINT pk_item_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "item" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                hero_id uuid NOT NULL,
                template_item_id uuid NOT NULL,
                notes TEXT NOT NULL DEFAULT '',
                quantity INT NOT NULL DEFAULT 1,
                CONSTRAINT pk_inventory_id PRIMARY KEY (id),
                CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES "hero"(id),
                CONSTRAINT fk_template_item_id FOREIGN KEY (template_item_id) REFERENCES "template_item"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Inventory");
        await queryRunner.query(`DROP TABLE "item"`);
        await queryRunner.query(`DROP TABLE "template_item"`);
    }
}
