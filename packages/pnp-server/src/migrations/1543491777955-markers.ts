import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Markers1543491777955 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Markers");
        await queryRunner.query(`
            CREATE TABLE "marker" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                battle_map_id uuid NOT NULL,
                hero_id uuid,
                description TEXT,
                latitude DOUBLE PRECISION NOT NULL,
                longitude DOUBLE PRECISION NOT NULL,
                CONSTRAINT pk_marker_id PRIMARY KEY (id),
                CONSTRAINT fk_battle_map_id FOREIGN KEY (battle_map_id) REFERENCES "battle_map"(id),
                CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES "hero"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Markers");
        await queryRunner.query(`DROP TABLE "marker"`);
    }
}
