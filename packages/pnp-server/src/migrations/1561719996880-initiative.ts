import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Initiative1561719996880 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Initiative");
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN initiative INT`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN current_combat_turns INT`);
        await queryRunner.query(`ALTER TABLE "monster" ADD COLUMN initiative INT`);
        await queryRunner.query(`ALTER TABLE "monster" ADD COLUMN current_combat_turns INT`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Initiative");
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN initative`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN current_combat_turns`);
        await queryRunner.query(`ALTER TABLE "monster" DROP COLUMN initative`);
        await queryRunner.query(`ALTER TABLE "monster" DROP COLUMN current_combat_turns`);
    }
}
