import { Initial1541417447677 } from "./1541417447677-initial";
import { Hero1541578791853 } from "./1541578791853-hero";
import { Battle1541765800114 } from "./1541765800114-battle";
import { Inventory1541805015021 } from "./1541805015021-inventory";
import { Notes1542977394049 } from "./1542977394049-notes";
import { BattleMaps1543306313894 } from "./1543306313894-battle-maps";
import { Markers1543491777955 } from "./1543491777955-markers";
import { Areas1543568456369 } from "./1543568456369-areas";
import { Maneuvers1558505248963 } from "./1558505248963-maneuvers";
import { BalanceStamina1558544035738 } from "./1558544035738-balance-stamina";
import { SpentStamina1558860961695 } from "./1558860961695-spent-stamina";
import { Presets1558865917482 } from "./1558865917482-presets";
import { PresetTokens1558868064244 } from "./1558868064244-preset-tokens";
import { NPC1558863150893 } from "./1558863150893-npc";
import { ApplyPresets1558886916542 } from "./1558886916542-apply-presets";
import { Monsters1558882035540 } from "./1558882035540-monsters";
import { MonsterPresets1559294546485 } from "./1559294546485-monster-presets";
import { MonsterMarkers1559996144992 } from "./1559996144992-monster-markers";
import { MarkerDescriptions1560080924750 } from "./1560080924750-marker-descriptions";
import { BattleMapNotes1559293597571 } from "./1559293597571-battle-map-notes";
import { ManeuversEquipmentEnhancement1560405887314 } from "./1560405887314-maneuvers-equipment-enhancements";
import { HideBattleMaps1561056848683 } from "./1561056848683-hide-battle-maps";
import { RemoveEmailFromGroups1561058563888 } from "./1561058563888-remove-email-group";
import { Tags1561180840342 } from "./1561180840342-tags";
import { TagPinningHiding1561369514572 } from "./1561369514572-tag-pinning-hiding";
import { MasterNotes1561375255862 } from "./1561375255862-master-notes";
import { Journal1561388855794 } from "./1561388855794-journal";
import { MasterHeroes1561558775107 } from "./1561558775107-master-heroes";
import { Initiative1561719996880 } from "./1561719996880-initiative";
import { Money1561719996880 } from "./1561992585228-money";
import { Skills1561995384296 } from "./1561995384296-skills";
import { Loot1562059035775 } from "./1562059035775-loot";
import { FixPkItemId1562059787649 } from "./1562059787649-fix-pk-item-id";

export const migrations = [
    Initial1541417447677,
    Hero1541578791853,
    Battle1541765800114,
    Inventory1541805015021,
    Notes1542977394049,
    BattleMaps1543306313894,
    Markers1543491777955,
    Areas1543568456369,
    Maneuvers1558505248963,
    BalanceStamina1558544035738,
    SpentStamina1558860961695,
    Presets1558865917482,
    PresetTokens1558868064244,
    NPC1558863150893,
    ApplyPresets1558886916542,
    Monsters1558882035540,
    MonsterPresets1559294546485,
    MonsterMarkers1559996144992,
    MarkerDescriptions1560080924750,
    BattleMapNotes1559293597571,
    ManeuversEquipmentEnhancement1560405887314,
    HideBattleMaps1561056848683,
    RemoveEmailFromGroups1561058563888,
    Tags1561180840342,
    TagPinningHiding1561369514572,
    MasterNotes1561375255862,
    Journal1561388855794,
    MasterHeroes1561558775107,
    Initiative1561719996880,
    Money1561719996880,
    Skills1561995384296,
    Loot1562059035775,
    FixPkItemId1562059787649,
];
