import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class NPC1558863150893 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: NPCs");
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN is_npc BOOLEAN NOT NULL DEFAULT FALSE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: NPCs");
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN is_npc`);
    }
}
