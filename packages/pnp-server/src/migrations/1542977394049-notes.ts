import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Notes1542977394049 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Notes");
        await queryRunner.query(`
            CREATE TABLE "note" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                hero_id uuid NOT NULL,
                note TEXT NOT NULL DEFAULT '',
                CONSTRAINT pk_note_id PRIMARY KEY (id),
                CONSTRAINT fk_hero_id FOREIGN KEY (hero_id) REFERENCES "hero"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Notes");
        await queryRunner.query(`DROP TABLE "note"`);
    }
}
