import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Battle1541765800114 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Battle");
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN focus INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN aim INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN awareness INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN swiftness INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN tension INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN power INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN stamina INT NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN constitution INT NOT NULL DEFAULT 10`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN damage INT NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Battle");
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN focus`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN aim`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN awareness`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN swiftness`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN tension`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN power`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN stamina`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN constitution`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN damage`);
    }
}
