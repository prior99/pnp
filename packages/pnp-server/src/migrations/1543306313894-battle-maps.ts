import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class BattleMaps1543306313894 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Battle Maps");
        await queryRunner.query(`
            CREATE TABLE "battle_map" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid NOT NULL,
                name VARCHAR NOT NULL,
                CONSTRAINT pk_battle_map_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Battle Maps");
        await queryRunner.query(`DROP TABLE "battle_map"`);
    }
}
