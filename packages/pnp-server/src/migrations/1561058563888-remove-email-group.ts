import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class RemoveEmailFromGroups1561058563888 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Remove email from groups");
        await queryRunner.query(`ALTER TABLE "group" DROP COLUMN email`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Remove email from groups");
        await queryRunner.query(`
            ALTER TABLE "group"
            ADD COLUMN email CHARACTER VARYING(200) NOT NULL DEFAULT('unknown@example.com'
        )`);
    }
}
