import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class BattleMapNotes1559293597571 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Battle map notes");
        await queryRunner.query(`ALTER TABLE "battle_map" ADD COLUMN description TEXT`);
        await queryRunner.query(`ALTER TABLE "battle_map" ADD COLUMN master_notes TEXT`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Battle map notes");
        await queryRunner.query(`ALTER TABLE "battle_map" DROP COLUMN description`);
        await queryRunner.query(`ALTER TABLE "battle_map" DROP COLUMN master_notes`);
    }
}
