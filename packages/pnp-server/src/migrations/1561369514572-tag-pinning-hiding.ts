import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class TagPinningHiding1561369514572
 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Tag pinning and hiding");
        await queryRunner.query(`ALTER TABLE "tag" ADD COLUMN pinned BOOLEAN NOT NULL DEFAULT true`);
        await queryRunner.query(`ALTER TABLE "tag" ADD COLUMN master_only BOOLEAN NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Tag pinning and hiding");
        await queryRunner.query(`ALTER TABLE "tag" DROP DOLUMN pinned`);
        await queryRunner.query(`ALTER TABLE "tag" DROP DOLUMN master_only`);
    }
}
