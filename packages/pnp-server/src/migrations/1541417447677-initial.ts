import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Initial1541417447677 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Initial");
        await queryRunner.query(`
            CREATE TABLE "group" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                name character varying(100) NOT NULL,
                password character varying(200) NOT NULL,
                master_password character varying(200) NOT NULL,
                email character varying(200) NOT NULL,
                CONSTRAINT pk_group_id PRIMARY KEY (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "token" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid NOT NULL,
                master bool NOT NULL DEFAULT false,
                CONSTRAINT pk_token_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Initial");
        await queryRunner.query(`DROP TABLE token`);
        await queryRunner.query(`DROP TABLE group`);
    }
}
