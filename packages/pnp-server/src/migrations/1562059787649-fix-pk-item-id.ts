import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class FixPkItemId1562059787649 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Fix pk_item_id");
        await queryRunner.query(`ALTER TABLE "template_item" RENAME CONSTRAINT pk_item_id TO pk_template_item_id`);
        await queryRunner.query(`ALTER TABLE "item" RENAME CONSTRAINT pk_inventory_id TO pk_item_id`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Fix pk_item_id");
        await queryRunner.query(`ALTER TABLE "template_item" RENAME CONSTRAINT pk_template_item_id TO pk_item_id`);
        await queryRunner.query(`ALTER TABLE "item" RENAME CONSTRAINT pk_item_id TO pk_inventory_id`);
    }
}
