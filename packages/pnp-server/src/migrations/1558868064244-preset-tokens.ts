import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class PresetTokens1558868064244 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: PresetTokens");
        await queryRunner.query(`
            CREATE TABLE "preset_token" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                preset_id uuid NOT NULL,
                CONSTRAINT pk_preset_token_id PRIMARY KEY (id),
                CONSTRAINT fk_preset_id FOREIGN KEY (preset_id) REFERENCES "preset"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: PresetTokens");
        await queryRunner.query(`DROP TABLE preset_token`);
    }
}
