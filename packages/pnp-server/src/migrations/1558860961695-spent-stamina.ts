import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class SpentStamina1558860961695 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Spent Stamina");
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN spent_stamina INT NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Spent Stamina");
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN spent_stamina`);
    }
}
