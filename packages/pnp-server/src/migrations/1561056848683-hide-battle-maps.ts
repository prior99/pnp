import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class HideBattleMaps1561056848683  implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Maneuvers Hide battle maps");
        await queryRunner.query(`ALTER TABLE "battle_map" ADD COLUMN master_only BOOLEAN NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Maneuvers Hide battle maps");
        await queryRunner.query(`ALTER TABLE "battle_map" DROP COLUMN master_only`);
    }
}
