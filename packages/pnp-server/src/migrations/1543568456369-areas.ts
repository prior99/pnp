import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Areas1543568456369 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Areas");
        await queryRunner.query(`
            CREATE TABLE "area" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                battle_map_id uuid NOT NULL,
                "top" DOUBLE PRECISION NOT NULL,
                "left" DOUBLE PRECISION NOT NULL,
                "bottom" DOUBLE PRECISION NOT NULL,
                "right" DOUBLE PRECISION NOT NULL,
                CONSTRAINT pk_area_id PRIMARY KEY (id),
                CONSTRAINT fk_battle_map_id FOREIGN KEY (battle_map_id) REFERENCES "battle_map"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Areas");
        await queryRunner.query(`DROP TABLE "area"`);
    }
}
