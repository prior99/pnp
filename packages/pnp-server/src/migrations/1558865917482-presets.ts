import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Presets1558865917482 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Presets");
        await queryRunner.query(`
            CREATE TABLE "preset" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                name character varying(100) NOT NULL,
                description text NOT NULL,
                password character varying(200) NOT NULL,
                CONSTRAINT pk_preset_id PRIMARY KEY (id)
            )
        `);
        await queryRunner.query(`ALTER TABLE "template_item" ALTER COLUMN group_id DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "template_item" ADD COLUMN preset_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "template_item"
            ADD CONSTRAINT fk_preset_id
            FOREIGN KEY (preset_id)
            REFERENCES "preset"(id)
        `);
        await queryRunner.query(`ALTER TABLE "maneuver" ALTER COLUMN group_id DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "maneuver" ADD COLUMN preset_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "maneuver"
            ADD CONSTRAINT fk_preset_id
            FOREIGN KEY (preset_id)
            REFERENCES "preset"(id)
        `);
        await queryRunner.query(`ALTER TABLE "battle_map" ALTER COLUMN group_id DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "battle_map" ADD COLUMN preset_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "battle_map"
            ADD CONSTRAINT fk_preset_id
            FOREIGN KEY (preset_id)
            REFERENCES "preset"(id)
        `);
        await queryRunner.query(`ALTER TABLE "hero" ALTER COLUMN group_id DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN preset_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "hero"
            ADD CONSTRAINT fk_preset_id
            FOREIGN KEY (preset_id)
            REFERENCES "preset"(id)
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Presets");
        await queryRunner.query(`ALTER TABLE "template_item" ALTER COLUMN group_id SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "template_item" DROP CONSTRAINT fk_preset_id`);
        await queryRunner.query(`ALTER TABLE "template_item" DROP COLUMN preset_id`);
        await queryRunner.query(`ALTER TABLE "maneuver" ALTER COLUMN group_id SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP CONSTRAINT fk_preset_id`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP COLUMN preset_id`);
        await queryRunner.query(`ALTER TABLE "battle_map" ALTER COLUMN group_id SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "battle_map" DROP CONSTRAINT fk_preset_id`);
        await queryRunner.query(`ALTER TABLE "battle_map" DROP COLUMN preset_id`);
        await queryRunner.query(`ALTER TABLE "hero" ALTER COLUMN group_id SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "hero" DROP CONSTRAINT fk_preset_id`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN preset_id`);
        await queryRunner.query(`DROP TABLE preset`);
    }
}
