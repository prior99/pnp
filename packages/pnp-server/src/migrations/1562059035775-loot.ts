import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Loot1562059035775 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Loot");
        await queryRunner.query(`
            CREATE TABLE "stash" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                group_id uuid,
                preset_id uuid,
                original_id uuid,
                money INT NOT NULL,
                name CHARACTER VARYING NOT NULL,
                CONSTRAINT pk_template_stash_id PRIMARY KEY (id),
                CONSTRAINT fk_group_id FOREIGN KEY (group_id) REFERENCES "group"(id),
                CONSTRAINT fk_preset_id FOREIGN KEY (preset_id) REFERENCES "preset"(id),
                CONSTRAINT fk_original_id FOREIGN KEY (original_id) REFERENCES "stash"(id),
                CONSTRAINT chk_group_preset_id CHECK ((group_id IS NULL) <> (preset_id IS NULL))
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "stash_item" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                stash_id uuid NOT NULL,
                template_item_id uuid NOT NULL,
                notes TEXT NOT NULL DEFAULT '',
                quantity INT NOT NULL DEFAULT 1,
                CONSTRAINT pk_stash_item_id PRIMARY KEY (id),
                CONSTRAINT fk_stash_id FOREIGN KEY (stash_id) REFERENCES "stash"(id),
                CONSTRAINT fk_template_item_id FOREIGN KEY (template_item_id) REFERENCES "template_item"(id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "marker_item" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                marker_id uuid NOT NULL,
                template_item_id uuid NOT NULL,
                notes TEXT NOT NULL DEFAULT '',
                quantity INT NOT NULL DEFAULT 1,
                CONSTRAINT pk_marker_item_id PRIMARY KEY (id),
                CONSTRAINT fk_marker_id FOREIGN KEY (marker_id) REFERENCES "marker"(id),
                CONSTRAINT fk_template_item_id FOREIGN KEY (template_item_id) REFERENCES "template_item"(id)
            )
        `);
        await queryRunner.query(`ALTER TABLE "marker" ADD COLUMN money INT`);
        await queryRunner.query(`ALTER TABLE "template_monster" ADD COLUMN stash_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "template_monster"
            ADD CONSTRAINT fk_stash_id
            FOREIGN KEY (stash_id)
            REFERENCES "stash"(id)
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Loot");
        await queryRunner.query(`ALTER TABLE "marker" DROP COLUMN money`);
        await queryRunner.query(`ALTER TABLE "template_monster" DROP CONSTRAINT fk_stash_id`);
        await queryRunner.query(`ALTER TABLE "template_monster" DROP COLUMN stash_id`);
        await queryRunner.query(`DROP TABLE "stash_item`);
        await queryRunner.query(`DROP TABLE "marker_item`);
        await queryRunner.query(`DROP TABLE "stash`);
    }
}
