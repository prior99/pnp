import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class ApplyPresets1558886916542 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Apply presets");
        await queryRunner.query(`ALTER TABLE "template_item" ADD COLUMN original_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "template_item"
            ADD CONSTRAINT fk_original_id
            FOREIGN KEY (original_id)
            REFERENCES "template_item"(id)
        `);
        await queryRunner.query(`ALTER TABLE "maneuver" ADD COLUMN original_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "maneuver"
            ADD CONSTRAINT fk_original_id
            FOREIGN KEY (original_id)
            REFERENCES "maneuver"(id)
        `);
        await queryRunner.query(`ALTER TABLE "battle_map" ADD COLUMN original_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "battle_map"
            ADD CONSTRAINT fk_original_id
            FOREIGN KEY (original_id)
            REFERENCES "battle_map"(id)
        `);
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN original_id uuid`);
        await queryRunner.query(`
            ALTER TABLE "hero"
            ADD CONSTRAINT fk_original_id
            FOREIGN KEY (original_id)
            REFERENCES "hero"(id)
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Apply presets");
        await queryRunner.query(`ALTER TABLE "template_item" DROP CONSTRAINT fk_original_id`);
        await queryRunner.query(`ALTER TABLE "template_item" DROP COLUMN original_id`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP CONSTRAINT fk_original_id`);
        await queryRunner.query(`ALTER TABLE "maneuver" DROP COLUMN original_id`);
        await queryRunner.query(`ALTER TABLE "battle_map" DROP CONSTRAINT fk_original_id`);
        await queryRunner.query(`ALTER TABLE "battle_map" DROP COLUMN original_id`);
        await queryRunner.query(`ALTER TABLE "hero" DROP CONSTRAINT fk_original_id`);
        await queryRunner.query(`ALTER TABLE "hero" DROP COLUMN original_id`);
    }
}
