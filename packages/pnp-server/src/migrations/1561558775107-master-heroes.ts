import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class MasterHeroes1561558775107 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Master heroes");
        await queryRunner.query(`ALTER TABLE "hero" ADD COLUMN master_only BOOLEAN NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Master heroes");
        await queryRunner.query(`ALTER TABLE "hero" DROP DOLUMN master_only`);
    }
}
