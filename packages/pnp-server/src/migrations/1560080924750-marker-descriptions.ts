import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class MarkerDescriptions1560080924750 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Marker descriptions");
        await queryRunner.query(`
            ALTER TABLE "marker"
                ADD COLUMN master_description TEXT NOT NULL DEFAULT '',
                ADD COLUMN name TEXT NOT NULL DEFAULT '',
                ADD COLUMN icon TEXT,
                Add COLUMN master_only BOOLEAN NOT NULL DEFAULT true
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Marker descriptions");
        await queryRunner.query(`
            ALTER TABLE "marker"
                DROP COLUMN master_description,
                DROP COLUMN name,
                DROP COLUMN icon,
                DROP COLUMN master_only
        `);
    }
}
