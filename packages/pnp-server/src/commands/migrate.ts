import { metadata, command, option, Command } from "clime";
import { info } from "winston";
import { Config } from "../config";
import { connect } from "../database";

class MigrateConfig extends Config {
    @option({ description: "number of migrations to revert"})
    public downgrade?: number;
}

@command({ description: "Perform necessary database migrations" })
export default class extends Command { // tslint:disable-line
    @metadata
    public async execute(config: MigrateConfig) {
        config.load();
        config.dbLogging = true;
        const db = await connect(config);

        if (config.downgrade) {
            info(`Downgrading ${config.downgrade} migrations`);
            for (let i = 0; i < config.downgrade; i++) {
                await db.undoLastMigration();
            }
        } else {
            await db.runMigrations();
        }
        await db.close();
    }
}
