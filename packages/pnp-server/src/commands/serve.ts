import { metadata, command, Command } from "clime";
import { TSDI } from "tsdi";
import { error, warn } from "winston";
import { Config } from "../config";
import { ConfigFactory } from "../factories";
import "../api";

@command({ description: "Start the server." })
export default class extends Command { // tslint:disable-line
    @metadata
    public async execute(config: Config) {
        config.load();
        // Load the configuration.
        const tsdi = new TSDI();
        // The config factory needs to be initialized first.
        tsdi.register(ConfigFactory);
        tsdi.get(ConfigFactory).config = config;
        // Now all other components may start up.
        tsdi.enableComponentScanner();

        let killed = false;
        const kill = async () => {
            if (killed) {
                error("CTRL^C detected. Terminating!");
                process.exit(1);
                return;
            }
            killed = true;
            warn("CTRL^C detected. Secure shutdown initiated.");
            warn("Press CTRL^C again to terminate at your own risk.");
            tsdi.close();
        };
        process.on("SIGINT", kill);
    }
}
