import { inject, external } from "tsdi";
import { Request } from "express";
import { Connection } from "typeorm";
import { ControllerValidation, Token, Group, Context, Preset, AuthToken } from "pnp-common";
import { getAuthToken } from "./auth-token";

@external
export class ServerContext implements Context {
    @inject public validation: ControllerValidation;
    @inject private db: Connection;

    private authToken: AuthToken;

    constructor(req: Request) {
        this.authToken = getAuthToken(req);
    }

    public async currentGroups() {
        const { authToken } = this;
        const { groupTokens } = authToken;
        if (!groupTokens || groupTokens.length === 0) { return; }
        return await this.db.getRepository(Group).createQueryBuilder("group")
            .innerJoin("group.tokens", "token")
            .where("token.id = ANY(:groupTokens)", { groupTokens })
            .getMany();
    }

    public async currentPresets() {
        const { authToken } = this;
        const { presetTokens } = authToken;
        if (!presetTokens || presetTokens.length === 0) { return; }
        return await this.db.getRepository(Preset).createQueryBuilder("preset")
            .innerJoin("preset.presetTokens", "presetToken")
            .where("presetToken.id = ANY(:presetTokens)", { presetTokens })
            .getMany();
    }

    public async isMaster(id: string): Promise<boolean> {
        const group = await this.db.getRepository(Group).findOne({ id });
        const { authToken } = this;
        return (await Promise.all(
            authToken.groupTokens.map(tokenId => this.db.getRepository(Token).findOne({
                id: tokenId,
                group: { id: group.id },
            })),
        )).some(token => token && token.master);
    }

    public async hasGroup(id: string): Promise<boolean> {
        return Boolean((await this.currentGroups()).find(group => group.id === id));
    }

    public async hasPreset(id: string): Promise<boolean> {
        const { authToken } = this;
        const { presetTokens } = authToken;
        if (!presetTokens) { return; }
        const preset = await this.db.getRepository(Preset).createQueryBuilder("preset")
            .innerJoin("preset.presetTokens", "presetToken")
            .where("presetToken.id = ANY(:presetTokens)", { presetTokens })
            .getOne();
        return Boolean(preset);
    }
}
