import { Request } from "express";
import { Context } from "pnp-common";
import { getAuthToken } from "./auth-token";

export async function checkAuthorization(request: Request, context: Context) {
    const authToken = getAuthToken(request);
    return typeof (await context.validation.tokenValid(authToken)).error === "undefined";
}
