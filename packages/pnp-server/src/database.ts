import { createConnection } from "typeorm";
import { info } from "winston";
import {
    Token,
    Group,
    Hero,
    Item,
    TemplateItem,
    Note,
    BattleMap,
    Marker,
    Area,
    Maneuver,
    LearnedManeuver,
    Preset,
    PresetToken,
    TemplateMonster,
    MonsterManeuver,
    Monster,
    BattleMapTag,
    Tag,
    HeroTag,
    JournalEntry,
    Stash,
    MarkerItem,
    StashItem,
} from "pnp-common";
import { Config } from "./config";
import { SnakeNamingStrategy } from "./snake-naming-strategy";
import { migrations } from "./migrations";

const entities = [
    Token,
    Group,
    Hero,
    Item,
    TemplateItem,
    Note,
    BattleMap,
    Marker,
    Area,
    Maneuver,
    LearnedManeuver,
    Preset,
    PresetToken,
    TemplateMonster,
    MonsterManeuver,
    Monster,
    Tag,
    BattleMapTag,
    HeroTag,
    JournalEntry,
    Stash,
    MarkerItem,
    StashItem,
];

export async function connect(config: Config) {
    const {
        dbName: database,
        dbLogging: logging,
        dbPassword: password,
        dbPort: port,
        dbUsername: username,
        dbHost: host,
    } = config;
    const type = "postgres";
    const namingStrategy = new SnakeNamingStrategy();
    const connection = await createConnection({
        entities, database, type, logging, password, port, username, host, migrations, namingStrategy,
    });
    info("Connected to database.");
    return connection;
}
