import { Request } from "express";
import { AuthToken, isAuthToken } from "pnp-common";

export function getAuthToken(req: Request): AuthToken {
    const header = req.get("authorization");
    if (!header) { return; }
    if (header.substr(0, 7) !== "Bearer ") { return; }
    try {
        const json = Buffer.from(header.substring(7, header.length), "base64").toString("utf8");
        const authToken = JSON.parse(json);
        if (!isAuthToken(authToken)) { return {}; }
        return authToken;
    } catch (err) {
        return {};
    }
}
