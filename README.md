# PnP

## Prerequisites

* yarn
* node
* postgres running
* postgres extension `uuid-ossp` must be available
* make

## Setup

After cloning run the following for the initial setup:

```sh
$ make db
$ psql -U postgres -d pnp  # use any DB superuser you have access to
	pnp => CREATE EXTENSION 'uuid-ossp';
	pnp => \q
$ make migrate
```

New changes might contain changes to the database, so run `make migrate` after pulling!

## Starting

```sh
$ make run
```
