default: build lint

.PHONY: node_modules
node_modules:
	yarn install
	yarn lerna bootstrap

.PHONY: build
build: node_modules
	yarn lerna run build

.PHONY: lint
lint: node_modules
	yarn lint

.PHONY: clean
clean:
	yarn lerna run clean

.PHONY: run
run: node_modules db
	# FORCE_COLOR enables colors for chalk again (for nicely colored error messages).
	# They are disabled for some reason by lerna
	FORCE_COLOR=1 yarn lerna run --parallel start

.PHONY: db
db:
	createdb pnp || true

.PHONY: clean-db
clean-db:
	dropdb pnp || true

.PHONY: migrate
migrate: build
	node packages/pnp-server/bin migrate
