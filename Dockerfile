FROM node:10-stretch

WORKDIR /pnp

# Install all dependencies used for puppeteer and for running the bot.
RUN apt-get -qq update && \
    apt-get -qq install -y build-essential

# Copy all files which will likely change very infrequently.
COPY ./tsconfig.json /pnp/tsconfig.json
COPY ./lerna.json /pnp/lerna.json
COPY ./tslint.json /pnp/tslint.json
COPY ./Makefile /pnp/Makefile

# Copy files changing frequently.
COPY ./packages /pnp/packages
COPY ./deploy /pnp/deply

# Install all dependencies.
COPY ./package.json /pnp/package.json
COPY ./yarn.lock /pnp/yarn.lock
RUN make node_modules
